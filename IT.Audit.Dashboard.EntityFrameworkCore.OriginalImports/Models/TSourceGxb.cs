﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace IT.Audit.Dashboard.EntityFrameworkCore.OriginalImports.Models
{
    [Table("t_Source_GXB")]
    public partial class TSourceGxb
    {
        [Key]
        [Column("ID")]
        public int Id { get; set; }
        [Column("User Name")]
        [StringLength(255)]
        public string UserName { get; set; }
        [Column("Full User name")]
        [StringLength(255)]
        public string FullUserName { get; set; }
        [Column("E-mail")]
        [StringLength(255)]
        public string EMail { get; set; }
        [Column("User Description")]
        [StringLength(255)]
        public string UserDescription { get; set; }
        [Column("Contact phone number")]
        [StringLength(255)]
        public string ContactPhoneNumber { get; set; }
        [Column("System User Type")]
        [StringLength(255)]
        public string SystemUserType { get; set; }
        [Column("Account is disabled")]
        [StringLength(255)]
        public string AccountIsDisabled { get; set; }
        [Column("Last Login Date/Time", TypeName = "datetime")]
        public DateTime? LastLoginDateTime { get; set; }
        [Column("Permission Group")]
        [StringLength(255)]
        public string PermissionGroup { get; set; }
        [Column("Business Object & Report Group")]
        [StringLength(255)]
        public string BusinessObjectReportGroup { get; set; }
        [Column("Permission Type")]
        [StringLength(255)]
        public string PermissionType { get; set; }
        [Column("Permission Name")]
        [StringLength(5000)]
        public string PermissionName { get; set; }
        [Column("Folder/Template Business Object")]
        [StringLength(255)]
        public string FolderTemplateBusinessObject { get; set; }
        [Column("Policy Type")]
        [StringLength(255)]
        public string PolicyType { get; set; }
        [Column("Folder/Template Type")]
        [StringLength(255)]
        public string FolderTemplateType { get; set; }
        [Column("Permission Value")]
        [StringLength(255)]
        public string PermissionValue { get; set; }
        [StringLength(255)]
        public string Division { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? DataDate { get; set; }
        [Column("C_Application_Login")]
        [StringLength(255)]
        public string CApplicationLogin { get; set; }
        [Column("C_ActiveYN")]
        [StringLength(255)]
        public string CActiveYn { get; set; }
        [Column("C_AdministratorYN")]
        [StringLength(255)]
        public string CAdministratorYn { get; set; }
        [Column("C_NonLicensedYN")]
        [StringLength(255)]
        public string CNonLicensedYn { get; set; }
        [Column("C_Toxic_CombinationYN")]
        [StringLength(255)]
        public string CToxicCombinationYn { get; set; }
    }
}