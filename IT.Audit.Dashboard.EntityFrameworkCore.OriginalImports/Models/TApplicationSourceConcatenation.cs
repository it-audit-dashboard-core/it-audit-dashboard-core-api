﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace IT.Audit.Dashboard.EntityFrameworkCore.OriginalImports.Models
{
    [Table("t_Application_Source_Concatenations")]
    public partial class TApplicationSourceConcatenation
    {
        [Key]
        [Column("ID")]
        public int Id { get; set; }
        [Column("ApplicationID")]
        public int ApplicationId { get; set; }
        [Required]
        [StringLength(255)]
        public string SourceColumnName { get; set; }
        [Required]
        [StringLength(255)]
        public string ConcatenationValue { get; set; }
        [Required]
        [StringLength(255)]
        public string FilterValueRule { get; set; }
    }
}