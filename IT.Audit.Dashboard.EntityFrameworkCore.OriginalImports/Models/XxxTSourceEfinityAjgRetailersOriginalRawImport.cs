﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace IT.Audit.Dashboard.EntityFrameworkCore.OriginalImports.Models
{
    [Keyless]
    [Table("xxx_t_Source_Efinity_AJG_Retailers_Original_Raw_Import")]
    public partial class XxxTSourceEfinityAjgRetailersOriginalRawImport
    {
        [StringLength(255)]
        public string Company { get; set; }
        [Column("Company locked")]
        [StringLength(255)]
        public string CompanyLocked { get; set; }
        [Column("Company tags")]
        [StringLength(4000)]
        public string CompanyTags { get; set; }
        [StringLength(255)]
        public string Division { get; set; }
        [Column("Division locked")]
        [StringLength(255)]
        public string DivisionLocked { get; set; }
        [Column("Division tags")]
        [StringLength(4000)]
        public string DivisionTags { get; set; }
        [StringLength(255)]
        public string Employee { get; set; }
        [StringLength(255)]
        public string Email { get; set; }
        [Column("Employee locked")]
        [StringLength(255)]
        public string EmployeeLocked { get; set; }
        [Column("Employee tags")]
        [StringLength(4000)]
        public string EmployeeTags { get; set; }
        [StringLength(4000)]
        public string Products { get; set; }
        [Column("Employee permissions")]
        [StringLength(255)]
        public string EmployeePermissions { get; set; }
        [Column("Last Login Date")]
        [StringLength(50)]
        public string LastLoginDate { get; set; }
    }
}