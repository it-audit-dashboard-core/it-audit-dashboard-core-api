﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using IT.Audit.Dashboard.EntityFrameworkCore.OriginalImports.Data;
using IT.Audit.Dashboard.EntityFrameworkCore.OriginalImports.Models;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore;
using System;


namespace IT.Audit.Dashboard.EntityFrameworkCore.OriginalImports.Data.Configurations
{
    public partial class TSourceEfinityAjgRetailerConfiguration : IEntityTypeConfiguration<TSourceEfinityAjgRetailer>
    {
        public void Configure(EntityTypeBuilder<TSourceEfinityAjgRetailer> entity)
        {
            entity.Property(e => e.CActiveYn).IsUnicode(false);

            entity.Property(e => e.CAdministratorYn).IsUnicode(false);

            entity.Property(e => e.Company).IsUnicode(false);

            entity.Property(e => e.CompanyLocked).IsUnicode(false);

            entity.Property(e => e.CompanyTags).IsUnicode(false);

            entity.Property(e => e.Division).IsUnicode(false);

            entity.Property(e => e.DivisionLocked).IsUnicode(false);

            entity.Property(e => e.DivisionTags).IsUnicode(false);

            entity.Property(e => e.Email).IsUnicode(false);

            entity.Property(e => e.Employee).IsUnicode(false);

            entity.Property(e => e.EmployeeLocked).IsUnicode(false);

            entity.Property(e => e.EmployeePermissions).IsUnicode(false);

            entity.Property(e => e.EmployeeTags).IsUnicode(false);

            entity.Property(e => e.Products).IsUnicode(false);

            OnConfigurePartial(entity);
        }

        partial void OnConfigurePartial(EntityTypeBuilder<TSourceEfinityAjgRetailer> entity);
    }
}
