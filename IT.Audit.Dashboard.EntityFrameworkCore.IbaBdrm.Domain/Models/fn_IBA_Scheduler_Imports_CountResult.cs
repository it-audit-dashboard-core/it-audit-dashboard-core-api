﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

#nullable enable

namespace IT.Audit.Dashboard.EntityFrameworkCore.IbaBdrm.Models
{
    public partial class fn_IBA_Scheduler_Imports_CountResult
    {
        public string? JobName { get; set; }
        public int? ImportCount { get; set; }
        public DateTime? ImportMax { get; set; }
        public int? ImportCountToday { get; set; }
    }
}
