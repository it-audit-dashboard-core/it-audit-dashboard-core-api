﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable enable

namespace IT.Audit.Dashboard.EntityFrameworkCore.IbaBdrm.Models
{
    [Table("t_IBA_Export_Job_Log")]
    public partial class TIbaExportJobLog
    {
        [Key]
        [Column("ID")]
        public int Id { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? RunDate { get; set; }
        [Column("IBA_Db")]
        [StringLength(100)]
        public string? IbaDb { get; set; }
        [Column("Export_Complete", TypeName = "datetime")]
        public DateTime? ExportComplete { get; set; }
    }
}