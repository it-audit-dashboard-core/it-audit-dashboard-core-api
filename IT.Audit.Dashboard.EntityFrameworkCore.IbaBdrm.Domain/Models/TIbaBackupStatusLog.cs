﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable enable

namespace IT.Audit.Dashboard.EntityFrameworkCore.IbaBdrm.Models
{
    [Table("t_IBA_Backup_Status_Log")]
    public partial class TIbaBackupStatusLog
    {
        [Key]
        [Column("ID")]
        public int Id { get; set; }
        [Column("BackupID")]
        public int? BackupId { get; set; }
        [StringLength(50)]
        public string? LogFileName { get; set; }
        [StringLength(50)]
        public string? BackupStatus { get; set; }
    }
}