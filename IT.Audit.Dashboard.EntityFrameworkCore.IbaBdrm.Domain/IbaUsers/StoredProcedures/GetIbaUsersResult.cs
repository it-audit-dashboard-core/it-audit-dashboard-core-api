﻿using System;

#nullable enable

namespace IT.Audit.Dashboard.EntityFrameworkCore.IbaBdrm.Domain
{
    public partial class GetIbaUsersResult
    {
        public string Name { get; set; } = default!;
        public string Instance { get; set; } = default!;
        public string Role { get; set; } = default!;
        public string Security { get; set; } = default!;
        public string Login { get; set; } = default!;
        public string? IsActive { get; set; }
        public string? UpdatedBy { get; set; }
        public DateTime? Updated { get; set; }
        public string? Disabled { get; set; }
        public string ServiceDeskRef { get; set; } = default!;
        public DateTime? CreatedDate { get; set; }
        public string CreatedRef { get; set; } = default!;
        public string? CreatedUser { get; set; }
        public string UserName { get; set; } = default!;
        public string ADOU { get; set; } = default!;
        public string AccountStatus { get; set; } = default!;
        public string? Office { get; set; }
        public string? AD_Department { get; set; }
        public string? City { get; set; }
        public string? Manager { get; set; }
        public DateTime? IBA_Load_Date { get; set; }
    }
}
