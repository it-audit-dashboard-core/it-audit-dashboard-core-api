﻿using IT.Audit.Dashboard.EntityFrameworkCore.Domain;
using IT.Audit.Dashboard.EntityFrameworkCore.IbaBdrm.Domain;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IT.Audit.Dashboard.EntityFrameworkCore.IbaBdrm
{
    public interface IIbaUserRepository : IGenericRepository<IbaUser>
    {
        Task<List<GetIbaUsersResult>> GetAsync();
    }
}
