﻿using System;

namespace IT.Audit.Dashboard.EntityFrameworkCore.IbaBdrm.Domain
{
    public partial class GetActiveDirectoryModifiedDateResult
    {
        public DateTime? LoadedDate { get; set; }
    }
}
