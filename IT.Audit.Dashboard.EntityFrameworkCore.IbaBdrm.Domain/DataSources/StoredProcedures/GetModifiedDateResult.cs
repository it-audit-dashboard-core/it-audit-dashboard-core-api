﻿using System;

namespace IT.Audit.Dashboard.EntityFrameworkCore.IbaBdrm.Domain 
{
    public partial class GetModifiedDateResult
    {
        public DateTime? LoadedDate { get; set; }
    }
}
