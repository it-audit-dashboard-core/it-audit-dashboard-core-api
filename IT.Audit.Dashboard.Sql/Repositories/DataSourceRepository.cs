﻿using IT.Audit.Dashboard.Domain.Shared;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
using IT.Audit.Dashboard.Sql.Domain;

namespace IT.Audit.Dashboard.Sql.Repositories
{
    public class DataSourceRepository : RepositoryBase, IDataSourceRepository
    {
        public DataSourceRepository(IConnectionProviderFactory connectionProviderFactory) 
            : base(connectionProviderFactory)
        {
        }

        public async Task<DateTime> GetModifiedDate(ConnectionType connectionType, string tableName)
        {
            var cs = GetConnectionString(connectionType);
            using (SqlConnection sqlConnection = new SqlConnection(cs))
            {
                sqlConnection.Open();

                SqlCommand sqlCommand = sqlConnection.CreateCommand();
                sqlCommand.CommandType = CommandType.Text;
                sqlCommand.CommandText = @$"SELECT MAX(Load_Date) FROM {tableName}";

                var loadDate = await sqlCommand.ExecuteScalarAsync();
                sqlConnection.Close();

                return loadDate == null ? DateTime.MinValue : DateTime.Parse(loadDate.ToString());
            }
        }
    }
}
