﻿using IT.Audit.Dashboard.Domain.Shared;
using IT.Audit.Dashboard.Sql.Domain;
using IT.Audit.Dashboard.Sql.Shared;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace IT.Audit.Dashboard.Sql.Repositories
{
    public class DashboardRepository : RepositoryBase, IDashboardRepository
    {
        public DashboardRepository(IConnectionProviderFactory connectionProviderFactory)
            : base(connectionProviderFactory)
        {
        }

        public Task AddAuthorisedUsersAsync(
            ConnectionType connectionType, 
            string userName, 
            string displayName, 
            string email, 
            List<UserApplicationGroup> userApplicationGroups)
        {
            var connectionString = GetConnectionString(connectionType);
            
            using (DatabaseHelper database = new DatabaseHelper(connectionString))
            {
                var parameters = new DatabaseParameterHelper();

                foreach (var group in userApplicationGroups)
                {
                    if (group.Key == "DashboardUser" && group.Group == "Domain Users")
                    {
                        string sql = @"INSERT INTO [dbo].[t_AuditDashboardUsersFromAD]
                                 ([User_Name], [Full_Name], [AD_Account_Status], [EmailAddress], [AD_Group_Name], [Field_Name])
                                 VALUES(@User_Name, @Full_Name, @AD_Account_Status, @EmailAddress, @AD_Group_Name, @Field_Name)";
                        string accountName = userName.Split('\\').Last();
                        parameters.Add("@User_Name", accountName);
                        parameters.Add("@Full_Name", displayName);
                        parameters.Add("@AD_Account_Status", "Active");
                        parameters.Add("@EmailAddress", email);
                        parameters.Add("@AD_Group_Name", group.Group);
                        parameters.Add("@Field_Name", group.Key);
                        
                        database.ExecuteNonQuery(sql, parameters.ParameterArray(), CommandType.Text);
                    }
                    else
                    {
                        string sql = "[dbo].[stp_insert_Group_Members]";
                        parameters.Add("@Domain", AppSettingKeys.LocalDomain);
                        parameters.Add("@ADGroupName", group.Group);
                        parameters.Add("@FieldName", group.Key);
                        database.ExecuteNonQuery(sql, parameters.ParameterArray(), CommandType.StoredProcedure);
                    }
                }
            }

            return Task.CompletedTask;
        }

        public Task DeleteUsersAsync(ConnectionType connectionType)
        {
            // stp_v2_ClearUsersFromAD
            //string sql = "TRUNCATE TABLE [dbo].[t_AuditDashboardUsersFromAD]";

            //database.ExecuteNonQuery(sql, null, CommandType.Text);
            throw new System.NotImplementedException();
        }

        public Task<List<IDashboardUser>> GetListAsync(ConnectionType connectionType, string userName)
        {
            //// stp_v2_GetUsersByUserName
            //// stp_v2_GetUsers
            //string sql = "SELECT * FROM [dbo].[t_AuditDashboardUsersFromAD]";

            //DatabaseParameterHelper parameters = new DatabaseParameterHelper();

            //if (userName != "")
            //{
            //    sql += " WHERE User_Name = @User_Name ORDER BY User_Name";

            //    parameters.Add("@User_Name", userName);
            //}

            //return database.GetDataTable(sql, parameters.ParameterArray());
            throw new System.NotImplementedException();
        }
    }
}
