﻿using IT.Audit.Dashboard.Domain.Shared;
using IT.Audit.Dashboard.Sql.Domain;
using IT.Audit.Dashboard.Sql.Shared;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Threading.Tasks;

namespace IT.Audit.Dashboard.Sql.Repositories
{
    public class IbaUsersRepository : RepositoryBase, IIbaUsersRepository
    {
        public IbaUsersRepository(IConnectionProviderFactory connectionProviderFactory) : base(connectionProviderFactory)
        {
        }

        public async Task<List<IbaUser>> Get()
        {
            using (SqlConnection connection = new SqlConnection(GetConnectionString(ConnectionType.SOX_IBA_BDRM)))
            {
                connection.Open();

                SqlCommand command = connection.CreateCommand();
                command.CommandType = CommandType.Text;
                command.CommandText = @"SELECT Name, 
                                            Instance, 
                                            Role, 
                                            Security, 
                                            Login, 
                                            [Is Active] AS IsActive, 
                                            [User] AS UpdatedBy, 
                                            Date AS Updated, 
                                            Disabled, 
                                            ISNULL([ServiceDesk Ref], '') AS ServiceDeskRef, 
                                            [Created Date] AS CreatedDate, 
                                            ISNULL([Created Ref], '') AS CreatedRef, 
                                            [Created User] AS CreatedUser, 
                                            ISNULL(ADList.User_Name, 'N/A') AS UserName, 
                                            ISNULL(ADList.AD_OU, 'N/A') AS ADOU, 
                                            ISNULL(ADList.AD_Account_Status, 'N/A') AS AccountStatus,
                                            ADList.Office AS Office,
                                            ADList.AD_Department AS AD_Department,
                                            ADList.City AS City,
                                            ADList.Manager AS Manager,
                                            t_IBA_Users.Load_Date AS IBA_Load_Date
                                            FROM t_IBA_Users
											LEFT OUTER JOIN	[{initialCatalog}].[dbo].[t_AD_Active_And_Inactive_Users] ADList
											ON t_IBA_Users.Login COLLATE DATABASE_DEFAULT = ADList.User_Name COLLATE DATABASE_DEFAULT
                                            ORDER BY Login, Instance";

                string initialCatalog = DatabaseHelper.ServerName(
                    GetConnectionString(ConnectionType.Active_Directory_Users)).Split('/')[1].Trim();

                command.CommandText = command.CommandText.Replace(IbaUsersConsts.InitialCatalogPlaceholder, initialCatalog);

                var users = new List<IbaUser>();

                using (SqlDataReader reader = await command.ExecuteReaderAsync())
                {
                    while (reader.Read())
                    {
                        var user = new IbaUser();

                        user.Name = !reader.IsDBNull(0) ? reader.GetString(0) : null;
                        user.Instance = !reader.IsDBNull(1) ? reader.GetString(1) : null;
                        user.Role = !reader.IsDBNull(2) ? reader.GetString(2) : null;
                        user.Security = !reader.IsDBNull(3) ? reader.GetString(3) : null;
                        user.Login = !reader.IsDBNull(4) ? reader.GetString(4) : null;
                        user.IsActive = !reader.IsDBNull(5) ? reader.GetString(5) == "Y" : false;
                        user.UpdatedBy = !reader.IsDBNull(6) ? reader.GetString(6) : null;
                        user.Updated = reader.GetDateTime(7);
                        user.Disabled = !reader.IsDBNull(8) ? reader.GetString(8) == "Y" : false;
                        user.ServiceDeskRef = !reader.IsDBNull(9) ? reader.GetString(9) : null;
                        user.CreatedDate = reader.GetDateTime(10);
                        user.CreatedRef = !reader.IsDBNull(11) ? reader.GetString(11) : null;
                        user.CreatedUser = !reader.IsDBNull(12) ? reader.GetString(12) : null;
                        user.UserName = !reader.IsDBNull(13) ? reader.GetString(13) : null;
                        user.ADOU = !reader.IsDBNull(14) ? reader.GetString(14) : null;
                        user.AccountStatus = !reader.IsDBNull(15) ? reader.GetString(15) : null;
                        user.Office = !reader.IsDBNull(16) ? reader.GetString(16) : null;
                        user.AdDepartment = !reader.IsDBNull(17) ? reader.GetString(17) : null;
                        user.City = !reader.IsDBNull(18) ? reader.GetString(18) : null;
                        user.Manager = !reader.IsDBNull(19) ? reader.GetString(19) : null;
                        user.IbaLoadDate = reader.GetDateTime(20);

                        users.Add(user);
                    }

                    reader.Close();
                }

                connection.Close();

                return users;
            }
        }

        /// <summary>
        /// Obselete?
        /// </summary>
        /// <returns></returns>
        private async Task<bool> FileExists()
        {
            string path = IbaUsersAppSettings.PathActiveUsers;

            return await Task.FromResult(File.Exists(path));
        }
    }
}
