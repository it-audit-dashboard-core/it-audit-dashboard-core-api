﻿using IT.Audit.Dashboard.Domain.Data;
using IT.Audit.Dashboard.Sql.Domain;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IT.Audit.Dashboard.Sql.ItAuditDashboard.Repositories
{
    public class UserApplicationRepository : IUserApplicationRepository
    {
        public async Task<List<UserApplication>> GetListAsync()
            => await Task.FromResult(UserApplicationData.UserApplications.ToList());

        public async Task<UserApplication> GetByKeyAsync(string key)
            => await Task.FromResult(UserApplicationData.UserApplications.FirstOrDefault(x=>x.Key == key));
    }
}
