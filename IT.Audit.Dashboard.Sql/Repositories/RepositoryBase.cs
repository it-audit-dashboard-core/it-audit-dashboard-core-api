﻿using IT.Audit.Dashboard.Domain.Shared;
using IT.Audit.Dashboard.Sql.Domain;

namespace IT.Audit.Dashboard.Sql.Repositories
{
    public abstract class RepositoryBase
    {
        protected readonly IConnectionProviderFactory ConnectionProviderFactory;

        public RepositoryBase(IConnectionProviderFactory connectionProviderFactory)
        {
            ConnectionProviderFactory = connectionProviderFactory;
        }

        public virtual string GetConnectionString(ConnectionType connectionType)
        {
            return ConnectionProviderFactory.CreateConnectionProvider(connectionType)?.ConnectionString;
        }
    }
}
