﻿using IT.Audit.Dashboard.Sql.Domain;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IT.Audit.Dashboard.Sql.Repositories
{
    public class UserApplicationGroupsRepository : IUserApplicationGroupsRepository
    {
        public UserApplicationGroupsRepository() { }


        public async Task<List<UserApplicationGroup>> GetListAsync()
            => await Task.FromResult(UserApplicationGroupData.UserApplicationGroups.ToList());

        public async Task<UserApplicationGroup> GetByKeyAsync(string key)
            => await Task.FromResult(UserApplicationGroupData.UserApplicationGroups.FirstOrDefault(x => x.Key == key));
    }
}
