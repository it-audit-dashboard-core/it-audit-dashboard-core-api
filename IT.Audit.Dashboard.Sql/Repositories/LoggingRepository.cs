﻿using IT.Audit.Dashboard.Domain;
using IT.Audit.Dashboard.Domain.Shared;
using IT.Audit.Dashboard.Sql.Domain;
using IT.Audit.Dashboard.Sql.Shared;
using System;
using System.Threading.Tasks;

namespace IT.Audit.Dashboard.Sql.Repositories
{
    public class LoggingRepository : RepositoryBase, ILoggingRepository
    {
        public LoggingRepository(IConnectionProviderFactory connectionProviderFactory) : base(connectionProviderFactory)
        {
        }

        public Task<int> ClearErrors()
        {
            //TODO: Implement EF Core SP Call
            throw new NotImplementedException();
        }

        // TODO: Stored Procedure LogError
        // TODO: Customise ILogger from Core??

        public async Task<string> LogError(string user, string controller, string action, string messageText)
        {
            try
            {
                var connectionString = GetConnectionString(ConnectionType.IT_Audit_Dashboard);

                using (DatabaseHelper database = new DatabaseHelper(connectionString))
                {
                    DatabaseParameterHelper parameters = new DatabaseParameterHelper();

                    string SQL = @"INSERT INTO [dbo].[t_WebSiteErrorLog] 
                                  ([ErrorTime],
                                   [UserName],
                                   [MachineName],
                                   [ModuleName],
                                   [FunctionName],
                                   [MessageText]) 
                                  VALUES 
                                  (@ErrorTime,
                                   @UserName,
                                   @MachineName,
                                   @ModuleName,
                                   @FunctionName,
                                   @MessageText) ";

                    parameters.Add("@ErrorTime", DateTime.Now);
                    parameters.Add("@UserName", user);
                    parameters.Add("@MachineName", Environment.MachineName);
                    parameters.Add("@ModuleName", controller); // getRouteValue(context, "controller"));
                    parameters.Add("@FunctionName", action); // getRouteValue(context, "action"));
                    parameters.Add("@MessageText", messageText);

                    var id = database.ExecuteNonQuery(SQL, parameters.ParameterArray(), System.Data.CommandType.Text);

                    return id > 0 ? "Message logged" : "Message Not Logged";
                }
            }
            catch (Exception ex)
            {
                return "Error logging message: " + ex.Message;
            }
        }
    }
}
