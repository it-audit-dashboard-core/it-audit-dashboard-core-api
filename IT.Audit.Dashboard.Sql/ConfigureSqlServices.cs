﻿using IT.Audit.Dashboard.Sql.Domain;
using IT.Audit.Dashboard.Sql.Providers;
using IT.Audit.Dashboard.Sql.Repositories;
using Microsoft.Extensions.DependencyInjection;

namespace IT.Audit.Dashboard.Sql
{
    public static class ConfigureSqlServices
    {
        public static IServiceCollection AddSqlRepositoryServices(this IServiceCollection services)
        {
            services.AddTransient<IConnectionProviderFactory, ConnectionProviderFactory>();
            services.AddTransient<IConnectionProvider, ConnectionProvider>();
            services.AddTransient<IDataSourceRepository, DataSourceRepository>();
            services.AddTransient<IIbaUsersRepository, IbaUsersRepository>();

            return services;
        }
    }
}
