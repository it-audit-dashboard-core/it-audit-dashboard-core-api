﻿using IT.Audit.Dashboard.Sql.Domain;
using Microsoft.Extensions.Configuration;

namespace IT.Audit.Dashboard.Sql.Providers
{
    public class ConnectionProvider : IConnectionProvider
    {
        public IConfiguration Configuration { get; }

        public virtual string ConnectionString { get; }

        public ConnectionProvider(IConfiguration configuration)
        {
            Configuration = configuration;
        }
    }
}
