﻿using IT.Audit.Dashboard.Domain.Shared;
using Microsoft.Extensions.Configuration;

namespace IT.Audit.Dashboard.Sql.Providers
{
    public class IbaConnectionProvider : ConnectionProvider
    {
        public IbaConnectionProvider(IConfiguration configuration) : base(configuration)
        {
        }

        public override string ConnectionString
        {
            get => Configuration.GetConnectionString(ConnectionType.SOX_IBA_BDRM.ToString());
        }
    }
}
