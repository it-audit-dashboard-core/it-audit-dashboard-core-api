﻿using IT.Audit.Dashboard.Domain.Shared;
using Microsoft.Extensions.Configuration;

namespace IT.Audit.Dashboard.Sql.Providers
{
    public class ITAuditDashboardConnectionProvider : ConnectionProvider
    {
        public ITAuditDashboardConnectionProvider(IConfiguration configuration) : base(configuration)
        {
        }

        public override string ConnectionString
        {
            get => Configuration.GetConnectionString(ConnectionType.IT_Audit_Dashboard.ToString());
        }
    }
}
