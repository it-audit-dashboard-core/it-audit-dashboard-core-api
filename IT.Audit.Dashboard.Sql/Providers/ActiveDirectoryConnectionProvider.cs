﻿using IT.Audit.Dashboard.Domain.Shared;
using Microsoft.Extensions.Configuration;

namespace IT.Audit.Dashboard.Sql.Providers
{
    public class ActiveDirectoryConnectionProvider: ConnectionProvider
    {
        public ActiveDirectoryConnectionProvider(IConfiguration configuration) : base(configuration)
        {
            
        }

        public override string ConnectionString
        {
            get => Configuration.GetConnectionString(ConnectionType.Active_Directory_Users.ToString());
        }

        //public override void SetConnectionString(ConnectionType connectionType)
        //{
        //    throw new System.NotImplementedException();
        //}
    }
}
