﻿using IT.Audit.Dashboard.Domain.Shared;
using IT.Audit.Dashboard.Sql.Domain;
using Microsoft.Extensions.Configuration;
using System;

namespace IT.Audit.Dashboard.Sql.Providers
{
    public class ConnectionProviderFactory : IConnectionProviderFactory
    {
        private readonly IConfiguration Configuration;

        public ConnectionProviderFactory(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConnectionProvider CreateConnectionProvider(ConnectionType connectionType)
        {
            ConnectionType = connectionType;
            ConnectionProvider provider;

            switch (connectionType)
            {
                case ConnectionType.SOX_IBA_BDRM:
                    provider = new IbaConnectionProvider(Configuration);
                    break;
                case ConnectionType.IT_Audit_Dashboard:
                    provider = new ITAuditDashboardConnectionProvider(Configuration);
                    break;
                case ConnectionType.Active_Directory_Users:
                    provider = new ActiveDirectoryConnectionProvider(Configuration);
                    break;
                default:
                    throw new NotImplementedException(nameof(CreateConnectionProvider));
            }

            return provider;
        }

        public ConnectionType ConnectionType { get; set; }
    }
}
