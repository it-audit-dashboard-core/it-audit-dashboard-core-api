﻿using System;
using System.Text;

namespace IT.Audit.Dashboard.Domain.Helpers
{
    public class SpreadsheetHelper
    {
        private StringBuilder data;

        public SpreadsheetHelper()
        {
            data = new StringBuilder();
        }

        public void startRow()
        {
            data.AppendLine("<Row>");
        }

        public void endRow()
        {
            data.AppendLine("</Row>");
        }

        public void addIntegerCell(int data, bool zeroToBlank)
        {
            if (data > 0 || !zeroToBlank)
                addCell(data.ToString());
            else
                addCell();
        }

        public void addDateCell(DateTime date)
        {
            if (date.ToString("yyyy-MM-dd") != "0001-01-01")
                addCell(date.ToString("yyyy-MM-dd"));
            else
                addCell();
        }

        public void addYesNoCell(bool data)
        {
            if (data)
                addCell("Yes");
            else
                addCell("No");
        }

        public void addYesNoCell(string data)
        {
            if (data == null)
                addCell("");
            else if (data == "")
                addCell("");
            else if (data.ToUpper() == "TRUE")
                addCell("Yes");
            else
                addCell("No");
        }

        public void addCell(string value = "")
        {
            if (value == null)
                value = "";

            if (value != "")
                data.AppendLine($"<Cell><Data ss:Type=\"String\">{value.Replace("<", "&lt;").Replace(">", "&gt;")}</Data></Cell>");
            else
                data.AppendLine($"<Cell><Data ss:Type=\"String\"></Data></Cell>");
        }

        public void addCell(bool value)
        {
            addCell(value.ToString());
        }
        public string getData()
        {
            return data.ToString();
        }

        public string getReportTemplate(string author, string filePathName, string reportName)
        {
            StringBuilder sb = new StringBuilder();

            string file = System.IO.File.ReadAllText(filePathName);
            sb.Append(file);

            sb.Replace("%AUTHOR%", author);
            sb.Replace("%LASTAUTHOR%", author);
            sb.Replace("%CREATED%", DateTime.Now.ToString("o")); //2015-05-06T09:01:07Z
            sb.Replace("%TITLE%", reportName + " " + DateTime.Now.ToShortDateString());

            return sb.ToString();
        }
    }

}
