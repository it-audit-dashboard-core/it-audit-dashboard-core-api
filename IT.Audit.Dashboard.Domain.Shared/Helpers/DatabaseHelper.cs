﻿namespace IT.Audit.Dashboard.Domain.Shared.Helpers
{
    public class DatabaseHelper
    {
        public static ConnectionType GetConnectionType(string key)
        {
            switch (key)
            {
                case "iba-users":
                    return ConnectionType.SOX_IBA_BDRM;
                case "ad-users":
                    return ConnectionType.Active_Directory_Users;
                case "it-audit-dashboard":
                    return ConnectionType.IT_Audit_Dashboard;
                default:
                    return ConnectionType.SOX_IBA_BDRM;
            }
        }
    }
}
