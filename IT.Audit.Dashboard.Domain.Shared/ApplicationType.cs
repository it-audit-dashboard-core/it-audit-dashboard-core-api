﻿namespace IT.Audit.Dashboard.Domain.Shared
{
    public enum ApplicationType
    {
        IBA,
        AD,
        BDRM,
        Acturis,
        CDL,
        ADGroupMembership,
        GenericUAR,
        GenericUARReferenceData
    }
}
