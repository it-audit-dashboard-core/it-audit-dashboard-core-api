﻿namespace IT.Audit.Dashboard.Domain.Shared
{
    public enum ConnectionType
    {
        IT_Audit_Dashboard = 0,
        SOX_IBA_BDRM = 1,
        Active_Directory_Users = 2,
        SOX_Original_Imports = 3,
        SOX_IDM = 4,
        SOX_Acturis = 5,
        SOX_CDL = 6,
        SOX_Generic_UAR = 7
    }

    public static class TableName
    {
        public static string WebSiteErrorLog = "WebSiteErrorLog";
    }
}
