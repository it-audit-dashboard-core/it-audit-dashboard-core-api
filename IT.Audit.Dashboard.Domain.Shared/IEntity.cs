﻿namespace IT.Audit.Dashboard.Domain.Shared
{
    public interface IEntity
    {
        int Id { get; set; }
    }
}
