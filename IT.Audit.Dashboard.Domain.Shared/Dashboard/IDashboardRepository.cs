﻿using IT.Audit.Dashboard.Domain.Shared;
using IT.Audit.Dashboard.Domain.Shared.Dashboard;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IT.Audit.Dashboard.Domain.Dashboard
{
    public interface IDashboardRepository
    {
        Task DeleteUsersAsync(ConnectionType connectionType);
        Task AddAuthorisedUsersAsync(ConnectionType connectionType);
        Task<List<IDashboardUser>> GetListAsync(ConnectionType connectionType, string userName);
    }
}