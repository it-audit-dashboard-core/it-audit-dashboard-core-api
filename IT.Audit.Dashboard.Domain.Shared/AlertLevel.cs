﻿namespace IT.Audit.Dashboard.Domain.Shared
{
    public enum AlertLevel
    {
        Info,
        Warning,
        Danger
    }
}
