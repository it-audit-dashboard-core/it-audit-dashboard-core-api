﻿namespace IT.Audit.Dashboard.Domain.Shared
{
    public class AppSettingKeys
    {
        public static string LocalDomain = "DC=emea,DC=ajgco,DC=com";
        public static string ADGroupMembershipIncludeNestedDuplicatesDefault = "false";
        public static string TemplatesPath = @"C:\Visual Studio 2019\Projects\IT Audit Dashboard-V6\Web\App_Data\";
        public static string ADGroupMembershipTemplate = "AD_Group_Membership_Template.xml";
    }
}
