﻿namespace IT.Audit.Dashboard.Domain.Shared
{
    public static class IbaUsersConsts
    {
        public static string CacheKeyIbaModifiedDate = "IBA_USERS";
        public static string CacheKeyIBAModifiedDate = "IBA_MODIFIED_DATE";
        public static string CacheKeyADModifiedDate = "AD_MODIFIED_DATE";

        public static string InitialCatalogPlaceholder = "{initialCatalog}";
    }
}
