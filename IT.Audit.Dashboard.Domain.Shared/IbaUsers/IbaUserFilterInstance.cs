﻿using System;

namespace IT.Audit.Dashboard.Domain
{
    public abstract class FilterChildBase
    {
        public FilterInstance Instance { get; set; }
        public string Name { get; set; }

        public override int GetHashCode()
        {
            int hashName = Name?.GetHashCode() ?? 0;
            int hashInstance = Instance?.Name.GetHashCode() ?? 0;

            return hashName ^ hashInstance;
        }

        public override string ToString()
        {
            return Name;
        }
    }

    public class FilterInstance : IComparable<FilterInstance>
    {
        public string Name { get; set; }

        public int CompareTo(FilterInstance other)
        {
            return this.Name.CompareTo(other.Name);
        }

        public override string ToString()
        {
            return Name;
        }

        public bool Equals(FilterInstance other)

        {
            return (Name == other.Name && Name == other.Name);
        }

        public override int GetHashCode()
        {
            int hashName = Name?.GetHashCode() ?? 0;

            return hashName;
        }
    }

    public class FilterSecurity : FilterChildBase, IComparable<FilterSecurity>, IEquatable<FilterSecurity>
    {
        public int CompareTo(FilterSecurity other)
        {
            int insComp = this.Instance.Name.CompareTo(other.Instance.Name);

            if (insComp != 0)
                return insComp;
            else
                return this.Name.CompareTo(other.Name);
        }

        public bool Equals(FilterSecurity other)
        {
            return (Name == other.Name && Instance.Name == other.Instance.Name);
        }
    }

    public class FilterRole : FilterChildBase, IComparable<FilterRole>, IEquatable<FilterRole>
    {
        public int CompareTo(FilterRole other)
        {
            int insComp = this.Instance.Name.CompareTo(other.Instance.Name);

            if (insComp != 0)
                return insComp;
            else
                return this.Name.CompareTo(other.Name);
        }

        public bool Equals(FilterRole other)
        {
            return (Name == other.Name && Instance.Name == other.Instance.Name);
        }
    }
}