﻿namespace IT.Audit.Dashboard.Domain.Shared
{
    public enum IbaUserActiveType
    {
        Active,
        Inactive,
        Both,
        NA
    }
}
