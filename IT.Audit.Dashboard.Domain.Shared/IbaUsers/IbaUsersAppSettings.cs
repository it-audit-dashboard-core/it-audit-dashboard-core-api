﻿namespace IT.Audit.Dashboard.Domain.Shared
{
    public class IbaUsersAppSettings
    {
        public static readonly string TableIbaUsers = "dbo.t_IBA_Users";

        // TODO: No corresponsing value in web.config in AngularJS
        public static readonly string PathActiveUsers = string.Empty;
        public static int CacheMinutes = 1;
    }
}
