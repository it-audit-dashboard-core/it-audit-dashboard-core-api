﻿namespace IT.Audit.Dashboard.Domain.Shared
{
    public static class ConnectionStringKey
    {
        public static string ITAuditDashboard = "IT_Audit_Dashboard";
        public static string IbaUsers = "SOX_IBA_BDRM";
    }
}
