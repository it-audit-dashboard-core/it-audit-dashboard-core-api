﻿using Microsoft.Data.SqlClient;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using IT.Audit.Dashboard.EntityFrameworkCore.IbaBdrm.Domain;
using System.Linq;

namespace IT.Audit.Dashboard.EntityFrameworkCore.IbaBdrm
{
    public partial class IbaBdrmDbContext
    {
        private IbaBdrmDbContextProcedures _procedures;

        public IbaBdrmDbContextProcedures Procedures
        {
            get
            {
                if (_procedures is null) _procedures = new IbaBdrmDbContextProcedures(this);
                return _procedures;
            }
            set
            {
                _procedures = value;
            }
        }

        public IbaBdrmDbContextProcedures GetProcedures()
        {
            return Procedures;
        }
    }

    public partial class IbaBdrmDbContextProcedures
    {
        private readonly IbaBdrmDbContext _context;

        public IbaBdrmDbContextProcedures(IbaBdrmDbContext context)
        {
            _context = context;
        }

        //public virtual async Task<int> stp_Append_BDRM_Schedule_LogAsync(DateTime? dtRunDate, string Subject, DateTime? dtLoad_Date, OutputParameter<int> returnValue = null, CancellationToken cancellationToken = default)
        //{
        //    var parameterreturnValue = new SqlParameter
        //    {
        //        ParameterName = "returnValue",
        //        Direction = System.Data.ParameterDirection.Output,
        //        SqlDbType = System.Data.SqlDbType.Int,
        //    };

        //    var sqlParameters = new []
        //    {
        //        new SqlParameter
        //        {
        //            ParameterName = "dtRunDate",
        //            Value = dtRunDate ?? Convert.DBNull,
        //            SqlDbType = System.Data.SqlDbType.DateTime,
        //        },
        //        new SqlParameter
        //        {
        //            ParameterName = "Subject",
        //            Size = 100,
        //            Value = Subject ?? Convert.DBNull,
        //            SqlDbType = System.Data.SqlDbType.VarChar,
        //        },
        //        new SqlParameter
        //        {
        //            ParameterName = "dtLoad_Date",
        //            Value = dtLoad_Date ?? Convert.DBNull,
        //            SqlDbType = System.Data.SqlDbType.DateTime,
        //        },
        //        parameterreturnValue,
        //    };
        //    var _ = await _context.Database.ExecuteSqlRawAsync("EXEC @returnValue = [dbo].[stp_Append_BDRM_Schedule_Log] @dtRunDate, @Subject, @dtLoad_Date", sqlParameters, cancellationToken);

        //    returnValue?.SetValue(parameterreturnValue.Value);

        //    return _;
        //}

        //public virtual async Task<int> stp_Append_IBA_Backup_LogAsync(DateTime? dtRunDate, string Subject, DateTime? dtLoad_Date, OutputParameter<int> returnValue = null, CancellationToken cancellationToken = default)
        //{
        //    var parameterreturnValue = new SqlParameter
        //    {
        //        ParameterName = "returnValue",
        //        Direction = System.Data.ParameterDirection.Output,
        //        SqlDbType = System.Data.SqlDbType.Int,
        //    };

        //    var sqlParameters = new []
        //    {
        //        new SqlParameter
        //        {
        //            ParameterName = "dtRunDate",
        //            Value = dtRunDate ?? Convert.DBNull,
        //            SqlDbType = System.Data.SqlDbType.DateTime,
        //        },
        //        new SqlParameter
        //        {
        //            ParameterName = "Subject",
        //            Size = 100,
        //            Value = Subject ?? Convert.DBNull,
        //            SqlDbType = System.Data.SqlDbType.VarChar,
        //        },
        //        new SqlParameter
        //        {
        //            ParameterName = "dtLoad_Date",
        //            Value = dtLoad_Date ?? Convert.DBNull,
        //            SqlDbType = System.Data.SqlDbType.DateTime,
        //        },
        //        parameterreturnValue,
        //    };
        //    var _ = await _context.Database.ExecuteSqlRawAsync("EXEC @returnValue = [dbo].[stp_Append_IBA_Backup_Log] @dtRunDate, @Subject, @dtLoad_Date", sqlParameters, cancellationToken);

        //    returnValue?.SetValue(parameterreturnValue.Value);

        //    return _;
        //}

        //public virtual async Task<int> stp_Append_IBA_Backup_Log_File_DataAsync(DateTime? dtBackupCompleteDate, string Subject, string RowData, OutputParameter<int> returnValue = null, CancellationToken cancellationToken = default)
        //{
        //    var parameterreturnValue = new SqlParameter
        //    {
        //        ParameterName = "returnValue",
        //        Direction = System.Data.ParameterDirection.Output,
        //        SqlDbType = System.Data.SqlDbType.Int,
        //    };

        //    var sqlParameters = new []
        //    {
        //        new SqlParameter
        //        {
        //            ParameterName = "dtBackupCompleteDate",
        //            Value = dtBackupCompleteDate ?? Convert.DBNull,
        //            SqlDbType = System.Data.SqlDbType.DateTime,
        //        },
        //        new SqlParameter
        //        {
        //            ParameterName = "Subject",
        //            Size = 100,
        //            Value = Subject ?? Convert.DBNull,
        //            SqlDbType = System.Data.SqlDbType.VarChar,
        //        },
        //        new SqlParameter
        //        {
        //            ParameterName = "RowData",
        //            Size = 255,
        //            Value = RowData ?? Convert.DBNull,
        //            SqlDbType = System.Data.SqlDbType.VarChar,
        //        },
        //        parameterreturnValue,
        //    };
        //    var _ = await _context.Database.ExecuteSqlRawAsync("EXEC @returnValue = [dbo].[stp_Append_IBA_Backup_Log_File_Data] @dtBackupCompleteDate, @Subject, @RowData", sqlParameters, cancellationToken);

        //    returnValue?.SetValue(parameterreturnValue.Value);

        //    return _;
        //}

        //public virtual async Task<int> stp_Append_IBA_Backup_Status_LogAsync(DateTime? dtBackupCompleteDate, string Subject, string LogFileName, string BackupStatus, OutputParameter<int> returnValue = null, CancellationToken cancellationToken = default)
        //{
        //    var parameterreturnValue = new SqlParameter
        //    {
        //        ParameterName = "returnValue",
        //        Direction = System.Data.ParameterDirection.Output,
        //        SqlDbType = System.Data.SqlDbType.Int,
        //    };

        //    var sqlParameters = new []
        //    {
        //        new SqlParameter
        //        {
        //            ParameterName = "dtBackupCompleteDate",
        //            Value = dtBackupCompleteDate ?? Convert.DBNull,
        //            SqlDbType = System.Data.SqlDbType.DateTime,
        //        },
        //        new SqlParameter
        //        {
        //            ParameterName = "Subject",
        //            Size = 100,
        //            Value = Subject ?? Convert.DBNull,
        //            SqlDbType = System.Data.SqlDbType.VarChar,
        //        },
        //        new SqlParameter
        //        {
        //            ParameterName = "LogFileName",
        //            Size = 50,
        //            Value = LogFileName ?? Convert.DBNull,
        //            SqlDbType = System.Data.SqlDbType.VarChar,
        //        },
        //        new SqlParameter
        //        {
        //            ParameterName = "BackupStatus",
        //            Size = 50,
        //            Value = BackupStatus ?? Convert.DBNull,
        //            SqlDbType = System.Data.SqlDbType.VarChar,
        //        },
        //        parameterreturnValue,
        //    };
        //    var _ = await _context.Database.ExecuteSqlRawAsync("EXEC @returnValue = [dbo].[stp_Append_IBA_Backup_Status_Log] @dtBackupCompleteDate, @Subject, @LogFileName, @BackupStatus", sqlParameters, cancellationToken);

        //    returnValue?.SetValue(parameterreturnValue.Value);

        //    return _;
        //}

        //public virtual async Task<List<stp_Append_IBA_DR_Backup_LogResult>> stp_Append_IBA_DR_Backup_LogAsync(DateTime? dtRunDate, DateTime? dtLoad_Date, string MessageSubject, string BackupStatus, OutputParameter<int> returnValue = null, CancellationToken cancellationToken = default)
        //{
        //    var parameterreturnValue = new SqlParameter
        //    {
        //        ParameterName = "returnValue",
        //        Direction = System.Data.ParameterDirection.Output,
        //        SqlDbType = System.Data.SqlDbType.Int,
        //    };

        //    var sqlParameters = new []
        //    {
        //        new SqlParameter
        //        {
        //            ParameterName = "dtRunDate",
        //            Value = dtRunDate ?? Convert.DBNull,
        //            SqlDbType = System.Data.SqlDbType.DateTime,
        //        },
        //        new SqlParameter
        //        {
        //            ParameterName = "dtLoad_Date",
        //            Value = dtLoad_Date ?? Convert.DBNull,
        //            SqlDbType = System.Data.SqlDbType.DateTime,
        //        },
        //        new SqlParameter
        //        {
        //            ParameterName = "MessageSubject",
        //            Size = 255,
        //            Value = MessageSubject ?? Convert.DBNull,
        //            SqlDbType = System.Data.SqlDbType.VarChar,
        //        },
        //        new SqlParameter
        //        {
        //            ParameterName = "BackupStatus",
        //            Size = 255,
        //            Value = BackupStatus ?? Convert.DBNull,
        //            SqlDbType = System.Data.SqlDbType.VarChar,
        //        },
        //        parameterreturnValue,
        //    };
        //    var _ = await _context.SqlQueryAsync<stp_Append_IBA_DR_Backup_LogResult>("EXEC @returnValue = [dbo].[stp_Append_IBA_DR_Backup_Log] @dtRunDate, @dtLoad_Date, @MessageSubject, @BackupStatus", sqlParameters, cancellationToken);

        //    returnValue?.SetValue(parameterreturnValue.Value);

        //    return _;
        //}

        //public virtual async Task<int> stp_Append_IBA_Export_LogAsync(DateTime? dtRunDate, string Subject, DateTime? dtLoad_Date, OutputParameter<int> returnValue = null, CancellationToken cancellationToken = default)
        //{
        //    var parameterreturnValue = new SqlParameter
        //    {
        //        ParameterName = "returnValue",
        //        Direction = System.Data.ParameterDirection.Output,
        //        SqlDbType = System.Data.SqlDbType.Int,
        //    };

        //    var sqlParameters = new []
        //    {
        //        new SqlParameter
        //        {
        //            ParameterName = "dtRunDate",
        //            Value = dtRunDate ?? Convert.DBNull,
        //            SqlDbType = System.Data.SqlDbType.DateTime,
        //        },
        //        new SqlParameter
        //        {
        //            ParameterName = "Subject",
        //            Size = 100,
        //            Value = Subject ?? Convert.DBNull,
        //            SqlDbType = System.Data.SqlDbType.VarChar,
        //        },
        //        new SqlParameter
        //        {
        //            ParameterName = "dtLoad_Date",
        //            Value = dtLoad_Date ?? Convert.DBNull,
        //            SqlDbType = System.Data.SqlDbType.DateTime,
        //        },
        //        parameterreturnValue,
        //    };
        //    var _ = await _context.Database.ExecuteSqlRawAsync("EXEC @returnValue = [dbo].[stp_Append_IBA_Export_Log] @dtRunDate, @Subject, @dtLoad_Date", sqlParameters, cancellationToken);

        //    returnValue?.SetValue(parameterreturnValue.Value);

        //    return _;
        //}

        //public virtual async Task<List<stp_Append_IBA_FX_XML_Console_Job_LogResult>> stp_Append_IBA_FX_XML_Console_Job_LogAsync(string FileName, DateTime? EmailReceived, OutputParameter<int> returnValue = null, CancellationToken cancellationToken = default)
        //{
        //    var parameterreturnValue = new SqlParameter
        //    {
        //        ParameterName = "returnValue",
        //        Direction = System.Data.ParameterDirection.Output,
        //        SqlDbType = System.Data.SqlDbType.Int,
        //    };

        //    var sqlParameters = new []
        //    {
        //        new SqlParameter
        //        {
        //            ParameterName = "FileName",
        //            Size = 255,
        //            Value = FileName ?? Convert.DBNull,
        //            SqlDbType = System.Data.SqlDbType.VarChar,
        //        },
        //        new SqlParameter
        //        {
        //            ParameterName = "EmailReceived",
        //            Value = EmailReceived ?? Convert.DBNull,
        //            SqlDbType = System.Data.SqlDbType.DateTime,
        //        },
        //        parameterreturnValue,
        //    };
        //    var _ = await _context.SqlQueryAsync<stp_Append_IBA_FX_XML_Console_Job_LogResult>("EXEC @returnValue = [dbo].[stp_Append_IBA_FX_XML_Console_Job_Log] @FileName, @EmailReceived", sqlParameters, cancellationToken);

        //    returnValue?.SetValue(parameterreturnValue.Value);

        //    return _;
        //}

        //public virtual async Task<int> stp_Append_IBA_Schedule_Error_Log_File_DataAsync(DateTime? dtScheduleCompleteDate, string Subject, string RowData, OutputParameter<int> returnValue = null, CancellationToken cancellationToken = default)
        //{
        //    var parameterreturnValue = new SqlParameter
        //    {
        //        ParameterName = "returnValue",
        //        Direction = System.Data.ParameterDirection.Output,
        //        SqlDbType = System.Data.SqlDbType.Int,
        //    };

        //    var sqlParameters = new []
        //    {
        //        new SqlParameter
        //        {
        //            ParameterName = "dtScheduleCompleteDate",
        //            Value = dtScheduleCompleteDate ?? Convert.DBNull,
        //            SqlDbType = System.Data.SqlDbType.DateTime,
        //        },
        //        new SqlParameter
        //        {
        //            ParameterName = "Subject",
        //            Size = 100,
        //            Value = Subject ?? Convert.DBNull,
        //            SqlDbType = System.Data.SqlDbType.VarChar,
        //        },
        //        new SqlParameter
        //        {
        //            ParameterName = "RowData",
        //            Size = 512,
        //            Value = RowData ?? Convert.DBNull,
        //            SqlDbType = System.Data.SqlDbType.VarChar,
        //        },
        //        parameterreturnValue,
        //    };
        //    var _ = await _context.Database.ExecuteSqlRawAsync("EXEC @returnValue = [dbo].[stp_Append_IBA_Schedule_Error_Log_File_Data] @dtScheduleCompleteDate, @Subject, @RowData", sqlParameters, cancellationToken);

        //    returnValue?.SetValue(parameterreturnValue.Value);

        //    return _;
        //}

        //public virtual async Task<int> stp_Append_IBA_Schedule_LogAsync(DateTime? dtRunDate, string Subject, DateTime? dtLoad_Date, OutputParameter<int> returnValue = null, CancellationToken cancellationToken = default)
        //{
        //    var parameterreturnValue = new SqlParameter
        //    {
        //        ParameterName = "returnValue",
        //        Direction = System.Data.ParameterDirection.Output,
        //        SqlDbType = System.Data.SqlDbType.Int,
        //    };

        //    var sqlParameters = new []
        //    {
        //        new SqlParameter
        //        {
        //            ParameterName = "dtRunDate",
        //            Value = dtRunDate ?? Convert.DBNull,
        //            SqlDbType = System.Data.SqlDbType.DateTime,
        //        },
        //        new SqlParameter
        //        {
        //            ParameterName = "Subject",
        //            Size = 100,
        //            Value = Subject ?? Convert.DBNull,
        //            SqlDbType = System.Data.SqlDbType.VarChar,
        //        },
        //        new SqlParameter
        //        {
        //            ParameterName = "dtLoad_Date",
        //            Value = dtLoad_Date ?? Convert.DBNull,
        //            SqlDbType = System.Data.SqlDbType.DateTime,
        //        },
        //        parameterreturnValue,
        //    };
        //    var _ = await _context.Database.ExecuteSqlRawAsync("EXEC @returnValue = [dbo].[stp_Append_IBA_Schedule_Log] @dtRunDate, @Subject, @dtLoad_Date", sqlParameters, cancellationToken);

        //    returnValue?.SetValue(parameterreturnValue.Value);

        //    return _;
        //}

        //public virtual async Task<List<stp_Backups_Report_CoreResult>> stp_Backups_Report_CoreAsync(DateTime? DateFrom, DateTime? DateTo, string EmailRecipient, string CopyRecipients, bool? DailyCheck, int? ApplicationID, int? ScheduledJobTypeID, OutputParameter<int> returnValue = null, CancellationToken cancellationToken = default)
        //{
        //    var parameterreturnValue = new SqlParameter
        //    {
        //        ParameterName = "returnValue",
        //        Direction = System.Data.ParameterDirection.Output,
        //        SqlDbType = System.Data.SqlDbType.Int,
        //    };

        //    var sqlParameters = new []
        //    {
        //        new SqlParameter
        //        {
        //            ParameterName = "DateFrom",
        //            Value = DateFrom ?? Convert.DBNull,
        //            SqlDbType = System.Data.SqlDbType.DateTime,
        //        },
        //        new SqlParameter
        //        {
        //            ParameterName = "DateTo",
        //            Value = DateTo ?? Convert.DBNull,
        //            SqlDbType = System.Data.SqlDbType.DateTime,
        //        },
        //        new SqlParameter
        //        {
        //            ParameterName = "EmailRecipient",
        //            Size = 255,
        //            Value = EmailRecipient ?? Convert.DBNull,
        //            SqlDbType = System.Data.SqlDbType.VarChar,
        //        },
        //        new SqlParameter
        //        {
        //            ParameterName = "CopyRecipients",
        //            Size = 255,
        //            Value = CopyRecipients ?? Convert.DBNull,
        //            SqlDbType = System.Data.SqlDbType.VarChar,
        //        },
        //        new SqlParameter
        //        {
        //            ParameterName = "DailyCheck",
        //            Value = DailyCheck ?? Convert.DBNull,
        //            SqlDbType = System.Data.SqlDbType.Bit,
        //        },
        //        new SqlParameter
        //        {
        //            ParameterName = "ApplicationID",
        //            Value = ApplicationID ?? Convert.DBNull,
        //            SqlDbType = System.Data.SqlDbType.Int,
        //        },
        //        new SqlParameter
        //        {
        //            ParameterName = "ScheduledJobTypeID",
        //            Value = ScheduledJobTypeID ?? Convert.DBNull,
        //            SqlDbType = System.Data.SqlDbType.Int,
        //        },
        //        parameterreturnValue,
        //    };
        //    var _ = await _context.SqlQueryAsync<stp_Backups_Report_CoreResult>("EXEC @returnValue = [dbo].[stp_Backups_Report_Core] @DateFrom, @DateTo, @EmailRecipient, @CopyRecipients, @DailyCheck, @ApplicationID, @ScheduledJobTypeID", sqlParameters, cancellationToken);

        //    returnValue?.SetValue(parameterreturnValue.Value);

        //    return _;
        //}

        //public virtual async Task<List<stp_BDRM_Backups_Daily_CheckResult>> stp_BDRM_Backups_Daily_CheckAsync(OutputParameter<int> returnValue = null, CancellationToken cancellationToken = default)
        //{
        //    var parameterreturnValue = new SqlParameter
        //    {
        //        ParameterName = "returnValue",
        //        Direction = System.Data.ParameterDirection.Output,
        //        SqlDbType = System.Data.SqlDbType.Int,
        //    };

        //    var sqlParameters = new []
        //    {
        //        parameterreturnValue,
        //    };
        //    var _ = await _context.SqlQueryAsync<stp_BDRM_Backups_Daily_CheckResult>("EXEC @returnValue = [dbo].[stp_BDRM_Backups_Daily_Check]", sqlParameters, cancellationToken);

        //    returnValue?.SetValue(parameterreturnValue.Value);

        //    return _;
        //}

        //public virtual async Task<List<stp_BDRM_Backups_ReportResult>> stp_BDRM_Backups_ReportAsync(DateTime? datefrom, DateTime? dateto, string replyto, OutputParameter<int> returnValue = null, CancellationToken cancellationToken = default)
        //{
        //    var parameterreturnValue = new SqlParameter
        //    {
        //        ParameterName = "returnValue",
        //        Direction = System.Data.ParameterDirection.Output,
        //        SqlDbType = System.Data.SqlDbType.Int,
        //    };

        //    var sqlParameters = new []
        //    {
        //        new SqlParameter
        //        {
        //            ParameterName = "datefrom",
        //            Value = datefrom ?? Convert.DBNull,
        //            SqlDbType = System.Data.SqlDbType.DateTime,
        //        },
        //        new SqlParameter
        //        {
        //            ParameterName = "dateto",
        //            Value = dateto ?? Convert.DBNull,
        //            SqlDbType = System.Data.SqlDbType.DateTime,
        //        },
        //        new SqlParameter
        //        {
        //            ParameterName = "replyto",
        //            Size = 255,
        //            Value = replyto ?? Convert.DBNull,
        //            SqlDbType = System.Data.SqlDbType.VarChar,
        //        },
        //        parameterreturnValue,
        //    };
        //    var _ = await _context.SqlQueryAsync<stp_BDRM_Backups_ReportResult>("EXEC @returnValue = [dbo].[stp_BDRM_Backups_Report] @datefrom, @dateto, @replyto", sqlParameters, cancellationToken);

        //    returnValue?.SetValue(parameterreturnValue.Value);

        //    return _;
        //}

        //public virtual async Task<List<stp_BDRM_Backups_Weekly_CheckResult>> stp_BDRM_Backups_Weekly_CheckAsync(OutputParameter<int> returnValue = null, CancellationToken cancellationToken = default)
        //{
        //    var parameterreturnValue = new SqlParameter
        //    {
        //        ParameterName = "returnValue",
        //        Direction = System.Data.ParameterDirection.Output,
        //        SqlDbType = System.Data.SqlDbType.Int,
        //    };

        //    var sqlParameters = new []
        //    {
        //        parameterreturnValue,
        //    };
        //    var _ = await _context.SqlQueryAsync<stp_BDRM_Backups_Weekly_CheckResult>("EXEC @returnValue = [dbo].[stp_BDRM_Backups_Weekly_Check]", sqlParameters, cancellationToken);

        //    returnValue?.SetValue(parameterreturnValue.Value);

        //    return _;
        //}

        //public virtual async Task<List<stp_BDRM_Scheduler_Exports_Daily_CheckResult>> stp_BDRM_Scheduler_Exports_Daily_CheckAsync(OutputParameter<int> returnValue = null, CancellationToken cancellationToken = default)
        //{
        //    var parameterreturnValue = new SqlParameter
        //    {
        //        ParameterName = "returnValue",
        //        Direction = System.Data.ParameterDirection.Output,
        //        SqlDbType = System.Data.SqlDbType.Int,
        //    };

        //    var sqlParameters = new []
        //    {
        //        parameterreturnValue,
        //    };
        //    var _ = await _context.SqlQueryAsync<stp_BDRM_Scheduler_Exports_Daily_CheckResult>("EXEC @returnValue = [dbo].[stp_BDRM_Scheduler_Exports_Daily_Check]", sqlParameters, cancellationToken);

        //    returnValue?.SetValue(parameterreturnValue.Value);

        //    return _;
        //}

        //public virtual async Task<List<stp_BDRM_Scheduler_Exports_Weekly_CheckResult>> stp_BDRM_Scheduler_Exports_Weekly_CheckAsync(OutputParameter<int> returnValue = null, CancellationToken cancellationToken = default)
        //{
        //    var parameterreturnValue = new SqlParameter
        //    {
        //        ParameterName = "returnValue",
        //        Direction = System.Data.ParameterDirection.Output,
        //        SqlDbType = System.Data.SqlDbType.Int,
        //    };

        //    var sqlParameters = new []
        //    {
        //        parameterreturnValue,
        //    };
        //    var _ = await _context.SqlQueryAsync<stp_BDRM_Scheduler_Exports_Weekly_CheckResult>("EXEC @returnValue = [dbo].[stp_BDRM_Scheduler_Exports_Weekly_Check]", sqlParameters, cancellationToken);

        //    returnValue?.SetValue(parameterreturnValue.Value);

        //    return _;
        //}

        //public virtual async Task<List<stp_Exports_Report_CoreResult>> stp_Exports_Report_CoreAsync(DateTime? DateFrom, DateTime? DateTo, string EmailRecipient, string CopyRecipients, bool? DailyCheck, int? ApplicationID, int? ScheduledJobTypeID, OutputParameter<int> returnValue = null, CancellationToken cancellationToken = default)
        //{
        //    var parameterreturnValue = new SqlParameter
        //    {
        //        ParameterName = "returnValue",
        //        Direction = System.Data.ParameterDirection.Output,
        //        SqlDbType = System.Data.SqlDbType.Int,
        //    };

        //    var sqlParameters = new []
        //    {
        //        new SqlParameter
        //        {
        //            ParameterName = "DateFrom",
        //            Value = DateFrom ?? Convert.DBNull,
        //            SqlDbType = System.Data.SqlDbType.DateTime,
        //        },
        //        new SqlParameter
        //        {
        //            ParameterName = "DateTo",
        //            Value = DateTo ?? Convert.DBNull,
        //            SqlDbType = System.Data.SqlDbType.DateTime,
        //        },
        //        new SqlParameter
        //        {
        //            ParameterName = "EmailRecipient",
        //            Size = 255,
        //            Value = EmailRecipient ?? Convert.DBNull,
        //            SqlDbType = System.Data.SqlDbType.VarChar,
        //        },
        //        new SqlParameter
        //        {
        //            ParameterName = "CopyRecipients",
        //            Size = 255,
        //            Value = CopyRecipients ?? Convert.DBNull,
        //            SqlDbType = System.Data.SqlDbType.VarChar,
        //        },
        //        new SqlParameter
        //        {
        //            ParameterName = "DailyCheck",
        //            Value = DailyCheck ?? Convert.DBNull,
        //            SqlDbType = System.Data.SqlDbType.Bit,
        //        },
        //        new SqlParameter
        //        {
        //            ParameterName = "ApplicationID",
        //            Value = ApplicationID ?? Convert.DBNull,
        //            SqlDbType = System.Data.SqlDbType.Int,
        //        },
        //        new SqlParameter
        //        {
        //            ParameterName = "ScheduledJobTypeID",
        //            Value = ScheduledJobTypeID ?? Convert.DBNull,
        //            SqlDbType = System.Data.SqlDbType.Int,
        //        },
        //        parameterreturnValue,
        //    };
        //    var _ = await _context.SqlQueryAsync<stp_Exports_Report_CoreResult>("EXEC @returnValue = [dbo].[stp_Exports_Report_Core] @DateFrom, @DateTo, @EmailRecipient, @CopyRecipients, @DailyCheck, @ApplicationID, @ScheduledJobTypeID", sqlParameters, cancellationToken);

        //    returnValue?.SetValue(parameterreturnValue.Value);

        //    return _;
        //}

        //public virtual async Task<List<stp_IBA_Active_Or_Disabled_Users_Inactive_In_ADResult>> stp_IBA_Active_Or_Disabled_Users_Inactive_In_ADAsync(OutputParameter<int> returnValue = null, CancellationToken cancellationToken = default)
        //{
        //    var parameterreturnValue = new SqlParameter
        //    {
        //        ParameterName = "returnValue",
        //        Direction = System.Data.ParameterDirection.Output,
        //        SqlDbType = System.Data.SqlDbType.Int,
        //    };

        //    var sqlParameters = new []
        //    {
        //        parameterreturnValue,
        //    };
        //    var _ = await _context.SqlQueryAsync<stp_IBA_Active_Or_Disabled_Users_Inactive_In_ADResult>("EXEC @returnValue = [dbo].[stp_IBA_Active_Or_Disabled_Users_Inactive_In_AD]", sqlParameters, cancellationToken);

        //    returnValue?.SetValue(parameterreturnValue.Value);

        //    return _;
        //}

        //public virtual async Task<List<stp_IBA_Active_Users_Weekly_CheckResult>> stp_IBA_Active_Users_Weekly_CheckAsync(int? sel_rec, OutputParameter<int> returnValue = null, CancellationToken cancellationToken = default)
        //{
        //    var parameterreturnValue = new SqlParameter
        //    {
        //        ParameterName = "returnValue",
        //        Direction = System.Data.ParameterDirection.Output,
        //        SqlDbType = System.Data.SqlDbType.Int,
        //    };

        //    var sqlParameters = new []
        //    {
        //        new SqlParameter
        //        {
        //            ParameterName = "sel_rec",
        //            Value = sel_rec ?? Convert.DBNull,
        //            SqlDbType = System.Data.SqlDbType.Int,
        //        },
        //        parameterreturnValue,
        //    };
        //    var _ = await _context.SqlQueryAsync<stp_IBA_Active_Users_Weekly_CheckResult>("EXEC @returnValue = [dbo].[stp_IBA_Active_Users_Weekly_Check] @sel_rec", sqlParameters, cancellationToken);

        //    returnValue?.SetValue(parameterreturnValue.Value);

        //    return _;
        //}

        //public virtual async Task<List<stp_IBA_Backups_Daily_CheckResult>> stp_IBA_Backups_Daily_CheckAsync(OutputParameter<int> returnValue = null, CancellationToken cancellationToken = default)
        //{
        //    var parameterreturnValue = new SqlParameter
        //    {
        //        ParameterName = "returnValue",
        //        Direction = System.Data.ParameterDirection.Output,
        //        SqlDbType = System.Data.SqlDbType.Int,
        //    };

        //    var sqlParameters = new []
        //    {
        //        parameterreturnValue,
        //    };
        //    var _ = await _context.SqlQueryAsync<stp_IBA_Backups_Daily_CheckResult>("EXEC @returnValue = [dbo].[stp_IBA_Backups_Daily_Check]", sqlParameters, cancellationToken);

        //    returnValue?.SetValue(parameterreturnValue.Value);

        //    return _;
        //}

        //public virtual async Task<List<stp_IBA_Backups_ReportResult>> stp_IBA_Backups_ReportAsync(DateTime? datefrom, DateTime? dateto, string replyto, OutputParameter<int> returnValue = null, CancellationToken cancellationToken = default)
        //{
        //    var parameterreturnValue = new SqlParameter
        //    {
        //        ParameterName = "returnValue",
        //        Direction = System.Data.ParameterDirection.Output,
        //        SqlDbType = System.Data.SqlDbType.Int,
        //    };

        //    var sqlParameters = new []
        //    {
        //        new SqlParameter
        //        {
        //            ParameterName = "datefrom",
        //            Value = datefrom ?? Convert.DBNull,
        //            SqlDbType = System.Data.SqlDbType.DateTime,
        //        },
        //        new SqlParameter
        //        {
        //            ParameterName = "dateto",
        //            Value = dateto ?? Convert.DBNull,
        //            SqlDbType = System.Data.SqlDbType.DateTime,
        //        },
        //        new SqlParameter
        //        {
        //            ParameterName = "replyto",
        //            Size = 255,
        //            Value = replyto ?? Convert.DBNull,
        //            SqlDbType = System.Data.SqlDbType.VarChar,
        //        },
        //        parameterreturnValue,
        //    };
        //    var _ = await _context.SqlQueryAsync<stp_IBA_Backups_ReportResult>("EXEC @returnValue = [dbo].[stp_IBA_Backups_Report] @datefrom, @dateto, @replyto", sqlParameters, cancellationToken);

        //    returnValue?.SetValue(parameterreturnValue.Value);

        //    return _;
        //}

        //public virtual async Task<List<stp_IBA_Backups_Weekly_CheckResult>> stp_IBA_Backups_Weekly_CheckAsync(OutputParameter<int> returnValue = null, CancellationToken cancellationToken = default)
        //{
        //    var parameterreturnValue = new SqlParameter
        //    {
        //        ParameterName = "returnValue",
        //        Direction = System.Data.ParameterDirection.Output,
        //        SqlDbType = System.Data.SqlDbType.Int,
        //    };

        //    var sqlParameters = new []
        //    {
        //        parameterreturnValue,
        //    };
        //    var _ = await _context.SqlQueryAsync<stp_IBA_Backups_Weekly_CheckResult>("EXEC @returnValue = [dbo].[stp_IBA_Backups_Weekly_Check]", sqlParameters, cancellationToken);

        //    returnValue?.SetValue(parameterreturnValue.Value);

        //    return _;
        //}

        //public virtual async Task<List<stp_IBA_Insert_User_Account_Report_DataResult>> stp_IBA_Insert_User_Account_Report_DataAsync(string FileName, string DataDate, OutputParameter<int> returnValue = null, CancellationToken cancellationToken = default)
        //{
        //    var parameterreturnValue = new SqlParameter
        //    {
        //        ParameterName = "returnValue",
        //        Direction = System.Data.ParameterDirection.Output,
        //        SqlDbType = System.Data.SqlDbType.Int,
        //    };

        //    var sqlParameters = new []
        //    {
        //        new SqlParameter
        //        {
        //            ParameterName = "FileName",
        //            Size = 255,
        //            Value = FileName ?? Convert.DBNull,
        //            SqlDbType = System.Data.SqlDbType.VarChar,
        //        },
        //        new SqlParameter
        //        {
        //            ParameterName = "DataDate",
        //            Size = 20,
        //            Value = DataDate ?? Convert.DBNull,
        //            SqlDbType = System.Data.SqlDbType.VarChar,
        //        },
        //        parameterreturnValue,
        //    };
        //    var _ = await _context.SqlQueryAsync<stp_IBA_Insert_User_Account_Report_DataResult>("EXEC @returnValue = [dbo].[stp_IBA_Insert_User_Account_Report_Data] @FileName, @DataDate", sqlParameters, cancellationToken);

        //    returnValue?.SetValue(parameterreturnValue.Value);

        //    return _;
        //}

        //public virtual async Task<List<stp_IBA_Insert_User_DataResult>> stp_IBA_Insert_User_DataAsync(string FileName, string DataDate, OutputParameter<int> returnValue = null, CancellationToken cancellationToken = default)
        //{
        //    var parameterreturnValue = new SqlParameter
        //    {
        //        ParameterName = "returnValue",
        //        Direction = System.Data.ParameterDirection.Output,
        //        SqlDbType = System.Data.SqlDbType.Int,
        //    };

        //    var sqlParameters = new []
        //    {
        //        new SqlParameter
        //        {
        //            ParameterName = "FileName",
        //            Size = 255,
        //            Value = FileName ?? Convert.DBNull,
        //            SqlDbType = System.Data.SqlDbType.VarChar,
        //        },
        //        new SqlParameter
        //        {
        //            ParameterName = "DataDate",
        //            Size = 20,
        //            Value = DataDate ?? Convert.DBNull,
        //            SqlDbType = System.Data.SqlDbType.VarChar,
        //        },
        //        parameterreturnValue,
        //    };
        //    var _ = await _context.SqlQueryAsync<stp_IBA_Insert_User_DataResult>("EXEC @returnValue = [dbo].[stp_IBA_Insert_User_Data] @FileName, @DataDate", sqlParameters, cancellationToken);

        //    returnValue?.SetValue(parameterreturnValue.Value);

        //    return _;
        //}

        //public virtual async Task<List<stp_IBA_Last_Login_ReportResult>> stp_IBA_Last_Login_ReportAsync(string LoginID, string ReplyTo, OutputParameter<int> returnValue = null, CancellationToken cancellationToken = default)
        //{
        //    var parameterreturnValue = new SqlParameter
        //    {
        //        ParameterName = "returnValue",
        //        Direction = System.Data.ParameterDirection.Output,
        //        SqlDbType = System.Data.SqlDbType.Int,
        //    };

        //    var sqlParameters = new []
        //    {
        //        new SqlParameter
        //        {
        //            ParameterName = "LoginID",
        //            Size = 50,
        //            Value = LoginID ?? Convert.DBNull,
        //            SqlDbType = System.Data.SqlDbType.VarChar,
        //        },
        //        new SqlParameter
        //        {
        //            ParameterName = "ReplyTo",
        //            Size = 255,
        //            Value = ReplyTo ?? Convert.DBNull,
        //            SqlDbType = System.Data.SqlDbType.VarChar,
        //        },
        //        parameterreturnValue,
        //    };
        //    var _ = await _context.SqlQueryAsync<stp_IBA_Last_Login_ReportResult>("EXEC @returnValue = [dbo].[stp_IBA_Last_Login_Report] @LoginID, @ReplyTo", sqlParameters, cancellationToken);

        //    returnValue?.SetValue(parameterreturnValue.Value);

        //    return _;
        //}

        //public virtual async Task<List<stp_IBA_Scheduler_Exports_Daily_CheckResult>> stp_IBA_Scheduler_Exports_Daily_CheckAsync(OutputParameter<int> returnValue = null, CancellationToken cancellationToken = default)
        //{
        //    var parameterreturnValue = new SqlParameter
        //    {
        //        ParameterName = "returnValue",
        //        Direction = System.Data.ParameterDirection.Output,
        //        SqlDbType = System.Data.SqlDbType.Int,
        //    };

        //    var sqlParameters = new []
        //    {
        //        parameterreturnValue,
        //    };
        //    var _ = await _context.SqlQueryAsync<stp_IBA_Scheduler_Exports_Daily_CheckResult>("EXEC @returnValue = [dbo].[stp_IBA_Scheduler_Exports_Daily_Check]", sqlParameters, cancellationToken);

        //    returnValue?.SetValue(parameterreturnValue.Value);

        //    return _;
        //}

        //public virtual async Task<List<stp_IBA_Scheduler_Exports_ReportResult>> stp_IBA_Scheduler_Exports_ReportAsync(DateTime? datefrom, DateTime? dateto, string replyto, OutputParameter<int> returnValue = null, CancellationToken cancellationToken = default)
        //{
        //    var parameterreturnValue = new SqlParameter
        //    {
        //        ParameterName = "returnValue",
        //        Direction = System.Data.ParameterDirection.Output,
        //        SqlDbType = System.Data.SqlDbType.Int,
        //    };

        //    var sqlParameters = new []
        //    {
        //        new SqlParameter
        //        {
        //            ParameterName = "datefrom",
        //            Value = datefrom ?? Convert.DBNull,
        //            SqlDbType = System.Data.SqlDbType.DateTime,
        //        },
        //        new SqlParameter
        //        {
        //            ParameterName = "dateto",
        //            Value = dateto ?? Convert.DBNull,
        //            SqlDbType = System.Data.SqlDbType.DateTime,
        //        },
        //        new SqlParameter
        //        {
        //            ParameterName = "replyto",
        //            Size = 255,
        //            Value = replyto ?? Convert.DBNull,
        //            SqlDbType = System.Data.SqlDbType.VarChar,
        //        },
        //        parameterreturnValue,
        //    };
        //    var _ = await _context.SqlQueryAsync<stp_IBA_Scheduler_Exports_ReportResult>("EXEC @returnValue = [dbo].[stp_IBA_Scheduler_Exports_Report] @datefrom, @dateto, @replyto", sqlParameters, cancellationToken);

        //    returnValue?.SetValue(parameterreturnValue.Value);

        //    return _;
        //}

        //public virtual async Task<List<stp_IBA_Scheduler_Exports_Weekly_CheckResult>> stp_IBA_Scheduler_Exports_Weekly_CheckAsync(OutputParameter<int> returnValue = null, CancellationToken cancellationToken = default)
        //{
        //    var parameterreturnValue = new SqlParameter
        //    {
        //        ParameterName = "returnValue",
        //        Direction = System.Data.ParameterDirection.Output,
        //        SqlDbType = System.Data.SqlDbType.Int,
        //    };

        //    var sqlParameters = new []
        //    {
        //        parameterreturnValue,
        //    };
        //    var _ = await _context.SqlQueryAsync<stp_IBA_Scheduler_Exports_Weekly_CheckResult>("EXEC @returnValue = [dbo].[stp_IBA_Scheduler_Exports_Weekly_Check]", sqlParameters, cancellationToken);

        //    returnValue?.SetValue(parameterreturnValue.Value);

        //    return _;
        //}

        //public virtual async Task<List<stp_IBA_Scheduler_Imports_End_Of_Day_SummaryResult>> stp_IBA_Scheduler_Imports_End_Of_Day_SummaryAsync(OutputParameter<int> returnValue = null, CancellationToken cancellationToken = default)
        //{
        //    var parameterreturnValue = new SqlParameter
        //    {
        //        ParameterName = "returnValue",
        //        Direction = System.Data.ParameterDirection.Output,
        //        SqlDbType = System.Data.SqlDbType.Int,
        //    };

        //    var sqlParameters = new []
        //    {
        //        parameterreturnValue,
        //    };
        //    var _ = await _context.SqlQueryAsync<stp_IBA_Scheduler_Imports_End_Of_Day_SummaryResult>("EXEC @returnValue = [dbo].[stp_IBA_Scheduler_Imports_End_Of_Day_Summary]", sqlParameters, cancellationToken);

        //    returnValue?.SetValue(parameterreturnValue.Value);

        //    return _;
        //}

        //public virtual async Task<List<stp_IBA_Scheduler_Imports_Hourly_CheckResult>> stp_IBA_Scheduler_Imports_Hourly_CheckAsync(OutputParameter<int> returnValue = null, CancellationToken cancellationToken = default)
        //{
        //    var parameterreturnValue = new SqlParameter
        //    {
        //        ParameterName = "returnValue",
        //        Direction = System.Data.ParameterDirection.Output,
        //        SqlDbType = System.Data.SqlDbType.Int,
        //    };

        //    var sqlParameters = new []
        //    {
        //        parameterreturnValue,
        //    };
        //    var _ = await _context.SqlQueryAsync<stp_IBA_Scheduler_Imports_Hourly_CheckResult>("EXEC @returnValue = [dbo].[stp_IBA_Scheduler_Imports_Hourly_Check]", sqlParameters, cancellationToken);

        //    returnValue?.SetValue(parameterreturnValue.Value);

        //    return _;
        //}

        //public virtual async Task<List<stp_IBA_Scheduler_Imports_ReportResult>> stp_IBA_Scheduler_Imports_ReportAsync(DateTime? datefrom, DateTime? dateto, string replyto, OutputParameter<int> returnValue = null, CancellationToken cancellationToken = default)
        //{
        //    var parameterreturnValue = new SqlParameter
        //    {
        //        ParameterName = "returnValue",
        //        Direction = System.Data.ParameterDirection.Output,
        //        SqlDbType = System.Data.SqlDbType.Int,
        //    };

        //    var sqlParameters = new []
        //    {
        //        new SqlParameter
        //        {
        //            ParameterName = "datefrom",
        //            Value = datefrom ?? Convert.DBNull,
        //            SqlDbType = System.Data.SqlDbType.DateTime,
        //        },
        //        new SqlParameter
        //        {
        //            ParameterName = "dateto",
        //            Value = dateto ?? Convert.DBNull,
        //            SqlDbType = System.Data.SqlDbType.DateTime,
        //        },
        //        new SqlParameter
        //        {
        //            ParameterName = "replyto",
        //            Size = 255,
        //            Value = replyto ?? Convert.DBNull,
        //            SqlDbType = System.Data.SqlDbType.VarChar,
        //        },
        //        parameterreturnValue,
        //    };
        //    var _ = await _context.SqlQueryAsync<stp_IBA_Scheduler_Imports_ReportResult>("EXEC @returnValue = [dbo].[stp_IBA_Scheduler_Imports_Report] @datefrom, @dateto, @replyto", sqlParameters, cancellationToken);

        //    returnValue?.SetValue(parameterreturnValue.Value);

        //    return _;
        //}

        //public virtual async Task<List<stp_IBA_Scheduler_Imports_Report_CoreResult>> stp_IBA_Scheduler_Imports_Report_CoreAsync(DateTime? DateFrom, DateTime? DateTo, string EmailRecipient, string CopyRecipients, OutputParameter<int> returnValue = null, CancellationToken cancellationToken = default)
        //{
        //    var parameterreturnValue = new SqlParameter
        //    {
        //        ParameterName = "returnValue",
        //        Direction = System.Data.ParameterDirection.Output,
        //        SqlDbType = System.Data.SqlDbType.Int,
        //    };

        //    var sqlParameters = new []
        //    {
        //        new SqlParameter
        //        {
        //            ParameterName = "DateFrom",
        //            Value = DateFrom ?? Convert.DBNull,
        //            SqlDbType = System.Data.SqlDbType.Date,
        //        },
        //        new SqlParameter
        //        {
        //            ParameterName = "DateTo",
        //            Value = DateTo ?? Convert.DBNull,
        //            SqlDbType = System.Data.SqlDbType.Date,
        //        },
        //        new SqlParameter
        //        {
        //            ParameterName = "EmailRecipient",
        //            Size = 255,
        //            Value = EmailRecipient ?? Convert.DBNull,
        //            SqlDbType = System.Data.SqlDbType.VarChar,
        //        },
        //        new SqlParameter
        //        {
        //            ParameterName = "CopyRecipients",
        //            Size = 255,
        //            Value = CopyRecipients ?? Convert.DBNull,
        //            SqlDbType = System.Data.SqlDbType.VarChar,
        //        },
        //        parameterreturnValue,
        //    };
        //    var _ = await _context.SqlQueryAsync<stp_IBA_Scheduler_Imports_Report_CoreResult>("EXEC @returnValue = [dbo].[stp_IBA_Scheduler_Imports_Report_Core] @DateFrom, @DateTo, @EmailRecipient, @CopyRecipients", sqlParameters, cancellationToken);

        //    returnValue?.SetValue(parameterreturnValue.Value);

        //    return _;
        //}

        //public virtual async Task<List<stp_IBA_Scheduler_Imports_Summary_Report_CoreResult>> stp_IBA_Scheduler_Imports_Summary_Report_CoreAsync(int? JobTypeID, DateTime? RunDate, DateTime? Now, string EmailSubject, string EmailRecipient, string CopyRecipients, OutputParameter<int> returnValue = null, CancellationToken cancellationToken = default)
        //{
        //    var parameterreturnValue = new SqlParameter
        //    {
        //        ParameterName = "returnValue",
        //        Direction = System.Data.ParameterDirection.Output,
        //        SqlDbType = System.Data.SqlDbType.Int,
        //    };

        //    var sqlParameters = new []
        //    {
        //        new SqlParameter
        //        {
        //            ParameterName = "JobTypeID",
        //            Value = JobTypeID ?? Convert.DBNull,
        //            SqlDbType = System.Data.SqlDbType.Int,
        //        },
        //        new SqlParameter
        //        {
        //            ParameterName = "RunDate",
        //            Value = RunDate ?? Convert.DBNull,
        //            SqlDbType = System.Data.SqlDbType.Date,
        //        },
        //        new SqlParameter
        //        {
        //            ParameterName = "Now",
        //            Value = Now ?? Convert.DBNull,
        //            SqlDbType = System.Data.SqlDbType.DateTime,
        //        },
        //        new SqlParameter
        //        {
        //            ParameterName = "EmailSubject",
        //            Size = 255,
        //            Value = EmailSubject ?? Convert.DBNull,
        //            SqlDbType = System.Data.SqlDbType.VarChar,
        //        },
        //        new SqlParameter
        //        {
        //            ParameterName = "EmailRecipient",
        //            Size = 255,
        //            Value = EmailRecipient ?? Convert.DBNull,
        //            SqlDbType = System.Data.SqlDbType.VarChar,
        //        },
        //        new SqlParameter
        //        {
        //            ParameterName = "CopyRecipients",
        //            Size = 255,
        //            Value = CopyRecipients ?? Convert.DBNull,
        //            SqlDbType = System.Data.SqlDbType.VarChar,
        //        },
        //        parameterreturnValue,
        //    };
        //    var _ = await _context.SqlQueryAsync<stp_IBA_Scheduler_Imports_Summary_Report_CoreResult>("EXEC @returnValue = [dbo].[stp_IBA_Scheduler_Imports_Summary_Report_Core] @JobTypeID, @RunDate, @Now, @EmailSubject, @EmailRecipient, @CopyRecipients", sqlParameters, cancellationToken);

        //    returnValue?.SetValue(parameterreturnValue.Value);

        //    return _;
        //}

        //public virtual async Task<List<stp_IBA_Scheduler_Imports_Weekly_CheckResult>> stp_IBA_Scheduler_Imports_Weekly_CheckAsync(OutputParameter<int> returnValue = null, CancellationToken cancellationToken = default)
        //{
        //    var parameterreturnValue = new SqlParameter
        //    {
        //        ParameterName = "returnValue",
        //        Direction = System.Data.ParameterDirection.Output,
        //        SqlDbType = System.Data.SqlDbType.Int,
        //    };

        //    var sqlParameters = new []
        //    {
        //        parameterreturnValue,
        //    };
        //    var _ = await _context.SqlQueryAsync<stp_IBA_Scheduler_Imports_Weekly_CheckResult>("EXEC @returnValue = [dbo].[stp_IBA_Scheduler_Imports_Weekly_Check]", sqlParameters, cancellationToken);

        //    returnValue?.SetValue(parameterreturnValue.Value);

        //    return _;
        //}

        //public virtual async Task<List<stp_IBA_User_Updates_Weekly_CheckResult>> stp_IBA_User_Updates_Weekly_CheckAsync(OutputParameter<int> returnValue = null, CancellationToken cancellationToken = default)
        //{
        //    var parameterreturnValue = new SqlParameter
        //    {
        //        ParameterName = "returnValue",
        //        Direction = System.Data.ParameterDirection.Output,
        //        SqlDbType = System.Data.SqlDbType.Int,
        //    };

        //    var sqlParameters = new []
        //    {
        //        parameterreturnValue,
        //    };
        //    var _ = await _context.SqlQueryAsync<stp_IBA_User_Updates_Weekly_CheckResult>("EXEC @returnValue = [dbo].[stp_IBA_User_Updates_Weekly_Check]", sqlParameters, cancellationToken);

        //    returnValue?.SetValue(parameterreturnValue.Value);

        //    return _;
        //}

        //public virtual async Task<List<stp_IBA_Users_Daily_CheckResult>> stp_IBA_Users_Daily_CheckAsync(OutputParameter<int> returnValue = null, CancellationToken cancellationToken = default)
        //{
        //    var parameterreturnValue = new SqlParameter
        //    {
        //        ParameterName = "returnValue",
        //        Direction = System.Data.ParameterDirection.Output,
        //        SqlDbType = System.Data.SqlDbType.Int,
        //    };

        //    var sqlParameters = new []
        //    {
        //        parameterreturnValue,
        //    };
        //    var _ = await _context.SqlQueryAsync<stp_IBA_Users_Daily_CheckResult>("EXEC @returnValue = [dbo].[stp_IBA_Users_Daily_Check]", sqlParameters, cancellationToken);

        //    returnValue?.SetValue(parameterreturnValue.Value);

        //    return _;
        //}

        public virtual async Task<List<GetIbaUsersResult>> GetIbaUsersAsync(OutputParameter<int> returnValue = null, CancellationToken cancellationToken = default)
        {
            var parameterreturnValue = new SqlParameter
            {
                ParameterName = "returnValue",
                Direction = System.Data.ParameterDirection.Output,
                SqlDbType = System.Data.SqlDbType.Int,
            };

            var sqlParameters = new []
            {
                parameterreturnValue,
            };
            var _ = await _context.SqlQueryAsync<GetIbaUsersResult>("EXEC @returnValue = [dbo].[stp_v2_GetIbaUsers]", sqlParameters, cancellationToken);

            returnValue?.SetValue(parameterreturnValue.Value);

            return _;
        }

        public virtual async Task<GetModifiedDateResult> GetModifiedDateAsync(OutputParameter<int> returnValue = null, CancellationToken cancellationToken = default)
        {
            var parameterreturnValue = new SqlParameter
            {
                ParameterName = "returnValue",
                Direction = System.Data.ParameterDirection.Output,
                SqlDbType = System.Data.SqlDbType.Int,
            };

            var sqlParameters = new []
            {
                parameterreturnValue,
            };
            var _ = await _context.SqlQueryAsync<GetModifiedDateResult>("EXEC @returnValue = [dbo].[stp_v2_GetIbaUsersModifiedDate]", sqlParameters, cancellationToken);

            returnValue?.SetValue(parameterreturnValue.Value);

            return _.Single();
        }

        public virtual async Task<List<GetActiveDirectoryModifiedDateResult>> GetActiveDirectoryModifiedDateAsync(OutputParameter<int> returnValue = null, CancellationToken cancellationToken = default)
        {
            var parameterreturnValue = new SqlParameter
            {
                ParameterName = "returnValue",
                Direction = System.Data.ParameterDirection.Output,
                SqlDbType = System.Data.SqlDbType.Int,
            };

            var sqlParameters = new[]
            {
                parameterreturnValue,
            };
            var _ = await _context.SqlQueryAsync<GetActiveDirectoryModifiedDateResult>("EXEC @returnValue = [dbo].[stp_v2_GetActiveDirectoryModifiedDate]", sqlParameters, cancellationToken);

            returnValue?.SetValue(parameterreturnValue.Value);

            return _;
        }

    }
}
