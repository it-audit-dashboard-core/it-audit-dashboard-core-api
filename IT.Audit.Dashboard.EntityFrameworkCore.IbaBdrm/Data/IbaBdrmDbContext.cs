﻿using Microsoft.EntityFrameworkCore;

#nullable disable

namespace IT.Audit.Dashboard.EntityFrameworkCore.IbaBdrm
{
    public partial class IbaBdrmDbContext : DbContext
    {
        public IbaBdrmDbContext(DbContextOptions<IbaBdrmDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<IbaUser> IbaUsers { get; set; }
        //public virtual DbSet<TBdrmScheduleJobLog> TBdrmScheduleJobLogs { get; set; }
        //public virtual DbSet<TIbaBackupJobLog> TIbaBackupJobLogs { get; set; }
        //public virtual DbSet<TIbaBackupLogFileDatum> TIbaBackupLogFileData { get; set; }
        //public virtual DbSet<TIbaBackupStatusLog> TIbaBackupStatusLogs { get; set; }
        //public virtual DbSet<TIbaExportJobLog> TIbaExportJobLogs { get; set; }
        //public virtual DbSet<TIbaFxXmlConsoleJobLog> TIbaFxXmlConsoleJobLogs { get; set; }
        //public virtual DbSet<TIbaScheduleErrorHistoryLog> TIbaScheduleErrorHistoryLogs { get; set; }
        //public virtual DbSet<TIbaScheduleJobLog> TIbaScheduleJobLogs { get; set; }
        //public virtual DbSet<TIbaUsersAccountReport> TIbaUsersAccountReports { get; set; }
        //public virtual DbSet<TIbaUsersAccountReportRecordCount> TIbaUsersAccountReportRecordCounts { get; set; }
        //public virtual DbSet<TIbaUsersRecordCount> TIbaUsersRecordCounts { get; set; }
        //public virtual DbSet<VwBackupLog> VwBackupLogs { get; set; }
        //public virtual DbSet<VwExport> VwExports { get; set; }
        //public virtual DbSet<VwIbaActiveUser> VwIbaActiveUsers { get; set; }
        //public virtual DbSet<VwIbaDuplicateUser> VwIbaDuplicateUsers { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "SQL_Latin1_General_CP1_CI_AS");

            modelBuilder.ApplyConfiguration(new Configurations.IbaUserConfiguration());
            //modelBuilder.ApplyConfiguration(new Configurations.TBdrmScheduleJobLogConfiguration());
            //modelBuilder.ApplyConfiguration(new Configurations.TIbaBackupJobLogConfiguration());
            //modelBuilder.ApplyConfiguration(new Configurations.TIbaBackupLogFileDatumConfiguration());
            //modelBuilder.ApplyConfiguration(new Configurations.TIbaBackupStatusLogConfiguration());
            //modelBuilder.ApplyConfiguration(new Configurations.TIbaExportJobLogConfiguration());
            //modelBuilder.ApplyConfiguration(new Configurations.TIbaFxXmlConsoleJobLogConfiguration());
            //modelBuilder.ApplyConfiguration(new Configurations.TIbaScheduleErrorHistoryLogConfiguration());
            //modelBuilder.ApplyConfiguration(new Configurations.TIbaScheduleJobLogConfiguration());
            //modelBuilder.ApplyConfiguration(new Configurations.TIbaUsersAccountReportConfiguration());
            //modelBuilder.ApplyConfiguration(new Configurations.VwBackupLogConfiguration());
            //modelBuilder.ApplyConfiguration(new Configurations.VwExportConfiguration());
            //modelBuilder.ApplyConfiguration(new Configurations.VwIbaActiveUserConfiguration());
            //modelBuilder.ApplyConfiguration(new Configurations.VwIbaDuplicateUserConfiguration());
            
            //OnModelCreatingGeneratedFunctions(modelBuilder);
            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
