﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using IT.Audit.Dashboard.EntityFrameworkCore.IbaBdrm.Staging;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore;
using System;


namespace IT.Audit.Dashboard.EntityFrameworkCore.IbaBdrm.Staging.Configurations
{
    public partial class TIbaFxXmlConsoleJobLogConfiguration : IEntityTypeConfiguration<TIbaFxXmlConsoleJobLog>
    {
        public void Configure(EntityTypeBuilder<TIbaFxXmlConsoleJobLog> entity)
        {
            entity.Property(e => e.WatcherName).IsUnicode(false);

            OnConfigurePartial(entity);
        }

        partial void OnConfigurePartial(EntityTypeBuilder<TIbaFxXmlConsoleJobLog> entity);
    }
}
