﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;


namespace IT.Audit.Dashboard.EntityFrameworkCore.IbaBdrm.Configurations
{
    public partial class IbaUserConfiguration : IEntityTypeConfiguration<IbaUser>
    {
        public void Configure(EntityTypeBuilder<IbaUser> entity)
        {
            entity.Property(e => e.CreatedRef).IsUnicode(false);

            entity.Property(e => e.CreatedUser).IsUnicode(false);

            entity.Property(e => e.Disabled).IsUnicode(false);

            entity.Property(e => e.Instance).IsUnicode(false);

            entity.Property(e => e.IsActive).IsUnicode(false);

            entity.Property(e => e.LastLoginTime).IsUnicode(false);

            entity.Property(e => e.Login).IsUnicode(false);

            entity.Property(e => e.Name).IsUnicode(false);

            entity.Property(e => e.Role).IsUnicode(false);

            entity.Property(e => e.Security).IsUnicode(false);

            entity.Property(e => e.ServiceDeskRef).IsUnicode(false);

            //entity.Property(e => e.User).IsUnicode(false);

            OnConfigurePartial(entity);
        }

        partial void OnConfigurePartial(EntityTypeBuilder<IbaUser> entity);
    }
}
