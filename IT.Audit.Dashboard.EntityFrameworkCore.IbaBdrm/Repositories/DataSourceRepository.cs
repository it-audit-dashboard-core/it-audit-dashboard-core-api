﻿using IT.Audit.Dashboard.Application.Dtos;
using IT.Audit.Dashboard.EntityFrameworkCore.Domain;
using System.Threading.Tasks;

namespace IT.Audit.Dashboard.EntityFrameworkCore.IbaBdrm.Repositories
{
    public class DataSourceRepository : GenericRepositoryIbaBdrm<DataSource>, IDataSourceRepository
    {
        public DataSourceRepository(IbaBdrmDbContext context) : base(context)
        {
        }

        public Task<DataSourceOutputDto> GetModifiedDate()
        {
            throw new System.NotImplementedException();
        }
    }
}
