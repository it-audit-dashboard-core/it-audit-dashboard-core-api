﻿using IT.Audit.Dashboard.EntityFrameworkCore.IbaBdrm.Domain;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IT.Audit.Dashboard.EntityFrameworkCore.IbaBdrm.Repositories
{
    public class IbaUserRepository : GenericRepositoryIbaBdrm<IbaUser>, IIbaUserRepository
    {
        public IbaUserRepository(IbaBdrmDbContext context) : base(context)
        {

        }

        public async Task<List<GetIbaUsersResult>> GetAsync()
        {
            var procedures = new IbaBdrmDbContextProcedures(DbContext);

            var outOverallCount = new OutputParameter<int>();
            return await procedures.GetIbaUsersAsync(outOverallCount);
        }
    }
}
