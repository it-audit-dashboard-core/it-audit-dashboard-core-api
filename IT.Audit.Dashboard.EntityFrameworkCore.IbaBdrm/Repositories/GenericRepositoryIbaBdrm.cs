﻿using IT.Audit.Dashboard.Domain.Shared;
using IT.Audit.Dashboard.EntityFrameworkCore.Domain;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace IT.Audit.Dashboard.EntityFrameworkCore.IbaBdrm.Repositories
{
    public class GenericRepositoryIbaBdrm<TEntity> : IGenericRepository<TEntity>
        where TEntity : class, IEntity
    {
        protected readonly IbaBdrmDbContext DbContext;

        public GenericRepositoryIbaBdrm(IbaBdrmDbContext dbContext)
        {
            DbContext = dbContext;
        }

        public IQueryable<TEntity> GetAll()
        {
            return DbContext.Set<TEntity>().AsNoTracking();
        }

        public async Task<TEntity> GetById(int id)
        {
            return await DbContext.Set<TEntity>()
                .AsNoTracking()
                .FirstOrDefaultAsync(e => e.Id == id);
        }

        public async Task<int> Create(TEntity entity)
        {
            await DbContext.Set<TEntity>().AddAsync(entity);
            return await DbContext.SaveChangesAsync();
        }

        public async Task Update(int id, TEntity entity)
        {
            DbContext.Set<TEntity>().Update(entity);
            await DbContext.SaveChangesAsync();
        }

        public async Task Delete(int id)
        {
            var entity = await DbContext.Set<TEntity>().FindAsync(id);
            DbContext.Set<TEntity>().Remove(entity);
            await DbContext.SaveChangesAsync();
        }
    }
}
