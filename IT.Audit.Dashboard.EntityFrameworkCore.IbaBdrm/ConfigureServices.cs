﻿using IT.Audit.Dashboard.EntityFrameworkCore.Domain;
using IT.Audit.Dashboard.EntityFrameworkCore.IbaBdrm.Repositories;
using Microsoft.Extensions.DependencyInjection;

namespace IT.Audit.Dashboard.EntityFrameworkCore.IbaBdrm
{
    public static class ConfigureServices
    {
        public static IServiceCollection AddIbaBdrmRepositoryServices(this IServiceCollection services)
        {
            services.AddTransient<IIbaUserRepository, IbaUserRepository>();
            services.AddTransient<IDataSourceRepository, DataSourceRepository>();

            return services;
        }
    }
}
