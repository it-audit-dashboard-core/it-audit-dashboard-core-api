﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using IT.Audit.Dashboard.EntityFrameworkCore.Cdl.Data;
using IT.Audit.Dashboard.EntityFrameworkCore.Cdl.Models;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore;
using System;

#nullable disable

namespace IT.Audit.Dashboard.EntityFrameworkCore.Cdl.Data.Configurations
{
    public partial class VwProgramExclusionConfiguration : IEntityTypeConfiguration<VwProgramExclusion>
    {
        public void Configure(EntityTypeBuilder<VwProgramExclusion> entity)
        {
            entity.ToView("vw_Program_Exclusions");

            entity.Property(e => e.Description).IsUnicode(false);

            entity.Property(e => e.ExclusionKey).IsUnicode(false);

            entity.Property(e => e.ShortReportDescription).IsUnicode(false);

            OnConfigurePartial(entity);
        }

        partial void OnConfigurePartial(EntityTypeBuilder<VwProgramExclusion> entity);
    }
}
