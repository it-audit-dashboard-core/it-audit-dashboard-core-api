﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using IT.Audit.Dashboard.EntityFrameworkCore.Cdl.Data;
using IT.Audit.Dashboard.EntityFrameworkCore.Cdl.Models;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore;
using System;

#nullable disable

namespace IT.Audit.Dashboard.EntityFrameworkCore.Cdl.Data.Configurations
{
    public partial class VwAdministratorsReportConfiguration : IEntityTypeConfiguration<VwAdministratorsReport>
    {
        public void Configure(EntityTypeBuilder<VwAdministratorsReport> entity)
        {
            entity.ToView("vw_Administrators_Report");

            entity.Property(e => e.Action).IsUnicode(false);

            entity.Property(e => e.ActionWorkOrderNumber).IsUnicode(false);

            entity.Property(e => e.ActiveDirectoryNetworkLoginStatus).IsUnicode(false);

            entity.Property(e => e.Approver).IsUnicode(false);

            entity.Property(e => e.Branch).IsUnicode(false);

            entity.Property(e => e.BusinessUnit).IsUnicode(false);

            entity.Property(e => e.CdlAccessExclusions).IsUnicode(false);

            entity.Property(e => e.CdlAccountActiveYN).IsUnicode(false);

            entity.Property(e => e.CdlAccountTemplateS).IsUnicode(false);

            entity.Property(e => e.CdlAdditionalAccess).IsUnicode(false);

            entity.Property(e => e.CdlHighRiskAccesses).IsUnicode(false);

            entity.Property(e => e.CdlUserName).IsUnicode(false);

            entity.Property(e => e.ExplanationNotes).IsUnicode(false);

            entity.Property(e => e.JobDescription).IsUnicode(false);

            entity.Property(e => e.Location).IsUnicode(false);

            entity.Property(e => e.Manager).IsUnicode(false);

            entity.Property(e => e.OpCode).IsUnicode(false);

            OnConfigurePartial(entity);
        }

        partial void OnConfigurePartial(EntityTypeBuilder<VwAdministratorsReport> entity);
    }
}
