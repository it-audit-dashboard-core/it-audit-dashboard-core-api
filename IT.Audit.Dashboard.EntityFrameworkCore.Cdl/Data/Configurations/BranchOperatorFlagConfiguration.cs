﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using IT.Audit.Dashboard.EntityFrameworkCore.Cdl.Data;
using IT.Audit.Dashboard.EntityFrameworkCore.Cdl.Models;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore;
using System;

#nullable disable

namespace IT.Audit.Dashboard.EntityFrameworkCore.Cdl.Data.Configurations
{
    public partial class BranchOperatorFlagConfiguration : IEntityTypeConfiguration<BranchOperatorFlag>
    {
        public void Configure(EntityTypeBuilder<BranchOperatorFlag> entity)
        {
            entity.Property(e => e.Branch).IsUnicode(false);

            entity.Property(e => e.Entity).IsUnicode(false);

            entity.Property(e => e.Mod001).IsUnicode(false);

            entity.Property(e => e.Mod002).IsUnicode(false);

            entity.Property(e => e.Mod003).IsUnicode(false);

            entity.Property(e => e.Mod004).IsUnicode(false);

            entity.Property(e => e.Mod005).IsUnicode(false);

            entity.Property(e => e.Mod006).IsUnicode(false);

            entity.Property(e => e.Mod007).IsUnicode(false);

            entity.Property(e => e.Mod008).IsUnicode(false);

            entity.Property(e => e.Mod009).IsUnicode(false);

            entity.Property(e => e.Mod010).IsUnicode(false);

            entity.Property(e => e.Mod011).IsUnicode(false);

            entity.Property(e => e.Mod012).IsUnicode(false);

            entity.Property(e => e.Mod013).IsUnicode(false);

            entity.Property(e => e.Mod014).IsUnicode(false);

            entity.Property(e => e.Mod015).IsUnicode(false);

            entity.Property(e => e.Mod016).IsUnicode(false);

            entity.Property(e => e.Mod017).IsUnicode(false);

            entity.Property(e => e.Mod018).IsUnicode(false);

            entity.Property(e => e.Mod019).IsUnicode(false);

            entity.Property(e => e.Mod020).IsUnicode(false);

            entity.Property(e => e.Mod021).IsUnicode(false);

            entity.Property(e => e.Mod022).IsUnicode(false);

            entity.Property(e => e.Mod023).IsUnicode(false);

            entity.Property(e => e.Mod024).IsUnicode(false);

            entity.Property(e => e.Mod025).IsUnicode(false);

            entity.Property(e => e.Mod026).IsUnicode(false);

            entity.Property(e => e.Mod027).IsUnicode(false);

            entity.Property(e => e.Mod028).IsUnicode(false);

            entity.Property(e => e.Mod029).IsUnicode(false);

            entity.Property(e => e.Mod030).IsUnicode(false);

            entity.Property(e => e.Mod031).IsUnicode(false);

            entity.Property(e => e.Mod032).IsUnicode(false);

            entity.Property(e => e.Mod033).IsUnicode(false);

            entity.Property(e => e.Mod034).IsUnicode(false);

            entity.Property(e => e.Mod035).IsUnicode(false);

            entity.Property(e => e.Mod036).IsUnicode(false);

            entity.Property(e => e.Mod037).IsUnicode(false);

            entity.Property(e => e.Mod038).IsUnicode(false);

            entity.Property(e => e.Mod039).IsUnicode(false);

            entity.Property(e => e.Mod040).IsUnicode(false);

            entity.Property(e => e.Mod041).IsUnicode(false);

            entity.Property(e => e.Mod042).IsUnicode(false);

            entity.Property(e => e.Mod043).IsUnicode(false);

            entity.Property(e => e.Mod044).IsUnicode(false);

            entity.Property(e => e.Mod045).IsUnicode(false);

            entity.Property(e => e.Mod046).IsUnicode(false);

            entity.Property(e => e.Mod047).IsUnicode(false);

            entity.Property(e => e.Mod048).IsUnicode(false);

            entity.Property(e => e.Mod049).IsUnicode(false);

            entity.Property(e => e.Mod050).IsUnicode(false);

            entity.Property(e => e.Mod051).IsUnicode(false);

            entity.Property(e => e.Mod052).IsUnicode(false);

            entity.Property(e => e.Mod053).IsUnicode(false);

            entity.Property(e => e.Mod054).IsUnicode(false);

            entity.Property(e => e.Mod055).IsUnicode(false);

            entity.Property(e => e.Mod056).IsUnicode(false);

            entity.Property(e => e.Mod057).IsUnicode(false);

            entity.Property(e => e.Mod058).IsUnicode(false);

            entity.Property(e => e.Mod059).IsUnicode(false);

            entity.Property(e => e.Mod060).IsUnicode(false);

            entity.Property(e => e.Mod061).IsUnicode(false);

            entity.Property(e => e.Mod062).IsUnicode(false);

            entity.Property(e => e.Mod063).IsUnicode(false);

            entity.Property(e => e.Mod064).IsUnicode(false);

            entity.Property(e => e.Mod065).IsUnicode(false);

            entity.Property(e => e.Mod066).IsUnicode(false);

            entity.Property(e => e.Mod067).IsUnicode(false);

            entity.Property(e => e.Mod068).IsUnicode(false);

            entity.Property(e => e.Mod069).IsUnicode(false);

            entity.Property(e => e.Mod070).IsUnicode(false);

            entity.Property(e => e.Mod071).IsUnicode(false);

            entity.Property(e => e.Mod072).IsUnicode(false);

            entity.Property(e => e.Mod073).IsUnicode(false);

            entity.Property(e => e.Mod074).IsUnicode(false);

            entity.Property(e => e.Mod075).IsUnicode(false);

            entity.Property(e => e.Mod076).IsUnicode(false);

            entity.Property(e => e.Mod077).IsUnicode(false);

            entity.Property(e => e.Mod078).IsUnicode(false);

            entity.Property(e => e.Mod079).IsUnicode(false);

            entity.Property(e => e.Mod080).IsUnicode(false);

            entity.Property(e => e.Mod081).IsUnicode(false);

            entity.Property(e => e.Mod082).IsUnicode(false);

            entity.Property(e => e.Mod083).IsUnicode(false);

            entity.Property(e => e.Mod084).IsUnicode(false);

            entity.Property(e => e.Mod085).IsUnicode(false);

            entity.Property(e => e.Mod086).IsUnicode(false);

            entity.Property(e => e.Mod087).IsUnicode(false);

            entity.Property(e => e.Mod088).IsUnicode(false);

            entity.Property(e => e.Mod089).IsUnicode(false);

            entity.Property(e => e.Mod090).IsUnicode(false);

            entity.Property(e => e.Mod091).IsUnicode(false);

            entity.Property(e => e.Mod092).IsUnicode(false);

            entity.Property(e => e.Mod093).IsUnicode(false);

            entity.Property(e => e.Mod094).IsUnicode(false);

            entity.Property(e => e.Mod095).IsUnicode(false);

            entity.Property(e => e.Mod096).IsUnicode(false);

            entity.Property(e => e.Mod097).IsUnicode(false);

            entity.Property(e => e.Mod098).IsUnicode(false);

            entity.Property(e => e.Mod099).IsUnicode(false);

            entity.Property(e => e.Mod100).IsUnicode(false);

            entity.Property(e => e.Mod101).IsUnicode(false);

            entity.Property(e => e.Mod102).IsUnicode(false);

            entity.Property(e => e.Mod103).IsUnicode(false);

            entity.Property(e => e.Mod104).IsUnicode(false);

            entity.Property(e => e.Mod105).IsUnicode(false);

            entity.Property(e => e.Mod106).IsUnicode(false);

            entity.Property(e => e.Mod107).IsUnicode(false);

            entity.Property(e => e.Mod108).IsUnicode(false);

            entity.Property(e => e.Mod109).IsUnicode(false);

            entity.Property(e => e.Mod110).IsUnicode(false);

            entity.Property(e => e.Mod111).IsUnicode(false);

            entity.Property(e => e.Mod112).IsUnicode(false);

            entity.Property(e => e.Mod113).IsUnicode(false);

            entity.Property(e => e.Mod114).IsUnicode(false);

            entity.Property(e => e.Mod115).IsUnicode(false);

            entity.Property(e => e.Mod116).IsUnicode(false);

            entity.Property(e => e.Mod117).IsUnicode(false);

            entity.Property(e => e.Mod118).IsUnicode(false);

            entity.Property(e => e.Mod119).IsUnicode(false);

            entity.Property(e => e.Mod120).IsUnicode(false);

            entity.Property(e => e.Mod121).IsUnicode(false);

            entity.Property(e => e.Mod122).IsUnicode(false);

            entity.Property(e => e.Mod123).IsUnicode(false);

            entity.Property(e => e.Mod124).IsUnicode(false);

            entity.Property(e => e.Mod125).IsUnicode(false);

            entity.Property(e => e.Mod126).IsUnicode(false);

            entity.Property(e => e.Mod127).IsUnicode(false);

            entity.Property(e => e.Mod128).IsUnicode(false);

            entity.Property(e => e.Mod129).IsUnicode(false);

            entity.Property(e => e.Mod130).IsUnicode(false);

            entity.Property(e => e.Mod131).IsUnicode(false);

            entity.Property(e => e.Mod132).IsUnicode(false);

            entity.Property(e => e.Mod133).IsUnicode(false);

            entity.Property(e => e.Mod134).IsUnicode(false);

            entity.Property(e => e.Mod135).IsUnicode(false);

            entity.Property(e => e.Mod136).IsUnicode(false);

            entity.Property(e => e.Mod137).IsUnicode(false);

            entity.Property(e => e.Mod138).IsUnicode(false);

            entity.Property(e => e.Mod139).IsUnicode(false);

            entity.Property(e => e.Mod140).IsUnicode(false);

            entity.Property(e => e.Mod141).IsUnicode(false);

            entity.Property(e => e.Mod142).IsUnicode(false);

            entity.Property(e => e.Mod143).IsUnicode(false);

            entity.Property(e => e.Mod144).IsUnicode(false);

            entity.Property(e => e.Mod145).IsUnicode(false);

            entity.Property(e => e.Mod146).IsUnicode(false);

            entity.Property(e => e.Mod147).IsUnicode(false);

            entity.Property(e => e.Mod148).IsUnicode(false);

            entity.Property(e => e.Mod149).IsUnicode(false);

            entity.Property(e => e.Opcode).IsFixedLength(true);

            OnConfigurePartial(entity);
        }

        partial void OnConfigurePartial(EntityTypeBuilder<BranchOperatorFlag> entity);
    }
}
