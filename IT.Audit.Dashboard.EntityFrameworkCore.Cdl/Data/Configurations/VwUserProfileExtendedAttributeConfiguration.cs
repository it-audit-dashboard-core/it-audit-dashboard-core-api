﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using IT.Audit.Dashboard.EntityFrameworkCore.Cdl.Data;
using IT.Audit.Dashboard.EntityFrameworkCore.Cdl.Models;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore;
using System;

#nullable disable

namespace IT.Audit.Dashboard.EntityFrameworkCore.Cdl.Data.Configurations
{
    public partial class VwUserProfileExtendedAttributeConfiguration : IEntityTypeConfiguration<VwUserProfileExtendedAttribute>
    {
        public void Configure(EntityTypeBuilder<VwUserProfileExtendedAttribute> entity)
        {
            entity.ToView("vw_User_Profile_Extended_Attributes");

            entity.Property(e => e.Branch).IsUnicode(false);

            entity.Property(e => e.Entity).IsUnicode(false);

            entity.Property(e => e.UserLogon).IsUnicode(false);

            OnConfigurePartial(entity);
        }

        partial void OnConfigurePartial(EntityTypeBuilder<VwUserProfileExtendedAttribute> entity);
    }
}
