﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using IT.Audit.Dashboard.EntityFrameworkCore.Cdl.Data;
using IT.Audit.Dashboard.EntityFrameworkCore.Cdl.Models;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore;
using System;

#nullable disable

namespace IT.Audit.Dashboard.EntityFrameworkCore.Cdl.Data.Configurations
{
    public partial class VwReportRequestHistoryConfiguration : IEntityTypeConfiguration<VwReportRequestHistory>
    {
        public void Configure(EntityTypeBuilder<VwReportRequestHistory> entity)
        {
            entity.ToView("vw_Report_Request_History");

            entity.Property(e => e.RalDescription).IsUnicode(false);

            entity.Property(e => e.ReportStatusDescription).IsUnicode(false);

            entity.Property(e => e.RequestedBy).IsUnicode(false);

            entity.Property(e => e.RequesterEmailAddress).IsUnicode(false);

            entity.Property(e => e.RequiredReviewerColumns).IsUnicode(false);

            OnConfigurePartial(entity);
        }

        partial void OnConfigurePartial(EntityTypeBuilder<VwReportRequestHistory> entity);
    }
}
