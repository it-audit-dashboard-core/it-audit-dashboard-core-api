﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using IT.Audit.Dashboard.EntityFrameworkCore.Cdl.Data;
using IT.Audit.Dashboard.EntityFrameworkCore.Cdl.Models;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore;
using System;

#nullable disable

namespace IT.Audit.Dashboard.EntityFrameworkCore.Cdl.Data.Configurations
{
    public partial class TTemplateProgramAccessConfiguration : IEntityTypeConfiguration<TTemplateProgramAccess>
    {
        public void Configure(EntityTypeBuilder<TTemplateProgramAccess> entity)
        {
            entity.HasIndex(e => new { e.Entity, e.Branch, e.TemplateId }, "IX_t_Template_Program_Access_Template")
                .HasFillFactor((byte)80);

            entity.Property(e => e.Access).IsUnicode(false);

            entity.Property(e => e.Active).IsUnicode(false);

            entity.Property(e => e.Branch).IsUnicode(false);

            entity.Property(e => e.Entity).IsUnicode(false);

            OnConfigurePartial(entity);
        }

        partial void OnConfigurePartial(EntityTypeBuilder<TTemplateProgramAccess> entity);
    }
}
