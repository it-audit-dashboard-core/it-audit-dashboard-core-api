﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using IT.Audit.Dashboard.EntityFrameworkCore.Cdl.Data;
using IT.Audit.Dashboard.EntityFrameworkCore.Cdl.Models;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore;
using System;

#nullable disable

namespace IT.Audit.Dashboard.EntityFrameworkCore.Cdl.Data.Configurations
{
    public partial class VwMismatchedOpnameConfiguration : IEntityTypeConfiguration<VwMismatchedOpname>
    {
        public void Configure(EntityTypeBuilder<VwMismatchedOpname> entity)
        {
            entity.ToView("vw_Mismatched_OPNAMES");

            entity.Property(e => e.AdAccountStatus).IsUnicode(false);

            entity.Property(e => e.AdDepartment).IsUnicode(false);

            entity.Property(e => e.AdDisplayName).IsUnicode(false);

            entity.Property(e => e.AdFirstName).IsUnicode(false);

            entity.Property(e => e.AdJobTitle).IsUnicode(false);

            entity.Property(e => e.AdLastName).IsUnicode(false);

            entity.Property(e => e.AdLocation).IsUnicode(false);

            entity.Property(e => e.AdUserName).IsUnicode(false);

            entity.Property(e => e.Branch).IsUnicode(false);

            entity.Property(e => e.BranchName).IsUnicode(false);

            entity.Property(e => e.CdlFirstName).IsUnicode(false);

            entity.Property(e => e.CdlLastName).IsUnicode(false);

            entity.Property(e => e.Entity).IsUnicode(false);

            entity.Property(e => e.Manager).IsUnicode(false);

            entity.Property(e => e.Opcode).IsUnicode(false);

            entity.Property(e => e.Opdesc).IsUnicode(false);

            entity.Property(e => e.Opname).IsUnicode(false);

            OnConfigurePartial(entity);
        }

        partial void OnConfigurePartial(EntityTypeBuilder<VwMismatchedOpname> entity);
    }
}
