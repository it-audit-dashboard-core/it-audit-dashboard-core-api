﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using IT.Audit.Dashboard.EntityFrameworkCore.Cdl.Data;
using IT.Audit.Dashboard.EntityFrameworkCore.Cdl.Models;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore;
using System;

#nullable disable

namespace IT.Audit.Dashboard.EntityFrameworkCore.Cdl.Data.Configurations
{
    public partial class TReportRequestUarDepartmentToLocationConfiguration : IEntityTypeConfiguration<TReportRequestUarDepartmentToLocation>
    {
        public void Configure(EntityTypeBuilder<TReportRequestUarDepartmentToLocation> entity)
        {
            entity.Property(e => e.AdDepartment).IsUnicode(false);

            entity.Property(e => e.ShortLocation).IsUnicode(false);

            entity.Property(e => e.UpdatedBy).IsUnicode(false);

            OnConfigurePartial(entity);
        }

        partial void OnConfigurePartial(EntityTypeBuilder<TReportRequestUarDepartmentToLocation> entity);
    }
}
