﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using IT.Audit.Dashboard.EntityFrameworkCore.Cdl.Data;
using IT.Audit.Dashboard.EntityFrameworkCore.Cdl.Models;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore;
using System;

#nullable disable

namespace IT.Audit.Dashboard.EntityFrameworkCore.Cdl.Data.Configurations
{
    public partial class VwCdlAccountConfiguration : IEntityTypeConfiguration<VwCdlAccount>
    {
        public void Configure(EntityTypeBuilder<VwCdlAccount> entity)
        {
            entity.ToView("vw_CDL_Accounts");

            entity.Property(e => e.AccountType).IsUnicode(false);

            entity.Property(e => e.AdLogin).IsUnicode(false);

            entity.Property(e => e.Branch).IsUnicode(false);

            entity.Property(e => e.Entity).IsUnicode(false);

            entity.Property(e => e.Opcode).IsUnicode(false);

            entity.Property(e => e.SubAccountType).IsUnicode(false);

            OnConfigurePartial(entity);
        }

        partial void OnConfigurePartial(EntityTypeBuilder<VwCdlAccount> entity);
    }
}
