﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable enable

namespace IT.Audit.Dashboard.EntityFrameworkCore.Cdl.Models
{
    [Table("t_Report_Request_Types")]
    public partial class TReportRequestType
    {
        [Key]
        [Column("ID")]
        public int Id { get; set; }
        [Required]
        [StringLength(50)]
        public string ReportStatusDescription { get; set; } = default!;
    }
}