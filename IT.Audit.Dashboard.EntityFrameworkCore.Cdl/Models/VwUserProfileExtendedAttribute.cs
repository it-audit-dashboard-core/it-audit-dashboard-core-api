﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable enable

namespace IT.Audit.Dashboard.EntityFrameworkCore.Cdl.Models
{
    [Keyless]
    public partial class VwUserProfileExtendedAttribute
    {
        [Required]
        [StringLength(2)]
        public string Entity { get; set; } = default!;
        [Required]
        [StringLength(1)]
        public string Branch { get; set; } = default!;
        [Required]
        [StringLength(2)]
        public string UserLogon { get; set; } = default!;
        public int Program { get; set; }
    }
}