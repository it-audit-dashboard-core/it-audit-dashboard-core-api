﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable enable

namespace IT.Audit.Dashboard.EntityFrameworkCore.Cdl.Models
{
    [Keyless]
    public partial class VwGenericAccountsReport
    {
        [Required]
        [StringLength(50)]
        public string Branch { get; set; } = default!;
        [Column("BranchID")]
        public int BranchId { get; set; }
        [Required]
        [Column("CDL Generic Account Name")]
        [StringLength(255)]
        public string CdlGenericAccountName { get; set; } = default!;
        [StringLength(255)]
        public string? Owner { get; set; }
        [Column("Description and Purpose")]
        [StringLength(255)]
        public string? DescriptionAndPurpose { get; set; }
        [Required]
        [StringLength(2)]
        public string OpCode { get; set; } = default!;
        [Required]
        [Column("ACTIVE")]
        [StringLength(1)]
        public string Active { get; set; } = default!;
        [StringLength(1)]
        public string? AdministratorP96 { get; set; }
        [Column("CDL Account Template(s)")]
        [StringLength(8000)]
        public string? CdlAccountTemplateS { get; set; }
        [Required]
        [Column("CDL Additional Access")]
        [StringLength(8000)]
        public string CdlAdditionalAccess { get; set; } = default!;
        [Required]
        [Column("CDL High Risk Accesses")]
        [StringLength(255)]
        public string CdlHighRiskAccesses { get; set; } = default!;
        [Required]
        [Column("CDL Access Exclusions")]
        [StringLength(8000)]
        public string CdlAccessExclusions { get; set; } = default!;
        [StringLength(255)]
        public string? Approver { get; set; }
    }
}