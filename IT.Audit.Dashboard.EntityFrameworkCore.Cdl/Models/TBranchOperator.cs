﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable enable

namespace IT.Audit.Dashboard.EntityFrameworkCore.Cdl.Models
{
    [Table("t_Branch_Operators")]
    public partial class TBranchOperator
    {
        [Key]
        [Column("ID")]
        public int Id { get; set; }
        [Required]
        [StringLength(2)]
        public string Entity { get; set; } = default!;
        [Required]
        [StringLength(1)]
        public string Branch { get; set; } = default!;
        [Column("Load_Date", TypeName = "datetime")]
        public DateTime LoadDate { get; set; }
        [Column("OPTYPE")]
        public short? Optype { get; set; }
        [Required]
        [Column("OPCODE")]
        [StringLength(254)]
        public string Opcode { get; set; } = default!;
        [Column("OPID")]
        public short? Opid { get; set; }
        [Required]
        [Column("OPNAME")]
        [StringLength(254)]
        public string Opname { get; set; } = default!;
        [Column("OPDESC")]
        [StringLength(254)]
        public string? Opdesc { get; set; }
        [Required]
        [Column("RFA")]
        [StringLength(14)]
        public string Rfa { get; set; } = default!;
        [Column("ACTIVE")]
        [StringLength(254)]
        public string? Active { get; set; }
    }
}