﻿using IT.Audit.Dashboard.EntityFrameworkCore.ItAuditDashboard.Domain;

namespace IT.Audit.Dashboard.EntityFrameworkCore.ItAuditDashboard
{
    public class UserApplicationGroupData
    {
        /*
            <add key="DashboardUser" value="Domain Users" />
            <add key="AD_Screen" value="G-EMEA-ITAuditDashboard-ActiveDirectoryUsersView-UAT" />
            <add key="Acturis_Screen" value="G-EMEA-ITAuditDashboard-ActurisUsersView-UAT" />
            <add key="Acturis_Editing" value="G-EMEA-ITAuditDashboard-ActurisUsersUpdate-UAT" />
            <add key="BDRM_Screen" value="G-EMEA-ITAuditDashboard-BDRMUsersView-UAT" />
            <add key="IBA_Screen" value="G-EMEA-ITAuditDashboard-IBABrokerAccountingUsersView-UAT" />
            <add key="User_Admin_Screen" value="G-EMEA-ITAuditDashboard-ITAuditDashboardUsersView-UAT" />
            <add key="CDL_Screen" value="G-EMEA-ITAuditDashboard-CDLUsersView-UAT" />
            <add key="CDL_Editing" value="G-EMEA-ITAuditDashboard-CDLUsersUpdate-UAT" />
            <add key="CDL_UserAccessList" value="G-EMEA-ITAuditDashboard-CDLUserAccessList-UAT" />
            <add key="CDL_ReferenceDataUpdate" value="G-EMEA-ITAuditDashboard-CDLReferenceDataUpdate-UAT" />
            <add key="AD_Group_Membership_Enquiry" value="G-EMEA-ITAuditDashboard-ADGroupMembershipEnquiry-Prod" />
            <add key="Generic_UAR_Application_Configure" value="G-EMEA-ITAuditDashboard-GenericUserAccessReviewConfigure-UAT" />
            <add key="Generic_Table_Editor_Configure" value="U-EMEA-ITAuditDashboard-GRefDataConfigure-UAT" />
            <add key="IDM_Screen" value="U-EMEA-ITAuditDashboard-IDMRequestsGGBUKView-UAT" />
         */
        public static readonly UserApplicationGroup[] UserApplicationGroups = new[]
        {
            new UserApplicationGroup(1, "DashboardUser", "Domain Users"),
            new UserApplicationGroup(2, "AD_Screen", "G-EMEA-ITAuditDashboard-ActiveDirectoryUsersView-UAT"),
            new UserApplicationGroup(3, "Acturis_Screen", "G-EMEA-ITAuditDashboard-ActurisUsersView-UAT"),
            new UserApplicationGroup(4, "Acturis_Editing", "G-EMEA-ITAuditDashboard-ActurisUsersUpdate-UAT"),
            new UserApplicationGroup(5, "BDRM_Screen", "G-EMEA-ITAuditDashboard-BDRMUsersView-UAT"),
            new UserApplicationGroup(6, "IBA_Screen", "G-EMEA-ITAuditDashboard-IBABrokerAccountingUsersView-UAT"),
            new UserApplicationGroup(7, "User_Admin_Screen", "G-EMEA-ITAuditDashboard-ITAuditDashboardUsersView-UAT"),
            new UserApplicationGroup(8, "CDL_Screen", "G-EMEA-ITAuditDashboard-CDLUsersView-UAT"),
            new UserApplicationGroup(9, "CDL_Editing", "G-EMEA-ITAuditDashboard-CDLUsersUpdate-UAT"),
            new UserApplicationGroup(10, "CDL_UserAccessList", "G-EMEA-ITAuditDashboard-CDLUserAccessList-UAT"),
            new UserApplicationGroup(11, "CDL_ReferenceDataUpdate", "G-EMEA-ITAuditDashboard-CDLReferenceDataUpdate-UAT"),
            new UserApplicationGroup(12, "AD_Group_Membership_Enquiry", "G-EMEA-ITAuditDashboard-ADGroupMembershipEnquiry-Prod"),
            new UserApplicationGroup(13, "Generic_UAR_Application_Configure", "U-EMEA-ITAuditDashboard-GRefDataConfigure-UAT"),
            new UserApplicationGroup(14, "Generic_Table_Editor_Configure", "U-EMEA-ITAuditDashboard-IDMRequestsGGBUKView-UAT"),
        };
    }
}
