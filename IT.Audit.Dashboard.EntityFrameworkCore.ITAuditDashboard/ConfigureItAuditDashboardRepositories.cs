﻿using IT.Audit.Dashboard.EntityFrameworkCore.ItAuditDashboard.Domain;
using IT.Audit.Dashboard.EntityFrameworkCore.ItAuditDashboard.Repositories;
using Microsoft.Extensions.DependencyInjection;

namespace IT.Audit.Dashboard.EntityFrameworkCore.ItAuditDashboard
{
    public static class ConfigureItAuditDashboardRepositories
    {
        public static IServiceCollection AddItAuditDashboardRepositoryServices(this IServiceCollection services)
        {
            //services.AddTransient<IDataInformationDomainManager, DataInformationDomainManager>();

            services.AddTransient<IPageSettingsRepository, PageSettingsRepository>();
            services.AddTransient<IUserApplicationRepository, UserApplicationRepository>();
            services.AddTransient<IUserApplicationGroupsRepository, UserApplicationGroupsRepository>();

            return services;
        }
    }
}
