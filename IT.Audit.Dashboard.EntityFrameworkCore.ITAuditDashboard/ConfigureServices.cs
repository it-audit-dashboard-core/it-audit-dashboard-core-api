﻿using IT.Audit.Dashboard.EntityFrameworkCore.ItAuditDashboard.Domain;
using IT.Audit.Dashboard.EntityFrameworkCore.ItAuditDashboard.Repositories;
using Microsoft.Extensions.DependencyInjection;

namespace IT.Audit.Dashboard.EntityFrameworkCore.ItAuditDashboard
{
    public static class ConfigureServices
    {
        public static IServiceCollection AddItAuditDashboardRepositoryServices(this IServiceCollection services)
        {
            services.AddTransient<IDashboardRepository, DashboardRepository>();
            services.AddTransient<IUserApplicationRepository, UserApplicationRepository>();
            services.AddTransient<IUserApplicationGroupsRepository, UserApplicationGroupsRepository>();
            services.AddTransient<IPageSettingsRepository, PageSettingsRepository>();
            services.AddTransient<ILoggingRepository, LoggingRepository>();

            return services;
        }
    }
}
