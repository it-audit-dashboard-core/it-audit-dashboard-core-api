﻿using IT.Audit.Dashboard.EntityFrameworkCore.ItAuditDashboard.Data;
using IT.Audit.Dashboard.EntityFrameworkCore.ItAuditDashboard.Domain;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IT.Audit.Dashboard.EntityFrameworkCore.ItAuditDashboard.Repositories
{
    public class UserApplicationGroupsRepository : GenericRepositoryITAuditDashboard<UserApplicationGroup>, IUserApplicationGroupsRepository
    {
        public UserApplicationGroupsRepository(ITAuditDashboardDbContext context) : base(context) { }


        public async Task<List<UserApplicationGroup>> GetListAsync()
            => await Task.FromResult(UserApplicationGroupData.UserApplicationGroups.ToList());

        public async Task<UserApplicationGroup> GetByKeyAsync(string key)
            => await Task.FromResult(UserApplicationGroupData.UserApplicationGroups.FirstOrDefault(x => x.Key == key));
    }
}
