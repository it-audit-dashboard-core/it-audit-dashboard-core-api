﻿using IT.Audit.Dashboard.EntityFrameworkCore.ItAuditDashboard.Data;
using System;
using System.Threading.Tasks;

namespace IT.Audit.Dashboard.EntityFrameworkCore.ItAuditDashboard.Repositories
{
    public class LoggingRepository : GenericRepositoryITAuditDashboard<WebSiteErrorLog>, ILoggingRepository
    {
        public LoggingRepository(ITAuditDashboardDbContext context) : base(context)
        {
        }

        public async Task<int> ClearErrors()
        {
            var procedures = new ITAuditDashboardDbContextProcedures(DbContext);

            var outOverallCount = new OutputParameter<int>();
            return await procedures.ClearRecentWebsiteLogsAsync(outOverallCount);
        }

        public async Task<int> LogError(string user, string controller, string action, string messageText)
        {
            var procedures = new ITAuditDashboardDbContextProcedures(DbContext);

            var outOverallCount = new OutputParameter<int>();
            return await procedures.LogErrorsAsync(DateTime.Now, user, Environment.MachineName, controller, action, messageText, outOverallCount);
        }
    }
}
