﻿using IT.Audit.Dashboard.Domain.Shared;
using IT.Audit.Dashboard.EntityFrameworkCore.ItAuditDashboard.Domain;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IT.Audit.Dashboard.EntityFrameworkCore.ItAuditDashboard.Repositories
{
    public class PageSettingsRepository : IPageSettingsRepository
    {
        public Task<List<PageSetting>> GetListAsync(int connectionTypeId)
        {
            return Task.FromResult(new List<PageSetting>
            {
                new PageSetting { Name = "IBA",  ConnectionType = (ConnectionType)connectionTypeId, TableName = "dbo.t_IBA_Users", CacheKey = "IBA_MODIFIED_DATE" },
                new PageSetting { Name = "AD", ConnectionType = (ConnectionType)connectionTypeId, TableName = "(SELECT MAX([AD_Last_Update]) Load_Date FROM[dbo].[vw_IBA_Active_Users]) AS Load_Date", CacheKey = "AD_MODIFIED_DATE" }
            });
        }
    }
}
