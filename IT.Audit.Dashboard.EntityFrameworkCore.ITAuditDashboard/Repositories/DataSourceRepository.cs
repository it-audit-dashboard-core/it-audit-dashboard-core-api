﻿using System;
using System.Threading.Tasks;
using IT.Audit.Dashboard.EntityFrameworkCore.Domain;

namespace IT.Audit.Dashboard.EntityFrameworkCore.ItAuditDashboard.Repositories
{
    public class DataSourceRepository : GenericRepositoryITAuditDashboard<DataSource>, IDataSourceRepository
    {
        public DataSourceRepository(ITAuditDashboardDbContext context) : base(context)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="connectionType"></param>
        /// <param name="tableName"></param>
        /// <returns></returns>
        public async Task<DateTime> GetModifiedDate(string tableName)
        {
            //using (SqlConnection sqlConnection = new SqlConnection(""))
            //{
            //    sqlConnection.Open();

            //    //SqlCommand sqlCommand = sqlConnection.CreateCommand();
            //    //sqlCommand.CommandType = CommandType.Text;
            //    //sqlCommand.CommandText = @$"SELECT MAX(Load_Date) FROM {tableName}";

            //    //var loadDate = await sqlCommand.ExecuteScalarAsync();
            //    //sqlConnection.Close();

            //    return loadDate == null ? DateTime.MinValue : DateTime.Parse(loadDate.ToString());
            //}
            //var procedures = new ITAuditDashboardDbContextProcedures(DbContext);

            //var outOverallCount = new OutputParameter<int>();
            //return await procedures.stp_v2(tableName, outOverallCount);
            throw new NotImplementedException(nameof(GetModifiedDate));
        }
    }
}
