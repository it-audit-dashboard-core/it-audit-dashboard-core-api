﻿using IT.Audit.Dashboard.EntityFrameworkCore.ItAuditDashboard.Data;
using IT.Audit.Dashboard.EntityFrameworkCore.ItAuditDashboard.Domain;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IT.Audit.Dashboard.EntityFrameworkCore.ItAuditDashboard.Repositories
{
    public class UserApplicationRepository : GenericRepositoryITAuditDashboard<UserApplication>, IUserApplicationRepository
    {
        public UserApplicationRepository(ITAuditDashboardDbContext context) : base(context)
        {
        }

        public async Task<List<UserApplication>> GetListAsync()
            => await Task.FromResult(UserApplicationData.UserApplications.ToList());

        public async Task<UserApplication> GetByKeyAsync(string key)
            => await Task.FromResult(UserApplicationData.UserApplications.FirstOrDefault(x=>x.Key == key));
    }
}
