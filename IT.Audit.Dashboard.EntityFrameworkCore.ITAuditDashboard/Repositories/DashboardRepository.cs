﻿using IT.Audit.Dashboard.EntityFrameworkCore.ItAuditDashboard.Data;
using IT.Audit.Dashboard.EntityFrameworkCore.ItAuditDashboard.Domain;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IT.Audit.Dashboard.EntityFrameworkCore.ItAuditDashboard.Repositories
{
    public class DashboardRepository : GenericRepositoryITAuditDashboard<DashboardUser>, IDashboardRepository
    {
        public DashboardRepository(ITAuditDashboardDbContext context) : base(context) { }

        public Task AddAuthorisedUsersAsync(
            string userName, 
            string displayName, 
            string email, 
            List<UserApplicationGroup> userApplicationGroups)
        {
            throw new System.NotImplementedException();
        }

        public Task DeleteUsersAsync()
        {
            throw new System.NotImplementedException();
        }

        public async Task<List<GetUsersByUserNameResult>> GetListAsync(
            string userName)
        {
            var procedures = new ITAuditDashboardDbContextProcedures(DbContext);

            var outOverallCount = new OutputParameter<int>();
            return await procedures.GetUsersByUserNameAsync(userName, outOverallCount);
        }
    }
}
