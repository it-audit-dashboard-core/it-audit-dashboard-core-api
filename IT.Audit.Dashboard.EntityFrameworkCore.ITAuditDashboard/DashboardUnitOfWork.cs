﻿using IT.Audit.Dashboard.EntityFrameworkCore.ItAuditDashboard.Data;
using IT.Audit.Dashboard.EntityFrameworkCore.ItAuditDashboard.Domain;
using IT.Audit.Dashboard.EntityFrameworkCore.ItAuditDashboard.Repositories;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace IT.Audit.Dashboard.EntityFrameworkCore.ItAuditDashboard
{
    public class DashboardUnitOfWork : IDashboardUnitOfWork, IDisposable
    {
        private readonly ITAuditDashboardDbContext _context;
        private readonly ILogger _logger;

        public DashboardUnitOfWork(ITAuditDashboardDbContext context, ILoggerFactory loggerFactory)
        {
            _context = context;
            _logger = loggerFactory.CreateLogger("logs");

            Dashboard = new DashboardRepository(context);
            UserApplicationGroups = new UserApplicationGroupsRepository(_context);
        }

        public IDashboardRepository Dashboard { get; private set; }

        public IUserApplicationGroupsRepository UserApplicationGroups { get; private set; }

        public async Task CompleteAsync()
        {
            await _context.SaveChangesAsync();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
