﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using IT.Audit.Dashboard.EntityFrameworkCore.ItAuditDashboard.Models;
using IT.Audit.Dashboard.EntityFrameworkCore.ItAuditDashboard.Domain;

namespace IT.Audit.Dashboard.EntityFrameworkCore.ItAuditDashboard.Data
{
    public partial class ITAuditDashboardDbContext
    {
        private ITAuditDashboardDbContextProcedures _procedures;

        public ITAuditDashboardDbContextProcedures Procedures
        {
            get
            {
                if (_procedures is null) _procedures = new ITAuditDashboardDbContextProcedures(this);
                return _procedures;
            }
            set
            {
                _procedures = value;
            }
        }

        public ITAuditDashboardDbContextProcedures GetProcedures()
        {
            return Procedures;
        }
    }

    public partial class ITAuditDashboardDbContextProcedures
    {
        private readonly ITAuditDashboardDbContext _context;

        public ITAuditDashboardDbContextProcedures(ITAuditDashboardDbContext context)
        {
            _context = context;
        }

        //public virtual async Task<List<stp_check_ADSI_PathResult>> stp_check_ADSI_PathAsync(string Domain, string Address, OutputParameter<bool?> Result, OutputParameter<int> returnValue = null, CancellationToken cancellationToken = default)
        //{
        //    var parameterResult = new SqlParameter
        //    {
        //        ParameterName = "Result",
        //        Direction = System.Data.ParameterDirection.Output,
        //        SqlDbType = System.Data.SqlDbType.Bit,
        //    };
        //    var parameterreturnValue = new SqlParameter
        //    {
        //        ParameterName = "returnValue",
        //        Direction = System.Data.ParameterDirection.Output,
        //        SqlDbType = System.Data.SqlDbType.Int,
        //    };

        //    var sqlParameters = new []
        //    {
        //        new SqlParameter
        //        {
        //            ParameterName = "Domain",
        //            Size = -1,
        //            Value = Domain ?? Convert.DBNull,
        //            SqlDbType = System.Data.SqlDbType.VarChar,
        //        },
        //        new SqlParameter
        //        {
        //            ParameterName = "Address",
        //            Size = -1,
        //            Value = Address ?? Convert.DBNull,
        //            SqlDbType = System.Data.SqlDbType.VarChar,
        //        },
        //        parameterResult,
        //        parameterreturnValue,
        //    };
        //    var _ = await _context.SqlQueryAsync<stp_check_ADSI_PathResult>("EXEC @returnValue = [dbo].[stp_check_ADSI_Path] @Domain, @Address, @Result OUTPUT", sqlParameters, cancellationToken);

        //    Result.SetValue(parameterResult.Value);
        //    returnValue?.SetValue(parameterreturnValue.Value);

        //    return _;
        //}

        //public virtual async Task<List<stp_create_LDAP_Address_From_PathResult>> stp_create_LDAP_Address_From_PathAsync(string Path, OutputParameter<string> Address, OutputParameter<string> Domain, OutputParameter<int> returnValue = null, CancellationToken cancellationToken = default)
        //{
        //    var parameterAddress = new SqlParameter
        //    {
        //        ParameterName = "Address",
        //        Size = -1,
        //        Direction = System.Data.ParameterDirection.Output,
        //        SqlDbType = System.Data.SqlDbType.VarChar,
        //    };
        //    var parameterDomain = new SqlParameter
        //    {
        //        ParameterName = "Domain",
        //        Size = -1,
        //        Direction = System.Data.ParameterDirection.Output,
        //        SqlDbType = System.Data.SqlDbType.VarChar,
        //    };
        //    var parameterreturnValue = new SqlParameter
        //    {
        //        ParameterName = "returnValue",
        //        Direction = System.Data.ParameterDirection.Output,
        //        SqlDbType = System.Data.SqlDbType.Int,
        //    };

        //    var sqlParameters = new []
        //    {
        //        new SqlParameter
        //        {
        //            ParameterName = "Path",
        //            Size = -1,
        //            Value = Path ?? Convert.DBNull,
        //            SqlDbType = System.Data.SqlDbType.VarChar,
        //        },
        //        parameterAddress,
        //        parameterDomain,
        //        parameterreturnValue,
        //    };
        //    var _ = await _context.SqlQueryAsync<stp_create_LDAP_Address_From_PathResult>("EXEC @returnValue = [dbo].[stp_create_LDAP_Address_From_Path] @Path, @Address OUTPUT, @Domain OUTPUT", sqlParameters, cancellationToken);

        //    Address.SetValue(parameterAddress.Value);
        //    Domain.SetValue(parameterDomain.Value);
        //    returnValue?.SetValue(parameterreturnValue.Value);

        //    return _;
        //}

        //public virtual async Task<int> stp_do_get_AD_group_membershipAsync(bool? FirstRun, int? GroupID, DateTime? LoadDate, string BatchID, bool? ADHoc, bool? IncludeNestedDuplicates, OutputParameter<int?> RecordsAdded, OutputParameter<int> returnValue = null, CancellationToken cancellationToken = default)
        //{
        //    var parameterRecordsAdded = new SqlParameter
        //    {
        //        ParameterName = "RecordsAdded",
        //        Direction = System.Data.ParameterDirection.Output,
        //        SqlDbType = System.Data.SqlDbType.Int,
        //    };
        //    var parameterreturnValue = new SqlParameter
        //    {
        //        ParameterName = "returnValue",
        //        Direction = System.Data.ParameterDirection.Output,
        //        SqlDbType = System.Data.SqlDbType.Int,
        //    };

        //    var sqlParameters = new []
        //    {
        //        new SqlParameter
        //        {
        //            ParameterName = "FirstRun",
        //            Value = FirstRun ?? Convert.DBNull,
        //            SqlDbType = System.Data.SqlDbType.Bit,
        //        },
        //        new SqlParameter
        //        {
        //            ParameterName = "GroupID",
        //            Value = GroupID ?? Convert.DBNull,
        //            SqlDbType = System.Data.SqlDbType.Int,
        //        },
        //        new SqlParameter
        //        {
        //            ParameterName = "LoadDate",
        //            Value = LoadDate ?? Convert.DBNull,
        //            SqlDbType = System.Data.SqlDbType.SmallDateTime,
        //        },
        //        new SqlParameter
        //        {
        //            ParameterName = "BatchID",
        //            Size = 32,
        //            Value = BatchID ?? Convert.DBNull,
        //            SqlDbType = System.Data.SqlDbType.Char,
        //        },
        //        new SqlParameter
        //        {
        //            ParameterName = "ADHoc",
        //            Value = ADHoc ?? Convert.DBNull,
        //            SqlDbType = System.Data.SqlDbType.Bit,
        //        },
        //        new SqlParameter
        //        {
        //            ParameterName = "IncludeNestedDuplicates",
        //            Value = IncludeNestedDuplicates ?? Convert.DBNull,
        //            SqlDbType = System.Data.SqlDbType.Bit,
        //        },
        //        parameterRecordsAdded,
        //        parameterreturnValue,
        //    };
        //    var _ = await _context.Database.ExecuteSqlRawAsync("EXEC @returnValue = [dbo].[stp_do_get_AD_group_membership] @FirstRun, @GroupID, @LoadDate, @BatchID, @ADHoc, @IncludeNestedDuplicates, @RecordsAdded OUTPUT", sqlParameters, cancellationToken);

        //    RecordsAdded.SetValue(parameterRecordsAdded.Value);
        //    returnValue?.SetValue(parameterreturnValue.Value);

        //    return _;
        //}

        //public virtual async Task<List<stp_get_AD_group_membershipResult>> stp_get_AD_group_membershipAsync(int? GroupID, bool? DisplayResults, bool? ADHoc, bool? IncludeNestedDuplicates, OutputParameter<string> BatchID, OutputParameter<int> returnValue = null, CancellationToken cancellationToken = default)
        //{
        //    var parameterBatchID = new SqlParameter
        //    {
        //        ParameterName = "BatchID",
        //        Size = 32,
        //        Direction = System.Data.ParameterDirection.Output,
        //        SqlDbType = System.Data.SqlDbType.Char,
        //    };
        //    var parameterreturnValue = new SqlParameter
        //    {
        //        ParameterName = "returnValue",
        //        Direction = System.Data.ParameterDirection.Output,
        //        SqlDbType = System.Data.SqlDbType.Int,
        //    };

        //    var sqlParameters = new []
        //    {
        //        new SqlParameter
        //        {
        //            ParameterName = "GroupID",
        //            Value = GroupID ?? Convert.DBNull,
        //            SqlDbType = System.Data.SqlDbType.Int,
        //        },
        //        new SqlParameter
        //        {
        //            ParameterName = "DisplayResults",
        //            Value = DisplayResults ?? Convert.DBNull,
        //            SqlDbType = System.Data.SqlDbType.Bit,
        //        },
        //        parameterBatchID,
        //        new SqlParameter
        //        {
        //            ParameterName = "ADHoc",
        //            Value = ADHoc ?? Convert.DBNull,
        //            SqlDbType = System.Data.SqlDbType.Bit,
        //        },
        //        new SqlParameter
        //        {
        //            ParameterName = "IncludeNestedDuplicates",
        //            Value = IncludeNestedDuplicates ?? Convert.DBNull,
        //            SqlDbType = System.Data.SqlDbType.Bit,
        //        },
        //        parameterreturnValue,
        //    };
        //    var _ = await _context.SqlQueryAsync<stp_get_AD_group_membershipResult>("EXEC @returnValue = [dbo].[stp_get_AD_group_membership] @GroupID, @DisplayResults, @BatchID OUTPUT, @ADHoc, @IncludeNestedDuplicates", sqlParameters, cancellationToken);

        //    BatchID.SetValue(parameterBatchID.Value);
        //    returnValue?.SetValue(parameterreturnValue.Value);

        //    return _;
        //}

        //public virtual async Task<List<stp_get_ADGroups_ForUserResult>> stp_get_ADGroups_ForUserAsync(bool? SaveData, string Username, string OUPath, OutputParameter<int> returnValue = null, CancellationToken cancellationToken = default)
        //{
        //    var parameterreturnValue = new SqlParameter
        //    {
        //        ParameterName = "returnValue",
        //        Direction = System.Data.ParameterDirection.Output,
        //        SqlDbType = System.Data.SqlDbType.Int,
        //    };

        //    var sqlParameters = new []
        //    {
        //        new SqlParameter
        //        {
        //            ParameterName = "SaveData",
        //            Value = SaveData ?? Convert.DBNull,
        //            SqlDbType = System.Data.SqlDbType.Bit,
        //        },
        //        new SqlParameter
        //        {
        //            ParameterName = "Username",
        //            Size = 510,
        //            Value = Username ?? Convert.DBNull,
        //            SqlDbType = System.Data.SqlDbType.NVarChar,
        //        },
        //        new SqlParameter
        //        {
        //            ParameterName = "OUPath",
        //            Size = 2048,
        //            Value = OUPath ?? Convert.DBNull,
        //            SqlDbType = System.Data.SqlDbType.NVarChar,
        //        },
        //        parameterreturnValue,
        //    };
        //    var _ = await _context.SqlQueryAsync<stp_get_ADGroups_ForUserResult>("EXEC @returnValue = [dbo].[stp_get_ADGroups_ForUser] @SaveData, @Username, @OUPath", sqlParameters, cancellationToken);

        //    returnValue?.SetValue(parameterreturnValue.Value);

        //    return _;
        //}

        //public virtual async Task<List<stp_get_DistinguishedNameResult>> stp_get_DistinguishedNameAsync(string Domain, string samAccountName, string AccountType, OutputParameter<int> returnValue = null, CancellationToken cancellationToken = default)
        //{
        //    var parameterreturnValue = new SqlParameter
        //    {
        //        ParameterName = "returnValue",
        //        Direction = System.Data.ParameterDirection.Output,
        //        SqlDbType = System.Data.SqlDbType.Int,
        //    };

        //    var sqlParameters = new []
        //    {
        //        new SqlParameter
        //        {
        //            ParameterName = "Domain",
        //            Size = -1,
        //            Value = Domain ?? Convert.DBNull,
        //            SqlDbType = System.Data.SqlDbType.VarChar,
        //        },
        //        new SqlParameter
        //        {
        //            ParameterName = "samAccountName",
        //            Size = 255,
        //            Value = samAccountName ?? Convert.DBNull,
        //            SqlDbType = System.Data.SqlDbType.VarChar,
        //        },
        //        new SqlParameter
        //        {
        //            ParameterName = "AccountType",
        //            Size = 50,
        //            Value = AccountType ?? Convert.DBNull,
        //            SqlDbType = System.Data.SqlDbType.VarChar,
        //        },
        //        parameterreturnValue,
        //    };
        //    var _ = await _context.SqlQueryAsync<stp_get_DistinguishedNameResult>("EXEC @returnValue = [dbo].[stp_get_DistinguishedName] @Domain, @samAccountName, @AccountType", sqlParameters, cancellationToken);

        //    returnValue?.SetValue(parameterreturnValue.Value);

        //    return _;
        //}

        //public virtual async Task<int> stp_insert_Group_MembersAsync(string Domain, string ADGroupName, string FieldName, OutputParameter<int> returnValue = null, CancellationToken cancellationToken = default)
        //{
        //    var parameterreturnValue = new SqlParameter
        //    {
        //        ParameterName = "returnValue",
        //        Direction = System.Data.ParameterDirection.Output,
        //        SqlDbType = System.Data.SqlDbType.Int,
        //    };

        //    var sqlParameters = new []
        //    {
        //        new SqlParameter
        //        {
        //            ParameterName = "Domain",
        //            Size = -1,
        //            Value = Domain ?? Convert.DBNull,
        //            SqlDbType = System.Data.SqlDbType.VarChar,
        //        },
        //        new SqlParameter
        //        {
        //            ParameterName = "ADGroupName",
        //            Size = -1,
        //            Value = ADGroupName ?? Convert.DBNull,
        //            SqlDbType = System.Data.SqlDbType.VarChar,
        //        },
        //        new SqlParameter
        //        {
        //            ParameterName = "FieldName",
        //            Size = 255,
        //            Value = FieldName ?? Convert.DBNull,
        //            SqlDbType = System.Data.SqlDbType.VarChar,
        //        },
        //        parameterreturnValue,
        //    };
        //    var _ = await _context.Database.ExecuteSqlRawAsync("EXEC @returnValue = [dbo].[stp_insert_Group_Members] @Domain, @ADGroupName, @FieldName", sqlParameters, cancellationToken);

        //    returnValue?.SetValue(parameterreturnValue.Value);

        //    return _;
        //}

        //public virtual async Task<List<stp_PurgeDeletedRecordsResult>> stp_PurgeDeletedRecordsAsync(int? DaysAgo, OutputParameter<int> returnValue = null, CancellationToken cancellationToken = default)
        //{
        //    var parameterreturnValue = new SqlParameter
        //    {
        //        ParameterName = "returnValue",
        //        Direction = System.Data.ParameterDirection.Output,
        //        SqlDbType = System.Data.SqlDbType.Int,
        //    };

        //    var sqlParameters = new []
        //    {
        //        new SqlParameter
        //        {
        //            ParameterName = "DaysAgo",
        //            Value = DaysAgo ?? Convert.DBNull,
        //            SqlDbType = System.Data.SqlDbType.Int,
        //        },
        //        parameterreturnValue,
        //    };
        //    var _ = await _context.SqlQueryAsync<stp_PurgeDeletedRecordsResult>("EXEC @returnValue = [dbo].[stp_PurgeDeletedRecords] @DaysAgo", sqlParameters, cancellationToken);

        //    returnValue?.SetValue(parameterreturnValue.Value);

        //    return _;
        //}

        //public virtual async Task<List<stp_RefreshADGroupsActiveUsersResult>> stp_RefreshADGroupsActiveUsersAsync(OutputParameter<int> returnValue = null, CancellationToken cancellationToken = default)
        //{
        //    var parameterreturnValue = new SqlParameter
        //    {
        //        ParameterName = "returnValue",
        //        Direction = System.Data.ParameterDirection.Output,
        //        SqlDbType = System.Data.SqlDbType.Int,
        //    };

        //    var sqlParameters = new []
        //    {
        //        parameterreturnValue,
        //    };
        //    var _ = await _context.SqlQueryAsync<stp_RefreshADGroupsActiveUsersResult>("EXEC @returnValue = [dbo].[stp_RefreshADGroupsActiveUsers]", sqlParameters, cancellationToken);

        //    returnValue?.SetValue(parameterreturnValue.Value);

        //    return _;
        //}

        public virtual async Task<int> ClearRecentWebsiteLogsAsync(OutputParameter<int> returnValue = null, CancellationToken cancellationToken = default)
        {
            var parameterreturnValue = new SqlParameter
            {
                ParameterName = "returnValue",
                Direction = System.Data.ParameterDirection.Output,
                SqlDbType = System.Data.SqlDbType.Int,
            };

            var sqlParameters = new []
            {
                parameterreturnValue,
            };
            var _ = await _context.Database.ExecuteSqlRawAsync("EXEC @returnValue = [dbo].[ClearRecentWebsiteLogs]", sqlParameters, cancellationToken);

            returnValue?.SetValue(parameterreturnValue.Value);

            return _;
        }

        public virtual async Task<int> ClearUsersFromADAsync(OutputParameter<int> returnValue = null, CancellationToken cancellationToken = default)
        {
            var parameterreturnValue = new SqlParameter
            {
                ParameterName = "returnValue",
                Direction = System.Data.ParameterDirection.Output,
                SqlDbType = System.Data.SqlDbType.Int,
            };

            var sqlParameters = new []
            {
                parameterreturnValue,
            };
            var _ = await _context.Database.ExecuteSqlRawAsync("EXEC @returnValue = [dbo].[ClearUsersFromAD]", sqlParameters, cancellationToken);

            returnValue?.SetValue(parameterreturnValue.Value);

            return _;
        }

        public virtual async Task<List<GetUsersResult>> GetUsersAsync(OutputParameter<int> returnValue = null, CancellationToken cancellationToken = default)
        {
            var parameterreturnValue = new SqlParameter
            {
                ParameterName = "returnValue",
                Direction = System.Data.ParameterDirection.Output,
                SqlDbType = System.Data.SqlDbType.Int,
            };

            var sqlParameters = new []
            {
                parameterreturnValue,
            };
            var _ = await _context.SqlQueryAsync<GetUsersResult>("EXEC @returnValue = [dbo].[stp_v2_GetUsers]", sqlParameters, cancellationToken);

            returnValue?.SetValue(parameterreturnValue.Value);

            return _;
        }

        public virtual async Task<List<GetUsersByUserNameResult>> GetUsersByUserNameAsync(string User_Name, OutputParameter<int> returnValue = null, CancellationToken cancellationToken = default)
        {
            var parameterreturnValue = new SqlParameter
            {
                ParameterName = "returnValue",
                Direction = System.Data.ParameterDirection.Output,
                SqlDbType = System.Data.SqlDbType.Int,
            };

            var sqlParameters = new []
            {
                new SqlParameter
                {
                    ParameterName = "User_Name",
                    Size = 100,
                    Value = User_Name ?? Convert.DBNull,
                    SqlDbType = System.Data.SqlDbType.VarChar,
                },
                parameterreturnValue,
            };
            var _ = await _context.SqlQueryAsync<GetUsersByUserNameResult>("EXEC @returnValue = [dbo].[stp_v2_GetUsersByUserName] @User_Name", sqlParameters, cancellationToken);

            returnValue?.SetValue(parameterreturnValue.Value);

            return _;
        }

        public virtual async Task<int> InsertDashboardUserAsync(string User_Name, string Full_Name, string AD_Account_Status, string EmailAddress, string AD_Group_Name, string Field_Name, OutputParameter<int> returnValue = null, CancellationToken cancellationToken = default)
        {
            var parameterreturnValue = new SqlParameter
            {
                ParameterName = "returnValue",
                Direction = System.Data.ParameterDirection.Output,
                SqlDbType = System.Data.SqlDbType.Int,
            };

            var sqlParameters = new []
            {
                new SqlParameter
                {
                    ParameterName = "User_Name",
                    Size = 100,
                    Value = User_Name ?? Convert.DBNull,
                    SqlDbType = System.Data.SqlDbType.VarChar,
                },
                new SqlParameter
                {
                    ParameterName = "Full_Name",
                    Size = 200,
                    Value = Full_Name ?? Convert.DBNull,
                    SqlDbType = System.Data.SqlDbType.VarChar,
                },
                new SqlParameter
                {
                    ParameterName = "AD_Account_Status",
                    Size = 300,
                    Value = AD_Account_Status ?? Convert.DBNull,
                    SqlDbType = System.Data.SqlDbType.VarChar,
                },
                new SqlParameter
                {
                    ParameterName = "EmailAddress",
                    Size = 300,
                    Value = EmailAddress ?? Convert.DBNull,
                    SqlDbType = System.Data.SqlDbType.VarChar,
                },
                new SqlParameter
                {
                    ParameterName = "AD_Group_Name",
                    Size = 1000,
                    Value = AD_Group_Name ?? Convert.DBNull,
                    SqlDbType = System.Data.SqlDbType.VarChar,
                },
                new SqlParameter
                {
                    ParameterName = "Field_Name",
                    Size = 100,
                    Value = Field_Name ?? Convert.DBNull,
                    SqlDbType = System.Data.SqlDbType.VarChar,
                },
                parameterreturnValue,
            };
            var _ = await _context.Database.ExecuteSqlRawAsync("EXEC @returnValue = [dbo].[InsertDashboardUser] @User_Name, @Full_Name, @AD_Account_Status, @EmailAddress, @AD_Group_Name, @Field_Name", sqlParameters, cancellationToken);

            returnValue?.SetValue(parameterreturnValue.Value);

            return _;
        }

        public virtual async Task<int> LogErrorsAsync(DateTime? ErrorTime, string UserName, string MachineName, string ModuleName, string FunctionName, string MessageText, OutputParameter<int> returnValue = null, CancellationToken cancellationToken = default)
        {
            var parameterreturnValue = new SqlParameter
            {
                ParameterName = "returnValue",
                Direction = System.Data.ParameterDirection.Output,
                SqlDbType = System.Data.SqlDbType.Int,
            };

            var sqlParameters = new []
            {
                new SqlParameter
                {
                    ParameterName = "ErrorTime",
                    Value = ErrorTime ?? Convert.DBNull,
                    SqlDbType = System.Data.SqlDbType.Date,
                },
                new SqlParameter
                {
                    ParameterName = "UserName",
                    Size = 200,
                    Value = UserName ?? Convert.DBNull,
                    SqlDbType = System.Data.SqlDbType.VarChar,
                },
                new SqlParameter
                {
                    ParameterName = "MachineName",
                    Size = 300,
                    Value = MachineName ?? Convert.DBNull,
                    SqlDbType = System.Data.SqlDbType.VarChar,
                },
                new SqlParameter
                {
                    ParameterName = "ModuleName",
                    Size = 300,
                    Value = ModuleName ?? Convert.DBNull,
                    SqlDbType = System.Data.SqlDbType.VarChar,
                },
                new SqlParameter
                {
                    ParameterName = "FunctionName",
                    Size = 1000,
                    Value = FunctionName ?? Convert.DBNull,
                    SqlDbType = System.Data.SqlDbType.VarChar,
                },
                new SqlParameter
                {
                    ParameterName = "MessageText",
                    Size = -1,
                    Value = MessageText ?? Convert.DBNull,
                    SqlDbType = System.Data.SqlDbType.VarChar,
                },
                parameterreturnValue,
            };
            var _ = await _context.Database.ExecuteSqlRawAsync("EXEC @returnValue = [dbo].[LogErrors] @ErrorTime, @UserName, @MachineName, @ModuleName, @FunctionName, @MessageText", sqlParameters, cancellationToken);

            returnValue?.SetValue(parameterreturnValue.Value);

            return _;
        }
    }
}
