﻿using Microsoft.EntityFrameworkCore;
using IT.Audit.Dashboard.EntityFrameworkCore.ItAuditDashboard.Domain;

#nullable disable

namespace IT.Audit.Dashboard.EntityFrameworkCore.ItAuditDashboard.Data
{
    public partial class ITAuditDashboardDbContext : DbContext
    {
        public ITAuditDashboardDbContext(DbContextOptions<ITAuditDashboardDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<AuditDashboardUsersFromAd> TAuditDashboardUsersFromAds { get; set; }
        public virtual DbSet<WebSiteErrorLog> TWebSiteErrorLogs { get; set; }

        //public virtual DbSet<TAccountControl> TAccountControls { get; set; }
        //public virtual DbSet<TAdaccountType> TAdaccountTypes { get; set; }
        //public virtual DbSet<TAdgroup> TAdgroups { get; set; }
        //public virtual DbSet<TAdgroupRequestDefinition> TAdgroupRequestDefinitions { get; set; }
        //public virtual DbSet<TAdgroupRequestDefinitionsLinkAdgroup> TAdgroupRequestDefinitionsLinkAdgroups { get; set; }
        //public virtual DbSet<TAdgroupsActiveUser> TAdgroupsActiveUsers { get; set; }
        //public virtual DbSet<TAdgroupsActiveUsersImport> TAdgroupsActiveUsersImports { get; set; }
        //public virtual DbSet<TAdgroupsAdhoc> TAdgroupsAdhocs { get; set; }
        //public virtual DbSet<TAdgroupstmp> TAdgroupstmps { get; set; }
        //public virtual DbSet<TDatabasesThatCanBeEditedFromItauditDashboard> TDatabasesThatCanBeEditedFromItauditDashboards { get; set; }
        //public virtual DbSet<TNestedGroupsExcludedFromSearch> TNestedGroupsExcludedFromSearches { get; set; }
        //public virtual DbSet<TParameter> TParameters { get; set; }
        //public virtual DbSet<TQueryText> TQueryTexts { get; set; }
        //public virtual DbSet<TTablesThatCanBeEditedFromItauditDashboard> TTablesThatCanBeEditedFromItauditDashboards { get; set; }
        //public virtual DbSet<TTablesThatCanBeEditedFromItauditDashboardColumn> TTablesThatCanBeEditedFromItauditDashboardColumns { get; set; }
        //public virtual DbSet<TTablesThatCanBeEditedFromItauditDashboardColumnRule> TTablesThatCanBeEditedFromItauditDashboardColumnRules { get; set; }
        //public virtual DbSet<TUserGroupMembership> TUserGroupMemberships { get; set; }
        //public virtual DbSet<VwAdgroupRequestDefinition> VwAdgroupRequestDefinitions { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "SQL_Latin1_General_CP1_CI_AS");

            modelBuilder.ApplyConfiguration(new Configurations.AuditDashboardUsersFromAdConfiguration());
            modelBuilder.ApplyConfiguration(new Configurations.WebSiteErrorLogConfiguration());

            //modelBuilder.ApplyConfiguration(new Configurations.TAccountControlConfiguration());
            //modelBuilder.ApplyConfiguration(new Configurations.TAdaccountTypeConfiguration());
            //modelBuilder.ApplyConfiguration(new Configurations.TAdgroupConfiguration());
            //modelBuilder.ApplyConfiguration(new Configurations.TAdgroupRequestDefinitionConfiguration());
            //modelBuilder.ApplyConfiguration(new Configurations.TAdgroupRequestDefinitionsLinkAdgroupConfiguration());
            //modelBuilder.ApplyConfiguration(new Configurations.TAdgroupsActiveUserConfiguration());
            //modelBuilder.ApplyConfiguration(new Configurations.TAdgroupsActiveUsersImportConfiguration());
            //modelBuilder.ApplyConfiguration(new Configurations.TAdgroupsAdhocConfiguration());
            //modelBuilder.ApplyConfiguration(new Configurations.TAdgroupstmpConfiguration());
            //modelBuilder.ApplyConfiguration(new Configurations.TDatabasesThatCanBeEditedFromItauditDashboardConfiguration());
            //modelBuilder.ApplyConfiguration(new Configurations.TNestedGroupsExcludedFromSearchConfiguration());
            //modelBuilder.ApplyConfiguration(new Configurations.TParameterConfiguration());
            //modelBuilder.ApplyConfiguration(new Configurations.TQueryTextConfiguration());
            //modelBuilder.ApplyConfiguration(new Configurations.TTablesThatCanBeEditedFromItauditDashboardConfiguration());
            //modelBuilder.ApplyConfiguration(new Configurations.TTablesThatCanBeEditedFromItauditDashboardColumnConfiguration());
            //modelBuilder.ApplyConfiguration(new Configurations.TTablesThatCanBeEditedFromItauditDashboardColumnRuleConfiguration());
            //modelBuilder.ApplyConfiguration(new Configurations.TUserGroupMembershipConfiguration());
            //modelBuilder.ApplyConfiguration(new Configurations.VwAdgroupRequestDefinitionConfiguration());

            OnModelCreatingGeneratedFunctions(modelBuilder);
            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
