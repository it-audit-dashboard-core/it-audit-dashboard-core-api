﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;
using IT.Audit.Dashboard.EntityFrameworkCore.ItAuditDashboard.Domain;

#nullable disable

namespace IT.Audit.Dashboard.EntityFrameworkCore.ItAuditDashboard.Data.Configurations
{
    public partial class AuditDashboardUsersFromAdConfiguration : IEntityTypeConfiguration<AuditDashboardUsersFromAd>
    {
        public void Configure(EntityTypeBuilder<AuditDashboardUsersFromAd> entity)
        {
            entity.HasKey(e => new { e.UserName, e.FieldName });

            entity.Property(e => e.UserName).IsUnicode(false);

            entity.Property(e => e.FieldName).IsUnicode(false);

            entity.Property(e => e.AdAccountStatus).IsUnicode(false);

            entity.Property(e => e.AdGroupName).IsUnicode(false); 

            entity.Property(e => e.EmailAddress).IsUnicode(false);

            entity.Property(e => e.FullName).IsUnicode(false);

            OnConfigurePartial(entity);
        }

        partial void OnConfigurePartial(EntityTypeBuilder<AuditDashboardUsersFromAd> entity);
    }
}
