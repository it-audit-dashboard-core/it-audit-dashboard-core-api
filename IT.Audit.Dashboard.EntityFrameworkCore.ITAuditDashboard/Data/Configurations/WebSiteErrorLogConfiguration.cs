﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace IT.Audit.Dashboard.EntityFrameworkCore.ItAuditDashboard.Data.Configurations
{
    public partial class WebSiteErrorLogConfiguration : IEntityTypeConfiguration<WebSiteErrorLog>
    {
        public void Configure(EntityTypeBuilder<WebSiteErrorLog> entity)
        {
            entity.Property(e => e.FunctionName).IsUnicode(false);

            entity.Property(e => e.MachineName).IsUnicode(false);

            entity.Property(e => e.MessageText).IsUnicode(false);

            entity.Property(e => e.ModuleName).IsUnicode(false);

            entity.Property(e => e.UserName).IsUnicode(false);

            OnConfigurePartial(entity);
        }

        partial void OnConfigurePartial(EntityTypeBuilder<WebSiteErrorLog> entity);
    }
}
