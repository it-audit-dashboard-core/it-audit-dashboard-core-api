﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace IT.Audit.Dashboard.EntityFrameworkCore.Domain
{
    public interface IPageSettingsRepository
    {
        Task<List<PageSettings>> GetListAsync(int connectionType);
    }
}
