﻿using IT.Audit.Dashboard.Domain.Shared;

namespace IT.Audit.Dashboard.EntityFrameworkCore.Domain
{
    public class PageSettings
    {
        public ConnectionType ConnectionType { get; set; }
        public string TableName { get; set; }
        public string CacheKey { get; set; }
        public string Name { get; set; }
    }
}
