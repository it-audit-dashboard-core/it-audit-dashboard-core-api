﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using IT.Audit.Dashboard.EntityFrameworkCore.Domain.Test;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore;
using System;


namespace IT.Audit.Dashboard.EntityFrameworkCore.Domain.Test.Configurations
{
    public partial class TAdgroupRequestDefinitionsLinkAdgroupConfiguration : IEntityTypeConfiguration<TAdgroupRequestDefinitionsLinkAdgroup>
    {
        public void Configure(EntityTypeBuilder<TAdgroupRequestDefinitionsLinkAdgroup> entity)
        {
            entity.HasKey(e => new { e.RequestDefinitionId, e.AdgroupsId });

            entity.HasOne(d => d.Adgroups)
                .WithMany(p => p.TAdgroupRequestDefinitionsLinkAdgroups)
                .HasForeignKey(d => d.AdgroupsId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_t_ADGroupRequestDefinitions_Link_ADGroups_t_ADGroups");

            entity.HasOne(d => d.RequestDefinition)
                .WithMany(p => p.TAdgroupRequestDefinitionsLinkAdgroups)
                .HasForeignKey(d => d.RequestDefinitionId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_t_ADGroupRequestDefinitions_Link_ADGroups_t_ADGroupRequestDefinitions");

            OnConfigurePartial(entity);
        }

        partial void OnConfigurePartial(EntityTypeBuilder<TAdgroupRequestDefinitionsLinkAdgroup> entity);
    }
}
