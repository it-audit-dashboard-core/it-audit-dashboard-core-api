﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using IT.Audit.Dashboard.EntityFrameworkCore.Domain.Test;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore;
using System;


namespace IT.Audit.Dashboard.EntityFrameworkCore.Domain.Test.Configurations
{
    public partial class TDatabasesThatCanBeEditedFromItauditDashboardConfiguration : IEntityTypeConfiguration<TDatabasesThatCanBeEditedFromItauditDashboard>
    {
        public void Configure(EntityTypeBuilder<TDatabasesThatCanBeEditedFromItauditDashboard> entity)
        {
            entity.Property(e => e.Adgroups).IsUnicode(false);

            entity.Property(e => e.DatabaseName).IsUnicode(false);

            OnConfigurePartial(entity);
        }

        partial void OnConfigurePartial(EntityTypeBuilder<TDatabasesThatCanBeEditedFromItauditDashboard> entity);
    }
}
