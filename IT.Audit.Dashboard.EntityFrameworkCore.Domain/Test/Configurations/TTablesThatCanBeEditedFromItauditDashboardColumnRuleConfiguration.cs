﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using IT.Audit.Dashboard.EntityFrameworkCore.Domain.Test;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore;
using System;


namespace IT.Audit.Dashboard.EntityFrameworkCore.Domain.Test.Configurations
{
    public partial class TTablesThatCanBeEditedFromItauditDashboardColumnRuleConfiguration : IEntityTypeConfiguration<TTablesThatCanBeEditedFromItauditDashboardColumnRule>
    {
        public void Configure(EntityTypeBuilder<TTablesThatCanBeEditedFromItauditDashboardColumnRule> entity)
        {
            entity.Property(e => e.Description).IsUnicode(false);

            OnConfigurePartial(entity);
        }

        partial void OnConfigurePartial(EntityTypeBuilder<TTablesThatCanBeEditedFromItauditDashboardColumnRule> entity);
    }
}
