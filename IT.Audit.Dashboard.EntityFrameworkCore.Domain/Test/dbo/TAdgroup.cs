﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace IT.Audit.Dashboard.EntityFrameworkCore.Domain.Test
{
    [Table("t_ADGroups")]
    public partial class TAdgroup
    {
        public TAdgroup()
        {
            TAdgroupRequestDefinitionsLinkAdgroups = new HashSet<TAdgroupRequestDefinitionsLinkAdgroup>();
        }

        [Required]
        [StringLength(750)]
        public string Address { get; set; }
        [Key]
        [Column("ID")]
        public int Id { get; set; }
        [Required]
        [StringLength(750)]
        public string Path { get; set; }
        [Required]
        [StringLength(255)]
        public string Domain { get; set; }

        [InverseProperty(nameof(TAdgroupRequestDefinitionsLinkAdgroup.Adgroups))]
        public virtual ICollection<TAdgroupRequestDefinitionsLinkAdgroup> TAdgroupRequestDefinitionsLinkAdgroups { get; set; }
    }
}