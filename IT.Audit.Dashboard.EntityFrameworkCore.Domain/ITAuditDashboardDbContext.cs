﻿using IT.Audit.Dashboard.Domain.Shared;
using IT.Audit.Dashboard.EntityFrameworkCore.Domain.Configurations;
using IT.Audit.Dashboard.EntityFrameworkCore.Domain.EF;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace IT.Audit.Dashboard.EntityFrameworkCore
{
    public partial class ITAuditDashboardDbContext : DbContext
    {
        private IConfiguration _configuration;

        public ITAuditDashboardDbContext(DbContextOptions<ITAuditDashboardDbContext> options, IConfiguration configuration)
            : base(options)
        {
            _configuration = configuration;
        }

        public virtual DbSet<AccountControl> AccountControls { get; set; }
        public virtual DbSet<ADAccountType> ADAccountTypes { get; set; }
        public virtual DbSet<ADGroup> ADGroups { get; set; }
        public virtual DbSet<ADGroupRequestDefinition> ADGroupRequestDefinitions { get; set; }
        public virtual DbSet<ADGroupRequestDefinitionsLinkADGroup> ADGroupRequestDefinitionsLinkADGroups { get; set; }
        public virtual DbSet<ADGroupsActiveUser> ADGroupsActiveUsers { get; set; }
        public virtual DbSet<ADGroupsActiveUsersImport> ADGroupsActiveUsersImports { get; set; }
        public virtual DbSet<ADGroupsAdhoc> ADGroupsAdhocs { get; set; }
        public virtual DbSet<ADGroupSmtp> ADGroupSmtps { get; set; }
        public virtual DbSet<AuditDashboardUsersFromAD> AuditDashboardUsersFromADs { get; set; }
        public virtual DbSet<DatabasesThatCanBeEditedFromItauditDashboard> DatabasesThatCanBeEditedFromItauditDashboards { get; set; }
        public virtual DbSet<NestedGroupsExcludedFromSearch> NestedGroupsExcludedFromSearchs { get; set; }
        public virtual DbSet<Parameter> Parameters { get; set; }
        public virtual DbSet<QueryText> QueryTexts { get; set; }
        public virtual DbSet<TablesThatCanBeEditedFromITAuditDashboard> TablesThatCanBeEditedFromITAuditDashboards { get; set; }
        public virtual DbSet<TablesThatCanBeEditedFromITAuditDashboardColumn> TablesThatCanBeEditedFromITAuditDashboardColumns { get; set; }
        public virtual DbSet<TablesThatCanBeEditedFromITAuditDashboardColumnRule> TablesThatCanBeEditedFromITAuditDashboardColumnRules { get; set; }
        public virtual DbSet<UserGroupMembership> UserGroupMemberships { get; set; }
        public virtual DbSet<WebSiteErrorLog> WebSiteErrorLogs { get; set; }
        public virtual DbSet<VwADGroupRequestDefinition> VwADGroupRequestDefinitions { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
            => options.UseSqlServer(_configuration.GetConnectionString(ConnectionStringKey.ITAuditDashboard));

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "SQL_Latin1_General_CP1_CI_AS");

            modelBuilder.ApplyConfiguration(new AccountControlConfiguration());
            modelBuilder.ApplyConfiguration(new ADAccountTypeConfiguration());
            modelBuilder.ApplyConfiguration(new ADGroupConfiguration());
            modelBuilder.ApplyConfiguration(new ADGroupRequestDefinitionConfiguration());
            modelBuilder.ApplyConfiguration(new ADGroupRequestDefinitionsLinkAdgroupConfiguration());
            modelBuilder.ApplyConfiguration(new ADGroupsActiveUserConfiguration());
            modelBuilder.ApplyConfiguration(new ADGroupsActiveUsersImportConfiguration());
            modelBuilder.ApplyConfiguration(new ADGroupsAdhocConfiguration());
            modelBuilder.ApplyConfiguration(new ADGroupSmtpConfiguration());
            modelBuilder.ApplyConfiguration(new AuditDashboardUsersFromAdConfiguration());
            modelBuilder.ApplyConfiguration(new DatabasesThatCanBeEditedFromItauditDashboardConfiguration());
            modelBuilder.ApplyConfiguration(new NestedGroupsExcludedFromSearchConfiguration());
            modelBuilder.ApplyConfiguration(new ParameterConfiguration());
            modelBuilder.ApplyConfiguration(new QueryTextConfiguration());
            modelBuilder.ApplyConfiguration(new TablesThatCanBeEditedFromItauditDashboardConfiguration());
            modelBuilder.ApplyConfiguration(new TablesThatCanBeEditedFromItauditDashboardColumnConfiguration());
            modelBuilder.ApplyConfiguration(new TablesThatCanBeEditedFromItauditDashboardColumnRuleConfiguration());
            modelBuilder.ApplyConfiguration(new UserGroupMembershipConfiguration());
            modelBuilder.ApplyConfiguration(new WebSiteErrorLogConfiguration());
            modelBuilder.ApplyConfiguration(new VwAdgroupRequestDefinitionConfiguration());
            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
