﻿using System.Collections.Generic;

namespace IT.Audit.Dashboard.EntityFrameworkCore.Domain
{
    public interface IDataSourceProvider
    {
        public IReadOnlyList<DataSource> DataSources { get; set; }
    }
}
