﻿using System;

namespace IT.Audit.Dashboard.Domain
{
    public interface IDataSource
    {
        int Id { get; set; }
        DateTime ModifiedDate { get; set; }
        string Name { get; set; }
    }
}