﻿using IT.Audit.Dashboard.Domain.Shared;
using System;

namespace IT.Audit.Dashboard.EntityFrameworkCore.Domain
{
    public class DataSource : IEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime ModifiedDate { get; set; }
    }
}
