﻿namespace IT.Audit.Dashboard.EntityFrameworkCore.Domain
{
    public interface IDataSourceProviderFactory
    {
        IDataSourceProvider CreateDataSourceProvider(string controllerName);
    }
}
