﻿using IT.Audit.Dashboard.Domain.Shared;
using IT.Audit.Dashboard.Sql.Domain;
using Microsoft.Extensions.Configuration;
using System;

namespace IT.Audit.Dashboard.Sql.Providers
{
    public class DataSourceProviderFactory : IConnectionProviderFactory
    {
        public IConnectionProvider CreateDataSourceProvider(string controllerName)
        {
            switch (controllerName)
            {
                case "iba-users":
                    return new IbaDataSourceProvider();
                    break;
                case "ad-users":
                    return new ActiveDirectoryUsersDataSourceProvider();
                    break;
                case ConnectionType.Active_Directory_Users:
                    provider = new ActiveDirectoryConnectionProvider(Configuration);
                    break;
                default:
                    throw new NotImplementedException(nameof(CreateConnectionProvider));
            }

            return provider;
        }

        public ConnectionType ConnectionType { get; set; }
    }
}
