﻿using Microsoft.EntityFrameworkCore;

namespace IT.Audit.Dashboard.EntityFrameworkCore.Domain
{
    public class DataSourceManager<TDbContext, TEntity> where TDbContext : DbContext where TEntity : class
    {
        private readonly IDataSourceRepository _dataSourceRepository;

        public DataSourceManager(IDataSourceRepository dataSourceRepository)
        {
            _dataSourceRepository = dataSourceRepository;
        }
    }
}
