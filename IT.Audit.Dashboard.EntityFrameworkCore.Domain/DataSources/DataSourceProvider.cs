﻿using System.Collections.Generic;

namespace IT.Audit.Dashboard.EntityFrameworkCore.Domain.DataSources
{
    public class DataSourceProvider : IDataSourceProvider
    {
        public IReadOnlyList<DataSource> DataSources { get; set; }
    }
}
