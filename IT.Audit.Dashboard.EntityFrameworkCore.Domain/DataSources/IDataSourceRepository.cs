﻿using IT.Audit.Dashboard.Application.Dtos;
using System.Threading.Tasks;

namespace IT.Audit.Dashboard.EntityFrameworkCore.Domain
{
    public interface IDataSourceRepository
    {
        public Task<DataSourceOutputDto> GetModifiedDate();
    }
}
