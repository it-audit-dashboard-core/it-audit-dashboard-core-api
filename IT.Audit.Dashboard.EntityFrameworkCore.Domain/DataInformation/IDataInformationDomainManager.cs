﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace IT.Audit.Dashboard.EntityFrameworkCore.Domain
{
    public interface IDataInformationDomainManager
    {
        Task<IReadOnlyList<DataSource>> GetFromDatabaseAsync(string controllerName);
    }
}