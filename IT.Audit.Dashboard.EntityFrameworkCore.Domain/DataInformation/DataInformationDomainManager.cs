﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IT.Audit.Dashboard.EntityFrameworkCore.Domain
{
    public class DataInformationDomainManager : IDataInformationDomainManager
    {
        private readonly IDataSourceProviderFactory _dataSourceProviderFactory;

        public DataInformationDomainManager(IDataSourceProviderFactory dataSourceProviderFactory)
        {
            _dataSourceProviderFactory = dataSourceProviderFactory;
        }

        //public async Task<DataSource> GetFromDatabaseAsync(string tableName)
        //{
        //    throw new NotImplementedException();
        //    //var date = await _dataSourceRepository.GetModifiedDate(tableName);
        //    //return new DataSource { ModifiedDate = date };
        //}

        public Task<DataSource> GetFromDatabaseAsync(string controllerName)
        {
            throw new NotImplementedException();
        }

        Task<IReadOnlyList<DataSource>> IDataInformationDomainManager.GetFromDatabaseAsync(string controllerName)
        {
            throw new NotImplementedException();
        }
    }
}
