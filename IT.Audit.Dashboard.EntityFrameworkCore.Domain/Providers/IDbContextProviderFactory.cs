﻿using IT.Audit.Dashboard.Domain.Shared;

namespace IT.Audit.Dashboard.EntityFrameworkCore.Domain
{
    public interface IDbContextProviderFactory
    {
        IDbContextProvider CreateDbContextProvider(ConnectionType connectionType);
    }
}
