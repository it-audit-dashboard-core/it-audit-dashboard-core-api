﻿using Microsoft.EntityFrameworkCore;

namespace IT.Audit.Dashboard.EntityFrameworkCore.Domain
{
    public interface IDbContextProvider
    {
        DbContext DbContext { get; }
    }
}
