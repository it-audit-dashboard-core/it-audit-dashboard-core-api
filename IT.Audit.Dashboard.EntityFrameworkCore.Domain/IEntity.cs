﻿namespace IT.Audit.Dashboard.EntityFrameworkCore.Domain
{
    public interface IEntity
    {
        int Id { get; set; }
    }
}
