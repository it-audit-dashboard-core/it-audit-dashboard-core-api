﻿namespace IT.Audit.Dashboard.EntityFrameworkCore.Domain.Data
{
    public class UserApplicationData
    {
        public static readonly UserApplication[] UserApplications = new[]
        {
            new UserApplication(1, "iba-users", "IBA Users", "/apps/iba-users", @"SOX_DB_DEV\SILO / SOX_IBA_BDRM"),
            new UserApplication(2, "ad-users", "Active Directory Users", "/apps/ad-users", @"SOX_DB_DEV\SILO / Active_Directory_Users"),
            new UserApplication(3, "bdrm-users", "BDRM Users", "['apps/bdrm-users']", @"\\emeukhc4ww39vd\BDRMTestFiles\Ajg.xml & \\emeukhc4ww39vd\BDRMTestFiles\User_Oim.xml"),
            new UserApplication(4, "acturis-users", "Acturis Users", "apps/acturis-users", @"SOX_DB_DEV\SILO / SOX_Acturis"),
            new UserApplication(5, "cdl-users", "CDL Users", "apps/cdl-users", @"SOX_DB_DEV\SILO / SOX_CDL"),
            new UserApplication(6, "ad-group-membership", "AD Group Membership", "apps/ad-group-membership", @"SOX_DB_DEV\SILO / IT_Audit_Dashboard"),
            new UserApplication(7, "generic-uar-users", "Generic UAR Application Users", "apps/ad-group-membership", @"SOX_DB_DEV\SILO / SOX_Generic_UAR & SOX_DB_DEV\SILO / SOX_Original_Imports"),
            new UserApplication(8, "edit-ad-users-scope", "Edit Users AD Organisational Unit Scope", "apps/ou-scope-users", @"SOX_DB_DEV\SILO / Active_Directory_Users")
        };
    }
}
