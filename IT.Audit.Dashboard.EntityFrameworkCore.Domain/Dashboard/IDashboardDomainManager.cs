﻿using IT.Audit.Dashboard.Domain.Shared;
using IT.Audit.Dashboard.Sql.Domain;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IT.Audit.Dashboard.EntityFrameworkCore.Domain
{
    public interface IDashboardDomainManager
    {
        Task<List<DashboardUser>> GetFromDatabaseAsync(string userName, bool refreshCache = false);
    }
}