﻿using IT.Audit.Dashboard.Domain.Shared;
using System.Collections.Generic;
using System.DirectoryServices;
using System.Threading.Tasks;

namespace IT.Audit.Dashboard.EntityFrameworkCore.Domain
{
    public class DashboardDomainManager : IDashboardDomainManager
    {
        private readonly IDashboardUnitOfWork _unitOfWork;

        public DashboardDomainManager(IDashboardUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

    }
}
