﻿using IT.Audit.Dashboard.Domain.Shared;

namespace IT.Audit.Dashboard.EntityFrameworkCore.Domain
{
    public class IDashboardUser : IEntity
    {
        public int Id { get; set; }
    }
}
