﻿using System.Threading.Tasks;

namespace IT.Audit.Dashboard.EntityFrameworkCore.Domain
{
    public interface IDashboardUnitOfWork
    {
        IDashboardRepository Dashboard { get; }
        IUserApplicationGroupsRepository UserApplicationGroups { get; }
        Task CompleteAsync();
    }
}
