﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace IT.Audit.Dashboard.EntityFrameworkCore.Domain
{
    public interface IDashboardRepository
    {
        Task DeleteUsersAsync();
        Task AddAuthorisedUsersAsync(string userName, string displayName, string email, List<UserApplicationGroup> userApplicationGroups);
        Task<List<GetUsersByUserNameResult>> GetListAsync(string userName);
    }
}