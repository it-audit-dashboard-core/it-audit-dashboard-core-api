﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace IT.Audit.Dashboard.EntityFrameworkCore.Domain
{
    public interface IRepository<T> where T : class
    {
        void Add(T entity);
        void Delete(T entity);
        void Update(T entity);
        IReadOnlyList<T> Get(Expression<Func<T, bool>> expression);
    }

}
