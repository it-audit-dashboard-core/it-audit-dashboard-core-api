﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IT.Audit.Dashboard.EntityFrameworkCore.Domain.IbaUsers
{
    public partial class TIbaFxXmlConsoleJobLog
    {
        public int Id { get; set; }
        public string WatcherName { get; set; }
        public bool WatcherEnabled { get; set; }
        public bool WatcherRunning { get; set; }
        public int EventsProcessed { get; set; }
        public DateTime? FirstEventProcessed { get; set; }
        public DateTime? LastEventProcessed { get; set; }
        public DateTime DataDate { get; set; }
    }
}
