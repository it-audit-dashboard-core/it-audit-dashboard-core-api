﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IT.Audit.Dashboard.EntityFrameworkCore.Domain.IbaUsers
{
    public partial class VwBackupLog
    {
        public DateTime? RunDate { get; set; }
        public string IbaDb { get; set; }
        public DateTime? BackupComplete { get; set; }
        public string LogFileName { get; set; }
        public string Instance { get; set; }
        public string BackupStatus { get; set; }
    }
}
