﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IT.Audit.Dashboard.EntityFrameworkCore.Domain.IbaUsers
{
    public partial class TIbaUsersAccountReport
    {
        public int Id { get; set; }
        public string Instance { get; set; }
        public string User { get; set; }
        public int? AccountHandler { get; set; }
        public int? AhCct { get; set; }
        public int? OwnerCct { get; set; }
        public DateTime LoadDate { get; set; }
    }
}
