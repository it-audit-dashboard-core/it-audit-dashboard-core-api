﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IT.Audit.Dashboard.EntityFrameworkCore.Domain.IbaUsers
{
    public partial class TIbaUsersRecordCount
    {
        public int Id { get; set; }
        public int RecordCount { get; set; }
        public int ActiveAccounts { get; set; }
        public int InactiveAccounts { get; set; }
        public int UniqueActiveUsers { get; set; }
        public DateTime LoadDate { get; set; }
        public DateTime SourceFileDate { get; set; }
    }
}
