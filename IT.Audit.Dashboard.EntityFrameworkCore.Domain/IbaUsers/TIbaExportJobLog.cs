﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IT.Audit.Dashboard.EntityFrameworkCore.Domain.IbaUsers
{
    public partial class TIbaExportJobLog
    {
        public int Id { get; set; }
        public DateTime? RunDate { get; set; }
        public string IbaDb { get; set; }
        public DateTime? ExportComplete { get; set; }
    }
}
