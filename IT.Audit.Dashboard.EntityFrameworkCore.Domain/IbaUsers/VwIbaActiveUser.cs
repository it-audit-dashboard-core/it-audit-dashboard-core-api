﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IT.Audit.Dashboard.EntityFrameworkCore.Domain.IbaUsers
{
    public partial class VwIbaActiveUser
    {
        public string Name { get; set; }
        public string Instance { get; set; }
        public string Role { get; set; }
        public string Security { get; set; }
        public string Login { get; set; }
        public string LastUpdatedBy { get; set; }
        public DateTime? IbaLastUpdated { get; set; }
        public string Office { get; set; }
        public string AdDepartment { get; set; }
        public string City { get; set; }
        public string AdOu { get; set; }
        public string Manager { get; set; }
        public string AdAccountStatus { get; set; }
        public DateTime? AdLastUpdate { get; set; }
    }
}
