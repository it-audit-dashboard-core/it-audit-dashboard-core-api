﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IT.Audit.Dashboard.EntityFrameworkCore.Domain.IbaUsers
{
    public partial class TBdrmScheduleJobLog
    {
        public int Id { get; set; }
        public DateTime RunDate { get; set; }
        public string JobName { get; set; }
        public DateTime ScheduleComplete { get; set; }
    }
}
