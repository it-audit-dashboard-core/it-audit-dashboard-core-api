﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IT.Audit.Dashboard.EntityFrameworkCore.Domain.IbaUsers
{
    public partial class TIbaBackupJobLog
    {
        public int Id { get; set; }
        public DateTime? RunDate { get; set; }
        public string IbaDb { get; set; }
        public DateTime? BackupComplete { get; set; }
    }
}
