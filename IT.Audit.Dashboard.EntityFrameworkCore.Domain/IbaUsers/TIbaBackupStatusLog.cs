﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IT.Audit.Dashboard.EntityFrameworkCore.Domain.IbaUsers
{
    public partial class TIbaBackupStatusLog
    {
        public int Id { get; set; }
        public int? BackupId { get; set; }
        public string LogFileName { get; set; }
        public string BackupStatus { get; set; }
    }
}
