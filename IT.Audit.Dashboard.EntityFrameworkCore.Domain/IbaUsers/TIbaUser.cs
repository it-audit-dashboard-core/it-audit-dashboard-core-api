﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IT.Audit.Dashboard.EntityFrameworkCore.Domain.IbaUsers
{
    public partial class TIbaUser
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Instance { get; set; }
        public string Role { get; set; }
        public string Security { get; set; }
        public string Login { get; set; }
        public string IsActive { get; set; }
        public string User { get; set; }
        public DateTime? Date { get; set; }
        public string ServiceDeskRef { get; set; }
        public string Disabled { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedRef { get; set; }
        public string CreatedUser { get; set; }
        public DateTime? LastLoginDate { get; set; }
        public string LastLoginTime { get; set; }
        public int? Port { get; set; }
        public DateTime? LoadDate { get; set; }
    }
}
