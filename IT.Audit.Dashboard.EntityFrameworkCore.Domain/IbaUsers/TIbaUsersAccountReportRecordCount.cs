﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IT.Audit.Dashboard.EntityFrameworkCore.Domain.IbaUsers
{
    public partial class TIbaUsersAccountReportRecordCount
    {
        public int Id { get; set; }
        public int RecordCount { get; set; }
        public DateTime LoadDate { get; set; }
        public DateTime SourceFileDate { get; set; }
    }
}
