﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IT.Audit.Dashboard.EntityFrameworkCore.Domain.IbaUsers
{
    public partial class TIbaScheduleErrorHistoryLog
    {
        public int Id { get; set; }
        public int JobLogId { get; set; }
        public string ErrorMessage { get; set; }
        public DateTime FirstOccurrence { get; set; }
        public DateTime LastOccurrence { get; set; }
    }
}
