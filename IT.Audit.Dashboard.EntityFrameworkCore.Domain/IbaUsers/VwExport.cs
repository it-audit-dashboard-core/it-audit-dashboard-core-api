﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IT.Audit.Dashboard.EntityFrameworkCore.Domain.IbaUsers
{
    public partial class VwExport
    {
        public int ScheduledJobTypeId { get; set; }
        public DateTime? RunDate { get; set; }
        public string JobName { get; set; }
        public DateTime? ExportComplete { get; set; }
    }
}
