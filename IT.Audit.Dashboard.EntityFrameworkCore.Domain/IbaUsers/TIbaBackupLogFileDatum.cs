﻿using System;
using System.Collections.Generic;

#nullable disable

namespace IT.Audit.Dashboard.EntityFrameworkCore.Domain.IbaUsers
{
    public partial class TIbaBackupLogFileDatum
    {
        public int Id { get; set; }
        public int? BackupId { get; set; }
        public string RowData { get; set; }
    }
}
