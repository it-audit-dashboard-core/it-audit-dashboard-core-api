﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using IT.Audit.Dashboard.EntityFrameworkCore.Domain.EF;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore;
using System;


namespace IT.Audit.Dashboard.EntityFrameworkCore.Domain.EF.Configurations
{
    public partial class TAdaccountTypeConfiguration : IEntityTypeConfiguration<TAdaccountType>
    {
        public void Configure(EntityTypeBuilder<TAdaccountType> entity)
        {
            entity.Property(e => e.Id).ValueGeneratedNever();

            entity.Property(e => e.Description).IsUnicode(false);

            OnConfigurePartial(entity);
        }

        partial void OnConfigurePartial(EntityTypeBuilder<TAdaccountType> entity);
    }
}
