﻿using IT.Audit.Dashboard.EntityFrameworkCore.Domain.ItAuditDashboard;
using System.Threading.Tasks;

namespace IT.Audit.Dashboard.EntityFrameworkCore.Domain
{
    public interface ILoggingRepository : IGenericRepository<ErrorLog>
    {
        Task<int> ClearErrors();
    }
}
