﻿using IT.Audit.Dashboard.Domain.Shared;
using Microsoft.EntityFrameworkCore;

namespace IT.Audit.Dashboard.EntityFrameworkCore.Domain
{
    public abstract class RepositoryBase
    {
        protected readonly IDbContextProviderFactory DbContextProviderFactory;

        public RepositoryBase(IDbContextProviderFactory dbContextProviderFactory)
        {
            DbContextProviderFactory = dbContextProviderFactory;
        }

        public virtual DbContext GetDbContext(ConnectionType connectionType)
        {
            return DbContextProviderFactory.CreateDbContextProvider(connectionType)?.DbContext;
        }
    }
}
