﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using IT.Dashboard.EntityFramework.ActiveDirectoryUsersDb.Data;
using IT.Dashboard.EntityFramework.ActiveDirectoryUsersDb.Models;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore;
using System;


namespace IT.Dashboard.EntityFramework.ActiveDirectoryUsersDb.Data.Configurations
{
    public partial class VwActiveUsersWithoutManagersNpaExcludedConfiguration : IEntityTypeConfiguration<VwActiveUsersWithoutManagersNpaExcluded>
    {
        public void Configure(EntityTypeBuilder<VwActiveUsersWithoutManagersNpaExcluded> entity)
        {
            entity.ToView("vw_Active_Users_Without_Managers_NPA_Excluded");

            OnConfigurePartial(entity);
        }

        partial void OnConfigurePartial(EntityTypeBuilder<VwActiveUsersWithoutManagersNpaExcluded> entity);
    }
}
