﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using IT.Dashboard.EntityFramework.ActiveDirectoryUsersDb.Data;
using IT.Dashboard.EntityFramework.ActiveDirectoryUsersDb.Models;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore;
using System;


namespace IT.Dashboard.EntityFramework.ActiveDirectoryUsersDb.Data.Configurations
{
    public partial class VwInboxReportAccessConfiguration : IEntityTypeConfiguration<VwInboxReportAccess>
    {
        public void Configure(EntityTypeBuilder<VwInboxReportAccess> entity)
        {
            entity.ToView("vw_Inbox_Report_Access");

            entity.Property(e => e.AuthorisedUser).IsUnicode(false);

            entity.Property(e => e.DisplayName).IsUnicode(false);

            entity.Property(e => e.EmailAddress).IsUnicode(false);

            entity.Property(e => e.ReportName).IsUnicode(false);

            OnConfigurePartial(entity);
        }

        partial void OnConfigurePartial(EntityTypeBuilder<VwInboxReportAccess> entity);
    }
}
