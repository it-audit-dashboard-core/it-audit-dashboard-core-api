﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using IT.Dashboard.EntityFramework.ActiveDirectoryUsersDb.Data;
using IT.Dashboard.EntityFramework.ActiveDirectoryUsersDb.Models;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore;
using System;


namespace IT.Dashboard.EntityFramework.ActiveDirectoryUsersDb.Data.Configurations
{
    public partial class VwSecondaryMailboxUsersOldConfiguration : IEntityTypeConfiguration<VwSecondaryMailboxUsersOld>
    {
        public void Configure(EntityTypeBuilder<VwSecondaryMailboxUsersOld> entity)
        {
            entity.ToView("vw_Secondary_Mailbox_Users_old");

            entity.Property(e => e.OwnerAdStatus).IsUnicode(false);

            entity.Property(e => e.OwnerCompany).IsUnicode(false);

            entity.Property(e => e.OwnerEmail).IsUnicode(false);

            entity.Property(e => e.OwnerJobTitle).IsUnicode(false);

            entity.Property(e => e.OwnerName).IsUnicode(false);

            entity.Property(e => e.PrimaryUn).IsUnicode(false);

            entity.Property(e => e.SmAdStatus).IsUnicode(false);

            entity.Property(e => e.SmEmail).IsUnicode(false);

            entity.Property(e => e.SmName).IsUnicode(false);

            entity.Property(e => e.SmUn).IsUnicode(false);

            OnConfigurePartial(entity);
        }

        partial void OnConfigurePartial(EntityTypeBuilder<VwSecondaryMailboxUsersOld> entity);
    }
}
