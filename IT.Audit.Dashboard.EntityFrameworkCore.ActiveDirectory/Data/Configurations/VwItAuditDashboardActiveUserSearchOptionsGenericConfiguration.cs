﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using IT.Dashboard.EntityFramework.ActiveDirectoryUsersDb.Data;
using IT.Dashboard.EntityFramework.ActiveDirectoryUsersDb.Models;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore;
using System;


namespace IT.Dashboard.EntityFramework.ActiveDirectoryUsersDb.Data.Configurations
{
    public partial class VwItAuditDashboardActiveUserSearchOptionsGenericConfiguration : IEntityTypeConfiguration<VwItAuditDashboardActiveUserSearchOptionsGeneric>
    {
        public void Configure(EntityTypeBuilder<VwItAuditDashboardActiveUserSearchOptionsGeneric> entity)
        {
            entity.ToView("vw_IT_Audit_Dashboard_Active_User_Search_Options_Generic");

            entity.Property(e => e.ApplicationName).IsUnicode(false);

            entity.Property(e => e.SearchParameterName).IsUnicode(false);

            entity.Property(e => e.SearchRootPath).IsUnicode(false);

            entity.Property(e => e.UpdatedBy).IsUnicode(false);

            entity.Property(e => e.UseType).IsUnicode(false);

            OnConfigurePartial(entity);
        }

        partial void OnConfigurePartial(EntityTypeBuilder<VwItAuditDashboardActiveUserSearchOptionsGeneric> entity);
    }
}
