﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using IT.Dashboard.EntityFramework.ActiveDirectoryUsersDb.Data;
using IT.Dashboard.EntityFramework.ActiveDirectoryUsersDb.Models;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore;
using System;


namespace IT.Dashboard.EntityFramework.ActiveDirectoryUsersDb.Data.Configurations
{
    public partial class TAdSearchOptionConfiguration : IEntityTypeConfiguration<TAdSearchOption>
    {
        public void Configure(EntityTypeBuilder<TAdSearchOption> entity)
        {
            entity.Property(e => e.SearchParameterName).IsUnicode(false);

            entity.Property(e => e.SearchRootPath).IsUnicode(false);

            entity.Property(e => e.UpdatedBy).IsUnicode(false);

            OnConfigurePartial(entity);
        }

        partial void OnConfigurePartial(EntityTypeBuilder<TAdSearchOption> entity);
    }
}
