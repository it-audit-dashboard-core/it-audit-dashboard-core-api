﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using IT.Dashboard.EntityFramework.ActiveDirectoryUsersDb.Data;
using IT.Dashboard.EntityFramework.ActiveDirectoryUsersDb.Models;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore;
using System;


namespace IT.Dashboard.EntityFramework.ActiveDirectoryUsersDb.Data.Configurations
{
    public partial class VwInvalidOuRecordConfiguration : IEntityTypeConfiguration<VwInvalidOuRecord>
    {
        public void Configure(EntityTypeBuilder<VwInvalidOuRecord> entity)
        {
            entity.ToView("vw_Invalid_OU_Records");

            entity.Property(e => e.Department).IsUnicode(false);

            entity.Property(e => e.Desk).IsUnicode(false);

            entity.Property(e => e.DisplayName).IsUnicode(false);

            entity.Property(e => e.Division).IsUnicode(false);

            entity.Property(e => e.Domain).IsUnicode(false);

            entity.Property(e => e.FirstName).IsUnicode(false);

            entity.Property(e => e.LastName).IsUnicode(false);

            OnConfigurePartial(entity);
        }

        partial void OnConfigurePartial(EntityTypeBuilder<VwInvalidOuRecord> entity);
    }
}
