﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using IT.Dashboard.EntityFramework.ActiveDirectoryUsersDb.Data;
using IT.Dashboard.EntityFramework.ActiveDirectoryUsersDb.Models;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore;
using System;


namespace IT.Dashboard.EntityFramework.ActiveDirectoryUsersDb.Data.Configurations
{
    public partial class VwTodaysEtlRunConfiguration : IEntityTypeConfiguration<VwTodaysEtlRun>
    {
        public void Configure(EntityTypeBuilder<VwTodaysEtlRun> entity)
        {
            entity.ToView("vw_Todays_ETL_Run");

            entity.Property(e => e.MetricCheckStatus).IsUnicode(false);

            entity.Property(e => e.MetricDescription).IsUnicode(false);

            OnConfigurePartial(entity);
        }

        partial void OnConfigurePartial(EntityTypeBuilder<VwTodaysEtlRun> entity);
    }
}
