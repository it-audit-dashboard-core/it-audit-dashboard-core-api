﻿using Microsoft.EntityFrameworkCore;

#nullable disable

namespace IT.Dashboard.EntityFramework.ActiveDirectoryUsersDb.Data
{
    public partial class ActiveDirectoryUsersDbContext : DbContext
    {
        public ActiveDirectoryUsersDbContext(DbContextOptions<ActiveDirectoryUsersDbContext> options)
            : base(options)
        {
        }

        //public virtual DbSet<TAdActiveAdminUser> TAdActiveAdminUsers { get; set; }
        //public virtual DbSet<TAdActiveAdminUsersYesterday> TAdActiveAdminUsersYesterdays { get; set; }
        //public virtual DbSet<TAdActiveAndInactiveAdminUser> TAdActiveAndInactiveAdminUsers { get; set; }
        //public virtual DbSet<TAdActiveAndInactiveUser> TAdActiveAndInactiveUsers { get; set; }
        //public virtual DbSet<TAdActiveUser> TAdActiveUsers { get; set; }
        //public virtual DbSet<TAdActiveUsersManagementStructure> TAdActiveUsersManagementStructures { get; set; }
        //public virtual DbSet<TAdActiveUsersYesterday> TAdActiveUsersYesterdays { get; set; }
        //public virtual DbSet<TAdDisabledAdminUser> TAdDisabledAdminUsers { get; set; }
        //public virtual DbSet<TAdDisabledUser> TAdDisabledUsers { get; set; }
        //public virtual DbSet<TAdEtlMetric> TAdEtlMetrics { get; set; }
        //public virtual DbSet<TAdEtlMetricType> TAdEtlMetricTypes { get; set; }
        //public virtual DbSet<TAdImportSourceOrganisationalUnit> TAdImportSourceOrganisationalUnits { get; set; }
        //public virtual DbSet<TAdMoversArchive> TAdMoversArchives { get; set; }
        //public virtual DbSet<TAdNewAdminUser> TAdNewAdminUsers { get; set; }
        //public virtual DbSet<TAdNewUser> TAdNewUsers { get; set; }
        //public virtual DbSet<TAdNewUsersWithMissingDatum> TAdNewUsersWithMissingData { get; set; }
        //public virtual DbSet<TAdNonCoreActiveUser> TAdNonCoreActiveUsers { get; set; }
        //public virtual DbSet<TAdNonCoreActiveUsersYesterday> TAdNonCoreActiveUsersYesterdays { get; set; }
        //public virtual DbSet<TAdNonCoreDisabledUser> TAdNonCoreDisabledUsers { get; set; }
        //public virtual DbSet<TAdNonCoreEligibleArchiveAndDelete> TAdNonCoreEligibleArchiveAndDeletes { get; set; }
        //public virtual DbSet<TAdNonPersonExcludedAccount> TAdNonPersonExcludedAccounts { get; set; }
        //public virtual DbSet<TAdPrimaryUser> TAdPrimaryUsers { get; set; }
        //public virtual DbSet<TAdSearchApplication> TAdSearchApplications { get; set; }
        //public virtual DbSet<TAdSearchApplicationOption> TAdSearchApplicationOptions { get; set; }
        //public virtual DbSet<TAdSearchApplicationType> TAdSearchApplicationTypes { get; set; }
        //public virtual DbSet<TAdSearchOption> TAdSearchOptions { get; set; }
        //public virtual DbSet<TAdUserLookup> TAdUserLookups { get; set; }
        //public virtual DbSet<TEmailDistibution> TEmailDistibutions { get; set; }
        //public virtual DbSet<TEmeaAdminUser> TEmeaAdminUsers { get; set; }
        //public virtual DbSet<TEmeaDisabledUser> TEmeaDisabledUsers { get; set; }
        //public virtual DbSet<TEmeaDisabledUsersArchive> TEmeaDisabledUsersArchives { get; set; }
        //public virtual DbSet<TEmeaNonCoreApplicationUsageType> TEmeaNonCoreApplicationUsageTypes { get; set; }
        //public virtual DbSet<TEmeaNonCoreUser> TEmeaNonCoreUsers { get; set; }
        //public virtual DbSet<TEmeaNonCoreUserApplicationUsage> TEmeaNonCoreUserApplicationUsages { get; set; }
        //public virtual DbSet<TEmeaNonDefinedUser> TEmeaNonDefinedUsers { get; set; }
        //public virtual DbSet<TEmeaUser> TEmeaUsers { get; set; }
        //public virtual DbSet<TEmeaUsersEmployeeToManagerStructure> TEmeaUsersEmployeeToManagerStructures { get; set; }
        //public virtual DbSet<TIdmEmeaOuReportExclusion> TIdmEmeaOuReportExclusions { get; set; }
        //public virtual DbSet<TIdmEmeaSecondaryManager> TIdmEmeaSecondaryManagers { get; set; }
        //public virtual DbSet<TInboxReportType> TInboxReportTypes { get; set; }
        //public virtual DbSet<TInboxReportUser> TInboxReportUsers { get; set; }
        //public virtual DbSet<TManagerContractorSplitReportRecipient> TManagerContractorSplitReportRecipients { get; set; }
        //public virtual DbSet<TManagerReport> TManagerReports { get; set; }
        //public virtual DbSet<TMissingManagerOuToLocation> TMissingManagerOuToLocations { get; set; }
        //public virtual DbSet<TMissingManagersTest> TMissingManagersTests { get; set; }
        //public virtual DbSet<NonEmeaManager> TNonEmeaManagers { get; set; }
        //public virtual DbSet<TTodaysEtlRun> TTodaysEtlRuns { get; set; }
        //public virtual DbSet<TodaysEtlRunChecksArchive> TTodaysEtlRunChecksArchives { get; set; }
        //public virtual DbSet<VwActiveAndInactiveEmeaAdminUserList> VwActiveAndInactiveEmeaAdminUserLists { get; set; }
        //public virtual DbSet<VwActiveAndInactiveEmeaUserList> VwActiveAndInactiveEmeaUserLists { get; set; }
        //public virtual DbSet<VwActiveAndInactiveLiveNonCoreUserList> VwActiveAndInactiveLiveNonCoreUserLists { get; set; }
        //public virtual DbSet<VwActiveAndInactiveNonCoreUserList> VwActiveAndInactiveNonCoreUserLists { get; set; }
        //public virtual DbSet<VwActiveEmeaUserList> VwActiveEmeaUserLists { get; set; }
        //public virtual DbSet<VwActiveManagerList> VwActiveManagerLists { get; set; }
        //public virtual DbSet<VwActiveNewUsersWithoutManager> VwActiveNewUsersWithoutManagers { get; set; }
        //public virtual DbSet<VwActiveNonCoreUserList> VwActiveNonCoreUserLists { get; set; }
        //public virtual DbSet<VwActiveNonPersonableAccount> VwActiveNonPersonableAccounts { get; set; }
        //public virtual DbSet<VwActiveUsersWithManager> VwActiveUsersWithManagers { get; set; }
        //public virtual DbSet<VwActiveUsersWithoutManager> VwActiveUsersWithoutManagers { get; set; }
        //public virtual DbSet<VwActiveUsersWithoutManagersNpaExcluded> VwActiveUsersWithoutManagersNpaExcludeds { get; set; }
        //public virtual DbSet<VwContractor> VwContractors { get; set; }
        //public virtual DbSet<VwDisabledEmeaUsersArchive> VwDisabledEmeaUsersArchives { get; set; }
        //public virtual DbSet<VwEmeaAccountsWithoutManager> VwEmeaAccountsWithoutManagers { get; set; }
        //public virtual DbSet<VwEmeaDisabledUsersArchiveAllAccount> VwEmeaDisabledUsersArchiveAllAccounts { get; set; }
        //public virtual DbSet<VwEmployeeToManagerStructure> VwEmployeeToManagerStructures { get; set; }
        //public virtual DbSet<VwEmployeesWithManager> VwEmployeesWithManagers { get; set; }
        //public virtual DbSet<VwIdmEmeaSecondaryManager> VwIdmEmeaSecondaryManagers { get; set; }
        //public virtual DbSet<VwInboxReportAccess> VwInboxReportAccesses { get; set; }
        //public virtual DbSet<VwInboxReportAccessSummary> VwInboxReportAccessSummaries { get; set; }
        //public virtual DbSet<VwInvalidOuRecord> VwInvalidOuRecords { get; set; }
        //public virtual DbSet<VwItAuditDashboardActiveUserSearchOption> VwItAuditDashboardActiveUserSearchOptions { get; set; }
        //public virtual DbSet<VwItAuditDashboardActiveUserSearchOptionsGeneric> VwItAuditDashboardActiveUserSearchOptionsGenerics { get; set; }
        //public virtual DbSet<VwMissingOrDisabledManager> VwMissingOrDisabledManagers { get; set; }
        //public virtual DbSet<VwOrphanedManager> VwOrphanedManagers { get; set; }
        //public virtual DbSet<VwSecondaryMailboxUser> VwSecondaryMailboxUsers { get; set; }
        //public virtual DbSet<VwSecondaryMailboxUsersOld> VwSecondaryMailboxUsersOlds { get; set; }
        //public virtual DbSet<VwTodaysEtlRun> VwTodaysEtlRuns { get; set; }
        //public virtual DbSet<VwUnknownNonPersonableAccount> VwUnknownNonPersonableAccounts { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "SQL_Latin1_General_CP1_CI_AS");

            //modelBuilder.ApplyConfiguration(new Configurations.TAdActiveAdminUserConfiguration());
            //modelBuilder.ApplyConfiguration(new Configurations.TAdActiveAdminUsersYesterdayConfiguration());
            //modelBuilder.ApplyConfiguration(new Configurations.TAdActiveAndInactiveAdminUserConfiguration());
            //modelBuilder.ApplyConfiguration(new Configurations.TAdActiveAndInactiveUserConfiguration());
            //modelBuilder.ApplyConfiguration(new Configurations.TAdActiveUserConfiguration());
            //modelBuilder.ApplyConfiguration(new Configurations.TAdActiveUsersManagementStructureConfiguration());
            //modelBuilder.ApplyConfiguration(new Configurations.TAdActiveUsersYesterdayConfiguration());
            //modelBuilder.ApplyConfiguration(new Configurations.TAdDisabledAdminUserConfiguration());
            //modelBuilder.ApplyConfiguration(new Configurations.TAdDisabledUserConfiguration());
            //modelBuilder.ApplyConfiguration(new Configurations.TAdEtlMetricConfiguration());
            //modelBuilder.ApplyConfiguration(new Configurations.TAdEtlMetricTypeConfiguration());
            //modelBuilder.ApplyConfiguration(new Configurations.TAdImportSourceOrganisationalUnitConfiguration());
            //modelBuilder.ApplyConfiguration(new Configurations.TAdMoversArchiveConfiguration());
            //modelBuilder.ApplyConfiguration(new Configurations.TAdNewAdminUserConfiguration());
            //modelBuilder.ApplyConfiguration(new Configurations.TAdNewUserConfiguration());
            //modelBuilder.ApplyConfiguration(new Configurations.TAdNewUsersWithMissingDatumConfiguration());
            //modelBuilder.ApplyConfiguration(new Configurations.TAdNonCoreActiveUserConfiguration());
            //modelBuilder.ApplyConfiguration(new Configurations.TAdNonCoreActiveUsersYesterdayConfiguration());
            //modelBuilder.ApplyConfiguration(new Configurations.TAdNonCoreDisabledUserConfiguration());
            //modelBuilder.ApplyConfiguration(new Configurations.TAdNonPersonExcludedAccountConfiguration());
            //modelBuilder.ApplyConfiguration(new Configurations.TAdSearchApplicationConfiguration());
            //modelBuilder.ApplyConfiguration(new Configurations.TAdSearchApplicationTypeConfiguration());
            //modelBuilder.ApplyConfiguration(new Configurations.TAdSearchOptionConfiguration());
            //modelBuilder.ApplyConfiguration(new Configurations.TAdUserLookupConfiguration());
            //modelBuilder.ApplyConfiguration(new Configurations.TEmailDistibutionConfiguration());
            //modelBuilder.ApplyConfiguration(new Configurations.TEmeaNonCoreApplicationUsageTypeConfiguration());
            //modelBuilder.ApplyConfiguration(new Configurations.TEmeaNonCoreUserApplicationUsageConfiguration());
            //modelBuilder.ApplyConfiguration(new Configurations.TEmeaUsersEmployeeToManagerStructureConfiguration());
            //modelBuilder.ApplyConfiguration(new Configurations.TIdmEmeaOuReportExclusionConfiguration());
            //modelBuilder.ApplyConfiguration(new Configurations.TIdmEmeaSecondaryManagerConfiguration());
            //modelBuilder.ApplyConfiguration(new Configurations.TInboxReportTypeConfiguration());
            //modelBuilder.ApplyConfiguration(new Configurations.TInboxReportUserConfiguration());
            //modelBuilder.ApplyConfiguration(new Configurations.TManagerContractorSplitReportRecipientConfiguration());
            //modelBuilder.ApplyConfiguration(new Configurations.TManagerReportConfiguration());
            //modelBuilder.ApplyConfiguration(new Configurations.TMissingManagerOuToLocationConfiguration());
            //modelBuilder.ApplyConfiguration(new Configurations.TMissingManagersTestConfiguration());
            //modelBuilder.ApplyConfiguration(new Configurations.TNonEmeaManagerConfiguration());
            //modelBuilder.ApplyConfiguration(new Configurations.VwActiveAndInactiveEmeaAdminUserListConfiguration());
            //modelBuilder.ApplyConfiguration(new Configurations.VwActiveAndInactiveEmeaUserListConfiguration());
            //modelBuilder.ApplyConfiguration(new Configurations.VwActiveAndInactiveLiveNonCoreUserListConfiguration());
            //modelBuilder.ApplyConfiguration(new Configurations.VwActiveAndInactiveNonCoreUserListConfiguration());
            //modelBuilder.ApplyConfiguration(new Configurations.VwActiveEmeaUserListConfiguration());
            //modelBuilder.ApplyConfiguration(new Configurations.VwActiveManagerListConfiguration());
            //modelBuilder.ApplyConfiguration(new Configurations.VwActiveNewUsersWithoutManagerConfiguration());
            //modelBuilder.ApplyConfiguration(new Configurations.VwActiveNonCoreUserListConfiguration());
            //modelBuilder.ApplyConfiguration(new Configurations.VwActiveNonPersonableAccountConfiguration());
            //modelBuilder.ApplyConfiguration(new Configurations.VwActiveUsersWithManagerConfiguration());
            //modelBuilder.ApplyConfiguration(new Configurations.VwActiveUsersWithoutManagerConfiguration());
            //modelBuilder.ApplyConfiguration(new Configurations.VwActiveUsersWithoutManagersNpaExcludedConfiguration());
            //modelBuilder.ApplyConfiguration(new Configurations.VwContractorConfiguration());
            //modelBuilder.ApplyConfiguration(new Configurations.VwDisabledEmeaUsersArchiveConfiguration());
            //modelBuilder.ApplyConfiguration(new Configurations.VwEmeaAccountsWithoutManagerConfiguration());
            //modelBuilder.ApplyConfiguration(new Configurations.VwEmeaDisabledUsersArchiveAllAccountConfiguration());
            //modelBuilder.ApplyConfiguration(new Configurations.VwEmployeeToManagerStructureConfiguration());
            //modelBuilder.ApplyConfiguration(new Configurations.VwEmployeesWithManagerConfiguration());
            //modelBuilder.ApplyConfiguration(new Configurations.VwIdmEmeaSecondaryManagerConfiguration());
            //modelBuilder.ApplyConfiguration(new Configurations.VwInboxReportAccessConfiguration());
            //modelBuilder.ApplyConfiguration(new Configurations.VwInboxReportAccessSummaryConfiguration());
            //modelBuilder.ApplyConfiguration(new Configurations.VwInvalidOuRecordConfiguration());
            //modelBuilder.ApplyConfiguration(new Configurations.VwItAuditDashboardActiveUserSearchOptionConfiguration());
            //modelBuilder.ApplyConfiguration(new Configurations.VwItAuditDashboardActiveUserSearchOptionsGenericConfiguration());
            //modelBuilder.ApplyConfiguration(new Configurations.VwMissingOrDisabledManagerConfiguration());
            //modelBuilder.ApplyConfiguration(new Configurations.VwOrphanedManagerConfiguration());
            //modelBuilder.ApplyConfiguration(new Configurations.VwSecondaryMailboxUserConfiguration());
            //modelBuilder.ApplyConfiguration(new Configurations.VwSecondaryMailboxUsersOldConfiguration());
            //modelBuilder.ApplyConfiguration(new Configurations.VwTodaysEtlRunConfiguration());
            //modelBuilder.ApplyConfiguration(new Configurations.VwUnknownNonPersonableAccountConfiguration());
            //OnModelCreatingGeneratedFunctions(modelBuilder);
            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
