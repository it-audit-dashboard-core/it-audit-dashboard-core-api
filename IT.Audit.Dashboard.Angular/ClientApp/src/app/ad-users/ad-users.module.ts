import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdUsersRoutingModule } from './ad-users-routing.module';
import { AdUsersComponent } from './ad-users.component';


@NgModule({
  declarations: [
    AdUsersComponent
  ],
  imports: [
    CommonModule,
    AdUsersRoutingModule
  ]
})
export class AdUsersModule { }
