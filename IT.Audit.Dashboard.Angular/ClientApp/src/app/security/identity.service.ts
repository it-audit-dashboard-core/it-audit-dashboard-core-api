import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { IdentityUserBase } from '../shared/security/identity-user-base';
import { IdentityUser } from './identity-user';


@Injectable({
  providedIn: 'root'
})
export class IdentityService {
  private uri: string = "https://localhost:44317/Account/";

  securityObject: IdentityUser = new IdentityUser();

  constructor(private http: HttpClient) { }

  /*
    Get the windows authentication from .NET Core
  */
  get(): Observable<IdentityUserBase> {
    return this.http
      .get<IdentityUserBase>(this.uri + "GetAuthenticatedUser")
      .pipe(
        map((response: IdentityUserBase) => {
          console.log('GetAuthenticatedUser() response: ')
          return response;
        })
      )
  }

  // private getFromCacheOrDatabase(userName: string): Observable<IdentityUser> {
  //   let user = localStorage.getItem(userName);

  //   if (user) {
  //     console.log("USER FROM CACHE!");
  //     let u = JSON.parse(user);
  //     console.log(of(u));
  //     return of(u);
  //   }
  //   else
  //   {
  //     let identityUser = this.get();
  //     localStorage.setItem(userName, JSON.stringify(identityUser)); 
    
  //     return identityUser;
  //   }
  //}

  login(entity: IdentityUserBase): Observable<IdentityUser> {
    this.securityObject.username = entity.username;

    switch (entity.username.toLowerCase()) {
      case "ribone":
        this.securityObject.canAccesssIbaUsers = true;
        break;
      case "afooks":
        this.securityObject.canAccesssADUsers = true;
        break;
      default:
        this.securityObject.username = "Invalid user name or password";
        break;
    }

    return of(this.securityObject);
  }

  logout(): void {
    this.securityObject.init();
  }
}
