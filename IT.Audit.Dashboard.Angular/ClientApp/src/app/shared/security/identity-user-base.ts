export class IdentityUserBase {
    username: string = "";
    bearerToken: string = "";
    isAuthenticated: boolean = false;
    authenticationType: string = "";

    init(): void{
        this.username = "";
        this.bearerToken = "";
        this.isAuthenticated = false;
        this.authenticationType = "";
    }
}