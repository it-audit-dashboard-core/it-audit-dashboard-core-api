import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ApplicationsMenuComponent } from './components/applications-menu/applications-menu.component';
import { DataSourcesComponent } from './components/data-sources/data-sources.component';
import { ConfigurationService } from './configuration/configuration.service';



@NgModule({
  declarations: [    
    ApplicationsMenuComponent,
    DataSourcesComponent
  ],
  imports: [
    CommonModule
  ],
  providers:[
    ConfigurationService
  ]
})
export class SharedModule { }
