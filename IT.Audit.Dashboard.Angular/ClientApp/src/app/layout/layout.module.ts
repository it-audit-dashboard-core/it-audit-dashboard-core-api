import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from "@angular/router";

import { LayoutComponent } from './layout.component';
import { PageFooterComponent } from './page-footer/page-footer.component';
import { PageTitleComponent } from './page-title/page-title.component';
import { SharedModule } from '../shared/shared.module';


@NgModule({
  declarations: [
    LayoutComponent,
    PageFooterComponent,
    PageTitleComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    SharedModule
  ]
})
export class LayoutModule { }
