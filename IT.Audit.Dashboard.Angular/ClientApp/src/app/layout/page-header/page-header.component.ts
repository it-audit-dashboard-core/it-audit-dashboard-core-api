import { AfterViewInit, Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
//import { IdentityUser } from '../../models/identity-user';
//import { ValidatedResultDto, ValidationFailureDto } from '../../models/validator-result';
//import { DataInformationOutputDto, DataSourceDto, UserApplicationDto } from '../../models/user-application';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { IdentityUserBase } from 'src/app/shared/security/identity-user-base';
import { IdentityService } from 'src/app/security/identity.service';
import { IdentityUser } from 'src/app/security/identity-user';

@Component({
  selector: 'app-page-header',
  templateUrl: './page-header.component.html',
  styleUrls: ['./page-header.component.css']
})
export class PageHeaderComponent implements AfterViewInit {
  environmentSettingsForm!: FormGroup;
  //@Input() identityUser!: IdentityUser;
  @Input() userName: string = "";
  userNameForm!: FormGroup;
  userNameControl!: FormControl;
  environmentMachine: FormControl | undefined;

  //identityUser!: IdentityUserBase;
  // userApplicationResultDto!: ValidatedResultDto<UserApplicationDto>;
  // dataInformationResultDto!: ValidatedResultDto<DataInformationOutputDto>;
  // dataSources!: DataSourceDto[];
  // validationFailures!: ValidationFailureDto[];
  //userName: string | undefined;

  constructor(
    private identityService: IdentityService,
    private formBuilder: FormBuilder) {

  }
  ngAfterViewInit(): void {
    // console.log('PH: ' + this.identityUser);
    // this.userName = this.identityUser?.username;
  }

  ngOnInit(): void {
    this.createForm();
    this.createUserNameForm();
  }

  createUserNameForm() {
    this.userNameForm = this.formBuilder.group({
      userNameControl: new FormControl('')
    });
  }

  createForm() {
    this.environmentSettingsForm = this.formBuilder.group({
      environmentMachine: new FormControl(''),
    });
  }

  onChange(e: any) {
    //console.log(e.target?.value);
  }
}
