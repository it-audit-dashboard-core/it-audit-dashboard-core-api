import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IbaUsersComponent } from './iba-users.component';
import { IbaUsersRoutingModule } from './iba-users-routing.module';



@NgModule({
  declarations: [
    IbaUsersComponent
  ],
  imports: [
    CommonModule,
    IbaUsersRoutingModule
  ]
})
export class IbaUsersModule { }
