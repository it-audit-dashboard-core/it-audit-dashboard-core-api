﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IT.Audit.Dashboard.Angular.Models
{
    public class ApplicationUser : IdentityUser
    {
    }
}
