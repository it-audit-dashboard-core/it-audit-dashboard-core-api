﻿using IT.Audit.Dashboard.Application.Contracts.Account;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace IT.Audit.Dashboard.SSL.Api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    [EnableCors("IT.Audit.Dashboard.Origin")]
    public class ReportPackController : ControllerBase
    {
        private readonly IUserAppService _userAppService;

        public ReportPackController(IUserAppService userAppService)
        {
            _userAppService = userAppService;
        }

    }
}
