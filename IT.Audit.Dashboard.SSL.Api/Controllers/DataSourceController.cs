﻿using IT.Audit.Dashboard.Application;
using IT.Audit.Dashboard.Application.Dtos;
using IT.Audit.Dashboard.SSL.Api.Helpers;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace IT.Audit.Dashboard.SSL.Api.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("IT.Audit.Dashboard.Origin")]
    public class DataSourceController : ControllerBase
    {
        private readonly IDataSourceAppService _dataSourceAppService;

        public DataSourceController(IDataSourceAppService dataSourceAppService)
        {
            _dataSourceAppService = dataSourceAppService;
        }

        [HttpGet]
        public async Task<DataSourceOutputDto> GetAsync(string controllerName)
        {
            return await _dataSourceAppService.GetAsync(controllerName);
        }
    }
}
