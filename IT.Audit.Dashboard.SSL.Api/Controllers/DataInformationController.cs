﻿using AutoMapper;
using IT.Audit.Dashboard.Application.Validators;
using IT.Audit.Dashboard.Application;
using IT.Audit.Dashboard.Application.Dtos;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using IT.Audit.Dashboard.Domain.Shared.Helpers;
using IT.Audit.Dashboard.Application.Contracts.Account;
using IT.Audit.Dashboard.SSL.Api.Helpers;

namespace IT.Audit.Dashboard.SSL.Api.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("IT.Audit.Dashboard.Origin")]
    public class DataInformationController : ControllerBase
    {
        private readonly IDataInformationAppService _dataInformationAppService;
        private readonly IUserAppService _userAppService;
        private readonly IMapper _mapper;

        public DataInformationController(IDataInformationAppService dataInformationAppService, IUserAppService userAppService, IMapper mapper)
        {
            _dataInformationAppService = dataInformationAppService;
            _userAppService = userAppService;
            _mapper = mapper;
        }

        [HttpGet("key")]
        public async Task<ValidatedResultDto<DataInformationOutputDto>> Get(string key)
        {
            //var result = await _userAppService.GetAuthenticatedUser();
            //if (IsUserInRole.IsInGroup("G-EMEA-ITAuditDashboard-ActiveDirectoryUsersView-UAT"))
            //    Console.WriteLine("true");

            var inputDto = new DataInformationInputDto { 
                ConnectionType = DatabaseHelper.GetConnectionType(key) };

            var outputDto = await _dataInformationAppService.GetAsync(inputDto);

            var validator = new ValidatedResult<DataInformationOutputDto>(_mapper)
                .Compile(new DataInformationOutputValidator(), outputDto);

            return validator;
        }
    }
}
