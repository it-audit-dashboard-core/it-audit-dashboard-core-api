﻿using IT.Audit.Dashboard.Application;
using IT.Audit.Dashboard.Application.Dtos;
using IT.Audit.Dashboard.Domain;
using IT.Audit.Dashboard.Domain.Helpers;
using IT.Audit.Dashboard.Domain.Shared;
using IT.Audit.Dashboard.Sql.Domain;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IT.Audit.Dashboard.SSL.Api.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("IT.Audit.Dashboard.Origin")]
    public class IbaUsersController : ControllerBase
    {
        private readonly IDataInformationAppService _dataInformationAppService;
        private readonly IIbaUsersAppService _ibaUsersAppService;
        private readonly IIbaUsersRepository _ibaUsersRepository;
        private IMemoryCache _cache;

        public IbaUsersController(
            IDataInformationAppService dataInformationAppService,
            IIbaUsersAppService ibaUsersAppService, 
            IIbaUsersRepository ibaUsersRepository,
            IMemoryCache cache)
        {
            _dataInformationAppService = dataInformationAppService;
            _ibaUsersAppService = ibaUsersAppService;
            _ibaUsersRepository = ibaUsersRepository;
            _cache = cache;
        }

        [HttpGet("LoginSpaces")]
        public IActionResult LoginSpaces()
        {
            StringBuilder sb = new StringBuilder();

            //List<IbaUser> loginSpaces =
            //    _dataSourceRepository.Get(ConnectionProviderFactory.CreateConnectionProviderFactory9)
            //        .Where(au => au.IsActive && au.Login != null && au.Login.Contains(' '))
            //        .OrderBy(au => au.Name)
            //        .ToList();

            ////header
            //if (loginSpaces.Count > 0)
            //    sb.AppendLine(
            //        $"<b>There {(loginSpaces.Count > 1 ? "are" : "is")} {loginSpaces.Count} login{(loginSpaces.Count > 1 ? "s" : "")} having an unintended space character within it.</b><br/><br/>");

            ////logins
            //foreach (IbaUser au in loginSpaces)
            //{
            //    sb.AppendLine($"<li>{au.Name} - {au.Instance} - '{au.Login}'</li>");
            //}

            // return Content(sb.ToString());

            throw new NotImplementedException($"Need to work on! {nameof(LoginSpaces)}");
        }

        [HttpGet("FilterControls")]
        public async Task<IActionResult> FilterControls()
        {
            var filterData = await _ibaUsersAppService.GetFilterData();

            return Ok(filterData);
        }

        [HttpGet("GetInstancesRolesSecurities")]
        public async Task<IActionResult> GetInstancesRolesSecurities()
        {
            var filterData = await _ibaUsersAppService.GetFilterData();

            //TODO: Add to DomainManager
            var instanceSecurityRoleDto = new InstanceSecurityRoleDto();

            #region AddInstances

            instanceSecurityRoleDto.InstanceResult = AddInstances(filterData);

            #endregion // AddInstances

            #region AddSecurities

            instanceSecurityRoleDto.SecurityResult = AddSecurities(filterData);

            #endregion // AddSecurities

            #region AddRoles

            instanceSecurityRoleDto.RoleResult = AddRoles(filterData);

            #endregion // AddRoles

            return Ok(instanceSecurityRoleDto);
        }

        private ParentResultDto AddRoles(FilterDataDto filterData)
        {
            int count = 0;

            var parent = new ParentResultDto("Roles");
            parent.Children.Add(new ChildDto("Select All", "Select All", true, false, string.Empty, false));

            foreach (var i in filterData.Instances)
            {
                List<FilterRole> filterDataRoles = filterData.Roles.Where(s => s.Instance.Name == i.Name).OrderBy(s => s.Name).ToList();

                parent.Children.Add(new ChildDto(string.Empty, string.Empty, true, false, i.Name, true));

                foreach (var r in filterDataRoles)
                {
                    parent.Children.Add(new ChildDto(r.Name, $"{i.Name}~{r.Name}", true, false, i.Name, false));
                    count++;
                };
            }

            parent.Count = count;
            return parent;
        }

        private ParentResultDto AddSecurities(FilterDataDto filterData)
        {
            int count = 0;

            var parent = new ParentResultDto("Securities");
            parent.Children.Add(new ChildDto("Select All", "Select All", true, false, string.Empty, false));

            foreach (var i in filterData.Instances)
            {
                List<FilterSecurity> filterDataSecurities = filterData.Securities.Where(s => s.Instance.Name == i.Name).OrderBy(s => s.Name).ToList();

                parent.Children.Add(new ChildDto(string.Empty, string.Empty, true, false, i.Name, true));

                foreach (var r in filterDataSecurities)
                {
                    parent.Children.Add(new ChildDto(r.Name, $"{i.Name}~{r.Name}", true, false, i.Name, false));
                    count++;
                };
            }

            parent.Count = filterData.Securities.Count;
            return parent;
        }

        private ParentResultDto AddInstances(FilterDataDto filterData)
        {
            var parent = new ParentResultDto("Instances");
            parent.Children.Add(new ChildDto("Select All", "Select All", true, false, string.Empty, false));
            parent.Children.Add(new ChildDto(string.Empty, string.Empty, true, false, "Instances", true));
            foreach (var i in filterData.Instances)
            {
                parent.Children.Add(new ChildDto(i.Name, i.Name, true, false, string.Empty, false));
            };

            return parent;
        }

        [HttpGet("GetInstances")]
        public async Task<IActionResult> GetInstances()
        {
            var filterData = await _ibaUsersAppService.GetFilterData();

            #region expected json format

            /*
                        var optgroups = [
                    {
                        label: 'Group 1', children: [
                            { label: 'Option 1.1', value: '1-1', selected: true },
                            { label: 'Option 1.2', value: '1-2' },
                            { label: 'Option 1.3', value: '1-3' }
                        ]
                    },
                    {
                        label: 'Group 2', children: [
                            { label: 'Option 2.1', value: '1' },
                            { label: 'Option 2.2', value: '2' },
                            { label: 'Option 2.3', value: '3', disabled: true }
                        ]
                    }
            ];
            */

            #endregion

            var instances = new List<Group>();

            var instance = new Group { label = "Instances" };

            filterData.Instances.ForEach(f =>
            {
                Child child = new Child
                {
                    label = f.Name,
                    value = f.Name,
                    selected = true
                };

                instance.children.Add(child);
            });

            instances.Add(instance);

            return Ok(instances);
        }

        [HttpGet("GetSecurities")]
        public async Task<IActionResult> GetSecurities()
        {
            var filterData = await _ibaUsersAppService.GetFilterData();

            List<Group> securities = new List<Group>();

            foreach (var instance in filterData.Instances)
            {
                Group instanceGroup = new Group { label = instance.Name };

                //get all securities
                foreach (var security in filterData.Securities.Where(f => f.Instance.Name == instance.Name).OrderBy(f => f.Name))
                {
                    Child child = new Child
                    {
                        label = security.Name,
                        value = $"{instance.Name}~{security.Name}",
                        selected = true
                    };

                    instanceGroup.children.Add(child);
                }

                securities.Add(instanceGroup);
            }

            return Ok(securities);
        }

        [HttpGet("GetRoles")]
        public async Task<IActionResult> GetRoles()
        {
            var filterData = await _ibaUsersAppService.GetFilterData();

            List<Group> roles = new List<Group>();

            foreach (var instance in filterData.Instances)
            {
                Group instanceGroup = new Group { label = instance.Name };

                //get all roles
                foreach (FilterRole role in filterData.Roles.Where(f => f.Instance.Name == instance.Name).OrderBy(f => f.Name))
                {
                    Child child = new Child { label = role.Name, value = $"{instance.Name}~{role.Name}", selected = true };

                    instanceGroup.children.Add(child);
                }

                roles.Add(instanceGroup);
            }

            return Ok(roles);
        }

        [HttpPost("Search")]
        public async Task<IActionResult> Search([FromBody] IbaSearchFilterDto postData)
        {
            var filteredUsers = await GetFilteredUsers(postData);

            return Ok(filteredUsers);
        }

        [HttpPost("CreateDownload")]
        public async Task<IActionResult> CreateDownload(IbaSearchFilterDto postData)
        {
            var filteredUsers = await GetFilteredUsers(postData);

            var search = (postData.Search ?? string.Empty).Trim().ToUpper().Replace("\"", string.Empty);
            
            if (search != string.Empty)
            {
                filteredUsers = (from fu in filteredUsers
                                 where
                                (fu.Name ?? "").ToUpper().Contains(search) ||
                                (fu.Instance ?? "").ToUpper().Contains(search) ||
                                (fu.Security ?? "").ToUpper().Contains(search) ||
                                (fu.Role ?? "").ToUpper().Contains(search) ||
                                (fu.Login ?? "").ToUpper().Contains(search) ||
                                fu.IsActive.ToString().ToUpper().Contains(search) ||
                                (fu.UpdatedBy ?? "").ToUpper().Contains(search) ||
                                fu.Updated.ToString("dd/MM/yyyy").Contains(search) ||
                                fu.Disabled.ToString().ToUpper().Contains(search) ||
                                (fu.ServiceDeskRef ?? "").ToUpper().Contains(search) ||
                                fu.CreatedDate.ToString("dd/MM/yyyy").Contains(search) ||
                                (fu.CreatedRef ?? "").ToUpper().Contains(search) ||
                                (fu.CreatedUser ?? "").ToUpper().Contains(search) ||
                                (fu.UserName ?? "").ToUpper().Contains(search) ||
                                (fu.ADOU ?? "").ToUpper().Contains(search) ||
                                (fu.AccountStatus ?? "").ToUpper().Contains(search)
                                 select fu).ToList();

            }

            //populate template and download
            StringBuilder sb = CreateReport(filteredUsers, User.Identity.Name);

            string id = Guid.NewGuid().ToString();

            // TODO: Cache
            //HttpContext.Request.Cache.Insert(id, sb, null, DateTime.Now.AddSeconds(20), TimeSpan.Zero);

            //return a cache id  to client, which will call GetDownload returning the ID for file retrieval and download
            return Ok(id);
        }

        [HttpGet("GetDownload")]
        public IActionResult GetDownload(string id)
        {
            //get document from cache
            var sb = new StringBuilder();// HttpContext.Cache[id] as StringBuilder;

            if (sb != null)
            {
                //return data    
                string contentType = "application/vnd.ms-excel";
                byte[] bytes = Encoding.UTF8.GetBytes(sb.ToString());

                FileContentResult result = new FileContentResult(bytes, contentType)
                {
                    FileDownloadName = $"IBA_Users_Report_{DateTime.Now:yyyyMMdd_HHmmss}.xml"
                };

                return result;
            }

            return StatusCode(StatusCodes.Status500InternalServerError, "sb is null! RB");
        }

        [HttpPost("GetFilteredUsers")]
        public async Task<List<IbaUserDto>> GetFilteredUsers([FromBody] IbaSearchFilterDto postData)
        {
            //get data
            var users = await _ibaUsersAppService.GetAsync();

            #region filter data

            var predicate = PredicateBuilder.True<IbaUserDto>();

            //name
            if (!string.IsNullOrEmpty(postData.Name))
            {
                predicate = predicate.And(p => p.Name.ToLower().Contains(postData.Name.ToLower()));
            }

            //login
            if (!string.IsNullOrEmpty(postData.Login))
            {
                predicate = predicate.And(p => p.Login.ToLower().Contains(postData.Login.ToLower()));
            }

            //updated by
            if (!string.IsNullOrEmpty(postData.UpdatedBy))
            {
                predicate = predicate.And(p => p.UpdatedBy.ToLower().Contains(postData.UpdatedBy.ToLower()));
            }

            //updated from and to
            if (postData.UpdatedFromDate.HasValue && postData.UpdatedToDate.HasValue)
            {
                predicate =
                    predicate.And(
                        p => p.Updated >= postData.UpdatedFromDate.Value && p.Updated <= postData.UpdatedToDate);
            }

            //updated from 
            if (postData.UpdatedFromDate.HasValue && !postData.UpdatedToDate.HasValue)
            {
                predicate = predicate.And(p => p.Updated >= postData.UpdatedFromDate.Value);
            }

            //updated to
            if (!postData.UpdatedFromDate.HasValue && postData.UpdatedToDate.HasValue)
            {
                predicate = predicate.And(p => p.Updated <= postData.UpdatedToDate);
            }

            //active
            if (!string.IsNullOrEmpty(postData.Active))
            {
                switch (postData.ActiveFlag)
                {
                    case IbaUserActiveType.Active:
                        predicate = predicate.And(p => p.IsActive);
                        break;
                    case IbaUserActiveType.Inactive:
                        predicate = predicate.And(p => !p.IsActive);
                        break;
                }
            }

            //instance - not required as security and role must always be supplied
            /* if (postData.Instances.Count > 0)
            {
                predicate = predicate.And(p => postData.Instances.Contains(p.Instance));
            }*/

            //security
            if (postData.Securities != null)
            {
                if (postData.Securities.Count > 0)
                {
                    var inner = PredicateBuilder.False<IbaUserDto>();

                    foreach (string item in postData.Securities)
                    {
                        string[] arr = item.Split('~');

                        string instance = arr[0];
                        string security = arr[1];

                        inner = inner.Or(p => p.Instance == instance && p.Security == security);
                    }

                    predicate = predicate.And(inner);
                }
            }

            //role
            if (postData.Roles != null)
            {
                if (postData.Roles.Count > 0)
                {
                    var inner = PredicateBuilder.False<IbaUserDto>();

                    foreach (string item in postData.Roles)
                    {
                        string[] arr = item.Split('~');

                        string instance = arr[0];
                        string role = arr[1];

                        inner = inner.Or(p => p.Instance == instance && p.Role == role);
                    }

                    predicate = predicate.And(inner);
                }
            }

            //Disabled
            if (!string.IsNullOrEmpty(postData.Disabled))
            {
                switch (postData.DisabledFlag)
                {
                    case IbaUserActiveType.Active:
                        predicate = predicate.And(p => !p.Disabled);
                        break;
                    case IbaUserActiveType.Inactive:
                        predicate = predicate.And(p => p.Disabled);
                        break;
                }
            }

            //ServiceDesk Ref
            if (!string.IsNullOrEmpty(postData.ServiceDeskRef))
            {
                predicate = predicate.And(p => p.ServiceDeskRef.ToLower().Contains(postData.ServiceDeskRef.ToLower()));
            }

            //Created Date from and to
            if (postData.CreatedFromDate.HasValue && postData.CreatedToDate.HasValue)
            {
                predicate =
                    predicate.And(
                        p => p.CreatedDate >= postData.CreatedFromDate.Value && p.CreatedDate <= postData.CreatedToDate);
            }

            //Created Date from 
            if (postData.CreatedFromDate.HasValue && !postData.CreatedToDate.HasValue)
            {
                predicate = predicate.And(p => p.CreatedDate >= postData.CreatedFromDate.Value);
            }

            //Created Date to
            if (!postData.CreatedFromDate.HasValue && postData.CreatedToDate.HasValue)
            {
                predicate = predicate.And(p => p.CreatedDate <= postData.CreatedToDate);
            }

            //Created Ref
            if (!string.IsNullOrEmpty(postData.CreatedRef))
            {
                predicate = predicate.And(p => p.CreatedRef.ToLower().Contains(postData.CreatedRef.ToLower()));
            }

            //Created User
            if (!string.IsNullOrEmpty(postData.CreatedUser))
            {
                predicate = predicate.And(p => p.CreatedUser.ToLower().Contains(postData.CreatedUser.ToLower()));
            }

            //User Name
            if (!string.IsNullOrEmpty(postData.UserName))
            {
                predicate = predicate.And(p => p.UserName.ToLower().Contains(postData.UserName.ToLower()));
            }

            //AD OU
            if (!string.IsNullOrEmpty(postData.ADOU))
            {
                predicate = predicate.And(p => p.ADOU.ToLower().Contains(postData.ADOU.ToLower()));
            }

            //Account Status
            if (!string.IsNullOrEmpty(postData.AccountStatus))
            {
                switch (postData.AccountStatusFlag)
                {
                    case IbaUserActiveType.Active:
                        predicate = predicate.And(p => p.AccountStatus.ToLower().Contains("Active".ToLower()));
                        break;
                    case IbaUserActiveType.Inactive:
                        predicate = predicate.And(p => p.AccountStatus.ToLower().Contains("Disabled".ToLower()));
                        break;
                    case IbaUserActiveType.NA:
                        predicate = predicate.And(p => p.AccountStatus.ToLower().Contains("N/A".ToLower()));
                        break;
                }
            }
            #endregion

            var filteredUsers = users.AsQueryable().Where(predicate);
            return filteredUsers.ToList();
        }

        [HttpGet("CreateReport")]
        public static StringBuilder CreateReport(List<IbaUserDto> users, string author)
        {
            StringBuilder sb = new StringBuilder();

            #region get report template

            // TODO: 
            string file = null;// System.IO.File.ReadAllText(ConfigurationManager.AppSettings["TemplatesPath"] + ConfigurationManager.AppSettings["IBAUsersTemplate"]);
            sb.Append(file);

            #endregion

            #region header

            sb.Replace("%AUTHOR%", author);
            sb.Replace("%LASTAUTHOR%", author);
            sb.Replace("%CREATED%", DateTime.Now.ToString("o")); //2015-05-06T09:01:07Z
            sb.Replace("%TITLE%", $"IBA Users Report @ {DateTime.Now.ToShortDateString()}");

            #endregion

            #region data

            StringBuilder data = new StringBuilder();

            foreach (var item in users)
            {
                data.AppendLine("<Row>");
                data.AppendLine($"<Cell><Data ss:Type=\"String\">{item.Name}</Data></Cell>");
                data.AppendLine($"<Cell><Data ss:Type=\"String\">{item.Instance}</Data></Cell>");
                data.AppendLine($"<Cell><Data ss:Type=\"String\">{item.Security}</Data></Cell>");
                data.AppendLine($"<Cell><Data ss:Type=\"String\">{item.Role}</Data></Cell>");
                data.AppendLine($"<Cell><Data ss:Type=\"String\">{item.Login}</Data></Cell>");
                data.AppendLine($"<Cell><Data ss:Type=\"String\">{item.IsActive}</Data></Cell>");
                data.AppendLine($"<Cell><Data ss:Type=\"String\">{item.UpdatedBy}</Data></Cell>");
                data.AppendLine($"<Cell ss:StyleID=\"s67\"><Data ss:Type=\"DateTime\">{item.Updated.ToString("yyyy-MM-dd")}</Data></Cell>");
                data.AppendLine($"<Cell><Data ss:Type=\"String\">{item.Disabled}</Data></Cell>");
                data.AppendLine($"<Cell><Data ss:Type=\"String\">{item.ServiceDeskRef}</Data></Cell>");
                data.AppendLine($"<Cell ss:StyleID=\"s67\"><Data ss:Type=\"DateTime\">{item.CreatedDate.ToString("yyyy-MM-dd")}</Data></Cell>");
                data.AppendLine($"<Cell><Data ss:Type=\"String\">{item.CreatedRef}</Data></Cell>");
                data.AppendLine($"<Cell><Data ss:Type=\"String\">{item.CreatedUser}</Data></Cell>");
                data.AppendLine($"<Cell><Data ss:Type=\"String\">{item.UserName}</Data></Cell>");
                data.AppendLine($"<Cell><Data ss:Type=\"String\">{item.AccountStatus}</Data></Cell>");
                data.AppendLine($"<Cell><Data ss:Type=\"String\">{item.Office}</Data></Cell>");
                data.AppendLine($"<Cell><Data ss:Type=\"String\">{item.AdDepartment}</Data></Cell>");
                data.AppendLine($"<Cell><Data ss:Type=\"String\">{item.City}</Data></Cell>");
                data.AppendLine($"<Cell><Data ss:Type=\"String\">{item.ADOU}</Data></Cell>");
                data.AppendLine($"<Cell><Data ss:Type=\"String\">{item.Manager}</Data></Cell>");
                data.AppendLine($"<Cell><Data ss:Type=\"String\">{item.IbaLoadDate.ToString("dd/MM/yyyy HH:mm:ss")}</Data></Cell>");
                data.AppendLine("</Row>");

                //xml date formatter ToString("o")
            }

            sb.Replace("%DATA%", data.ToString());

            #endregion

            return sb;
        }
    }
}