﻿using IT.Audit.Dashboard.Application.Contracts.Account;
using IT.Audit.Dashboard.Domain.Security;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System.Security.Principal;
using System.Threading.Tasks;

namespace IT.Audit.Dashboard.Web.Controllers
{
    [Route("[controller]")]
    [ApiController]
    [EnableCors("IT.Audit.Dashboard.Origin")]
    public class AccountController : ControllerBase
    {
        private readonly IUserAppService _userAppService;

        public AccountController(IUserAppService userAppService)
        {
            _userAppService = userAppService;
        }

        [HttpGet]
        public string Get()
        {
            IPrincipal p = HttpContext.User;
            return p.Identity.Name;
        }

        [HttpGet("GetAuthenticatedUser")]
        public async Task<IdentityUser> GetUser()
        {
            var result = await _userAppService.GetAuthenticatedUser();

            if (!result.IsAuthenticated) return null;

            HttpContext.Items["User"] = result;
            return result;
        }
    }
}
