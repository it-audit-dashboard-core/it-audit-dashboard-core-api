﻿using AutoMapper;
using IT.Audit.Dashboard.Application.Validators;
using IT.Audit.Dashboard.Application;
using IT.Audit.Dashboard.Application.Dtos;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using IT.Audit.Dashboard.Domain.Shared;
using IT.Audit.Dashboard.Domain.Security;
using IT.Audit.Dashboard.Application.Contracts.Account;
using System;

namespace IT.Audit.Dashboard.SSL.Api.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("IT.Audit.Dashboard.Origin")]
    public class DashboardController : ControllerBase
    {
        private readonly IDashboardAppService _dashboardAppService;
        private readonly IUserAppService _userAppService;
        private readonly IMapper _mapper;

        public DashboardController(IDashboardAppService dashboardAppService, IUserAppService userAppService, IMapper mapper)
        {
            _dashboardAppService = dashboardAppService;
            _userAppService = userAppService;
            _mapper = mapper;
        }

        [HttpGet("{userName}/{refreshCache}")]
        public async Task<ValidatedResultDto<DashboardOutputDto>> GetUserApplications(string userName, bool refreshCache)
        {
            //var result = await _userAppService.GetAuthenticatedUser();
            //if (IsUserInRole.IsInGroup("G-EMEA-ITAuditDashboard-ActiveDirectoryUsersView-UAT"))
            //    Console.WriteLine("true");

            var inputDto = new DashboardInputDto
            {
                UserName = userName,
                RefreshCache = refreshCache,
                ConnectionType = ConnectionType.IT_Audit_Dashboard
            };

            var outputDto = await _dashboardAppService.GetAsync(inputDto);

            var validator = new ValidatedResult<DashboardOutputDto>(_mapper)
                    .Compile(new DashboardOutputDtoValidator(), outputDto);

            return validator;
        }

        [HttpGet("GetNotNeeded")]
        public Task<DashboardOutputDto> Get(bool RefreshCache)
        {
            return Task.FromResult(new DashboardOutputDto());
        }
    }
}
