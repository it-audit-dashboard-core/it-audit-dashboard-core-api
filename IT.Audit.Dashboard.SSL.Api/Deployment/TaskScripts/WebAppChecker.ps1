# ------------------------------------------------------------------
# -------Web Application Checker------------------------------------
# ------------------------------------------------------------------

param(  [Parameter(Mandatory=$true)][string]$WebAppUrl)

if ( (Get-PSSnapin -Name "Microsoft.sharepoint.powershell" -ErrorAction SilentlyContinue) -eq $null )
{
    Add-PSSnapin "Microsoft.sharepoint.powershell";
}

try
{
    $webapp = Get-SPWebApplication -Identity $WebAppUrl 
    
    if($webapp -eq $null)
    {
        throw "The Web Application Does not exist: $WebAppUrl"
    }
    
    $webAppDisplayName = $webapp.DisplayName;
    
    Write-Output "The validator Succeeded. The name of the web application is: $webAppDisplayName"
}
catch
{
    throw "Unable to Access the Web Application: $WebAppUrl"
}