<#
The purpose of this script is to update the environment variables values accordingly to the msbuild file
This script does not create environment fields. Only updates values

#>

Param($environment,[string]$msbuildFilePath,[string]$k2HostServer)
. .\IncludeFunctions.ps1

#Add-Type -AssemblyName System.Object

If ($environment -eq $null) {$environment = GetEnvironment}


function Process-EnvironmentVariablesFromMsBuild
{
  Param([string]$environment,[string]$msbuildFilePath,[string]$k2HostServer)

    [XML]$xmlDoc = Get-Content $msbuildFilePath
     
     
     $connString = $xmlDoc.project.propertyGroup | ? { $_.Condition -eq "`$(Environment) == '" + $environment + "'" }
     $connString.EnvironmentFields.Root.Field | %{
            
            $fieldNode =  $_
           
           Update-EnvironmentVariableValue -k2HostServer $k2HostServer -envVariableName $fieldNode.Name -envVariableValue $fieldNode.Value
           #write-host $fieldNode.Name " " $fieldNode.Value
 }

}

function Update-EnvironmentVariableValue {

    Param([string]$k2HostServer,[string]$envVariableName,[string]$envVariableValue)
    
    [Reflection.Assembly]::LoadWithPartialName("SourceCode.EnvironmentSettings.Client")
    [Reflection.Assembly]::LoadWithPartialName("SourceCode.HostClientAPI")

    $ConnString = New-Object SourceCode.Hosting.Client.BaseAPI.SCConnectionStringBuilder

    $ConnString.Authenticate = $TRUE
    $ConnString.Host = $k2HostServer
    $ConnString.Integrated = $TRUE
    $ConnString.IsPrimaryLogin = $TRUE
    $ConnString.Port = 5555

    $environmentLibrary = New-Object SourceCode.EnvironmentSettings.Client.EnvironmentSettingsManager($FALSE,$FALSE)

    $environmentLibrary.ConnectToServer($ConnString.ConnectionString);  

    $environmentLibrary.InitializeSettingsManager();

    if ($environmentLibrary.CurrentEnvironment -ne $null)

    { 
    
        if($environmentLibrary.CurrentEnvironment.EnvironmentFields.GetItemByName($envVariableName).FieldName -ne $null) {           
            $environmentLibrary.CurrentEnvironment.EnvironmentFields.GetItemByName($envVariableName).Value = $envVariableValue
            $environmentLibrary.CurrentEnvironment.EnvironmentFields.GetItemByName($EnvVariableName).SaveUpdate()
        }
        else
        {
            write-host $EnvVariableName " does not exists."
        }

    }
    
     $environmentLibrary.Disconnect()
     $environmentLibrary.Dispose()
     

}

#delete the next 2 lines
$msbuildFilePath = "C:\Users\s-dms-00d-spk2\Desktop\DeployTOcheckin\Test Workflow Package.msbuild"
$k2HostServer = "localhost"

Process-EnvironmentVariablesFromMsBuild -k2HostServer $k2HostServer -environment $environment -msbuildFilePath $msbuildFilePath

