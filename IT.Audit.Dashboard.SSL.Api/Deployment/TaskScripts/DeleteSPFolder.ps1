# ------------------------------------------------------------------
# -------Delete Folders---------------------------------------------
# ------------------------------------------------------------------
param(  [Parameter(Mandatory=$true)][string]$WebUrl,
        [Parameter(Mandatory=$true)][string]$FolderRelativePath)

if ( (Get-PSSnapin -Name "Microsoft.sharepoint.powershell" -ErrorAction SilentlyContinue) -eq $null )
{
    Add-PSSnapin "Microsoft.sharepoint.powershell";
}

$web = Get-SPWeb $WebUrl

if($web -ne $null)
{
    $folderCustomCss = $web.GetFolder($FolderRelativePath);
    if($folderCustomCss.Exists)
    {
        $folderCustomCss.Delete();
        Write-Output "folder: $FolderRelativePath, has been Deleted";
    }
}
else
{
    throw "The site does not exist: $web"
}
$web.Dispose();