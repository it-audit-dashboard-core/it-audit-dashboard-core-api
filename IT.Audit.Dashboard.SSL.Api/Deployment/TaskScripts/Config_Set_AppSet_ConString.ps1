﻿# ------------------------------------------------------------------
# -------------Config Update app setting or connection string--------------
# ------------------------------------------------------------------
param(  [Parameter(Mandatory=$true)][string]$XMLDocPath,
        [Parameter(Mandatory=$true)][string]$NameKey,
        [ValidateSet("AppSetting", "ConnectionString")][string]$Target,
        [Parameter(Mandatory=$false)][string]$Value = ""
     )

#$Target = "ConnectionString"
#$XMLDocPath = "C:\Users\afooks\source\repos\cats-ews\AJG.CATS.ExchangeWebService\AJG.CATS.EWS.IntegrationTests\bin\Debug\AJG.CATS.EWS.IntegrationTests.dll.config"
#$Value = "some value here"
#$NameKey = "CATS_EWS"

if($Target -eq "AppSetting")
{
    $XPath = "//configuration/appSettings/add[@key='$NameKey']"
    $AttributeName = "value"
}
else 
{
    $XPath = "//configuration/connectionStrings/add[@name='$NameKey']"
    $AttributeName = "connectionString"
}

function ParseXmlEmelment($xmlNode, $AttributeName, $AttributeValue)
{
    if($xmlNode.HasAttribute($AttributeName))
    {
        $xmlNode.SetAttribute($AttributeName, $AttributeValue)
    }
}

if(Test-Path $XMLDocPath)
{
    [xml]$document = Get-Content $XMLDocPath
    
    $XpathNode = $document.SelectSingleNode($XPath)    
    
    if($XpathNode)
    {
        $XpathNode | %{
            $Node = $_
            $Node.Attributes

            ParseXmlEmelment -xmlNode $Node -attributeName $AttributeName -AttributeValue $Value
        }
        $document.Save($XMLDocPath)
    }
    else
    {
        Write-Output "No Matching Output found."
    }
}
else
{
    throw ("The terms xml doc couldn't be found: " + $XMLDocPath)
}