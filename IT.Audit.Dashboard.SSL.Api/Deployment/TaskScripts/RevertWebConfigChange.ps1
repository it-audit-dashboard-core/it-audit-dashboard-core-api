# ------------------------------------------------------------------
# -----Revert Web Config Change using SPWebConfigModification-------
# ------------------------------------------------------------------
param(  [Parameter(Mandatory=$true)][string]$WebAppUrl,
        [Parameter(Mandatory=$true)][string]$ConfigModOwner)

if ( (Get-PSSnapin -Name "Microsoft.sharepoint.powershell" -ErrorAction SilentlyContinue) -eq $null )
{
    Add-PSSnapin "Microsoft.sharepoint.powershell";
}

$WebApp = Get-SPWebApplication $WebAppUrl

if($WebApp -ne $null)
{
    $mods = @()

    foreach ( $mod in $WebApp.WebConfigModifications ) {
        if ( $mod.Owner -eq $ConfigModOwner ) {
            $mods += $mod
        }
    }

    foreach ( $mod in $mods ) {
        [void] $WebApp.WebConfigModifications.Remove( $mod ) 
		Write-Output "Removed modeification: $mod";
    }

    $WebApp.Update()
    $WebApp.Parent.ApplyWebConfigModifications()
}
else
{
    throw "The Web App: $WebAppUrl, does not exist."
}