# ------------------------------------------------------------------
# -------Add SharePoint Sites to MVC SharePoint Sites table---------
# ------------------------------------------------------------------
param(  [Parameter(Mandatory=$true)][string]$ConnectionString,
        [Parameter(Mandatory=$true)][string]$ClaimsSiteConfigFilePath,
        [Parameter(Mandatory=$true)][string]$LogFilePath = $(throw "LogFilePath is a required parameter."),
        [Parameter(Mandatory=$true)][ValidateSet("Dev", "Integration", "UAT", "Production")]$TargetSystem
       )

#$ConnectionString = "Data Source=EMEUKHC4DB07PD\SP2010;Initial Catalog=ClaimsWorkflow;Integrated Security=True;Pooling=False;MultipleActiveResultSets=true;"
#$ClaimsSiteConfigFilePath = "C:\Users\afooks\source\repos\gcms\GCMS-SharePoint\Deployment\ClaimsSitesConfig.xml" 
#$LogFilePath = "C:\Users\afooks\Log.txt"
#$TargetSystem = "Dev"

function Write-LocalLog ($logMessage)
{
    # Write to the
    Write-host $logMessage
    # Write to the log file.
    Add-Content -LiteralPath $LogFilePath -Value ("  *** Logged Direct From Script: " + $logMessage)
}

# Check Xml File exists.
if(-not (Test-Path $ClaimsSiteConfigFilePath)){
    Write-LocalLog -logMessage ("XmlFile Could not be found.")
    throw
}
# Check Connection String ok
$sqlConnectionTest = new-object System.Data.SqlClient.SqlConnection $ConnectionString
$sqlConnectionTest.Open()
$sqlConnectionTest.Close()

function AddSiteToDB($SiteInfo,$webappUrl)
{
    # Create and open a database connection
    $sqlConnection = new-object System.Data.SqlClient.SqlConnection $ConnectionString
    $sqlConnection.Open()
     
    #Create a command object
    $sqlCommand = $sqlConnection.CreateCommand()
    $sqlCommand.CommandText = "select * from [ClaimsWorkflow].[dbo].[SharepointSite] where [SiteName] = '$($SiteInfo.name)'"
    
    $sqlReader = $sqlCommand.ExecuteReader()
    $siteExists = $false

    while ($sqlReader.Read()) 
    {        
        $sqlReader['SiteName']
        $siteNameSql = $sqlReader['SiteName']        
        if($siteNameSql.ToLower() -eq $($SiteInfo.name))
        {
            $siteExists = $true
            break;
        }
    }
    
    $sqlCommand = $sqlConnection.CreateCommand()
    if(-not $siteExists)
    {    
        Write-LocalLog -logMessage "Inserting: SiteName: $($SiteInfo.name), IsActive: $($SiteInfo.GCMSSPSiteDB_IsActive), ManagedPath: $($SiteInfo.urlPath), WebAppUrl: $($webappUrl), DocumentPreviewType: $($SiteInfo.GCMSSPSiteDB_DocPreviewType)"                             
        $sqlCommand.CommandText = "insert into [ClaimsWorkflow].[dbo].[SharepointSite] values ('$($SiteInfo.name)','$($SiteInfo.GCMSSPSiteDB_IsActive)','$($SiteInfo.urlPath.trim('/'))','$webappUrl','$($SiteInfo.GCMSSPSiteDB_DocPreviewType)','')"
    }
    else {       
        Write-LocalLog -logMessage "Updating: SiteName: $($SiteInfo.name), IsActive: $($SiteInfo.GCMSSPSiteDB_IsActive), ManagedPath: $($SiteInfo.urlPath), WebAppUrl: $($webappUrl), DocumentPreviewType: $($SiteInfo.GCMSSPSiteDB_DocPreviewType)"   
        $sqlCommand.CommandText = "UPDATE [ClaimsWorkflow].[dbo].[SharepointSite] Set IsActive = '$($SiteInfo.GCMSSPSiteDB_IsActive)', ManagedPath = '$($SiteInfo.urlPath.trim('/'))', WebAppUrl = '$webappUrl', DocumentPreviewType = '$($SiteInfo.GCMSSPSiteDB_DocPreviewType)', DocumentPreviewConfiguration = '' where SiteName = '$($SiteInfo.name)'"                
    }
    $sqlReader = $sqlCommand.ExecuteReader()
    $sqlConnection.Close()
}
[xml]$configXmlFile = Get-Content $ClaimsSiteConfigFilePath
$EnvironmentConfigSection = $configXmlFile.SelectSingleNode("/Root/Environments/Environment[@target='" + $TargetSystem + "']")
$EnvironmentConfigSection.SiteDetails | %{
    $site = $_    
    $AddToDB = [System.Convert]::ToBoolean($site.addToGCMSSPSiteDB);    
    if($AddToDB)
    {        
        AddSiteToDB -SiteInfo $site -webappUrl $EnvironmentConfigSection.webAppUrl
    }
}