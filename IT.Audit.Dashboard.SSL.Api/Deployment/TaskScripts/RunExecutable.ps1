﻿# ------------------------------------------------------------------
# --------------- Run Executabe ------------------------------------
# ------------------------------------------------------------------
param(  [Parameter(Mandatory=$true)][string]$exePath,
        [Parameter(Mandatory=$false)][string]$commaSeperatedParams,
        [Parameter(Mandatory=$false)][string]$LogFilePath)


#$exePath = "C:\inetpub\ClaimsWorkflowMVCSite\bin\ef6.exe"
#$commaSeperatedParams = "database,update,--config,C:\inetpub\ClaimsWorkflowMVCSite\web.config,--connection-string-name,ClaimsWorkflowDatabase,--target,LeadInsurerStringLengthIncrease,--assembly,C:\inetpub\ClaimsWorkflowMVCSite\bin\AJG.Claims.DAL.dll,--verbose"
#$LogFilePath = ""

function Write-LocalLog ($logMessage)
{
    # Write to the
    Write-host $logMessage
    # Write to the log file.
    if(-not [System.String]::IsNullOrEmpty($LogFilePath)) {
        Add-Content -LiteralPath $LogFilePath -Value ("  *** Logged Direct From Script: " + $logMessage)
    }
}

if(-not [System.IO.File]::Exists($exePath)) {
    throw ("The Exe specifeid does not exist: $exePath")
}

$AllArgs = [System.Collections.ArrayList]::new()

$commaSeperatedParams.Split(',') | % {
    $arg = $_
    $null = $AllArgs.Add($arg)
}

$log = & $exePath $AllArgs.ToArray()

Write-LocalLog -logMessage $log