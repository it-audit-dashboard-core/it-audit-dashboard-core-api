# ------------------------------------------------------------------
# -------Empty Recycle Bin------------------------------------------
# ------------------------------------------------------------------
param(  [Parameter(Mandatory=$true)][string]$SiteUrl)

if ( (Get-PSSnapin -Name "Microsoft.sharepoint.powershell" -ErrorAction SilentlyContinue) -eq $null )
{
    Add-PSSnapin "Microsoft.sharepoint.powershell";
}

$site = Get-SPSite $SiteUrl

if($site -ne $null)
{
    $site.RecycleBin.DeleteAll();
    Write-Output("Recycle Bin cleared successfully: $SiteUrl");
}
else
{
    throw "The site collection does not exist: $site"
}

$site.Dispose();