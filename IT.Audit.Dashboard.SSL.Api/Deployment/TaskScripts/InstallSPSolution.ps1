# ------------------------------------------------------------------
# -------Install Solution-------------------------------------------
# ------------------------------------------------------------------
param(  [string]$WebAppUrl,
        [Parameter(Mandatory=$true)][string]$SolutionPath,
        [Parameter(Mandatory=$true)][string]$SolutionId,
        [Parameter(Mandatory=$true)][string]$SolutionName)

if ( (Get-PSSnapin -Name "Microsoft.sharepoint.powershell" -ErrorAction SilentlyContinue) -eq $null )
{
    Add-PSSnapin "Microsoft.sharepoint.powershell";
}

#$WebAppUrl = "http://dmsukdev.emea.ajgco.com"
#$SolutionPath = "E:\ClaimsWorkflowDeployments\AJG_ClaimsWorkflow_DeploymentPackage_06Oct2015_09_27\AJG_ClaimsWorkflow_SharePointDeployment\AJG.Claims.SharePoint.wsp"
#$SolutionId = "834e22fd-d506-4400-bcc8-6eacae51beb4"
#$SolutionName = "AJG.Claims.SharePoint"

function WaitForJobToFinish([string]$SolutionFileName)
{ 
	Start-Sleep -Seconds 2
    $JobName = "*solution-deployment*$SolutionFileName*"
	Write-Output("Job Nameis: $JobName");

    # Check that the Get-TimerJob command is working.....
    $AllTimerJobs = Get-SPTimerJob
    if(-not $AllTimerJobs)
    {
        throw "The Get-SPTimerJob query returned zero results....there could be a problem with the timer service on this farm..."
    }

    $job = Get-SPTimerJob | ?{ $_.Name -like $JobName }
    if ($job -eq $null)
    {
        Write-Output('Timer job not found');
    }
    else
    {
        $JobFullName = $job.Name
        Write-Output("Waiting to finish job $JobFullName");
        
        while ((Get-SPTimerJob $JobFullName) -ne $null) 
        {
            Write-Output(".");
            Start-Sleep -Seconds 2
        }
       Write-Output("Finished waiting for job..");
    }
}

if($WebAppUrl)
{
    $webApp = Get-SPWebApplication $WebAppUrl -ErrorAction SilentlyContinue
    if($webApp -ne $null)
    {
        if(Test-Path $SolutionPath)
        {
            Write-Output("Adding solution");
            Add-SPSolution $SolutionPath

            Write-Output("Installing solution to Web Application $webUrl");
            Install-SPSolution –Identity $SolutionId -WebApplication $WebAppUrl -GACDeployment -Force

            Write-Output('Waiting for job to finish');
            WaitForJobToFinish $SolutionName
        }
        else
        {
            throw "The specified solution could not be found at path: $SolutionPath"
        }
    }
    else
    {
        throw "The Web App does not exist: $WebAppUrl"
    }
}
else
{
    if(Test-Path $SolutionPath)
    {
        Write-Output("Adding solution");
        Add-SPSolution $SolutionPath

        Write-Output("Installing solution to Web Application $webUrl");
        Install-SPSolution –Identity $SolutionId -GACDeployment -Force

        Write-Output('Waiting for job to finish');
        WaitForJobToFinish $SolutionName
    }
    else
    {
        throw "The specified solution could not be found at path: $SolutionPath"
    }
}

try
{
    $solution = Get-SPSolution -Identity $SolutionPath.Substring($SolutionPath.LastIndexOf('\')+1);
}
catch { throw ("There has been a problem retrieving the solution, post deploy, using name: " + $SolutionPath.Substring($SolutionPath.LastIndexOf('\')+1)) }

if($solution)
{
    $lastOperationResult = $solution.LastOperationResult
    if($lastOperationResult -eq [Microsoft.SharePoint.Administration.SPSolutionOperationResult]::DeploymentSucceeded)
    {
        Write-Output('Solution installation completed successfully');
    }
    else
    {
        throw ("The Solution Failed to install correctly. Please go to Central admin > Solution Management to investigate the failure reason.");
    }
}