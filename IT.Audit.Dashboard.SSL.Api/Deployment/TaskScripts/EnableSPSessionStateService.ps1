﻿# ------------------------------------------------------------------
# -------SP Enable SP Session State Service-------------------------
# ------------------------------------------------------------------
param()

if ( (Get-PSSnapin -Name "Microsoft.sharepoint.powershell" -ErrorAction SilentlyContinue) -eq $null )
{
    Add-PSSnapin "Microsoft.sharepoint.powershell";
}

Enable-SPSessionStateService -DefaultProvision
Write-Output "SP Session State service has been enabled -DefaultProvision"