<#
This file needs to be executed after K2PackagePreDeployment.ps1
#>

Param(	[Parameter(Mandatory=$true)][string]$xmlFilePath,
		[Parameter(Mandatory=$true)][string]$k2HostServer)

function Load-EnvironmentFieldsFromK2Server
{
    Param([string]$k2HostServer)
    
    [Reflection.Assembly]::LoadWithPartialName("SourceCode.EnvironmentSettings.Client")
    [Reflection.Assembly]::LoadWithPartialName("SourceCode.HostClientAPI")

    $arrEnvFields = @()

    $ConnString = New-Object SourceCode.Hosting.Client.BaseAPI.SCConnectionStringBuilder

    $ConnString.Authenticate = $TRUE
    $ConnString.Host = $k2HostServer
    $ConnString.Integrated = $TRUE
    $ConnString.IsPrimaryLogin = $TRUE
    $ConnString.Port = 5555

    $environmentLibrary = New-Object SourceCode.EnvironmentSettings.Client.EnvironmentSettingsManager($FALSE,$FALSE)

    $environmentLibrary.ConnectToServer($ConnString.ConnectionString);  

    $environmentLibrary.InitializeSettingsManager();

    if ($environmentLibrary.CurrentEnvironment -ne $null)

    {

        Foreach ( $f in $environmentLibrary.CurrentEnvironment.EnvironmentFields)

        {

            $arrEnvFields += $f.FieldName
            #$output = $output + $f.FieldName + ":" + $f.Value + "`n"

        }

    }
    
    return $arrEnvFields
    
}
 
function Update-EnvironmentFieldsXmlFile
{
    Param([string]$xmlFilePath,[string]$k2HostServer)
 
 
    $arrEnvFields = @()
    
    $arrEnvFields = Load-EnvironmentFieldsFromK2Server $k2HostServer
 
    [XML]$xmlDoc = Get-Content $xmlFilePath
 
    # Loop through all of the default option resolve nodes
    $xmlDoc.configuration.package.resolveOptions.specificOptions.resolve | %{
 
        $resolveNode = $_
       
        if($resolveNode.namespace -eq "urn:SourceCode/EnvironmentSettings/Fields")
        {
            # Check if the var exists in K2 by calling the K2Variable-Exists function
            if(Check-K2FieldExists -fieldName $resolveNode.name -envFieldNames $arrEnvFields){
                $resolveNode.action = "UseExisting"
            }else{
                $resolveNode.action = "Deploy"
            }
        }
    }
 
    $xmlDoc.Save($xmlFilePath)
}
 
function Check-K2FieldExists
{
    # Add aditional Params here as needed
    Param([string]$fieldName, [array]$envFieldNames)
    
    #write-host $fieldName
 
    if($envFieldNames -contains $fieldName){
        return $true
    }
    else{
        return $false
    }
}
 
#Delete The Next 2 Lines
#$xmlFilePath = "C:\Projects\AJG.K2.Utilities.Deployment\AJG.K2.SmartObject.Management\PowerShell\tiago test 1 Package.xml"
#$k2HostServer = "localhost"
 
Update-EnvironmentFieldsXmlFile -xmlFilePath $xmlFilePath -k2HostServer $k2HostServer