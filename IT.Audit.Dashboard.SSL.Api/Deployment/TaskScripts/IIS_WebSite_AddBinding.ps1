﻿# ------------------------------------------------------------------
# ---------------IIS Create Web Site Binding -----------------------
# ------------------------------------------------------------------
PARAM(
        [Parameter(Mandatory=$true)][string]$WebSiteName,
        [Parameter(Mandatory=$true)][string]$Port,
        [Parameter(Mandatory=$true)][string]$IPAdress,
        [Parameter(Mandatory=$false)][string]$HostHeader
    )

#$WebSiteName = "Claims Workflow MVC Site"
#$IPAdress = "*"
#$Port = "2020"
#$HostHeader = ""

Import-Module WebAdministration

$binding = Get-WebBinding -Name $WebSiteName -Port $Port -IPAddress $IPAdress -HostHeader $HostHeader

if($binding)
{
    Write-Output "Binding already Exists: -Name $WebSiteName -IPAddress $IPAdress -Port $Port -HostHeader $HostHeader"
}
else
{
    New-WebBinding -Name $WebSiteName -IPAddress $IPAdress -Port $Port -HostHeader $HostHeader
    Write-Output "Adding Binding: -Name $WebSiteName -IPAddress $IPAdress -Port $Port -HostHeader $HostHeader"   
}