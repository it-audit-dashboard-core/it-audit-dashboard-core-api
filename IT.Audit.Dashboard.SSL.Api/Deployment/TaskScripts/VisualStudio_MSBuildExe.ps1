﻿# ------------------------------------------------------------------
# -------------Visual Studio MsBuild Solution / Project--------
# Switches Explained here
# https://docs.microsoft.com/en-us/visualstudio/msbuild/msbuild-command-line-reference?view=vs-2019
#
# ------------------------------------------------------------------
Param(
        [parameter(mandatory=$true)][ValidateNotNullorEmpty()][string]$projectFilePath = "",
        [parameter(mandatory=$true)][ValidateSet("clean","build","rebuild","publish")][string]$action,
        [parameter(mandatory=$true)][ValidateSet("debug","release")][string]$buildConfig
     )

#$projectFilePath = "C:\Users\afooks\source\repos\fusion\BFEClient\BFEClient.vbproj"
#$action = "publish"
#$buildConfig = "release"

function Get-LatestBuildExePath
{
    Param (
        [parameter(mandatory=$true)][ValidateSet("MsBuild.exe", "devenv.exe")][string]$targetExe
    )    
    [bool] $vsSetupExists = $null -ne (Get-Command Get-VSSetupInstance -ErrorAction SilentlyContinue)
    if (!$vsSetupExists)
    {
        Write-Verbose "Importing the VSSetup module in order to determine TF.exe path..." -Verbose
        Install-Module VSSetup -Scope CurrentUser -Force     
    }
    [string] $visualStudioInstallationPath = (Get-VSSetupInstance | Select-VSSetupInstance -Latest -Require Microsoft.Component.MSBuild).InstallationPath
    $msBuildExecutableFilePath = (Get-ChildItem $visualStudioInstallationPath -Recurse -Filter $targetExe | Select-Object -First 1).FullName
    $script:exePath = $msBuildExecutableFilePath

    if(-Not [System.IO.File]::Exists($script:exePath)) {
        throw "The $targetExe file could not be found!"
    }
}
function Execute-exe 
{    
    $script:result = (& "$($script:exePath)" $projectFilePath  -t:$action -p:Configuration=$buildConfig | Out-String)
}
function Check-TheExeResult {
    # Search for Erros and throw exception if exist
    if(([regex]::Matches($script:result,'[1-9]\d* Error\(s\)')).Count -gt 0) {    
        Throw ("An Error occured: $script:result")
    }
}

Get-LatestBuildExePath -targetExe MsBuild.exe
Write-Host "the MsBuild.exe file being used is: $script:exePath"
Execute-exe
Check-TheExeResult