# ------------------------------------------------------------------
# -------Disable Feature--------------------------------------------
# ------------------------------------------------------------------
param(  [Parameter(Mandatory=$true)]$Url, 
        [Parameter(Mandatory=$true)]$FeatureID, 
                [ValidateSet("Web", "web", "Site", "site", "WebApplication")][Parameter(Mandatory=$true)]$Scope)

if ( (Get-PSSnapin -Name "Microsoft.sharepoint.powershell" -ErrorAction SilentlyContinue) -eq $null )
{
    Add-PSSnapin "Microsoft.sharepoint.powershell";
}
#$url = "http://dmsuktest.emea.ajgco.com"
#$FeatureID = "e7ed6d3d-3df8-41cb-8a6e-c9ce7cbb27f3"
#$Scope = ""
if($Scope.ToLower() -eq "web")
{
    $SPEntity = Get-SPWeb $Url
}
if($Scope.ToLower() -eq "site")
{
    $SPEntity = Get-SPSite $Url
}
if($Scope.ToLower() -eq "webapplication")
{
    $SPEntity = Get-SPWebApplication $Url
}

if($SPEntity -eq $null)
{
    throw "$Scope does not exist: $SPEntity"
}
else
{
    try
    {
        if ($SPEntity.Features[$FeatureID] -ne $null)
        {
            Disable-SPFeature -Identity $FeatureID -url $Url -Confirm:$false -Force 
            Write-Output "Feature Disabled: $FeatureID"
        }
        else
        {
            Write-Output "Feature already Disabled: $FeatureID"
        }
    }
    catch
    {
        throw "There has been an error in disabling the feature"
    }
}
if($Scope.ToLower() -ne "webapplication")
{
    $SPEntity.Dispose();
}