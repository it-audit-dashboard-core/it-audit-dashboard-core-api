﻿# ------------------------------------------------------------------
# -------IIS app pool recycle---------------------------------------
# ------------------------------------------------------------------
PARAM(
        [ValidateSet("IISReset", "AppPoolRecycle")][string]$Action,
        [Parameter(Mandatory=$false)][string]$AppPoolName
    )

#$Action = "AppPoolRecycle"
#$AppPoolName = "Test Claims MVC App Pool"

Import-Module WebAdministration

if($Action.ToLower() -eq "iisreset")
{
    invoke-command -scriptblock {iisreset}
    Write-Output "iisreset completed successfully."
}
else
{
    $apppool = Get-WebAppPoolState -Name $AppPoolName -ErrorAction SilentlyContinue
    if($apppool)
    {
         if($apppool.Value -eq "Started")
         {
            Write-Output "App pool is currently in a started state"
            Restart-WebAppPool -Name $AppPoolName
            Write-Output "App pool successfully restarted"
         }
         elseif($apppool.Value -eq "Stopped")
         {
            Write-Output "App pool is currently Stopped"
            Start-WebAppPool -Name $AppPoolName
            Write-Output "App pool successfully restarted"
         }
    }
    else 
    {
        throw "The app pool couldnt be found."
    }
}