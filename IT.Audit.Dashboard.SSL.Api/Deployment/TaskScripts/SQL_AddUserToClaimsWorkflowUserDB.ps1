# ------------------------------------------------------------------
# -------Add User to Claims Workflow User Table---------
# ------------------------------------------------------------------
param(  [Parameter(Mandatory=$true)][string]$ConnectionString,
        [Parameter(Mandatory=$true)][string]$UserDisplayName,
        [Parameter(Mandatory=$true)][string]$UserName,
        [Parameter(Mandatory=$true)][int]$TeamId,
        [Parameter(Mandatory=$true)][string]$IsActive
       )

#$ConnectionString = "Data Source=SMIUKVDBD121;Initial Catalog=ClaimsWorkflow;Integrated Security=True;Pooling=False;MultipleActiveResultSets=true;"
#$UserDisplayName = "DeleteMe2"
#$UserName = "emea\DeleteMe"
#$TeamId = 4
#[bool]$IsActive = $true

[int]$IsActiveInt = $IsActive
[string]$IsActiveString = $IsActiveInt 

# Create and open a database connection
$sqlConnection = new-object System.Data.SqlClient.SqlConnection $ConnectionString
$sqlConnection.Open()

# Create a command object
$sqlCommand = $sqlConnection.CreateCommand()
$sqlCommand.CommandText = "select * from [ClaimsWorkflow].[dbo].[User] where [Username] = '$UserName'"

$sqlReader = $sqlCommand.ExecuteReader()

$userExists = $false

if($sqlReader)
{
    while ($sqlReader.Read())
    {
        $username = $sqlReader["Username"]
        
        if($username.ToLower() -eq $UserName)
        {
            $userExists = $true
            break;
        }
    }
}

if($userExists)
{
    # We need to Update the user fields.
    $sqlCommand = $sqlConnection.CreateCommand()
    $sqlCommand.CommandText = "Update [ClaimsWorkflow].[dbo].[User] Set Name='$UserDisplayName', TeamId=$TeamId, IsActive=$IsActiveInt Where UserName='$UserName'"
    $sqlReader = $sqlCommand.ExecuteReader()
}
else
{
    # Add the user
    $sqlCommand = $sqlConnection.CreateCommand()
    $sqlCommand.CommandText = "Insert Into [ClaimsWorkflow].[dbo].[User] (Name,Username,TeamId,IsActive) Values ('$UserDisplayName','$UserName',$TeamId,$IsActiveInt)"
    $sqlReader = $sqlCommand.ExecuteReader()
}