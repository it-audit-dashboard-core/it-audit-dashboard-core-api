# ------------------------------------------------------------------
# Set Application Pool Property
# ------------------------------------------------------------------
param(
        [Parameter(Mandatory=$true)][string]$AppPoolName = $(throw "AppPoolName is a required parameter."),
        [Parameter(Mandatory=$true)][ValidateSet("IdleTimeout","StartMode")]$AppPoolPropertyName,
        [Parameter(Mandatory=$true)][string]$AppPoolPropertyValue = $(throw "AppPoolPropertyValue is a required parameter.")
)

# Sample values - NOTE: Comment out before release!!
#$AppPoolName = "Claims Workflow MVC App Pool"
#$AppPoolPropertyName = "idleTimeout"
# 00:08:00 (hours:mins:secs)
#$AppPoolPropertyValue = "0"

# Sample values - NOTE: Comment out before release!!
#$AppPoolName = "Claims Workflow MVC App Pool"
#$AppPoolPropertyName = "startMode"
# 00:08:00 (hours:mins:secs)
#$AppPoolPropertyValue = "AlwaysRunning"

Import-Module WebAdministration

$ApplicationPool = Get-Item -Path "IIS:\AppPools\$AppPoolName"
if($ApplicationPool)
{
    try
    {
        switch ($AppPoolPropertyName)
        {
            "IdleTimeout" {
                Write-Output "setting IdleTimeout to $AppPoolPropertyValue";
                set-itemproperty ("IIS:\AppPools\Claims Workflow MVC App Pool") -Name processModel.idleTimeout -Value $AppPoolPropertyValue               
                break;
            }
            "StartMode" {
                Write-Output "setting StartMode to $AppPoolPropertyValue";
                set-itemproperty ("IIS:\AppPools\$AppPoolName") -Name startMode -Value $AppPoolPropertyValue
                break;
            }
        }
        Write-Output "App Pool setting succeeded";
    }
    catch
    { 
        Write-Output "There has been a problem setting the property";
        throw;
    }
}
else
{
    Write-Output "The app pool: '$AppPoolName' could not be found";
    throw;
}