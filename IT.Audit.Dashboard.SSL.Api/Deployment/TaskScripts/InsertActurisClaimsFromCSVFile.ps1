# ------------------------------------------------------------------
# -------Import Acturis claims from csv file Powershell script------
# ------------------------------------------------------------------
param(  [Parameter(Mandatory=$true)][string]$ConnectionString,
        [Parameter(Mandatory=$true)][string]$FilePath,
        [Parameter(Mandatory=$true)][string]$LogFilePath
       )

#$FilePath = "\\development01\devteam$\Andy Fooks\insertstamenets.csv"
#$ConnectionString = "Data Source=SMIUKVDBD121;Initial Catalog=ClaimsWorkflow;Integrated Security=True;Pooling=False;MultipleActiveResultSets=true;"
#$LogFilePath = "C:\Users\afooks\Documents\AAAA\ImportActurisclaimsFromcsvFile_Error.txt"

function Write-LocalLog ($logMessage)
{
    # Write to the
    Write-Output $logMessage
    # Write to the log file.
    Add-Content -LiteralPath $LogFilePath -Value ("  *** Logged Direct From Script: " + $logMessage)
}

Function CreateConnection($ConnectionString)
{
    $sqlConnection = new-object System.Data.SqlClient.SqlConnection $ConnectionString
    $sqlConnection.Open()
    return $sqlConnection
}

$fileContent = Get-Content $FilePath
$currentSqlCommand = ""

$startTime = Get-Date

$counter = 0

$fileContent | % {
    $line = $_
    
    $currentSqlCommand = $currentSqlCommand + $line

    if($currentSqlCommand.EndsWith(';'))
    {
        # Create and open a database connection
        if(($sqlConnection -eq $null) -or ($sqlConnection.State.ToString().ToLower() -eq "closed"))
        {
            $sqlConnection = CreateConnection -ConnectionString $ConnectionString
        }
        
        $counter++
        #$currentSqlCommand.ToString() -Replace "[a-zA-Z0-9]+'[a-zA-Z0-9]+"
        #$currentSqlCommand.ToString() -match "[a-zA-Z0-9]+'[a-zA-Z0-9]+" $currentSqlCommand.Replace($Matches[0,1], 
        
        #Create a command object
        $sqlCommand = $sqlConnection.CreateCommand()
        $sqlCommand.CommandText = $currentSqlCommand
        
        try{
            $sqlReader = $sqlCommand.ExecuteReader()
        }
        catch{
            Write-LocalLog -logMessage ("Error on: $currentSqlCommand. Error Message" + $error[0])
            try
            {
                Write-LocalLog -logMessage ("    Trying Again..." + $error[0])
                $sqlConnection = CreateConnection -ConnectionString $ConnectionString
                $sqlReader = $sqlCommand.ExecuteReader()
            }
            catch
            {
                Write-LocalLog -logMessage ("    2nd Attempt Error on: $currentSqlCommand. Error Message" + $error[0])
            }
        }
        
        $endTime = Get-Date
        Write-LocalLog -logMessage ("Count: $counter. Started at: $startTime. Time Now: $endTime")
        
        $currentSqlCommand = ""
    }
}