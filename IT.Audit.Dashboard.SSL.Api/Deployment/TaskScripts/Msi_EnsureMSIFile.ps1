﻿# ------------------------------------------------------------------
# ----------------------Replace/Update MSIFile----------------------
# ------------------------------------------------------------------
Param(  
        [parameter(mandatory=$true)][ValidateNotNullorEmpty()][string]$msiFile,
        [parameter()][ValidateNotNullorEmpty()][string]$targetDir,
        [parameter(mandatory=$true)][ValidateNotNullorEmpty()][string]$msiProductCodeGuid
     )

# $msiProductCodeGuid = "{CBC5B00A-67C1-475C-A6D7-7B7F5F99744B}"    
# $msiFile = "C:\Users\afooks\Downloads\AJG.Claims.ExchangeNotificationServiceSetup.msi"
# $targetDir = "C:\EMailListener\plusssomemorestuff"

function Remove-MSI(
        [parameter(mandatory=$true)][ValidateNotNullorEmpty()][string]$msiProductCodeGuid
    )
{
    # Remove the curly brackets if exist
    $msiProductCodeGuid = $msiProductCodeGuid.Replace('{', '').Replace('}', '')

    # If the passed string in not a valid GUID this with throw an exception. This is what we want.
    $a = [Guid]$msiProductCodeGuid

    # Msiexec want the guid with curly brackets, so add them.
    $msiProductCodeGuid = ("{" + $msiProductCodeGuid + "}")

    $arguments = @(
    "/x"
    "`"$msiProductCodeGuid`""
    "/qn"
    "/norestart"
    )

    Write-Output "Uninstalling MSI with Product Guid of: $msiProductCodeGuid"
    $process = Start-Process -FilePath msiexec.exe -ArgumentList $arguments -Wait -PassThru

    if ($process.ExitCode -eq 0){
         Write-Output "MSI: $msiProductCodeGuid has been successfully uninstalled"
    }
    elseif($process.ExitCode -eq 1605)
    {
        Write-Output "MSI: $msiProductCodeGuid was not installed"
    }
    else 
    {
        throw ("installer exit code  $($process.ExitCode) for file  $($msiProductCodeGuid)")
    }
}

function Install-msi(
        [parameter(mandatory=$true)][ValidateNotNullorEmpty()][string]$msiFile,
        [parameter()][ValidateNotNullorEmpty()][string]$targetDir
    )
{
    $arguments = @(
        "/i"
        "`"$msiFile`""
        "/qn"
        "/norestart"
    )

    # If the path isnt valid, it will throw and exception..this is what we want.
    $ignoreResult = [system.io.directory]::CreateDirectory($targetDir)
    $arguments += "TARGETDIR=`"$targetDir`""

    Write-Output "Installing: $msiFile, to: $targetDir....."

    $process = Start-Process -FilePath msiexec.exe -ArgumentList $arguments -Wait -PassThru
    if ($process.ExitCode -eq 0){
        Write-Output "$msiFile has been successfully installed"
    }
    else {
        throw ("installer exit code  $($process.ExitCode) for file  $($msifile)")
    }
}

Remove-MSI -msiProductCodeGuid $msiProductCodeGuid
Install-msi -msiFile $msiFile -targetDir $targetDir