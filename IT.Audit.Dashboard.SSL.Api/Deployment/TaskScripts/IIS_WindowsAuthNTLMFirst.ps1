# ------------------------------------------------------------------
# ---------------IIS Windows Auth ntlm first------------------------
# ------------------------------------------------------------------
PARAM(
        [Parameter(Mandatory=$true)][string]$SiteName
    )

#$SiteName = "Claims Workflow MVC Site"

Import-Module WebAdministration

Remove-WebConfigurationProperty -PSPath IIS:\ -Location $SiteName -filter system.webServer/security/authentication/windowsAuthentication/providers -name "."

Add-WebConfiguration -Filter system.webServer/security/authentication/windowsAuthentication/providers -PSPath IIS:\ -Location $SiteName -Value NTLM
Add-WebConfiguration -Filter system.webServer/security/authentication/windowsAuthentication/providers -PSPath IIS:\ -Location $SiteName -Value Negotiate