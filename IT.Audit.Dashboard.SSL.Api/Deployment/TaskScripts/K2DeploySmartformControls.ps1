param([Parameter(Mandatory=$true)][string]$SmartformControlXmlPath,[Parameter(Mandatory=$true)] [string]$SmartformDesignerPath, [Parameter(Mandatory=$true)] [string]$SmartformRuntimePath, [Parameter(Mandatory=$true)] [string]$ControlUtilExePath)


#--------------------------------------------------------------------------------------------------
# Test Values
#--------------------------------------------------------------------------------------------------
#SmartformControlXmlPath = "C:\Temp\BrowserMessaging\CustomSMFControlsConfig.xml"
#$SourePath = "C:\Temp\BrowserMessaging\"
#$ControlUtilExePath = "C:\Program Files (x86)\K2 blackpearl\Bin\controlutil.exe"
#$_path = "C:\Program Files (x86)\K2 blackpearl\Bin\"
#$SmartformDesignerPath = "C:\Program Files (x86)\K2 blackpearl\K2 SmartForms Designer\bin\"
#$SmartformRunTimePath = "C:\Program Files (x86)\K2 blackpearl\K2 SmartForms Runtime\bin\"
#--------------------------------------------------------------------------------------------------

iisreset

[XML]$xmlDoc = Get-Content $SmartformControlXmlPath

Set-Location $SmartformDesignerPath

foreach($control in $xmlDoc.SmartFormControl.Control)
{
    #Write-Host "Deploying: " + $control.AssemblyName
    $fullpath = $SourePath + $control.AssemblyName 
    
    #Write-Host "Copying to SMF bins"
    
    xcopy $fullpath $SmartformDesignerPath /y /r 
    xcopy $fullpath $SmartformRuntimePath /y /r 
    
    $HostPath = $SmartformDesignerPath + $control.AssemblyName

	#Write-Host "Regestering control"
	
	& $ControlUtilExePath register -assembly:$control.AssemblyName 
}


