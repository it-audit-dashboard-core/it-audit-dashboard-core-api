# ------------------------------------------------------------------
# -------Bulk Add AD group / user to SP Group--------------------
# ------------------------------------------------------------------
param(  [Parameter(Mandatory=$true)]$WebAppUrl,
        [Parameter(Mandatory=$true)]$SharePointGroupName,
        [Parameter(Mandatory=$true)]$ADGroupOrUserName,
        [Parameter(Mandatory=$true)]$ManagedPath,
        [ValidateSet("Exact", "StartsWith", "EndsWith", "Contains")][Parameter(Mandatory=$true)]$ADGroupNameComparision)

if ( (Get-PSSnapin -Name "Microsoft.sharepoint.powershell" -ErrorAction SilentlyContinue) -eq $null )
{
    Add-PSSnapin "Microsoft.sharepoint.powershell";
}

#$WebAppUrl = "http://dmsukdev.emea.ajgco.com"
#$SharePointGroupName = "Claims Handlers"
#$ADGroupOrUserName = "emea\g-cwf-members-dev"
#$ManagedPath = "Claims"
#$ADGroupNameComparision = "EndsWith"

$webApp = Get-SPWebApplication -Identity $WebAppUrl -ErrorAction SilentlyContinue
if(-not $webApp)
{
    throw "The Web Application could not be found"
}
$managedpath = Get-SPManagedPath -WebApplication $WebAppUrl -Identity $ManagedPath -ErrorAction SilentlyContinue
if(-not $managedpath)
{
    throw "The managed Path could not be found"
}

function IsMatch($Matchtype, $SPGroupName, $targetGrpNameString) 
{
    if ($Matchtype -eq "Exact")
    {
        return ($SPGroupName.ToLower() -eq $targetGrpNameString.ToLower())
    }
    elseif ($Matchtype -eq "StartsWith")
    {
        return ($SPGroupName.ToLower().StartsWith($targetGrpNameString.ToLower()))
    }
    elseif ($Matchtype -eq "EndsWith")
    {
        return ($SPGroupName.ToLower().EndsWith($targetGrpNameString.ToLower()))
    }
    elseif ($Matchtype -eq "Contains")
    {
        return ($SPGroupName.ToLower().Contains($targetGrpNameString.ToLower()))
    }
    else
    {
      return $false
    }
}

$webApp.Sites | Where-Object { $_.ServerRelativeUrl.ToLower().StartsWith("/" + $managedpath.Name.ToLower()) } | %{
    $site = $_
    
    $site.RootWeb.SiteGroups | %{
    
        $openGroup = $_ 
        
        if((IsMatch -Matchtype $ADGroupNameComparision -SPGroupName $openGroup.Name -targetGrpNameString $SharePointGroupName))
        {
			$user = $site.RootWeb.EnsureUser($ADGroupOrUserName)
			if ($user)
			{
				$openGroup.AddUser($user)
                $openGroup.Update()
				Write-Output ("Added user: " + $user.Name + ", To: " + $openGroup.Name)
			}
			else
			{
				Write-Output ("User: " + $currentUser.Name + " could not be found, so was not added to the security group.  Continuing anyway...")
			}
        }
    }
    $site.RootWeb.Update()
}
