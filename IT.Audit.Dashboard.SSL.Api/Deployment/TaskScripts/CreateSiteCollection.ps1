param($NewSiteUrl, $ContentDatabase, $WebSiteName, $WebsiteDesc, $Template, $PrimaryLogin, $PrimaryLoginEmail)

if ( (Get-PSSnapin -Name "Microsoft.sharepoint.powershell" -ErrorAction SilentlyContinue) -eq $null )
{
    Add-PSSnapin "Microsoft.sharepoint.powershell";
}

function ADUserExists($name)
{
    if($name -ne $null)
    {
        $Searcher =  [ADSISearcher]"(sAMAccountName=$Name)"
        $Results =  $Searcher.FindOne()
        If ($Results -ne $Null) 
        {
            return $true
        }
    }
    return $false
}
function GetUserNameFromDomainQualifiedAccount($account)
{
    if($account -ne $null){
        $loginName = $account.Split("\")[1]
        if($loginName -ne $null)
        {
            return $loginName
        }
        else
        {
            return $account
        }
    }
    return $null
}

try
{
    $PrimaryLoginName = GetUserNameFromDomainQualifiedAccount($PrimaryLogin)
    $AdAccountExists = ADUserExists($PrimaryLoginName)
    
    if(!$AdAccountExists)
    {
        throw "The Ad account is Invalid: $PrimaryLogin"
    }
    
    [Microsoft.SharePoint.SPWeb]$siteExists = Get-SPWeb $NewSiteUrl -ErrorAction SilentlyContinue

    if($siteExists -eq $null)
    {
        Write-Output "Creating site $NewSiteUrl"
        $site = New-SPSite "$NewSiteUrl" -Name "$WebSiteName"  -Description "$WebsiteDesc" -OwnerAlias "$PrimaryLogin" -OwnerEmail "$PrimaryLoginEmail" -Template "$Template" –Language 1033
    }
    else
    {
        Write-Output "Site already exists: $NewSiteUrl."
    }
    
    [Microsoft.SharePoint.SPWeb]$newSite = Get-SPWeb $NewSiteUrl -ErrorAction SilentlyContinue
    
    if($newSite -eq $null)
    {
        throw "The site has not been created: $NewSiteUrl"
    }
}
catch
{
    $ExceptionMessage = $_.Exception.Message
    Write-Output "An Error Occured in CreateSiteCollectionScript - $ExceptionMessage"
    throw $_.Exception
}