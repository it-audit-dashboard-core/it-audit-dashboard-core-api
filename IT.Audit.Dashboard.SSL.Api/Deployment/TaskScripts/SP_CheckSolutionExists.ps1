# ------------------------------------------------------------------
# Check if a sp solution exists
# ------------------------------------------------------------------
param(
        [Parameter(Mandatory=$true)][string]$SolutionFullName,
        [Parameter(Mandatory=$true)][string]$SuccessIfExists
)

if ( (Get-PSSnapin -Name "Microsoft.sharepoint.powershell" -ErrorAction SilentlyContinue) -eq $null )
{
    Add-PSSnapin "Microsoft.sharepoint.powershell";
}

#$SuccessIfExists = "true"
#$SolutionFullName = "AJG.Claims.SharePoint.wsp"

$SuccessIfExistsBool = [System.Convert]::ToBoolean($SuccessIfExists);

try
{
    $solution = Get-SPSolution -Identity $SolutionFullName -ErrorAction SilentlyContinue;
}
catch{}


if($solution)
{
    if($SuccessIfExistsBool -eq $true)
    {
        Write-Output ('Success: The following solution exists: ' + $SolutionFullName);
    }
    else
    {
        throw ('Failure: The Following solution Exists: ' + $SolutionFullName);
    }
}
else
{
    if($SuccessIfExistsBool -eq $true)
    {
        throw ('Failure: The following solution could not be found: ' + $SolutionFullName);
    }
    else
    {
        Write-Output ('Success: The following solution does not exist: ' + $SolutionFullName);
    }
}