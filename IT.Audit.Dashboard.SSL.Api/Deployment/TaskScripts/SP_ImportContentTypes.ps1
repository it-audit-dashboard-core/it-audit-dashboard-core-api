﻿# ------------------------------------------------------------------
# -------import Content types--------------------------------------
# ------------------------------------------------------------------

param(
	[Parameter(Mandatory=$true)][string]$xmlFilePath,
	[Parameter(Mandatory=$true)][string]$siteURL
)

if ( (Get-PSSnapin -Name "Microsoft.sharepoint.powershell" -ErrorAction SilentlyContinue) -eq $null )
{
    Add-PSSnapin "Microsoft.sharepoint.powershell";
}

$siteURL = "http://dmsuktest.emea.ajgco.com/dms/Claims"
$xmlFilePath = "E:\Installation\23_BaseTaxonomy Sites and Libraries\Claims Custom CT etc\SiteContentTypes-Claims.xml"

$destSite = Get-SPSite $siteURL -ErrorAction SilentlyContinue
$destWeb = Get-SPWeb $siteURL
Write-Output "  Adding site collection content types to: " $siteURL

if ($destWeb -eq $null)
{
	Write-Output "Error, site could not be opened."
	Break
}

#Create Site Content Types
$ctsXML = [xml](Get-Content($xmlFilePath))

$halt = $false

# Loop for each ContentType Element.
$ctsXML.ContentTypes.ContentType | ForEach-Object {

	$currentContentType = $_

    $spContentType = $null;
    $spContentType = $destWeb.ContentTypes[$currentContentType.Name]
    if($spContentType -ne $null) {
        $halt = $true
        Write-Output "** Content type already exists: " $spContentType.Name
        
    }
}

if($halt) {
    Write-Output "** Please delete the above content types or re-create the site before applying this script."
    Break
}

# Loop for each ContentType Element.
$ctsXML.ContentTypes.ContentType | ForEach-Object {

	$currentContentType = $_

    Write-Output " SiteCollection Content type: " $currentContentType.Name

    #Create Content Type object inheriting from parent
    $spContentType = $null
	$spContentType = New-Object Microsoft.SharePoint.SPContentType ($currentContentType.ID,$destWeb.ContentTypes,$currentContentType.Name)

    #Set Content Type description and group
    $spContentType.Description = $currentContentType.Description
    $spContentType.Group = $currentContentType.Group

	$fieldOrder = ""
	
    if($currentContentType.Fields -ne $null) {

		# Loop for each Field Element.
        $currentContentType.Fields.Field | ForEach-Object {
		
			$currentField = $_
        
            if($currentField -and !$spContentType.FieldLinks[$currentField.DisplayName])
            {
                Write-Output "     Field: " $currentField.DisplayName

                $spField = $null
                $spField = $destWeb.Fields[$currentField.DisplayName]

                if($spField -eq $null)
                {
                    #Create the new site custom column.

					#Ensure any Lookup Lists have the corred source Id's.
					$xmlDoc = [xml]$currentField.OuterXml
					$nodeType = $xmlDoc.SelectSingleNode("Field/@Type")
					
					if ($nodeType.Value.StartsWith("Lookup"))
					{
						$nodeName = $xmlDoc.SelectSingleNode("Field/@DisplayName")
						Write-Output "       Found Lookup Field name:" $nodeName.value
						#if  list exists.
						
						$lookupList = $openWeb.Lists[$nodeName.value]
						
						Write-Output "       Lookup List matched:" $lookupList.Id
						
						if ($lookupList)
						{
							# Matching list fonud to lookup fieldname, set correct Id's.
							$nodeList = $xmlDoc.SelectSingleNode("Field/@List")
							$nodeList.value = "{" + $lookupList.Id + "}"

							$nodeWebId = $xmlDoc.SelectSingleNode("Field/@WebId")
							$nodeWebId.value = $openWeb.Id
							Write-Output "       Lookup List and WebId was corrected to this site."
							
						}
					
					}
										
					$spField = $destWeb.Fields.AddFieldAsXml($xmlDoc.OuterXml)

                    $spField = $destWeb.Fields[$currentField.DisplayName]
                    Write-Output "                              New site Field created: " $currentField.DisplayName
					
					if ($currentField.DisplayName.EndsWith("SubType"))
					{
						$fieldOrder = $fieldOrder + $xmlDoc.SelectSingleNode("Field/@Name").Value
					}
                }

                #Create a field link for the Content Type by getting an existing column
                $spFieldLink = New-Object Microsoft.SharePoint.SPFieldLink ($spField)


                #Check to see if column should be Optional, Required or Hidden
                if ($_.Required -eq "TRUE") {$spFieldLink.Required = $true}
                if ($_.Hidden -eq "TRUE") {$spFieldLink.Hidden = $true}

                #Add column to Content Type
                $newFieldLink = $spContentType.FieldLinks.Add($spFieldLink)
            }
        }

	}


	# Loop for each Reorder Element. 
    $currentContentType.Reorder | ForEach-Object {
	$additionalField = $false
	$fieldList = ""
	
		if ($_)
		{
			# Specific order
			$currentReorder = $_

			Write-Output "     Reorder: " $currentReorder
			$spContentType.FieldLinks.Reorder($currentReorder.Split(','))
		}
		else
		{
			# Auto ordering.
			if ($fieldOrder)
			{
				Write-Output "     Order: " $fieldOrder
				$spContentType.FieldLinks.Reorder($fieldOrder.Split(','))
			}
		}
		
	}

	
	# Loop for each XmlDocument Element.   This is used to add support for document set content types, shared fields, welcome page columns, etc.
    $currentContentType.XmlDocuments.XmlDocument | ForEach-Object {

		if ($_)
		{
			$currentXmlDocument = $_
			
			$nameSpaceURI = $currentXmlDocument.NamespaceURI

			[xml]$xmlDoc = [xml]$currentXmlDocument.OuterXml
			$ns = New-Object Xml.XmlNamespaceManager $xmlDoc.NameTable
			$ns.AddNamespace("act", $uri)
			
			$node = $xmlDoc.SelectSingleNode("XmlDocument/*[1]", $ns)			

			[xml]$xmlDocReplacement = [xml]$node.OuterXml
			
			## TODO: Make XML definition more friendly and supportable: 
			## for each SharedField or WelcomePageField/FieldName element
			## lookup the id and add it in place of FieldName.
			## Check - that field is in the content type.
			## support an "(ALL) value that adds id's in the (re)order of the VISIBLE fields in the ReOrder element.

			$spContentType.XmlDocuments.Delete($nameSpaceURI)
			$spContentType.XmlDocuments.Add($xmlDocReplacement)

		}
	}
		
    #Create Content Type on the site and update Content Type object
    $ct = $destWeb.ContentTypes.Add($spContentType)
	
	$spContentType.Update()
}
    Write-Output "SiteCollection Content Types for " $destWeb.Url end.

$destWeb.Dispose()

