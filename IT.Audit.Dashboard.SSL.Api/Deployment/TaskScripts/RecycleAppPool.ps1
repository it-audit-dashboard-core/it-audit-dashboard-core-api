# ------------------------------------------------------------------
# -------Recycle App Pool-------------------------------------------
# ------------------------------------------------------------------
param(  [Parameter(Mandatory=$true)]$AppPoolName)

try
{

    $appPool = get-wmiobject -namespace "root\MicrosoftIISv2" -class "IIsApplicationPool" | Where-Object {$_.Name -eq "W3SVC/APPPOOLS/$AppPoolName"}

    if($appPool -eq $null)
    {
        throw "The App pool could not be found.." 

    }

    $appPool.Recycle();

    Write-Output "App pool recycled: $AppPoolName."
}
catch
{
    throw "There has been an error recycling the app pool: $AppPoolName. Exception: $_";
}