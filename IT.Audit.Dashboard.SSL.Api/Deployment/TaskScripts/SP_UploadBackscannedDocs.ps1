# ------------------------------------------------------------------
# ---------------Copy Directory-------------------------------------
# ------------------------------------------------------------------
#param(  [Parameter(Mandatory=$true)][string]$ConnectionString,
#        [Parameter(Mandatory=$true)][string]$DirectoryPath,
#        [Parameter(Mandatory=$true)][string]$SharePointWebAppUrl,
#        [Parameter(Mandatory=$true)][string]$SharePointManagedPath
#       )

if ( (Get-PSSnapin -Name "Microsoft.sharepoint.powershell" -ErrorAction SilentlyContinue) -eq $null )
{
    Add-PSSnapin "Microsoft.sharepoint.powershell";
}       

# Example /  Development variables
# Comment out before publishing
$ConnectionString = "Data Source=SMIUKVDBP132;Initial Catalog=ClaimsWorkflow;Integrated Security=True;Pooling=False;MultipleActiveResultSets=true;"
$DirectoryPath = '\\development01\devteam$\Andy Fooks\CMS Richo Backscans\3'
$SharePointWebAppUrl = "http://dmsuk.emea.ajgco.com"
$SharePointManagedPath = "claims"

$SPWebApplication = Get-SPWebApplication -Identity $SharePointWebAppUrl -ErrorAction silentlycontinue
$SPManagedPath = Get-SPManagedPath -WebApplication $SharePointWebAppUrl -Identity "/$SharePointManagedPath"  -WarningAction silentlycontinue
$runDate = Get-Date
$csvOutputLogPath = ($DirectoryPath.TrimEnd('\') + "\CMS_BackScanLog_" + $runDate.ToString("dd_MMMM_yyyy_H_mm_ss") + ".csv")

# functions
function Get-DBFieldValue ($whereFieldValue, $dbcolumnToReturn, $whereFieldName, $dbTableName)
{   
    # Create and open a database connection
    $sqlConnection = new-object System.Data.SqlClient.SqlConnection $ConnectionString
    $sqlConnection.Open()

    # Create a command object
    $sqlCommand = $sqlConnection.CreateCommand()
    $sqlCommand.CommandText = "SELECT * FROM [ClaimsWorkflow].[dbo].[$dbTableName] where [$whereFieldName] = '$whereFieldValue'"

    $sqlReader = $sqlCommand.ExecuteReader()

    if($sqlReader)
    {
        while ($sqlReader.Read())
        {
            $returnValue = $sqlReader[$dbcolumnToReturn]
            if($returnValue)
            {
                $sqlConnection.Close()
                return $returnValue;
            }
        }
    }
    $sqlConnection.Close()
}

function Get-GXBPolicyRef($FileRef)
{   
    return $FileRef.Substring(0, 9)
}

function Get-FileRefFromDocumentPrefix($DocumentPrefix)
{   
    if($DocumentPrefix.ToLower().StartsWith("bdrm"))
    {
        return $DocumentPrefix.Substring(0, 9)
    }
    else
    {
        return $DocumentPrefix.Substring(0, 11)
    }
}
function Get-IsItAPolicyOrClaimDocument($fullPDFDocName)
{
    if($fullPDFDocName.split('_')[$fullPDFDocName.split('_').Count-1].ToLower().StartsWith("cd"))
    {
        return "ClaimsDoc";
    }
    elseif($fullPDFDocName.split('_')[$fullPDFDocName.split('_').Count-1].ToLower().StartsWith("sp"))
    {
        return "PolicyDoc";
    }
    else
    {
        return "";
    }
}

function Save-DocToSP($SPWeb, $pdf, $stream, $docSetName, $isPolicyDoc)
{
    $UploadDateTime = Get-Date
    $originalDate = Get-Date -Date "2013-01-01 12:00:00Z"

    $SPFile = $SPWeb.Files.Add($SPWeb.ServerRelativeUrl + "/Claims Documents/$docSetName/" + $pdf.Name.Replace("&", "and"),$stream, $true)

    $name = $pdf.Name.Replace(".pdf", "").Replace(".PDF","")
    
    $SPFile.Item["Title"] = $name
    $SPFile.Item["cwfIsParentEmail"] = "false"
    $SPFile.Item["cwfdroppedintokl"] = "false"
    $SPFile.Item["cwforiginaldate"] = $originalDate.ToString()
    $SPFile.Item["Document_x0020_Type"] = "Ricoh Scan"
    
    $SPFile.Item.Update()
   
    $fileFullURL = $SharePointWebAppUrl.trim('/') + $SPFile.ServerRelativeUrl
    
    Add-Content -Path $csvOutputLogPath -Value ('"'+ $pdf.Name + '"' +","+'"' + $pdf.FullName + '"'+","+$isPolicyDoc+","+!$isPolicyDoc+","+$docSetName+","+"True"+","+"True"+","+'"'+$fileFullURL+'"'+","+$UploadDateTime.ToString("dd MMMM yyyy H:mm:ss"))
    Write-Output ("The File was successfully saved to: " + $SPFile.ServerRelativeUrl)
}

# Validation
if(-not(Test-Path $DirectoryPath))
{
    Write-Output "Directory does not exist"
    throw
}
if(-not($SPWebApplication))
{
    Write-Output "Web Application does not exist"
    throw
}
if(-not($SPManagedPath))
{
    Write-Output "Web Application does not exist"
    throw
}

Add-Content -Path $csvOutputLogPath -Value "File Name,File Full Path,Is Policy Doc,Is Claim Doc,File ref,Has Document Store,Saved to Sharepoint,SP Document Url,Upload DateTime"

if(Test-Path $DirectoryPath)
{
    Get-ChildItem -Path $DirectoryPath -Include '*.pdf' -Recurse | % {
        $pdf = $_
        $localFile = get-item -LiteralPath $pdf.FullName        
        $stream = $localFile.OpenRead()
        
        if((Get-IsItAPolicyOrClaimDocument -fullPDFDocName $pdf.Name) -eq "ClaimsDoc")
        {
            $fileRef = $pdf.Name.split('_')[0]
            $fileRef = Get-FileRefFromDocumentPrefix -DocumentPrefix $fileRef
            $claimFileStoreName = Get-DBFieldValue -whereFieldValue $fileRef -dbcolumnToReturn "SPSiteName" -whereFieldName "FileRef" -dbTableName "ClaimFile"
            
            $SPWeb = Get-SPWeb "$SharePointWebAppUrl/$SharePointManagedPath/$claimFileStoreName" -ErrorAction silentlycontinue
            if($SPWeb)
            {
                Save-DocToSP -SPWeb $SPWeb -pdf $pdf -stream $stream -docSetName $fileRef -isPolicyDoc $false
            }
            else
            {
                Add-Content -Path $csvOutputLogPath -Value ('"'+$pdf.Name+'"'+","+'"'+$pdf.FullName+'"'+","+"False"+","+"True"+","+'"'+$fileRef+'"'+","+"False"+","+"False"+","+""+","+"")
                Write-Output "There is no File store for the file with reference: $fileRef"
            }
            $SPWeb.Close()
        }
        elseif((Get-IsItAPolicyOrClaimDocument -fullPDFDocName $pdf.Name) -eq "PolicyDoc")
        {
            $fileRef = $pdf.Name.split('_')[0]
            $policyref = Get-GXBPolicyRef -FileRef $fileRef
            $policyFileStoreName = Get-DBFieldValue -whereFieldValue $policyref -dbcolumnToReturn "FileStore" -whereFieldName "PolicyRef" -dbTableName "PolicyFileStore"
            
            $SPWeb = Get-SPWeb "$SharePointWebAppUrl/$SharePointManagedPath/$policyFileStoreName" -ErrorAction silentlycontinue
            if($SPWeb)
            {
                Save-DocToSP -SPWeb $SPWeb -pdf $pdf -stream $stream -docSetName $policyref -isPolicyDoc $true
            }
            else
            {
                $pdfName = $pdf.Name
                $pdfFulleName = $pdf.FullName
                Add-Content -Path $csvOutputLogPath -Value ('"'+$pdf.Name+'"'+","+'"'+$pdf.FullName+'"'+","+"True"+","+"False"+","+'"'+$fileRef+'"'+","+"False"+","+"False"+","+""+","+"")
                Write-Output "There is no File store for the file with reference: $fileRef"
            }
            $SPWeb.Close()
        }
        else 
        {
            Add-Content -Path $csvOutputLogPath -Value ('"'+$pdf.Name+'"'+","+'"'+$pdf.FullName+'"'+","+"False"+","+"False"+","+'"'+$fileRef+'"'+","+"False"+","+"False"+","+""+","+"")
            Write-Output "Is Not a Policy or Claim Document."
        }
        
        $stream.Close()
    }
}
else
{
    Write-Host "Directory Path Does not exist."
}