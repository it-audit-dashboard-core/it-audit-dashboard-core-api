﻿# ------------------------------------------------------------------
# ---------------IIS Create Web Site--------------------------------
# ------------------------------------------------------------------
PARAM(
        [Parameter(Mandatory=$true)][string]$WebSiteName,
        [Parameter(Mandatory=$true)][string]$Port,
        [Parameter(Mandatory=$true)][string]$PhysicalPath,
        [Parameter(Mandatory=$true)][string]$ApplicationPool,
        [Parameter(Mandatory=$false)][string]$HostHeader
    )

#$WebSiteName = "Claims Workflow AF"
#$Port = "80"
#$PhysicalPath = "C:\inetpub\ClaimsWorkflowMVCSite"
#$ApplicationPool = "Claims Workflow App Pool"
#$HostHeader = "claimsuktest.emea.ajgco.com"

Import-Module WebAdministration

#Remove-Website -Name "Claims Workflow AF"
if($HostHeader)
{
    New-WebSite -Name $WebSiteName -HostHeader $HostHeader -Port $Port -PhysicalPath $PhysicalPath -ApplicationPool $ApplicationPool -Force
    Write-Output "Site created with specified host header"
}
else
{
    New-WebSite -Name $WebSiteName -Port $Port -PhysicalPath $PhysicalPath -ApplicationPool $ApplicationPool -Force
    Write-Output "Site created with no host header"   
}