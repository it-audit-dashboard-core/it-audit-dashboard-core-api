# ------------------------------------------------------------------
# Set Application Pool Property
# ------------------------------------------------------------------
param(
        [Parameter(Mandatory=$true)][string]$AppPoolName = $(throw "AppPoolName is a required parameter."),
        [Parameter(Mandatory=$true)][ValidateSet("IdleTimeout")]$AppPoolPropertyName,
        [Parameter(Mandatory=$true)][string]$AppPoolPropertyValue = $(throw "AppPoolPropertyValue is a required parameter.")
)

# Sample values - NOTE: Comment out before release!!
#$AppPoolName = "Claims Workflow MVC App Pool"
#$AppPoolPropertyName = "IdleTimeout"
#$AppPoolPropertyValue = "0"

$ApplicationPool = Get-WmiObject -Class IISApplicationPoolSetting -Namespace "root/microsoftiisv2" | Where-Object {$_.Name -eq "W3SVC/APPPOOLS/$AppPoolName"}
if($ApplicationPool)
{
    try
    {
        switch ($AppPoolPropertyName)
        {
            "IdleTimeout" {
                Write-Output "setting IdleTimeout to $AppPoolPropertyValue";
                $ApplicationPool.IdleTimeout=$AppPoolPropertyValue
                break;
            }
        }
        $ApplicationPool.Put()
        Write-Output "App Pool setting succeeded";
    }
    catch
    { 
        Write-Output "There has been a problem setting the property";
        throw;
    }
}
else
{
    Write-Output "The app pool: '$AppPoolName' could not be found";
    throw;
}