﻿# ------------------------------------------------------------------
# ---------------Copy Directory-------------------------------------
# ------------------------------------------------------------------
PARAM(
        [Parameter(Mandatory=$true)][string]$SourcePath,
        [Parameter(Mandatory=$true)][string]$DestinationPath,
        [ValidateSet("true", "false")][string]$Recurse
    )

#[string]$SourcePath = "C:\Source\repos\it-audit-dashboard-core-api\IT.Audit.Dashboard.SSL.Api\bin\Debug\IT_Audit_Dashboard_Core_DeploymentPackage\Code"
#[string]$DestinationPath = "C:\Users\ribone\Downloads\it-audit-dashboard-core-api"
#[string]$Recurse = "true"

$recurseBool = [System.Convert]::ToBoolean($Recurse);

New-Item -Path $DestinationPath -ItemType "directory" -ErrorAction SilentlyContinue

if($recurseBool)
{
    $DirectoryItems = Get-ChildItem -Path $SourcePath -Recurse
}
else
{
    $DirectoryItems = Get-ChildItem -Path $SourcePath
}

function Do-Work{
    $DirectoryItems | Copy-Item -ErrorAction SilentlyContinue -Destination {   
    if ($_.PSIsContainer) {               
        Join-Path $DestinationPath $_.Parent.FullName.Substring($SourcePath.length)
    } 
    else 
    {               
        Join-Path $DestinationPath $_.FullName.Substring($SourcePath.length)
    }            
 } -Force
}

try
{
    Do-Work
} catch{}
Do-Work

Write-Output "Copy files has completed"