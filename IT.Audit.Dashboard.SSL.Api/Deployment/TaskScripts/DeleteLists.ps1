# ------------------------------------------------------------------
# -------Delete Lists-----------------------------------------------
# ------------------------------------------------------------------
param(  [Parameter(Mandatory=$true)][string]$WebUrl,
        [Parameter(Mandatory=$true)][string]$ListNames)

if ( (Get-PSSnapin -Name "Microsoft.sharepoint.powershell" -ErrorAction SilentlyContinue) -eq $null )
{
    Add-PSSnapin "Microsoft.sharepoint.powershell";
}

$ListsNamesList = $ListNames.Split(',')

$web = Get-SPWeb $WebUrl
if($web -ne $null)
{
    ForEach ($listName in $ListsNamesList)
    {
        Write-Output $listName
        $list = $web.Lists[$listName];
        if($list -ne $null)
        {
            $list.Delete();
            Write-Output("List '$listName' deleted successfully");
        }
    }
    Write-Output("List Cleanup finished successfully");
}
else
{
    throw "The site does not exist: $web"
}
$web.Dispose();