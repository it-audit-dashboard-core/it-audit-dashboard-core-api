﻿# ------------------------------------------------------------------
# ---------------Sharepoint User Solution---------------------------
# ------------------------------------------------------------------
PARAM(
        [ValidateSet("Add", "Remove", "Replace")][string]$Action,
        [Parameter(Mandatory=$true)][string]$SolutionIdentity,
        [Parameter(Mandatory=$false)][string]$SolutionLiteralPath,
        [Parameter(Mandatory=$true)][string]$SiteUrl
    )

#[string]$Action = "Replace"
#[string]$SolutionIdentity = "AndyTestTemplate.wsp"
#[string]$SolutionLiteralPath = "C:\Users\ser-dms-00d-spadmin\Downloads\AndyTestTemplate.wsp"
#[string]$SiteUrl = "http://dmsukdev.emea.ajgco.com/dms/Claims"

if ( (Get-PSSnapin -Name "Microsoft.sharepoint.powershell" -ErrorAction SilentlyContinue) -eq $null )
{
    Add-PSSnapin "Microsoft.sharepoint.powershell";
}

# Check that the solution file exists.
if(($Action.ToLower() -eq "add") -or ($Action.ToLower() -eq "replace"))
{
    if(-not (Test-Path -Path $SolutionLiteralPath))
    {
        throw ("The SP User Solution: $SolutionLiteralPath could not be found")
    }
}

# Check that the Site collection exists
if(-not (Get-SPSite $SiteUrl -ErrorAction:silentlycontinue))
{
    throw ("The SP Site: $SiteUrl could not be found")
}

# Try to add the solution, but throw an error if there is already one deployed..
function AddSPUserSolution(
    [Parameter(Mandatory=$true)][string]$SolutionIdentity,
    [Parameter(Mandatory=$false)][string]$SolutionLiteralPath,
    [Parameter(Mandatory=$false)][string]$SiteUrl)
{
    if(-not (Get-SPUserSolution -Identity $SolutionIdentity -Site $SiteUrl -ErrorAction:silentlycontinue))
    {
        Write-Output "Adding SP User Solution: $SolutionIdentity"
        Add-SPUserSolution -LiteralPath $SolutionLiteralPath  -Site $SiteUrl -Confirm:$false -ErrorAction:silentlycontinue
    }
    else
    {
        throw ("There is already a solution with this name deployed to $SiteUrl")
    }
}

# Try to instal solution - throw error if the solution is not available.
function InstallSPUserSolution(
    [Parameter(Mandatory=$true)][string]$SolutionIdentity,
    [Parameter(Mandatory=$false)][string]$SiteUrl)
{
    $SPUserSolution = Get-SPUserSolution -Identity $SolutionIdentity -Site $SiteUrl -ErrorAction:silentlycontinue
    if($SPUserSolution)
    {
        # Activate the solution if it is deactivated.
        if($SPUserSolution.Status.ToString().ToLower() -ne "activated")
        {
            Write-Output "Installing SP User Solution: $SolutionIdentity"
            Install-SPUserSolution -Identity $SolutionIdentity -Site $SiteUrl -Confirm:$false -ErrorAction:silentlycontinue
        }
    }
    else
    {
        throw ("The Solution Could not be found.")
    }
}

# Uninstall the solution: throw exception if the solution is not successfully disabled
function UninstallSPUserSolution(
    [Parameter(Mandatory=$true)][string]$SolutionIdentity,
    [Parameter(Mandatory=$false)][string]$SiteUrl)
{
    # Uninstall the solution if the solution exists and is installed 
    $SPUserSolution = Get-SPUserSolution -Identity $SolutionIdentity -Site $SiteUrl -ErrorAction:silentlycontinue
    if($SPUserSolution)
    {
        # Activate the solution if it is deactivated.
        if($SPUserSolution.Status.ToString().ToLower() -eq "activated")
        {
            Write-Output "Uninstalling SP User Solution: $SolutionIdentity"
            Uninstall-SPUserSolution -Identity $SolutionIdentity -Site $SiteUrl -Confirm:$false -ErrorAction:silentlycontinue
        }
    }
    $SPUserSolution = Get-SPUserSolution -Identity $SolutionIdentity -Site $SiteUrl -ErrorAction:silentlycontinue
    if($SPUserSolution)
    {
        # Ensure that the solution is deactivated
        if($SPUserSolution.Status.ToString().ToLower() -eq "activated")
        {
            throw ("Was unabele to uninstall the SP User solution: $SolutionIdentity")
        }
    }
}

# Remove the solution: if the soltion is not removed sucessfully, throw an error.
function RemoveSPUserSolution(
    [Parameter(Mandatory=$true)][string]$SolutionIdentity,
    [Parameter(Mandatory=$false)][string]$SiteUrl)
{
    # Uninstall the solution if the solution exists and is installed 
    $SPUserSolution = Get-SPUserSolution -Identity $SolutionIdentity -Site $SiteUrl -ErrorAction:silentlycontinue
    if($SPUserSolution)
    {
        # Activate the solution if it is deactivated.
        if($SPUserSolution.Status.ToString().ToLower() -eq "activated")
        {
            throw ("The SP User Solution: $SolutionIdentity is activated. Deactivate before trying to Remove.")
        }
        else
        {
            Write-Output "Removeing SP User Solution: $SolutionIdentity"
            Remove-SPUserSolution -Identity $SolutionIdentity -Site $SiteUrl -Confirm:$false -ErrorAction:silentlycontinue
        }
    }
    $SPUserSolution = Get-SPUserSolution -Identity $SolutionIdentity -Site $SiteUrl -ErrorAction:silentlycontinue
    if($SPUserSolution)
    {
        throw ("Was unabele to Remove the SP User solution: $SolutionIdentity")
    }
}

# Do what needs to be done
if($Action.ToLower() -eq "add")
{
    AddSPUserSolution -SolutionIdentity $SolutionIdentity -SiteUrl $SiteUrl -SolutionLiteralPath $SolutionLiteralPath
    InstallSPUserSolution -SolutionIdentity $SolutionIdentity -SiteUrl $SiteUrl
}
elseif($Action.ToLower() -eq "remove")
{
    UninstallSPUserSolution -SolutionIdentity $SolutionIdentity -SiteUrl $SiteUrl
    RemoveSPUserSolution -SolutionIdentity $SolutionIdentity -SiteUrl $SiteUrl
}
else{
    UninstallSPUserSolution -SolutionIdentity $SolutionIdentity -SiteUrl $SiteUrl
    RemoveSPUserSolution -SolutionIdentity $SolutionIdentity -SiteUrl $SiteUrl
    AddSPUserSolution -SolutionIdentity $SolutionIdentity -SiteUrl $SiteUrl -SolutionLiteralPath $SolutionLiteralPath
    InstallSPUserSolution -SolutionIdentity $SolutionIdentity -SiteUrl $SiteUrl
}