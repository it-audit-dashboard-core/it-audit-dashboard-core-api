# ------------------------------------------------------------------
# ---------------Check File Exists----------------------------------
# ------------------------------------------------------------------
param(  [Parameter(Mandatory=$true)][string]$FilePath,
        [Parameter(Mandatory=$true)][string]$SuccessIfExists)

$SuccessIfExistsBool = [System.Convert]::ToBoolean($SuccessIfExists);
if(Test-Path $FilePath)
{
    if($SuccessIfExistsBool -eq $true)
    {
        Write-Output ('Success: The following file exists: ' + $FilePath);
    }
    else
    {
        throw ('Failure: The Following File Exists: ' + $FilePath);
    }
}
else
{
    if($SuccessIfExistsBool -eq $true)
    {
        throw ('Failure: The following File could not be found: ' + $FilePath);
    }
    else
    {
        Write-Output ('Success: The following file does not exist: ' + $FilePath);
    }
}