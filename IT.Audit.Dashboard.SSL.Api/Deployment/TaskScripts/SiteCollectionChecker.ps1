# ------------------------------------------------------------------
# -------Site Collection Check----------------------------------------
# ------------------------------------------------------------------

param(  [Parameter(Mandatory=$true)][string]$SiteCollectionUrl)

if ( (Get-PSSnapin -Name "Microsoft.sharepoint.powershell" -ErrorAction SilentlyContinue) -eq $null )
{
    Add-PSSnapin "Microsoft.sharepoint.powershell";
}

try
{
    $siteCollection = Get-SPSite $SiteCollectionUrl 
    
    if($siteCollection -eq $null)
    {
        throw "The Site Collection could not be found: $SiteCollectionUrl"
    }
    
    $SiteCollectionID = $siteCollection.ID;
    
    Write-Output "The validator Succeeded. The ID of the Site Collection is: $SiteCollectionID"
}
catch
{
    throw "Unable to Access the Site collection: $SiteCollectionUrl"
}