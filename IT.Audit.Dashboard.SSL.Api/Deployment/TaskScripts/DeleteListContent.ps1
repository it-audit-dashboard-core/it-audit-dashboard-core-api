# ------------------------------------------------------------------
# -------Delete List Content----------------------------------------
# ------------------------------------------------------------------

param(  [Parameter(Mandatory=$true)][string]$WebUrl,
        [Parameter(Mandatory=$true)][string]$ListNames)

if ( (Get-PSSnapin -Name "Microsoft.sharepoint.powershell" -ErrorAction SilentlyContinue) -eq $null )
{
    Add-PSSnapin "Microsoft.sharepoint.powershell";
}

$ListsNamesList = $ListNames.Split(',')

$web = Get-SPWeb $WebUrl

if($web -ne $null)
{
    foreach($listName in $ListsNamesList){
        $SPList = $web.Lists[$listName];
        if($SPList -ne $null)
        {
            foreach ($item in $SPList.Items)
            {
                $SPList.GetItemById($item.ID).Delete();
            }
            Write-Output("List Items(s) successfully deleted from: $listName");
        }
    }
}
else
{
    throw "The site does not exist: $web"
}
$web.Dispose();