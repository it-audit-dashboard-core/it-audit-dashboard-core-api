﻿# ------------------------------------------------------------------
# ------------------------Uninstall MSI File------------------------
# ------------------------------------------------------------------
Param(
        [parameter(mandatory=$true)][ValidateNotNullorEmpty()][string]$msiProductCodeGuid
     )

# $msiProductCodeGuid = "{CBC5B00A-67C1-475C-A6D7-7B7F5F99744B}"

# Remove the curly brackets if exist
$msiProductCodeGuid = $msiProductCodeGuid.Replace('{', '').Replace('}', '')

# If the passed string in not a valid GUID this with throw an exception. This is what we want.
$a = [Guid]$msiProductCodeGuid

# Msiexec want the guid with curly brackets, so add them.
$msiProductCodeGuid = ("{" + $msiProductCodeGuid + "}")

$arguments = @(
"/x"
"`"$msiProductCodeGuid`""
"/qn"
"/norestart"
)

Write-Output "Uninstalling MSI with Product Guid of: $msiProductCodeGuid"
$process = Start-Process -FilePath msiexec.exe -ArgumentList $arguments -Wait -PassThru

if ($process.ExitCode -eq 0){
     Write-Output "MSI: $msiProductCodeGuid has been successfully uninstalled"
}
elseif($process.ExitCode -eq 1605)
{
    Write-Output "MSI: $msiProductCodeGuid was not installed"
}
else 
{
    throw ("installer exit code  $($process.ExitCode) for file  $($msiProductCodeGuid)")
}
