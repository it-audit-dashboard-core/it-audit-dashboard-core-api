# ------------------------------------------------------------------
# ---             Sharepoint - Add remove mime type              ---
# mime types here:                                               ---
# http://technet.microsoft.com/en-us/library/bb742440.aspx#ECAA  ---
# ------------------------------------------------------------------
param(  [Parameter(Mandatory=$true)][string]$WebAppUrl,
        [Parameter(Mandatory=$true)][string]$MimeType,
        [Parameter(Mandatory=$true)][string]$AddMimeType)

$addMimeTypeBool = [System.Convert]::ToBoolean($AddMimeType);

If ( (Get-PSSnapin -Name "Microsoft.SharePoint.PowerShell" -ErrorAction SilentlyContinue) -eq $null ) 
{ 
    Add-PSSnapin "Microsoft.SharePoint.PowerShell" 
} 

$WebApp = Get-SPWebApplication $WebAppUrl 

if($addMimeTypeBool)
{
    If ($WebApp.AllowedInlineDownloadedMimeTypes -notcontains $MimeType) 
    {
        $WebApp.AllowedInlineDownloadedMimeTypes.Add($MimeType) 
        $WebApp.Update()
        Write-Output "The $MimeType MIME type has been successfully added." 
    }
    Else
    {
        Write-Output "The $MimeType MIME type already present on web app."
    }
}
else
{
    If ($WebApp.AllowedInlineDownloadedMimeTypes -contains $MimeType) 
    {
        $WebApp.AllowedInlineDownloadedMimeTypes.Remove($MimeType)
        $WebApp.Update()
        Write-Output "The $MimeType MIME type has been successfully removed." 
    }
    Else
    {
        Write-Output "The $MimeType MIME does not exist on web app."
    }
}