# ------------------------------------------------------------------
# --------Create/Update a SharePoint Role Definition----------------
# ------------------------------------------------------------------
param(  [Parameter(Mandatory=$true)][string]$SPWebUrl,
        [Parameter(Mandatory=$true)][string]$RoleDefinitionName,
        [Parameter(Mandatory=$true)][string]$RoleDefinitionDescription,
        [Parameter(Mandatory=$true)][string]$BasePermissions_CommaSeperated
     )

# Use the command bellow \/ to get a list of possible BasePermission values    
# [System.Enum]::GetNames("Microsoft.SharePoint.SPBasePermissions")

if ((Get-PSSnapin -Name "Microsoft.sharepoint.powershell" -ErrorAction SilentlyContinue) -eq $null )
{
    Add-PSSnapin "Microsoft.sharepoint.powershell";
}

#$SPWebUrl = "http://dmsukdev.emea.ajgco.com/claims/Liability";
#$RoleDefinitionName = "Add Only";
#$RoleDefinitionDescription = "Some role description";
#$BasePermissions_CommaSeperated = "ViewListItems,AddListItems,Open";
#$BasePermissions_CommaSeperated = "ViewListItems";

$spWeb = Get-SPWeb $SPWebUrl;
$spRoleDefinition = $spWeb.RoleDefinitions[$RoleDefinitionName];

if($spRoleDefinition -eq $null){    
    # Role Definition Doesn't Exist
    Write-Output "Adding Role Definition"
    $spRoleDefinition = New-Object Microsoft.SharePoint.SPRoleDefinition;
    $spRoleDefinition.Name = $RoleDefinitionName;  
    $spRoleDefinition.Description = $RoleDefinitionDescription;
   
    $spRoleDefinition.BasePermissions = $BasePermissions_CommaSeperated;    
    $spWeb.RoleDefinitions.Add($spRoleDefinition);
    Write-Output "The Role definition has been added"
}
else
{
    Write-Output "The Role definition already exists"
    # The Role already exists...ensure the base permissions are correct.
    $spRoleDefinition.BasePermissions = $BasePermissions_CommaSeperated;
    $spRoleDefinition.Description = $RoleDefinitionDescription;
    $spRoleDefinition.Update();
    Write-Output "Updating role definition BasePermissions and Description"
}

$spWeb.Dispose();