﻿# ------------------------------------------------------------------
# -------------Visual Studio Devenv build Solution / Project---------------
# Switches Explained here
# https://docs.microsoft.com/en-us/visualstudio/ide/reference/devenv-command-line-switches?view=vs-2019
#
# ------------------------------------------------------------------
Param(
        [parameter(mandatory=$false)][ValidateNotNullorEmpty()][string]$solutionFilePath = "",
        [parameter(mandatory=$false)][ValidateNotNullorEmpty()][string]$projectFilePath = "",
        [parameter(mandatory=$true)][ValidateSet("project","solution","projectInSolution")][string]$target,
        [parameter(mandatory=$true)][ValidateSet("clean","build","rebuild","deploy")][string]$action,
        [parameter(mandatory=$true)][ValidateSet("debug","release", "")][string]$buildConfig      
     )

#$solutionFilePath = "C:\Users\afooks\source\repos\fusion\Fusion Service - Security Server Components\Fusion Security Server Components.sln"
#$projectFilePath = "C:\Users\afooks\source\repos\fusion\Fusion Service - Security Server Components\Fusion Security Server Setup\BFESecurityServerSetup.vdproj"
#$solutionFilePath = "C:\Users\afooks\source\repos\ConsoleApp1\ConsoleApp1.sln"
#$projectFilePath = "C:\Users\afooks\source\repos\ConsoleApp1\ConsoleApp1\ConsoleApp1.csproj"
#$target = "projectInSolution"
#$action = "Rebuild"
#$buildConfig = "Release|Win32"

function validate-Input
{    
    # Check the values based on the target
    if($target -eq "projectInSolution" -and (-not [System.IO.File]::Exists($solutionFilePath) -or -not [System.IO.File]::Exists($projectFilePath))){        
        throw "target of: $target, requires that both solutionFilePath and projectFilePath have a value and the files exist."
    }
    if($target -eq "project" -and -not [System.IO.File]::Exists($projectFilePath)) {
         throw "target of: $target, requires that both projectFilePath has a value and the file exists."
    }
    if($target -eq "project" -and -not [System.IO.File]::Exists($solutionFilePath)) {
         throw "target of: $target, requires that both solutionFilePath has a value and the file exists."
    }
    # Validate values based on the action
    if(($action -eq "build" -or $action -eq "rebuild" -or $action -eq "deploy") -and [string]::IsNullOrEmpty($buildConfig)) {
        throw "action of: $action, requires that both buildConfig must be set to either debug or release."
    }
}
function Get-LatestBuildExePath
{
    Param (
        [parameter(mandatory=$true)][ValidateSet("MsBuild.exe", "devenv.exe")][string]$targetExe
    )
    [bool] $vsSetupExists = $null -ne (Get-Command Get-VSSetupInstance -ErrorAction SilentlyContinue)
    if (!$vsSetupExists)
    {
        Write-Verbose "Importing the VSSetup module in order to determine TF.exe path..." -Verbose
        Install-Module VSSetup -Scope CurrentUser -Force     
    }
    [string] $visualStudioInstallationPath = (Get-VSSetupInstance | Select-VSSetupInstance -Latest -Require Microsoft.Component.MSBuild).InstallationPath
    $msBuildExecutableFilePath = (Get-ChildItem $visualStudioInstallationPath -Recurse -Filter $targetExe | Select-Object -First 1).FullName
    $script:exePath = $msBuildExecutableFilePath

    if(-Not [System.IO.File]::Exists($script:exePath)) {
        throw "The $targetExe file could not be found!"
    }
}
function Get-LogPath {
    $script:TemplogPath = [System.IO.Path]::Combine($PSScriptRoot, "TempLog_$((Get-Date).ToString("ddMMMyyy_HHmmss")).txt")
}
function Execute-exe 
{    
    # If build or rebuild
    if($action -eq 'build' -or $action -eq 'rebuild' -or $action -eq 'deploy') {        
        if($target -eq 'project') {
            & "$($script:exePath)" $projectFilePath /$action $buildConfig /out $script:TemplogPath | Out-Null
        }
        if($target -eq 'solution') {
            & "$($script:exePath)" $solutionFilePath /$action $buildConfig /out $script:TemplogPath | Out-Null
        }
        if($target -eq 'projectInSolution') {
            write-host "entered projectin solution"
            & "$($script:exePath)" $solutionFilePath /$action 'Debug' /Project $projectFilePath /projectconfig 'Debug' /out $script:TemplogPath | Out-Null
        }
    }
    if($action -eq 'clean') {
        if($target -eq 'project') {
            & "$($script:exePath)" $projectFilePath /$action $buildConfig /out $script:TemplogPath | Out-Null
        }
        if($target -eq 'solution') {
            & "$($script:exePath)" $solutionFilePath /$action $buildConfig /out $script:TemplogPath | Out-Null
        }
    }
}
function Check-TheExeResult {
    # now check the result
    $logContent = Get-Content -Path $script:TemplogPath
    $logstring = $logContent | Out-String
    Write-Host $logstring
    # Delete the Log
    if([System.IO.File]::Exists($script:TemplogPath)){
       [System.IO.File]::Delete($script:TemplogPath)
    }
    # Search for Erros and throw exception if exist
    if(([regex]::Matches($logstring,'ERROR:')).Count -gt 0) {    
        Throw ("An Error occured: $logstring")
    }
}
validate-Input
Get-LogPath
Get-LatestBuildExePath -targetExe devenv.exe
Write-Host "the devenv.exe file being used is: $script:exePath"
Write-Host "The devenv temp log is being written here: $script:TemplogPath"
Execute-exe
Check-TheExeResult

#Add-Content -Path "C:\Users\afooks\source\repos\ConsoleApp1\FusionWCFServiceSetup.vdproj" -Value "   "