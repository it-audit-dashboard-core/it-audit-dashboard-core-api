﻿using AutoMapper;
using FluentValidation.AspNetCore;
using IT.Audit.Dashboard.Application;
using IT.Audit.Dashboard.Domain;
using IT.Audit.Dashboard.Domain.Shared;
using IT.Audit.Dashboard.EntityFrameworkCore.Domain;
using IT.Audit.Dashboard.EntityFrameworkCore.Repositories;
using IT.Audit.Dashboard.Sql.Domain;
using IT.Audit.Dashboard.Sql.Providers;
using IT.Audit.Dashboard.SSL.Api;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using System;

namespace IT.Audit.Dashboard.EntityFrameworkCore
{
    public static class IServiceCollectionExtensions
    {
        public static IServiceCollection ConfigureCors(this IServiceCollection services)
        {
            return services.AddCors(options =>
            {
                options.AddPolicy("IT.Audit.Dashboard.Origin",
                builder => builder.WithOrigins("https://localhost:4200").AllowAnyHeader().AllowAnyMethod().AllowCredentials());
            });
        }

        public static IServiceCollection ConfigureDatabase(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddTransient<IConnectionProviderFactory, ConnectionProviderFactory>();
            services.AddTransient<IConnectionProvider, ConnectionProvider>();

            services.AddTransient<IDashboardUnitOfWork, DashboardUnitOfWork>();
            services.AddScoped<Func<DbFactory>>((provider) => () => provider.GetService<DbFactory>());

            services.AddDbContext<ITAuditDashboardDbContext>(options =>
            {
                options.UseSqlServer(configuration.GetConnectionString(ConnectionStringKey.ITAuditDashboard));
            });

            return services;
        }

        public static IServiceCollection ConfigureRepositories(this IServiceCollection services)
        {
            return services
                .AddScoped<IDataSourceRepository, DataSourceRepository>() 
                .AddScoped<IUserApplicationRepository, UserApplicationRepository>()
                .AddScoped<IUserApplicationGroupsRepository, UserApplicationGroupsRepository>()
                .AddScoped<IPageSettingsRepository, PageSettingsRepository>()
                .AddScoped<IIbaUsersRepository, IbaUsersRepository>();
        }

        public static IServiceCollection ConfigureServices(this IServiceCollection services)
        {
            return services
                .AddScoped<IDashboardDomainManager, DashboardDomainManager>()
                .AddScoped<IDataInformationDomainManager, DataInformationDomainManager>()
                .AddScoped<IDashboardAppService, DashboardAppService>()
                .AddScoped<IDataInformationAppService, DataInformationAppService>()
                .AddScoped<IPageSettingsAppService, PageSettingsAppService>()
                .AddScoped<IIbaUsersAppService, IbaUsersAppService>()
                .AddScoped<IUserApplicationAppService, UserApplicationAppService>();

        }
        public static IServiceCollection ConfigureSwagger(this IServiceCollection services)
        {
            return services
                .AddSwaggerGen(c =>
                {
                    c.SwaggerDoc("v1", new OpenApiInfo { Title = "IT.Audit.Dashboard.SSL.Api", Version = "v1" });
                });

        }
        public static IServiceCollection ConfigureAutoMapper(this IServiceCollection services)
        {
            return services
                .AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies())
                .AddTransient<IMapper, Mapper>();
        }

        public static IServiceCollection ConfigureApplication(this IServiceCollection services)
        {
            services.AddMvc(config =>
            {
                var policy = new AuthorizationPolicyBuilder()
                    .RequireAuthenticatedUser()
                    .Build();
                config.Filters.Add(new AuthorizeFilter(policy));
            });

            services.AddControllers()
                .AddFluentValidation(s =>
                {
                    s.RegisterValidatorsFromAssemblyContaining<Startup>();
                    s.DisableDataAnnotationsValidation = true;
                });

            return services;
        }
        public static IServiceCollection ConfigureConnectionProvider(this IServiceCollection services)
        {
            return services
                .AddScoped<IConnectionProviderFactory, ConnectionProviderFactory>()
                .AddScoped<IConnectionProvider, ConnectionProvider>();
        }
    }
}
