using AutoMapper;
using FluentValidation.AspNetCore;
using IT.Audit.Dashboard.Application;
using IT.Audit.Dashboard.Domain.Security;
using IT.Audit.Dashboard.Domain.Shared;
using IT.Audit.Dashboard.EntityFrameworkCore;
using IT.Audit.Dashboard.EntityFrameworkCore.IbaBdrm;
using IT.Audit.Dashboard.EntityFrameworkCore.ItAuditDashboard;
using IT.Audit.Dashboard.EntityFrameworkCore.ItAuditDashboard.Data;
using IT.Audit.Dashboard.Sql;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Server.IISIntegration;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using System;
using System.Text.Json.Serialization;

namespace IT.Audit.Dashboard.SSL.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("IT.Audit.Dashboard.Origin",
                builder => builder.WithOrigins("https://localhost:4200").AllowAnyHeader().AllowAnyMethod().AllowCredentials());
            });

            //services.AddAuthentication(options =>
            //{
            //    options.DefaultAuthenticateScheme = IISDefaults.AuthenticationScheme;
            //});
            //services.AddAuthorization();

            var itAuditDashboardconnectionString = Configuration.GetConnectionString(ConnectionStringKey.ITAuditDashboard);
            var ibaBdrmconnectionString = Configuration.GetConnectionString(ConnectionStringKey.IbaUsers);

            services.AddDbContext<ITAuditDashboardDbContext>(options
                => options.UseSqlServer(itAuditDashboardconnectionString));

            services.AddDbContext<IbaBdrmDbContext>(options
                => options.UseSqlServer(ibaBdrmconnectionString));

            services.AddTransient<IClaimsTransformation, DashboardClaimsTransformation>();
            services.AddAuthentication(IISDefaults.AuthenticationScheme);

            services.AddMvc(config =>
            {
                var policy = new AuthorizationPolicyBuilder()
                    .RequireAuthenticatedUser()
                    .Build();
                config.Filters.Add(new AuthorizeFilter(policy));
            });

            //services.AddAuthentication(IISDefaults.AuthenticationScheme);

            services.AddControllers()
                .AddFluentValidation(s =>
                {
                    s.RegisterValidatorsFromAssemblyContaining<Startup>();
                    s.DisableDataAnnotationsValidation = true;
                })
                .AddJsonOptions(o => o.JsonSerializerOptions.ReferenceHandler = ReferenceHandler.Preserve);

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "IT.Audit.Dashboard.SSL.Api", Version = "v1" });
            });

            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
            services.AddTransient<IMapper, Mapper>();
            
            services.AddTransient<ISecurityManager, SecurityManager>();

            services.AddApplicationServices();
            services.AddRepositoryServices();
            //services.AddSqlRepositoryServices();
            services.AddIbaBdrmRepositoryServices();
            services.AddItAuditDashboardRepositoryServices();

        }

        /*
                services
                .ConfigureCors()
                .ConfigureDatabase(Configuration)
                .AddAuthentication(IISDefaults.AuthenticationScheme);

            services
                .ConfigureApplication()
                .ConfigureSwagger()
                .ConfigureAutoMapper()
                .ConfigureConnectionProvider()
                .ConfigureServices()
                .ConfigureRepositories();
        */

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "IT.Audit.Dashboard.SSL.Api v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            //app.UseClaimsTransformation();
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseCors("IT.Audit.Dashboard.Origin");

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
