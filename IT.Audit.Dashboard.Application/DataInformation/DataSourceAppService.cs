﻿using AutoMapper;
using System.Threading.Tasks;
using IT.Audit.Dashboard.Domain.Shared;
using Microsoft.Extensions.Caching.Memory;
using IT.Audit.Dashboard.Sql.Domain;
using IT.Audit.Dashboard.Application.Dtos;

namespace IT.Audit.Dashboard.Application
{
    /// <summary>
    /// Replaces Modified date
    /// </summary>
    public class DataSourceAppService : IDataSourceAppService
    {
        private readonly IDataSourceRepository _repository;
        private readonly IMemoryCache _cache;
        private readonly IMapper _mapper;

        public DataSourceAppService(IDataSourceRepository repository, IMemoryCache cache, IMapper mapper)
        {
            _repository = repository;
            _cache = cache;
            _mapper = mapper;
        }

        private async Task<DataSourceDto> GetAsync(ConnectionType connectionType, string tableName, string cacheKey) 
        {
            return await _cache.GetOrCreateAsync(
                IbaUsersConsts.CacheKeyIbaModifiedDate,
                entry => GetFromDatabaseAsync(connectionType, tableName));
        }

        public Task<DataSourceDto> GetAsync(DataInformationInputDto inputDto)
        {
            throw new System.NotImplementedException();
        }

        public async Task<DataSourceDto> GetFromDatabaseAsync(ConnectionType connectionType, string tableName)
        {
            var result = await _repository.GetModifiedDate(connectionType, tableName);
            return _mapper.Map<DataSourceDto>(result);
        }

        public Task<DataSourceDto> GetFromDatabaseAsync(DataInformationInputDto inputDto)
        {
            throw new System.NotImplementedException();
        }
    }
}
