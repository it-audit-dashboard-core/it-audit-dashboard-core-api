﻿using AutoMapper;
using IT.Audit.Dashboard.Application.Dtos;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Memory;
using System.Collections.Generic;
using IT.Audit.Dashboard.EntityFrameworkCore.Domain;

namespace IT.Audit.Dashboard.Application
{
    /// <summary>
    /// Replaces Modified date
    /// </summary>
    public class DataInformationAppService : IDataInformationAppService
    {
        private readonly IDataInformationDomainManager _domainManager;
        private readonly IPageSettingsAppService _pageSettingsAppService;
        private readonly IMemoryCache _cache;
        private readonly IMapper _mapper;

        public DataInformationAppService(
            IDataInformationDomainManager domainManager,
            IPageSettingsAppService pageSettingsAppService,
            IMemoryCache cache,
            IMapper mapper)
        {
            _domainManager = domainManager;
            _pageSettingsAppService = pageSettingsAppService;
            _cache = cache;
            _mapper = mapper;
        }

        private async Task<List<DataSourceOutputDto>> GetFromCacheOrDatabaseAsync(DataSourceInputDto inputDto)
        {
            var pageSettings = inputDto.PageSettings;
            var dataSources = new List<DataSource>();

            foreach (var setting in pageSettings)
            {
                var dataSource = await _cache.GetOrCreateAsync(
                    setting.CacheKey,
                    entry => _domainManager.GetFromDatabaseAsync(setting.TableName));

                //dataSource.Name = setting.Name;
                //dataSources.Add(dataSource);
            }

            return _mapper.Map<List<DataSourceOutputDto>>(dataSources);
        }

        public async Task<DataInformationOutputDto> GetAsync(DataInformationInputDto inputDto)
        {
            // Get connectionType from dto and load PageSettings
            var dataSourceInputDto = new DataSourceInputDto
            {
                PageSettings = await _pageSettingsAppService.GetListAsync(inputDto.ConnectionType)
            };

            var dataInformationOutputDtos = new DataInformationOutputDto { 
                DataSources = await GetFromCacheOrDatabaseAsync(dataSourceInputDto)
            };

            return dataInformationOutputDtos;
        }
    }
}
