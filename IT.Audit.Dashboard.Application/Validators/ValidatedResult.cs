﻿using AutoMapper;
using FluentValidation;
using IT.Audit.Dashboard.Application.Dtos;
using System.Collections.Generic;

namespace IT.Audit.Dashboard.Application.Validators
{
    public class ValidatedResult<TOutput> 
    {
        private readonly IMapper _mapper;

        public ValidatedResult(IMapper mapper)
        {
            _mapper = mapper;
        }

        public ValidatedResultDto<TOutput> Compile(AbstractValidator<TOutput> validator, TOutput output)
        {
            var result = validator.Validate(output);

            var errors = result.Errors.Count == 0 ? null : _mapper.Map<List<ValidationFailureDto>>(result.Errors);

            return new ValidatedResultDto<TOutput>(output, errors);
        }
    }
}
