﻿using FluentValidation;
using IT.Audit.Dashboard.Application.Contracts.Validators;
using IT.Audit.Dashboard.Application.Dtos;
using System;
using System.Linq;

namespace IT.Audit.Dashboard.Application.Validators
{
    public class DashboardOutputDtoValidator : AbstractValidator<DashboardOutputDto>
    {
        public DashboardOutputDtoValidator()
        {

        }
    }

    public class DataInformationOutputValidator : AbstractValidator<DataInformationOutputDto>
    {
        public DataInformationOutputValidator()
        {
            RuleFor(x => x.DataSources)
                .Must(x => x != null ?
                    x.All(y => y.Equals(x.FirstOrDefault())) : true)
                .WithMessage(x => ValidatorMessages.DataSourcesNotSynchronised)
                .WithSeverity(Severity.Info);

            RuleFor(x => x.DataSources)
                .Must(x => x != null ?
                    x.All(y => y.ModifiedDate.ToShortDateString() == DateTime.Now.ToShortDateString()) : true)
                .WithMessage(x => ValidatorMessages.DataSourcesNotImportedToday)
                .WithSeverity(Severity.Warning);
        }
    }
}
