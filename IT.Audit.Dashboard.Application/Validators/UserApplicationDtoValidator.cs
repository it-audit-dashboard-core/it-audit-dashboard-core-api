﻿using FluentValidation;
using IT.Audit.Dashboard.Application.Dtos;

namespace IT.Audit.Dashboard.Application
{
    public class UserApplicationDtoValidator : AbstractValidator<UserApplicationDto>
    {
        public UserApplicationDtoValidator()
        {
            RuleFor(x => x.Id).NotEqual(0);
            RuleFor(x => x.Title).NotEmpty();
            RuleFor(x => x.Route).NotEmpty();
            //RuleFor(x => x.Key).NotEmpty();
        }
    }
}
