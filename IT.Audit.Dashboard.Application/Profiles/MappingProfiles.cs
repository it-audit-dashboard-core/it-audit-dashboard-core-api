﻿using AutoMapper;
using FluentValidation.Results;
using IT.Audit.Dashboard.Application.Dtos;
using IT.Audit.Dashboard.EntityFrameworkCore.Domain;
using IT.Audit.Dashboard.EntityFrameworkCore.ItAuditDashboard.Domain;

namespace IT.Audit.Dashboard.Application.Profiles
{
    public class ITAuditDashboardMappingProfile : Profile
    {
        public ITAuditDashboardMappingProfile()
        {
            CreateMap<UserApplication, UserApplicationDto>();
            CreateMap<DataSource, DataSourceOutputDto>();
            CreateMap<DataInformationInputDto, DataInformationOutputDto>();
            CreateMap<Sql.Domain.IbaUser, IbaUserDto>();
            CreateMap<PageSetting, PageSettingsOutputDto>();
            CreateMap<ValidationFailure, ValidationFailureDto>();
        }
    }
}
