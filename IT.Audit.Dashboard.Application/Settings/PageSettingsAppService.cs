﻿using AutoMapper;
using IT.Audit.Dashboard.Application.Dtos;
using IT.Audit.Dashboard.Domain.Shared;
using IT.Audit.Dashboard.EntityFrameworkCore.ItAuditDashboard.Domain;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IT.Audit.Dashboard.Application
{
    public class PageSettingsAppService : IPageSettingsAppService
    {
        private readonly IPageSettingsRepository _pageSettingsRepository;
        private readonly IMapper _mapper;

        public PageSettingsAppService(IPageSettingsRepository pageSettingsRepository, IMapper mapper)
        {
            _pageSettingsRepository = pageSettingsRepository;
            _mapper = mapper;
        }

        public async Task<List<PageSettingsOutputDto>> GetListAsync(ConnectionType connectionType)
        {
            var pageSettings = await _pageSettingsRepository.GetListAsync((int)connectionType);
            return _mapper.Map<List<PageSettingsOutputDto>>(pageSettings);
        }
    }
}
