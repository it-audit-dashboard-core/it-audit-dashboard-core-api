﻿using AutoMapper;
using IT.Audit.Dashboard.Application.Dtos;
using IT.Audit.Dashboard.Domain;
using IT.Audit.Dashboard.Domain.Shared;
using IT.Audit.Dashboard.EntityFrameworkCore.IbaBdrm;
using IT.Audit.Dashboard.EntityFrameworkCore.IbaBdrm.Domain;
using Microsoft.Extensions.Caching.Memory;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IT.Audit.Dashboard.Application
{
    public class IbaUsersAppService : IIbaUsersAppService
    {
        private readonly IMemoryCache _cache;
        private readonly IIbaUserRepository _ibaUsersRepository;
        private readonly IMapper _mapper;

        public IbaUsersAppService(
            IIbaUserRepository ibaUsersRepository,
            IMemoryCache cache, 
            IMapper mapper)
        {
            _cache = cache;
            _ibaUsersRepository = ibaUsersRepository;
            _mapper = mapper;
        }

        public async Task<List<IbaUserDto>> GetAsync()
        {
            return await _cache.GetOrCreateAsync(
                IbaUsersConsts.CacheKeyIbaModifiedDate,
                entry => GetFromDatabaseAsync());
        }

        public async Task<FilterDataDto> GetFilterData()
        {
            FilterDataDto filterData = new FilterDataDto();

            IReadOnlyList<IbaUserDto> users = await GetAsync();

            filterData.Instances = users
                .Select(f => f.Instance)
                .Distinct()
                .OrderBy(f => f)
                .Select(f => new FilterInstance { Name = f }).ToList();

            filterData.Roles = (from user in users
                                select new FilterRole { Instance = new FilterInstance() { Name = user.Instance }, Name = user.Role })
                                .Distinct()
                                .OrderBy(f => f.Instance)
                                .ThenBy(f => f.Name)
                                .ToList();

            filterData.Securities = (from user in users 
                                     select new FilterSecurity() { Instance = new FilterInstance() { Name = user.Instance }, Name = user.Security })
                                .Distinct()
                                .OrderBy(f => f.Instance)
                                .ThenBy(f => f.Name)
                                .ToList();

            return _mapper.Map<FilterDataDto>(filterData);
        }

        private async Task<List<IbaUserDto>> GetFromDatabaseAsync()
        {
            var users = await _ibaUsersRepository.GetAsync();

            return _mapper.Map<List<GetIbaUsersResult>, List<IbaUserDto>>(users);
        }
    }
}
