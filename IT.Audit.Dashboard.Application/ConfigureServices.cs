﻿using IT.Audit.Dashboard.Application.Contracts.Account;
using IT.Audit.Dashboard.EntityFrameworkCore.ItAuditDashboard;
using IT.Audit.Dashboard.EntityFrameworkCore.ItAuditDashboard.Domain;
using Microsoft.Extensions.DependencyInjection;

namespace IT.Audit.Dashboard.Application
{
    public static class ConfigureServices
    {
        public static IServiceCollection AddApplicationServices(this IServiceCollection services)
        {
            services.AddTransient<IDashboardUnitOfWork, DashboardUnitOfWork>();
            services.AddTransient<IDataSourceAppService, DataSourceAppService>();
            services.AddTransient<IDataInformationAppService, DataInformationAppService>();
            services.AddTransient<IDashboardAppService, DashboardAppService>();
            services.AddTransient<IPageSettingsAppService, PageSettingsAppService>();
            services.AddTransient<IIbaUsersAppService, IbaUsersAppService>();
            services.AddTransient<IUserAppService, UserAppService>(); 
            services.AddTransient<IUserApplicationAppService, UserApplicationAppService>();

            return services;
        }
    }
}
