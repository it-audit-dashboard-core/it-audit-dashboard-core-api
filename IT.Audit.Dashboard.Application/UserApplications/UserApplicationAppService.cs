﻿using AutoMapper;
using IT.Audit.Dashboard.Application.Dtos;
using IT.Audit.Dashboard.Application.Validators;
using IT.Audit.Dashboard.EntityFrameworkCore.ItAuditDashboard.Domain;
using System.Collections.Generic;
using System.IO;
using System.Text.Json;
using System.Threading.Tasks;

namespace IT.Audit.Dashboard.Application
{
    public class UserApplicationAppService : IUserApplicationAppService
    {
        private readonly IUserApplicationRepository _userApplicationRepository;
        private readonly IMapper _mapper;

        public UserApplicationAppService(IUserApplicationRepository userApplicationRepository, IMapper mapper)
        {
            _userApplicationRepository = userApplicationRepository;
            _mapper = mapper;
        }

        public async Task<ValidatedResultDto<List<UserApplicationDto>>> GetListAsync()
        {
            var userApplications = await _userApplicationRepository.GetListAsync();

            var outputDto = _mapper.Map<List<UserApplicationDto>>(userApplications);

            var validator = new ValidatedResult<List<UserApplicationDto>>(_mapper)
                .Compile(new UserApplicationDtoListValidator(), outputDto);

            return validator;
        }

        public async Task<ValidatedResultDto<UserApplicationDto>> GetByKeyAsync(string key)
        {
            var userApplication = await _userApplicationRepository.GetByKeyAsync(key);

            var outputDto = _mapper.Map<UserApplicationDto>(userApplication);

            var validator = new ValidatedResult<UserApplicationDto>(_mapper)
                .Compile(new UserApplicationDtoValidator(), outputDto);

            return validator;
        }

        public async Task<IEnumerable<UserApplicationDto>> GetFromFile(string path)
        {
            using FileStream openStream = File.OpenRead(path);
            var userApplications = await JsonSerializer.DeserializeAsync<List<UserApplicationDto>>(openStream);
            return userApplications;
        }
    }
}
