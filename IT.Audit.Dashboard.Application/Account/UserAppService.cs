﻿using IT.Audit.Dashboard.Application.Contracts.Account;
using IT.Audit.Dashboard.Domain.Security;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace IT.Audit.Dashboard.Application
{
    public class UserAppService : IUserAppService
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly AppSettings _appSettings;

        public UserAppService(IHttpContextAccessor httpContextAccessor, IOptions<AppSettings> appSettings)
        {
            _appSettings = appSettings.Value;
            _httpContextAccessor = httpContextAccessor;
        }

        public Task<IdentityUser> GetAuthenticatedUser()
        {
            var user = new IdentityUser()
            {
                Username = _httpContextAccessor.HttpContext.User.Identity?.Name,
                IsAuthenticated = _httpContextAccessor.HttpContext.User.Identity != null && _httpContextAccessor.HttpContext.User.Identity.IsAuthenticated,
                AuthenticationType = _httpContextAccessor.HttpContext.User.Identity?.AuthenticationType,
                Claims = _httpContextAccessor.HttpContext.User.Claims
            };

            if (!user.IsAuthenticated) return null;

            return Task.FromResult(user);
        }

        private string GenerateJwtToken(IdentityUser user)
        {
            // generate token that is valid for 7 days
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
                {
                //TODO: Change to token returned from WinAuth??
                Subject = new ClaimsIdentity(new[] { new Claim("userName", user.Username) }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };

            //TODO: Get all allowed Apps from database for this user

            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }
    }
}
