﻿using AutoMapper;
using IT.Audit.Dashboard.Application.Contracts.Logging;
using IT.Audit.Dashboard.Domain;
using System.Threading.Tasks;

namespace IT.Audit.Dashboard.Application
{
    public class LoggingAppService : ILoggingAppService
    {
        private readonly ILoggingRepository _loggingRepository;
        private readonly IMapper _mapper;

        public LoggingAppService(ILoggingRepository loggingRepository, IMapper mapper)
        {
            _loggingRepository = loggingRepository;
            _mapper = mapper;
        }

        public async Task<ErrorOutputDto> LogError(ErrorLogDto errorLog)
        {
            var entity = _mapper.Map<ErrorLogDto>(errorLog);
            var result = await _loggingRepository.LogError(entity.UserName, entity.ModuleName, entity.FunctionName, entity.MessageText);
            return new ErrorOutputDto { Result = result };
        }

        public async Task<ErrorOutputDto> ClearErrors()
        {
            var result = await _loggingRepository.ClearErrors();
            return new ErrorOutputDto { Result = result.ToString() };
        }
    }
}
