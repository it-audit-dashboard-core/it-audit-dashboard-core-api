﻿using Microsoft.Extensions.Caching.Memory;
using System;
using System.Threading.Tasks;

namespace IT.Audit.Dashboard.Domain
{
    public class CacheService<T> : ICacheService<T>
    {
        private readonly IMemoryCache _cache;

        public CacheService(IMemoryCache cache)
        {
            _cache = cache;
        }

        public Task<T> GetAsync(string cacheKey, Func<string, Task<T>> factory)
        {
            //return await _cache.GetOrCreateAsync(
            //    cacheKey, () => factory);
            throw new NotImplementedException(nameof(GetAsync));
        }

        public void Remove(string cacheKey)
        {
            _cache.Remove(cacheKey);
        }

        public T Set<T>(string cacheKey, T value)
        {
            return _cache.Set<T>(cacheKey, value);
        }

        public bool TryGet<T>(string cacheKey, out T value)
        {
            value = _cache.Get<T>(cacheKey);
            return value != null;
        }
    }

    public interface ICacheService<T>
    {
        bool TryGet<T>(string cacheKey, out T value);
        T Set<T>(string cacheKey, T value);
        void Remove(string cacheKey);
    }
}
