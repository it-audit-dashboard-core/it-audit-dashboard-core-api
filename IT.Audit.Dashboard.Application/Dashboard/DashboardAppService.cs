﻿using IT.Audit.Dashboard.Application.Dtos;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Memory;
using AutoMapper;
using System.DirectoryServices;
using IT.Audit.Dashboard.Domain.Shared;
using IT.Audit.Dashboard.EntityFrameworkCore.ItAuditDashboard.Domain;

namespace IT.Audit.Dashboard.Application
{
    public class DashboardAppService : IDashboardAppService
    {
        private readonly IDashboardUnitOfWork _unitOfWork;
        private readonly IMemoryCache _cache;
        private readonly IMapper _mapper;

        public DashboardAppService(
            IDashboardUnitOfWork unitOfWork,
            IMemoryCache cache,
            IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _cache = cache;
            _mapper = mapper;
        }

        private async Task<DashboardOutputDto> GetFromCacheOrDatabaseAsync(DashboardInputDto inputDto)
        {
            return await _cache.GetOrCreateAsync(
                GetCacheKey(inputDto.UserName),
                entry => GetFromDatabaseAsync(inputDto.UserName, inputDto.RefreshCache));
        }


        public async Task<DashboardOutputDto> GetFromDatabaseAsync(
            string userName,
            bool refreshCache = false)
        {
            // Clear Users
            await _unitOfWork.Dashboard.DeleteUsersAsync();

            // Insert Authorised Users
            // TODO: May have to split username
            var displayName = GetActiveDirectoryExtendedProperties(userName, AppSettingKeys.LocalDomain, "displayname") ?? "";
            var email = GetActiveDirectoryExtendedProperties(userName, AppSettingKeys.LocalDomain, "mail") ?? "";

            var userApplicationGroups = await _unitOfWork.UserApplicationGroups.GetListAsync();
            await _unitOfWork.Dashboard.AddAuthorisedUsersAsync(userName, displayName, email, userApplicationGroups);

            // Get Users From Database
            var users = await _unitOfWork.Dashboard.GetListAsync(userName);
            await _unitOfWork.CompleteAsync();

            return await Task.FromResult(new DashboardOutputDto { });
        }

        private static string GetActiveDirectoryExtendedProperties(string userName, string domain, string property)
        {
            var rootEntry = new DirectoryEntry("LDAP://" + domain);

            DirectorySearcher srch = new DirectorySearcher(rootEntry);
            srch.SearchScope = SearchScope.Subtree;

            srch.Filter = "(&(objectClass=User) (sAMAccountName=" + userName + "))";

            srch.PropertiesToLoad.Add(property);

            foreach (SearchResult sr in srch.FindAll())
            {
                if (sr.Properties[property].Count > 0)
                {
                    return sr.Properties[property][0].ToString();
                }
            }

            return string.Empty;
        }

        public async Task<DashboardOutputDto> GetAsync(DashboardInputDto inputDto)
        {
            return await GetFromCacheOrDatabaseAsync(inputDto);
        }

        private string GetCacheKey(string userName)
        {
            return $"{userName}_DashboardUser";
        }
    }
}
