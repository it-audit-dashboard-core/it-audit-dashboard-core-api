﻿using IT.Audit.Dashboard.Application.Dtos;
using IT.Audit.Dashboard.Domain.Shared;
using IT.Audit.Dashboard.EntityFrameworkCore.Domain;
using System.Threading.Tasks;

namespace IT.Audit.Dashboard.Application
{
    public class DataSourceAppService : IDataSourceAppService
    {
        private readonly IDataSourceRepository _dataSourceRepository;

        public DataSourceAppService(IDataSourceRepository dataSourceRepository)
        {
            _dataSourceRepository = dataSourceRepository;
        }

        public async Task<DataSourceOutputDto> GetAsync(string controller)
        {
            var connectionType = ConnectionType.SOX_IBA_BDRM;
            var tableName = "t_iba_users";

            //TODO: Choose DbContext

            var result = await _dataSourceRepository.GetModifiedDate();
            return new DataSourceOutputDto { ModifiedDate = result.ModifiedDate };
        }
    }
}
