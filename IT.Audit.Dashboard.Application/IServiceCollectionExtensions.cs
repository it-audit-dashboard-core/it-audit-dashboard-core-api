﻿using IT.Audit.Dashboard.Application;
using IT.Audit.Dashboard.Domain;
using IT.Audit.Dashboard.Domain.Shared;
using IT.Audit.Dashboard.EntityFrameworkCore.Domain;
using IT.Audit.Dashboard.EntityFrameworkCore.Repositories;
using IT.Audit.Dashboard.Sql.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace IT.Audit.Dashboard.EntityFrameworkCore
{
    public static class IServiceCollectionExtensions
    {
        public static IServiceCollection ConfigureDatabase(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<ITAuditDashboardDbContext>(options =>
            {
                options.UseSqlServer(configuration.GetConnectionString(ConnectionStringKey.ITAuditDashboard));
            });

            services.AddScoped<Func<DbFactory>>((provider) => () => provider.GetService<DbFactory>());

            return services;
        }

        public static IServiceCollection ConfigureCors(this IServiceCollection services)
        {
            return services.AddCors(options =>
            {
                options.AddPolicy("IT.Audit.Dashboard.Origin",
                builder => builder.WithOrigins("https://localhost:4200").AllowAnyHeader().AllowAnyMethod().AllowCredentials());
            });
        }

        public static IServiceCollection AddRepositories(this IServiceCollection services)
        {
            return services
                .AddScoped<IDataSourceRepository, DataSourceRepository>() 
                .AddScoped<IUserApplicationRepository, UserApplicationRepository>()
                .AddScoped<IUserApplicationGroupsRepository, UserApplicationGroupsRepository>()
                .AddScoped<IPageSettingsRepository, PageSettingsRepository>()
                .AddScoped<IIbaUsersRepository, IbaUsersRepository>();
        }

        public static IServiceCollection AddServices(this IServiceCollection services)
        {
            return services
                .AddTransient<IDashboardDomainManager, DashboardDomainManager>()
                .AddTransient<IDataInformationDomainManager, DataInformationDomainManager>()
                .AddTransient<IDashboardAppService, DashboardAppService>()
                .AddTransient<IDataInformationAppService, DataInformationAppService>()
                .AddTransient<IPageSettingsAppService, PageSettingsAppService>()
                .AddTransient<IIbaUsersAppService, IbaUsersAppService>()
                .AddTransient<IUserApplicationAppService, UserApplicationAppService>();

        }

        public void ConfigureSwaggerServices(IServiceCollection services)
        {
            services.AddSwaggerGen(
                options =>
                {
                    options.SwaggerDoc("v1", new OpenApiInfo { Title = "BookStore API", Version = "v1" });
                    options.DocInclusionPredicate((docName, description) => true);
                    options.CustomSchemaIds(type => type.FullName);
                }
            );
        }
    }
}
