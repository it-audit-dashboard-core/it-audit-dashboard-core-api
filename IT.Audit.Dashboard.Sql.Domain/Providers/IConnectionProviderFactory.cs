﻿using IT.Audit.Dashboard.Domain.Shared;

namespace IT.Audit.Dashboard.Sql.Domain
{
    public interface IConnectionProviderFactory
    {
        IConnectionProvider CreateConnectionProvider(ConnectionType connectionType);
    }
}
