﻿namespace IT.Audit.Dashboard.Sql.Domain
{
    public interface IConnectionProvider
    {
        //IConfiguration Configuration { get; }

        string ConnectionString { get; }

        //void SetConnectionString(ConnectionType connectionType);
    }
}
