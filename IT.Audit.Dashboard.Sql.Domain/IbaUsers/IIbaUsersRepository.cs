﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace IT.Audit.Dashboard.Sql.Domain
{
    public interface IIbaUsersRepository
    {
        Task<List<IbaUser>> Get();
    }
}
