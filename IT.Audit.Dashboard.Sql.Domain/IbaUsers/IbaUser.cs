﻿using System;

namespace IT.Audit.Dashboard.Sql.Domain
{
    public class IbaUser
    {
        public string Name { get; set; }
        public string Instance { get; set; }
        public string Role { get; set; }
        public string Security { get; set; }
        public string Login { get; set; }
        public bool IsActive { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime Updated { get; set; }
        public bool Disabled { get; set; }
        public string ServiceDeskRef { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedRef { get; set; }
        public string CreatedUser { get; set; }
        public string UserName { get; set; }
        public string ADOU { get; set; }
        public string AccountStatus { get; set; }
        public string Office { get; set; }
        public string AdDepartment { get; set; }
        public string City { get; set; }
        public string Manager { get; set; }
        public DateTime IbaLoadDate { get; set; }
    }
}
