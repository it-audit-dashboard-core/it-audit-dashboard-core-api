﻿using IT.Audit.Dashboard.Domain.Shared;

namespace IT.Audit.Dashboard.Sql.Domain
{
    public class UserApplication : IEntity
    {
        public UserApplication(int id, string key, string title, string route, string dataSources)
        {
            Id = id;
            Key = key;
            Title = title;
            Route = route;
            DataSources = dataSources;
        }

        public int Id { get; set; }
        public string Key { get; }
        public string Title { get; }
        public string DataSources { get; }
        //public List<DataSource> DataSources { get; set; }
        public string Route { get; }
    }
}
