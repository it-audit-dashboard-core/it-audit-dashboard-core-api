﻿using IT.Audit.Dashboard.Domain.Shared;

namespace IT.Audit.Dashboard.Sql.Domain
{
    public class UserApplicationGroup : IEntity
    {
        public UserApplicationGroup(int id, string key, string group)
        {
            Id = id;
            Key = key;
            Group = group;
        }

        public int Id { get; set; }
        public string Key { get; set; }
        public string Group { get; set; }
    }
}
