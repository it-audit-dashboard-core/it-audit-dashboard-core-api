﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IT.Audit.Dashboard.Sql.Domain
{
    [Table("WebSiteErrorLog")]
    public class ErrorLog
    {
        public int Id { get; set; }

        [Required]
        public DateTime ErrorTime { get; set; }

        [Required]
        [StringLength(50)]
        public string UserName { get; set; }

        [Required]
        [StringLength(50)]
        public string MachineName { get; set; }

        [Required]
        [StringLength(100)]
        public string ModuleName { get; set; }

        [Required]
        [StringLength(100)]
        public string FunctionName { get; set; }

        [Required]
        public string MessageText { get; set; }
    }
}