﻿namespace IT.Audit.Dashboard.Sql.Domain
{
    public interface ILoggingRepository
    {
        string LogError(string user, string controller, string action, string messageText);
    }
}
