﻿using IT.Audit.Dashboard.Domain.Shared;
using System;
using System.Threading.Tasks;

namespace IT.Audit.Dashboard.Sql.Domain
{
    public interface IDataSourceRepository
    {
        Task<DateTime> GetModifiedDate(ConnectionType connectionType, string tableName);
    }
}
