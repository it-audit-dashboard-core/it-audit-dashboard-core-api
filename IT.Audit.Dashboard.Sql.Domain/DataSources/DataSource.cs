﻿using System;

namespace IT.Audit.Dashboard.Sql.Domain
{
    public class DataSource
    {
        public string Name { get; set; }
        public DateTime ModifiedDate { get; set; }
    }
}
