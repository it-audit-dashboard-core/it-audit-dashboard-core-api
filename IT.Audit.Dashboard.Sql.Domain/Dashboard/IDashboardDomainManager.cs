﻿using IT.Audit.Dashboard.Domain.Shared;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IT.Audit.Dashboard.EntityFrameworkCore.Domain
{
    public interface IDashboardDomainManager
    {
        Task<List<IDashboardUser>> GetFromDatabaseAsync(
            ConnectionType connectionType, string userName, bool refreshCache = false);
    }
}