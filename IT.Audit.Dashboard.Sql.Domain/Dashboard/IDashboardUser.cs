﻿using IT.Audit.Dashboard.Domain.Shared;

namespace IT.Audit.Dashboard.Sql.Domain
{
    public class IDashboardUser : IEntity
    {
        public int Id { get; set; }
    }
}
