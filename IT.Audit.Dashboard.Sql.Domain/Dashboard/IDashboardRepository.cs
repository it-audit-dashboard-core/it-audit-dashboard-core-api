﻿using IT.Audit.Dashboard.Domain.Shared;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IT.Audit.Dashboard.Sql.Domain
{
    public interface IDashboardRepository
    {
        Task DeleteUsersAsync(ConnectionType connectionType);
        Task AddAuthorisedUsersAsync(ConnectionType connectionType, string userName, string displayName, string email, List<UserApplicationGroup> userApplicationGroups);
        Task<List<IDashboardUser>> GetListAsync(ConnectionType connectionType, string userName);
    }
}