﻿using System.Threading.Tasks;

namespace IT.Audit.Dashboard.Sql.Domain
{
    public interface IDashboardUnitOfWork
    {
        IDashboardRepository Dashboard { get; }
        IUserApplicationGroupsRepository UserApplicationGroups { get; }
        Task CompleteAsync();
    }
}
