﻿using IT.Audit.Dashboard.Application;
using IT.Audit.Dashboard.Application.Dtos;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IT.Audit.Dashboard.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    [EnableCors("IT.Audit.Dashboard.Origin")]
    public class UserApplicationController : ControllerBase
    {
        private readonly IUserApplicationAppService _userApplicationAppService;
        private readonly ILogger<UserApplicationController> _logger;

        public UserApplicationController(IUserApplicationAppService userApplicationAppService, ILogger<UserApplicationController> logger)
        {
            _userApplicationAppService = userApplicationAppService;
            _logger = logger;
        }

        [HttpGet("GetListAsync")]
        public async Task<ValidatedResultDto<List<UserApplicationDto>>> GetListAsync()
        {
            return await _userApplicationAppService.GetListAsync();
        }

        [HttpGet("GetByKeyAsync")]
        public async Task<ValidatedResultDto<UserApplicationDto>> GetByKeyAsync(string key)
        {
            return await _userApplicationAppService.GetByKeyAsync(key);
        }
    }
}
