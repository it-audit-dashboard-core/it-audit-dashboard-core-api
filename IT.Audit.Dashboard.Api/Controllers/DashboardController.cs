﻿using AutoMapper;
using IT.Audit.Dashboard.Application.Validators;
using IT.Audit.Dashboard.Application;
using IT.Audit.Dashboard.Application.Dtos;
using IT.Audit.Dashboard.Domain.Shared;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace IT.Audit.Dashboard.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("IT.Audit.Dashboard.Origin")]
    public class DashboardController : ControllerBase
    {
        private readonly IDataInformationAppService _dataInformationAppService;
        private readonly IMapper _mapper;

        public DashboardController(IDataInformationAppService dataInformationAppService, IMapper mapper)
        {
            _dataInformationAppService = dataInformationAppService;
            _mapper = mapper;
        }

        [HttpGet("id")]
        public async Task<ValidatedResultDto<DataInformationOutputDto>> Get(string key)
        {
            var inputDto = new DataInformationInputDto { ConnectionType = GetConnectionType(key) };

            var outputDto = await _dataInformationAppService.GetAsync(inputDto);

            var validator = new ValidatedResult<DataInformationOutputDto>(_mapper)
                .Compile(new DataInformationOutputValidator(), outputDto);

            return validator;
        }

        private ConnectionType GetConnectionType(string key)
        {
            switch (key)
            {
                case "iba-users":
                    return ConnectionType.IbaUsers;
                case "ad-users":
                    return ConnectionType.ActiveDirectory;
                case "it-audit-dashboard":
                    return ConnectionType.ITAuditDashboard;
                default:
                    return ConnectionType.IbaUsers;
            }
        }

    }
}
