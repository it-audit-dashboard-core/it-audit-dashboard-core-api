﻿using IT.Audit.Dashboard.Application;
using IT.Audit.Dashboard.Application.Contracts.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace IT.Audit.Dashboard.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoggingController : ControllerBase
    {
        private readonly ILoggingAppService _loggingAppService;

        public LoggingController(ILoggingAppService loggingAppService)
        {
            _loggingAppService = loggingAppService;
        }

        [HttpPost]
        public async Task<ErrorOutputDto> LogError(string user, string message, string controller, string action)
        {
            //var user = @"emea\ribone";
            //string message = "IT Audit Dashboard v2 Test Message";
            //string controller = "TestController";
            //string action = "TestAction";

            var dto = new ErrorLogDto { UserName = user, ModuleName = controller, FunctionName = action, MachineName = Environment.MachineName, MessageText = message, ErrorTime = DateTime.Now };

            return await _loggingAppService.LogError(dto);
        }
    }
}
