using AutoMapper;
using IT.Audit.Dashboard.Application;
using IT.Audit.Dashboard.EntityFrameworkCore;
using IT.Audit.Dashboard.EntityFrameworkCore.Repositories;
using IT.Audit.Dashboard.Domain.Shared;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using System;
using IT.Audit.Dashboard.Sql.Domain;
using IT.Audit.Dashboard.Sql.Providers;
using IT.Audit.Dashboard.EntityFrameworkCore.Domain;
using IT.Audit.Dashboard.Sql.Repositories;
using IT.Audit.Dashboard.Domain;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Server.IISIntegration;

namespace IT.Audit.Dashboard.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("IT.Audit.Dashboard.Origin",
                builder => builder.WithOrigins("https://localhost:4200").AllowAnyHeader().AllowAnyMethod());
            });

            //services.AddHttpsRedirection();
            var connectionString = Configuration.GetConnectionString(ConnectionStringKey.ITAuditDashboard);

            services.AddDbContext<ITAuditDashboardDbContext>(options
                => options.UseSqlServer(connectionString));

            // Windows Authentication
            services.AddAuthentication(IISDefaults.AuthenticationScheme);
            services.AddMvc(config =>
            {
                var policy = new AuthorizationPolicyBuilder()
                    .RequireAuthenticatedUser()
                    .Build();
                config.Filters.Add(new AuthorizeFilter(policy));
            });

            services.AddControllers()
                .AddFluentValidation(s =>
                {
                    s.RegisterValidatorsFromAssemblyContaining<Startup>();
                    s.DisableDataAnnotationsValidation = true;
                });

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "IT.Audit.Dashboard.Api", Version = "v1" });
            });

            services.AddMemoryCache();

            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
            services.AddTransient<IMapper, Mapper>();

            services.AddTransient<IConnectionProviderFactory, ConnectionProviderFactory>();
            services.AddTransient<IConnectionProvider, ConnectionProvider>();

            services.AddTransient<ILoggingRepository, EntityFrameworkCore.Repositories.LoggingRepository>();
            services.AddTransient<ILoggingRepository, Sql.Repositories.LoggingRepository>();
            services.AddTransient<ILoggingAppService, LoggingAppService>();

            services.AddTransient<IPageSettingsAppService, PageSettingsAppService>();
            services.AddTransient<IPageSettingsRepository, PageSettingsRepository>();
            services.AddTransient<IDataInformationAppService, DataInformationAppService>();
            services.AddTransient<IDataInformationDomainManager, DataInformationDomainManager>(); 
            services.AddTransient<IDataSourceRepository, DataSourceRepository>();

            services.AddTransient<IIbaUsersAppService, IbaUsersAppService>();
            services.AddTransient<IIbaUsersRepository, Sql.Repositories.IbaUsersRepository>();

            services.AddTransient<IUserApplicationAppService, UserApplicationAppService>();
            services.AddTransient<IUserApplicationRepository, UserApplicationRepository>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "IT.Audit.Dashboard.Api v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();
            
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseCors("IT.Audit.Dashboard.Origin");

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}