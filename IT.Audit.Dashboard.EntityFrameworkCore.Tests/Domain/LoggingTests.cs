﻿using IT.Audit.Dashboard.EntityFramework.Tests.Helpers;
using IT.Audit.Dashboard.EntityFrameworkCore.ItAuditDashboard;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace IT.Audit.Dashboard.EntityFramework.Tests
{
    public class LoggingTests
    {
        [Fact]
        public async Task ShouldLogError()
        {
            var user = @"emea\ribone";
            string message = "IT Audit Dashboard v2 Test Message";
            string controller = "TestController";
            string action = "TestAction";

            ILoggingRepository sut = ITAuditDashoardHelper.GetInMemoryLoggingRepository();
            
            var entity = new WebSiteErrorLog { 
                UserName = user, 
                ModuleName = controller, 
                FunctionName = action, 
                MachineName = Environment.MachineName, 
                MessageText = message, 
                ErrorTime = DateTime.Now };

            await sut.Create(entity);

            Assert.Equal(1, sut.GetAll().Count());
            Assert.Equal(@"emea\ribone", entity.UserName);
            Assert.Equal("TestController", entity.ModuleName);
            Assert.Equal("TestAction", entity.FunctionName);
        }
    }
}
