﻿using IT.Audit.Dashboard.EntityFrameworkCore.ItAuditDashboard;
using IT.Audit.Dashboard.EntityFrameworkCore.ItAuditDashboard.Data;
using IT.Audit.Dashboard.EntityFrameworkCore.ItAuditDashboard.Domain;
using IT.Audit.Dashboard.EntityFrameworkCore.ItAuditDashboard.Repositories;
using Microsoft.EntityFrameworkCore;

namespace IT.Audit.Dashboard.EntityFramework.Tests.Helpers
{
    public class ITAuditDashoardHelper
    {
        private static ITAuditDashboardDbContext GetDbContext()
        {
            DbContextOptions<ITAuditDashboardDbContext> options;

            var builder = new DbContextOptionsBuilder<ITAuditDashboardDbContext>();
            builder.UseInMemoryDatabase(databaseName: "ITAuditDashboardDbInMemory");
            options = builder.Options;

            ITAuditDashboardDbContext dbContext = new ITAuditDashboardDbContext(options);
            dbContext.Database.EnsureDeleted();
            dbContext.Database.EnsureCreated();

            return dbContext;
        }

        internal static ILoggingRepository GetInMemoryLoggingRepository()
        {
            return new LoggingRepository(GetDbContext());
        }

        internal static IDashboardRepository GetInMemoryDashboardRepository()
        {
            return new DashboardRepository(GetDbContext());
        }
    }
}
