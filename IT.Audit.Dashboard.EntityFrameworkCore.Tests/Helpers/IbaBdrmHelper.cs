﻿using IT.Audit.Dashboard.EntityFrameworkCore.IbaBdrm;
using IT.Audit.Dashboard.EntityFrameworkCore.IbaBdrm.Repositories;
using Microsoft.EntityFrameworkCore;
using System;

namespace IT.Audit.Dashboard.EntityFramework.Tests.Helpers
{
    public class IbaBdrmHelper
    {
        private static IbaBdrmDbContext GetDbContext()
        {
            DbContextOptions<IbaBdrmDbContext> options;

            var builder = new DbContextOptionsBuilder<IbaBdrmDbContext>();
            builder.UseInMemoryDatabase(databaseName: "IbaBdrmDbInMemory");
            options = builder.Options;

            IbaBdrmDbContext dbContext = new IbaBdrmDbContext(options);
            dbContext.Database.EnsureDeleted();
            dbContext.Database.EnsureCreated();

            return dbContext;
        }

        public static IIbaUserRepository GetInMemoryIbaUserRepository()
        {
            return new IbaUserRepository(GetDbContext());
        }

        public static DataSourceRepository<IbaBdrmDbContext> GetInMemoryDataSourceRepository(bool loadData = false)
        {
            var dbContext = GetDbContext();
            
            if (!loadData) 
                return new DataSourceRepository<IbaBdrmDbContext>(dbContext);

            dbContext.IbaUsers.Add(
                new IbaUser
                {
                    Id = 1,
                    Name = "Name",
                    Instance = "Instance",
                    Role = "Role",
                    Security = "Security",
                    Login = "Login",
                    IsActive = "true",
                    User = "User",
                    Date = DateTime.Now,
                    ServiceDeskRef = "ServiceDeskRef",
                    Disabled = "Disabled",
                    CreatedDate = DateTime.Now,
                    CreatedRef = "CreatedRef",
                    CreatedUser = "CreatedUser",
                    LastLoginDate = DateTime.Now,
                    LastLoginTime = "LastLoginTime",
                    Port = 1,
                    LoadDate = new DateTime(2001, 1, 1, 1, 0, 0)
                });

            dbContext.SaveChanges();

            return new DataSourceRepository<IbaBdrmDbContext>(dbContext);
        }
    }
}
