﻿namespace IT.Audit.Dashboard.EntityFramework.Tests
{
    public static class ConfigConsts
    {
        public const string ConfigSectionKey = "ITAuditDashboard";
        public const string ConfigSectionValue = @"Data Source=SOX_DB_DEV\SILO;Initial Catalog=IT_Audit_Dashboard;Persist Security Info=True;User ID=reportuser;Password=reportuser";
        public const string ConnectionStringsSectionName = "ConnectionStrings";
    }
}
