﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace IT.Audit.Dashboard.EntityFrameworkCore.ItAuditDashboard.Domain
{
    public interface IUserApplicationGroupsRepository
    {
        Task<List<UserApplicationGroup>> GetListAsync();
        Task<UserApplicationGroup> GetByKeyAsync(string key);
    }
}
