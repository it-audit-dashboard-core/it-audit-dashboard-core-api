﻿using IT.Audit.Dashboard.Domain.Shared;

namespace IT.Audit.Dashboard.EntityFrameworkCore.ItAuditDashboard.Domain
{
    public class DashboardUser : IEntity
    {
        public int Id { get; set; }
        public string User_Name { get; set; }
        public string Full_Name { get; set; }
        public bool IBA_Screen { get; set; }
        public bool AD_Screen { get; set; }
        public bool BDRM_Screen { get; set; }
        public bool Acturis_Screen { get; set; }
        public bool Acturis_Editing { get; set; }
        public bool User_Admin_Screen { get; set; }
        public bool CDL_Screen { get; set; }
        public bool CDL_Editing { get; set; }
        public bool CDL_UserAccessList { get; set; }
        public bool CDL_ReferenceDataUpdate { get; set; }
        public bool AD_Group_Membership_Enquiry { get; set; }
        public bool Generic_UAR_Application_Configure { get; set; }
        public bool Generic_UAR_Application_Workbook_Update { get; set; }
        public int Generic_Application_Access_Count_All_Apps { get; set; } = 0;
        public int Generic_Application_Access_Count_Current_Apps { get; set; } = 0;
        public int Generic_Application_Request_Access_List_Count_Current_Apps { get; set; } = 0;
        public int Generic_Table_Editor_Database_Count { get; set; } = 0;
        public bool Generic_Table_Editor_Configure { get; set; }
        public bool IDM_Screen { get; set; }
        public string AD_Account_Status { get; set; }
        public string EmailAddress { get; set; }
    }
}
