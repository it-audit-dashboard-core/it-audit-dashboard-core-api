﻿namespace IT.Audit.Dashboard.EntityFrameworkCore.ItAuditDashboard.Domain
{
    public partial class GetUsersByUserNameResult
    {
        public string User_Name { get; set; } = default!;
        public string Full_Name { get; set; } = default!;
        public string AD_Account_Status { get; set; } = default!;
        public string EmailAddress { get; set; } = default!;
        public string AD_Group_Name { get; set; } = default!;
        public string Field_Name { get; set; } = default!;
    }
}
