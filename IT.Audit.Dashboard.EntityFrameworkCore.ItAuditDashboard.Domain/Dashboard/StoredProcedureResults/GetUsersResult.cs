﻿namespace IT.Audit.Dashboard.EntityFrameworkCore.ItAuditDashboard.Models
{
    public partial class GetUsersResult
    {
        public string User_Name { get; set; } = default!;
        public string Full_Name { get; set; } = default!;
        public string AD_Account_Status { get; set; } = default!;
        public string EmailAddress { get; set; } = default!;
        public string AD_Group_Name { get; set; } = default!;
        public string Field_Name { get; set; } = default!;
    }
}
