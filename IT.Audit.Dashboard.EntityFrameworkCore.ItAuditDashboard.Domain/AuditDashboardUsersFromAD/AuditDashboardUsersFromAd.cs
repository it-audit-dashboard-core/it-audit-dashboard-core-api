﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

#nullable enable

namespace IT.Audit.Dashboard.EntityFrameworkCore.ItAuditDashboard.Domain
{
    [Table("t_AuditDashboardUsersFromAD")]
    public partial class AuditDashboardUsersFromAd
    {
        [Key]
        [Column("User_Name")]
        [StringLength(50)]
        public string UserName { get; set; } = default!;
        [Required]
        [Column("Full_Name")]
        [StringLength(255)]
        public string FullName { get; set; } = default!;
        [Required]
        [Column("AD_Account_Status")]
        [StringLength(50)]
        public string AdAccountStatus { get; set; } = default!;
        [Required]
        [StringLength(255)]
        public string EmailAddress { get; set; } = default!;
        [Required]
        [Column("AD_Group_Name")]
        [StringLength(255)]
        public string AdGroupName { get; set; } = default!;
        [Key]
        [Column("Field_Name")]
        [StringLength(255)]
        public string FieldName { get; set; } = default!;
    }
}