﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace IT.Audit.Dashboard.EntityFrameworkCore.ItAuditDashboard.Domain
{
    public interface IPageSettingsRepository
    {
        Task<List<PageSetting>> GetListAsync(int connectionType);
    }
}
