﻿using IT.Audit.Dashboard.Domain.Shared;

namespace IT.Audit.Dashboard.EntityFrameworkCore.ItAuditDashboard.Domain
{
    public class PageSetting
    {
        public ConnectionType ConnectionType { get; set; }
        public string TableName { get; set; }
        public string CacheKey { get; set; }
        public string Name { get; set; }
    }
}
