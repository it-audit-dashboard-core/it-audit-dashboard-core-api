﻿using IT.Audit.Dashboard.Domain.Shared;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

#nullable enable

namespace IT.Audit.Dashboard.EntityFrameworkCore.ItAuditDashboard
{
    [Table("t_WebSiteErrorLog")]
    public partial class WebSiteErrorLog : IEntity
    {
        [Key]
        [Column("ID")]
        public int Id { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime ErrorTime { get; set; }
        [Required]
        [StringLength(50)]
        public string UserName { get; set; } = default!;
        [Required]
        [StringLength(50)]
        public string MachineName { get; set; } = default!;
        [Required]
        [StringLength(100)]
        public string ModuleName { get; set; } = default!;
        [Required]
        [StringLength(100)]
        public string FunctionName { get; set; } = default!;
        [Required]
        public string MessageText { get; set; } = default!;
    }
}