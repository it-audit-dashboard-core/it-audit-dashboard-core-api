﻿using IT.Audit.Dashboard.EntityFrameworkCore.Domain;
using System.Threading.Tasks;

namespace IT.Audit.Dashboard.EntityFrameworkCore.ItAuditDashboard
{
    public interface ILoggingRepository : IGenericRepository<WebSiteErrorLog>
    {
        Task<int> ClearErrors();
    }
}
