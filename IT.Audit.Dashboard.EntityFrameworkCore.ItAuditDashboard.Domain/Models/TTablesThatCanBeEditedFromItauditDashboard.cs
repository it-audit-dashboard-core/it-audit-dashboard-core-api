﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable enable

namespace IT.Audit.Dashboard.EntityFrameworkCore.ItAuditDashboard.Models
{
    [Table("t_TablesThatCanBeEditedFromITAuditDashboard")]
    [Index(nameof(Id), Name = "IX_t_TablesThatCanBeEditedFromITAuditDashboard", IsUnique = true)]
    public partial class TTablesThatCanBeEditedFromItauditDashboard
    {
        [Column("ID")]
        public int Id { get; set; }
        [Key]
        [StringLength(255)]
        public string DatabaseName { get; set; } = default!;
        [Key]
        [StringLength(255)]
        public string TableName { get; set; } = default!;
        [Required]
        [StringLength(255)]
        public string UpdatedBy { get; set; } = default!;
        [Column(TypeName = "datetime")]
        public DateTime Updated { get; set; }

        public virtual ICollection<TTablesThatCanBeEditedFromItauditDashboardColumn> TTablesThatCanBeEditedFromItauditDashboardColumns { get; set; }
    }
}