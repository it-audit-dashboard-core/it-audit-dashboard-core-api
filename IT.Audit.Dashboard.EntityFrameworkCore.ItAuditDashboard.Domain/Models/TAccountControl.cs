﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable enable

namespace IT.Audit.Dashboard.EntityFrameworkCore.ItAuditDashboard.Models
{
    [Table("t_AccountControl")]
    public partial class TAccountControl
    {
        [Key]
        public int Value { get; set; }
        [Required]
        [StringLength(255)]
        public string Description { get; set; } = default!;
        [StringLength(50)]
        public string? DisplayStatus { get; set; }
        public bool? DisplayPasswordExpiryDate { get; set; }
    }
}