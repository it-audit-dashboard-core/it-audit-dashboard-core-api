﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;

namespace IT.Audit.Dashboard.Sql.Shared
{
    public enum TargetEnvironment
    {
        DEV,
        UAT,
        PROD,
        SIT,
        NONE
    }

    public enum TargetApp
    {
        ItAuditDashboard
    }

    public class ImportData
    {
        public string Message { get; set; } = "";
        public List<string> ColumnNames { get; set; }
        public List<string>[] ColumnValues { get; set; }
        public List<ColumnMetaData> ColumnMetaData { get; set; }
    }

    public class ColumnMetaData
    {
        public int ColumnSize { get; set; }
        public string ColumnDataType { get; set; }
        public bool ColumnAllowNull { get; set; }
        public bool AllowEditing { get; set; }
    }

    public class DatabaseHelper : IDisposable
    {
        private bool disposed = false;
        private SqlConnection connection;
        private SqlTransaction transaction;
        private SqlCommand command = new SqlCommand();
        private string sql = "";
        private string sqlWhereCondition = "";
        private string sqlOrderBy = "";

        public enum CreateWithTransaction { Yes, No };

        public DatabaseHelper(string connectionString, CreateWithTransaction useTransaction = CreateWithTransaction.No)
        {

            connection = new SqlConnection(connectionString);

            connection.Open();

            command.Connection = connection;

            if (useTransaction == CreateWithTransaction.Yes)
            {
                transaction = connection.BeginTransaction();
                command.Transaction = transaction;
            }

        }

        public void CommitTransaction()
        {
            transaction.Commit();
        }

        public void RollbackTransaction()
        {
            transaction.Rollback();
        }

        protected void CloseConnection()
        {
            if (connection.State != ConnectionState.Closed)
                try
                {
                    connection.Close();
                }
                catch
                {
                }
        }

        public int ExecuteNonQuery(string SQL, SqlParameter[] sqlParams, CommandType commandType)
        {

            command.CommandType = commandType;
            command.CommandText = SQL;
            command.Parameters.Clear();

            if (sqlParams != null)
                command.Parameters.AddRange(sqlParams);

            return command.ExecuteNonQuery();

        }

        public object ExecuteScalar(string SQL, SqlParameter[] sqlParams, CommandType commandType)
        {

            command.CommandType = commandType;
            command.CommandText = SQL;
            command.Parameters.Clear();

            if (sqlParams != null)
                command.Parameters.AddRange(sqlParams);

            return command.ExecuteScalar();

        }

        public DataTable ExecuteReader(string SQL, SqlParameter[] sqlParams, CommandType commandType)
        {

            command.CommandType = commandType;
            command.CommandText = SQL;
            command.Parameters.Clear();

            if (sqlParams != null)
                command.Parameters.AddRange(sqlParams);

            DataTable dt = new DataTable();
            dt.Load(command.ExecuteReader());

            return dt;

        }

        public DataTable GetDataTable(string SQL, SqlParameter[] sqlParams)
        {

            command.CommandType = CommandType.Text;
            command.CommandText = SQL;
            command.Parameters.Clear();

            if (sqlParams != null)
                command.Parameters.AddRange(sqlParams);

            SqlDataAdapter da = new SqlDataAdapter(command);
            DataTable dt = new DataTable();

            da.Fill(dt);

            return dt;
        }

        public List<TType> GetData<TType>(DataTable dataTable, TType targetItem) where TType : new()
        {
            var lst = new List<TType>();

            foreach (DataRow dataRow in dataTable.Rows)
            {

                targetItem = new TType();

                PropertyInfo[] properties = typeof(TType).GetProperties();
                foreach (PropertyInfo property in properties)
                {

                    foreach (DataColumn column in dataTable.Columns)
                    {
                        if (column.ColumnName.ToLower() == property.Name.ToLower())
                        {
                            if (dataRow[property.Name].ToString() != "")
                                property.SetValue(targetItem, Convert.ChangeType(dataRow[property.Name].ToString(), property.PropertyType));
                        }

                    }

                }

                lst.Add(targetItem);

            }

            return lst;

        }

        public void SetSQL(string SQL)
        {
            sql = SQL;
        }

        public string GetSQL()
        {
            return sql + sqlWhereCondition + sqlOrderBy;
        }

        public void AddWhereCondition(string condition)
        {
            if (condition.Trim() != "")
            {
                if (sqlWhereCondition == "")
                    sqlWhereCondition = " WHERE";
                else
                    sqlWhereCondition += " AND";

                sqlWhereCondition += " " + condition.Trim();
            }
        }

        public void AddOrderBy(string orderBy)
        {
            if (orderBy.Trim() != "")
            {
                if (sqlOrderBy == "")
                    sqlOrderBy = " ORDER BY";
                else
                    sqlOrderBy += " ,";

                sqlOrderBy += " " + orderBy.Trim();
            }
        }

        public void ClearSQL()
        {
            sql = "";
            sqlWhereCondition = "";
            sqlOrderBy = "";
        }

        public List<String> GetTableColumnNames(string objectName, string orderByExpression = "")
        {
            List<String> result = new List<String>();

            if (orderByExpression == "")
                orderByExpression = "ORDER BY name";

            string SQL = @"SELECT name AS COLUMN_NAME
                            FROM sys.columns
                            WHERE object_id=object_id(@objectName) " + orderByExpression;

            List<SqlParameter> parameters = new List<SqlParameter>();

            parameters.Add(new SqlParameter("@objectName", objectName));

            foreach (DataRow row in GetDataTable(SQL, parameters.ToArray()).Rows)
            {
                result.Add(row["COLUMN_NAME"].ToString());
            }

            return result;
        }

        public static DataTable getEmptyTableFromType<TType>(TType targetItem) where TType : new()
        {
            DataTable result = new DataTable();

            PropertyInfo[] properties = typeof(TType).GetProperties();
            foreach (PropertyInfo property in properties)
            {
                result.Columns.Add(new DataColumn(property.Name, System.Type.GetType("System." + property.PropertyType.Name)));
            }

            return result;
        }

        public ImportData FromDataTable(DataTable dt)
        {
            ImportData results = new ImportData();

            results.ColumnNames = new List<string>();

            foreach (DataColumn col in dt.Columns)
                results.ColumnNames.Add(col.ColumnName);

            if (dt.Rows.Count > 0)
            {

                results.ColumnValues = new List<string>[dt.Rows.Count];
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    results.ColumnValues[i] = new List<string>();

                    foreach (object item in dt.Rows[i].ItemArray)
                        results.ColumnValues[i].Add(item.ToString());
                }

            }

            return results;
        }

        public static List<string> getStringListFromDataTableColumn(DataTable dt, string columnName)
        {
            var result = new List<string>();

            foreach (DataRow dr in dt.Rows)
            {
                result.Add(dr[columnName].ToString());
            }

            return result;
        }

        ~DatabaseHelper()
        {

            CloseConnection();
            Dispose(false);

        }

        public void Dispose()
        {
            Dispose(true);

            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {

                if (disposing)
                {

                    if (command != null)
                        command.Dispose();

                    if (transaction != null)
                        transaction.Dispose();

                    if (connection != null)
                        connection.Dispose();

                    command = null;
                    transaction = null;
                    connection = null;

                }

                disposed = true;

            }
        }

        public static string ServerName(string connectionString)
        {
            string[] arr = connectionString.Split(';');

            string initialCatalog = null, dataSource = null;

            foreach (string item in arr)
            {
                if (item.IndexOf("DATA SOURCE", StringComparison.CurrentCultureIgnoreCase) > -1)
                {
                    var s = item.Split('=');
                    dataSource = s[1];
                }

                if (item.StartsWith("INITIAL CATALOG", StringComparison.CurrentCultureIgnoreCase))
                {
                    var s = item.Split('=');
                    initialCatalog = s[1];
                }
            }

            return $"{dataSource} / {initialCatalog}";
        }
    }
}
