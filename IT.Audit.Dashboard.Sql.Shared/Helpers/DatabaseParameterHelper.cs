﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace IT.Audit.Dashboard.Sql.Shared
{
    public class DatabaseParameterHelper
    {
        private List<SqlParameter> parameters;

        public DatabaseParameterHelper()
        {
            parameters = new List<SqlParameter>();
        }

        public SqlParameter[] ParameterArray()
        {
            return parameters.ToArray();
        }

        public SqlParameter GetParameter(string name)
        {
            for (var counter = 0; counter < parameters.Count; counter++)
            {
                if (parameters[counter].ParameterName.ToLower() == name.ToLower())
                    return parameters[counter];
            }

            return null;
        }

        public void Add(string name, object value, ParameterDirection direction = ParameterDirection.Input)
        {
            if (value == null)
                value = DBNull.Value;

            parameters.Add(new SqlParameter(name, value));

            parameters[parameters.Count - 1].Direction = direction;
        }

        public void Add(string name, SqlDbType dataType, int size = -1, ParameterDirection direction = ParameterDirection.Input)
        {
            parameters.Add(new SqlParameter(name, dataType, size));

            parameters[parameters.Count - 1].Direction = direction;
        }
    }

}
