﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace IT.Audit.Dashboard.EntityFrameworkCore.Domain
{
    public interface IUserApplicationRepository
    {
        Task<List<UserApplication>> GetListAsync();
        Task<UserApplication> GetByKeyAsync(string key);
    }
}
