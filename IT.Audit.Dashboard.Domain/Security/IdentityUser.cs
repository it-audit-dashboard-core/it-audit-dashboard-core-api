﻿using System.Collections.Generic;
using System.Security.Claims;

namespace IT.Audit.Dashboard.Domain.Security
{
    public class IdentityUser
    {
        public string Username { get; set; }
        public bool IsAuthenticated { get; set; }
        public string AuthenticationType { get; set; }
        public string JwtToken { get; set; }

        public IEnumerable<Claim> Claims { get; set; }
    }
}
