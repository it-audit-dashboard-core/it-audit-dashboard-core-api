﻿using Microsoft.AspNetCore.Authentication;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace IT.Audit.Dashboard.Domain.Security
{
    public class DashboardClaimsTransformation : IClaimsTransformation
    {
        private readonly ISecurityManager _securityManager;

        public DashboardClaimsTransformation(ISecurityManager securityManager)
        {
            _securityManager = securityManager;
        }

        public Task<ClaimsPrincipal> TransformAsync(ClaimsPrincipal principal)
        {
            var clone = principal.Clone();
            var newIdentity = (ClaimsIdentity)clone.Identity;

            // Support AD and local accounts
            var nameId = principal.Claims
                .FirstOrDefault(c => 
                c.Type == ClaimTypes.NameIdentifier || c.Type == ClaimTypes.Name);

            if (nameId == null)
            {
                return Task.FromResult(principal);
            }
            
            var ci = (ClaimsIdentity)principal.Identity;

            //TODO: Get security claims using SecurityManager
            var c = new Claim(ci.RoleClaimType, "Admin1");
            ci.AddClaim(c);

            return Task.FromResult(principal);
        }
    }
}

//// Get user from database
//var user = await _userService.GetByUserName(nameId.Value);
//if (user == null)
//{
//    return principal;
//}

//// Add role claims to cloned identity
//foreach (var role in user.Roles)
//{
//    var claim = new Claim(newIdentity.RoleClaimType, role.Name);
//    newIdentity.AddClaim(claim);
//}
