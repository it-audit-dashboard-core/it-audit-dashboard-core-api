﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace IT.Audit.Dashboard.EntityFrameworkCore.Domain.EF
{
    [Table("t_ADGroupsActiveUsers_Imports")]
    public partial class ADGroupsActiveUsersImport
    {
        [Key]
        [StringLength(50)]
        public string ActionType { get; set; }
        [Key]
        [Column(TypeName = "smalldatetime")]
        public DateTime ActionTime { get; set; }
        public int Records { get; set; }
        public int UserCount { get; set; }
    }
}