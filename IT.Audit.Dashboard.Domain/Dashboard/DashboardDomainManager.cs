﻿using IT.Audit.Dashboard.Domain.Shared;
using IT.Audit.Dashboard.EntityFrameworkCore.Domain;
using System.Collections.Generic;
using System.DirectoryServices;
using System.Threading.Tasks;

namespace IT.Audit.Dashboard.EntityFrameworkCore.Domain
{
    public class DashboardDomainManager : IDashboardDomainManager
    {
        private readonly IDashboardUnitOfWork _unitOfWork;
        private readonly IDashboardRepository _dashboardRepository;
        private readonly IUserApplicationGroupsRepository _userApplicationGroupsRepository;

        public DashboardDomainManager(IDashboardUnitOfWork unitOfWork, IDashboardRepository dashboardRepository, IUserApplicationGroupsRepository userApplicationGroupsRepository)
        {
            _unitOfWork = unitOfWork;
            _dashboardRepository = dashboardRepository;
            _userApplicationGroupsRepository = userApplicationGroupsRepository;
        }

        private static string GetActiveDirectoryExtendedProperties(string userName, string domain, string property)
        {
            var rootEntry = new DirectoryEntry("LDAP://" + domain);

            DirectorySearcher srch = new DirectorySearcher(rootEntry);
            srch.SearchScope = SearchScope.Subtree;

            srch.Filter = "(&(objectClass=User) (sAMAccountName=" + userName + "))";

            srch.PropertiesToLoad.Add(property);

            foreach (SearchResult sr in srch.FindAll())
            {
                if (sr.Properties[property].Count > 0)
                {
                    return sr.Properties[property][0].ToString();
                }
            }

            return string.Empty;

        }

        public async Task<List<IDashboardUser>> GetFromDatabaseAsync(
            ConnectionType connectionType, 
            string userName, 
            bool refreshCache = false)
        {
            // Clear Users
            await _unitOfWork.Dashboard.DeleteUsersAsync(connectionType);

            // Insert Authorised Users
            var displayName = GetActiveDirectoryExtendedProperties(userName, AppSettingKeys.LocalDomain, "displayname") ?? "";
            var email = GetActiveDirectoryExtendedProperties(userName, AppSettingKeys.LocalDomain, "mail") ?? "";
            
            var userApplicationGroups = await _unitOfWork.UserApplicationGroups.GetListAsync();
            await _unitOfWork.Dashboard.AddAuthorisedUsersAsync(connectionType, userName, displayName, email, userApplicationGroups);

            // Get Users From Database
            var users = await _unitOfWork.Dashboard.GetListAsync(connectionType, userName);
            await _unitOfWork.CompleteAsync();

            return users;
        }
    }
}
