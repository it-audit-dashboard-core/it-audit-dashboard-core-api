﻿using System.Threading.Tasks;

namespace IT.Audit.Dashboard.Domain
{
    public interface ILoggingRepository //: IGenericRepository<ErrorLog>
    {
        Task<string> LogError(string user, string controller, string action, string messageText);
        Task<int> ClearErrors();
    }
}
