﻿using System;

namespace IT.Audit.Dashboard.Domain
{
    public interface IEntityBase<TKey>
    {
        TKey Id { get; set; }
    }

    public interface IDeletedEntity<TKey> : IEntityBase<TKey>
    {
        bool IsDeleted { get; set; }
    }

    public interface IDeleteEntity<TKey> : IDeletedEntity<TKey>, IEntityBase<TKey>
    {
    }

    public interface IAuditedEntity<TKey> : IEntityBase<TKey>
    {
        DateTime CreatedDate { get; set; }
        string CreatedBy { get; set; }
        DateTime? UpdatedDate { get; set; }
        string UpdatedBy { get; set; }
    }
    public interface IAuditEntity<TKey> : IAuditedEntity<TKey>, IDeleteEntity<TKey>
    {
    }
}
