﻿using System.Threading.Tasks;

namespace IT.Audit.Dashboard.Domain
{
    public interface IUnitOfWork
    {
        Task CommitAsync();
    }
}
