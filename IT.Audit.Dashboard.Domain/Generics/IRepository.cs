﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace IT.Audit.Dashboard.Domain
{
    public interface IRepository<T> where T : class
    {
        void Add(T entity);
        void Delete(T entity);
        void Update(T entity);
        IQueryable List(Expression<Func<T, bool>> expression);
    }
}
