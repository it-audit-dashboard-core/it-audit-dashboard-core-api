﻿using IT.Audit.Dashboard.Domain.Shared;
using System.Threading.Tasks;

namespace IT.Audit.Dashboard.Domain
{
    public interface IDataInformationDomainManager
    {
        Task<DataSource> GetFromDatabaseAsync(ConnectionType connectionType, string tableName);
    }
}