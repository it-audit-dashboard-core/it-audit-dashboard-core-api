﻿using IT.Audit.Dashboard.Application.Dtos;
using IT.Audit.Dashboard.EntityFrameworkCore.Domain;
using System.Threading.Tasks;

namespace IT.Audit.Dashboard.EntityFrameworkCore.Repositories
{
    public class DataSourceRepository : GenericRepository<TEntity>, IDataSourceRepository
        where TDbContext : DbContext where TEntity : class, IEntity
    {
        public DataSourceRepository(TDbContext context) : base(context)
        {
        }

        public Task<DataSourceOutputDto> GetModifiedDate()
        {
            throw new System.NotImplementedException();
        }
    }
}
