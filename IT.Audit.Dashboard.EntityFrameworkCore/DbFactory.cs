﻿using System;

namespace IT.Audit.Dashboard.EntityFrameworkCore
{
    public class DbFactory : IDisposable
    {
        private bool _disposed;
        private Func<ITAuditDashboardDbContext> _instanceFunc;
        private ITAuditDashboardDbContext _context;
        public ITAuditDashboardDbContext DbContext => _context ?? (_context = _instanceFunc.Invoke());

        public DbFactory(Func<ITAuditDashboardDbContext> contextFactory)
        {
            _instanceFunc = contextFactory;
        }

        public void Dispose()
        {
            if (!_disposed && _context != null)
            {
                _disposed = true;
                _context.Dispose();
            }
        }
    }
}
