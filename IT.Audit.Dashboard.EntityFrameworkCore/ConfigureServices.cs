﻿using IT.Audit.Dashboard.EntityFrameworkCore.Domain;
using Microsoft.Extensions.DependencyInjection;

namespace IT.Audit.Dashboard.EntityFrameworkCore
{
    public static class ConfigureServices
    {
        public static IServiceCollection AddRepositoryServices(this IServiceCollection services)
        {
            services.AddTransient<IDataInformationDomainManager, DataInformationDomainManager>();
            services.AddTransient<IDataSourceProviderFactory, DataSourceProviderFactory>();
            return services;
        }
    }
}
