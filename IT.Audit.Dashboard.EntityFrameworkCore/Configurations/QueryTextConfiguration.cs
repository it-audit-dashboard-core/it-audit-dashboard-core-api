﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using IT.Audit.Dashboard.EntityFrameworkCore.Domain.EF;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore;
using System;


namespace IT.Audit.Dashboard.EntityFrameworkCore.Domain.Configurations
{
    public partial class QueryTextConfiguration : IEntityTypeConfiguration<QueryText>
    {
        public void Configure(EntityTypeBuilder<QueryText> entity)
        {
            entity.Property(e => e.BatchId)
                .IsUnicode(false)
                .IsFixedLength(true);

            entity.Property(e => e.Query).IsUnicode(false);

            OnConfigurePartial(entity);
        }

        partial void OnConfigurePartial(EntityTypeBuilder<QueryText> entity);
    }
}
