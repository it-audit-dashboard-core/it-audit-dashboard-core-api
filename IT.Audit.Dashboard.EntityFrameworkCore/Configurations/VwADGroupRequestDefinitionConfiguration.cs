﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using IT.Audit.Dashboard.EntityFrameworkCore.Domain.EF;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore;
using System;


namespace IT.Audit.Dashboard.EntityFrameworkCore.Domain.Configurations
{
    public partial class VwAdgroupRequestDefinitionConfiguration : IEntityTypeConfiguration<VwADGroupRequestDefinition>
    {
        public void Configure(EntityTypeBuilder<VwADGroupRequestDefinition> entity)
        {
            entity.ToView("vw_ADGroupRequestDefinitions");

            entity.Property(e => e.DeletedBy).IsUnicode(false);

            entity.Property(e => e.Description).IsUnicode(false);

            entity.Property(e => e.DisplayName).IsUnicode(false);

            entity.Property(e => e.Name).IsUnicode(false);

            entity.Property(e => e.UpdatedBy).IsUnicode(false);

            OnConfigurePartial(entity);
        }

        partial void OnConfigurePartial(EntityTypeBuilder<VwADGroupRequestDefinition> entity);
    }
}
