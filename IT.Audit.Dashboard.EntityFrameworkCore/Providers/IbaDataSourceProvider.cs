﻿using IT.Audit.Dashboard.EntityFrameworkCore.Domain;
using IT.Audit.Dashboard.EntityFrameworkCore.IbaBdrm;
using System.Collections.Generic;

namespace IT.Audit.Dashboard.EntityFrameworkCore
{
    public class IbaDataSourceProvider : IDataSourceProvider
    {
        private readonly IIbaUserRepository _ibaUserRepository;

        public IbaDataSourceProvider(IIbaUserRepository ibaUserRepository)
        {
            _ibaUserRepository = ibaUserRepository;
        }

        public IReadOnlyList<DataSource> DataSources { get => throw new System.NotImplementedException(); set => throw new System.NotImplementedException(); }
    }
}