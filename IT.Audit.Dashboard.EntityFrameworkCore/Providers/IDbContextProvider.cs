﻿using Microsoft.EntityFrameworkCore;

namespace IT.Audit.Dashboard.Sql.Domain
{
    public interface IDbContextProvider
    {
        DbContext DbContext { get; }
    }
}
