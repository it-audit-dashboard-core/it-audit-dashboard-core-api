﻿using IT.Audit.Dashboard.EntityFrameworkCore.Domain;
using System.Collections.Generic;

namespace IT.Audit.Dashboard.EntityFrameworkCore
{
    public class ActiveDirectoryUsersDataSourceProvider : IDataSourceProvider
    {
        public IReadOnlyList<DataSource> DataSources { get; set; }
    }
}