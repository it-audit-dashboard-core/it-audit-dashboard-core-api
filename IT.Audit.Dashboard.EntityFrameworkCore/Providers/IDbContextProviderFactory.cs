﻿using IT.Audit.Dashboard.Domain.Shared;

namespace IT.Audit.Dashboard.Sql.Domain
{
    public interface IDbContextProviderFactory
    {
        IDbContextProvider CreateDbContextProvider(ConnectionType connectionType);
    }
}
