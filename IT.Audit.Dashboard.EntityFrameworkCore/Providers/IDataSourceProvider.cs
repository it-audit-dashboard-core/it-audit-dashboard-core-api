﻿using System.Collections.Generic;

namespace IT.Audit.Dashboard.EntityFrameworkCore.Domain.DataSources
{
    public interface IDataSourceProvider
    {
        public IReadOnlyList<DataSource> DataSources { get; set; }
    }
}
