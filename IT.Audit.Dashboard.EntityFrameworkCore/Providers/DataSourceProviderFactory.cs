﻿using IT.Audit.Dashboard.EntityFrameworkCore.Domain;
using IT.Audit.Dashboard.EntityFrameworkCore.IbaBdrm;
using System;

namespace IT.Audit.Dashboard.EntityFrameworkCore
{
    public class DataSourceProviderFactory : IDataSourceProviderFactory 
    {
        //private readonly IServiceProvider _serviceProvider;
        private readonly IIbaUserRepository _ibaUserRepository;

        public DataSourceProviderFactory(IIbaUserRepository ibaUserRepository)
        {
            //_serviceProvider = serviceProvider;
            _ibaUserRepository = ibaUserRepository;
        }

        public IDataSourceProvider CreateDataSourceProvider(string controllerName)
        {
            switch (controllerName)
            {
                case "iba-users":
                    //var provider = _serviceProvider.GetService(typeof(IbaBdrmDbContext));
                    return new IbaDataSourceProvider(_ibaUserRepository);
                case "ad-users":
                    return new ActiveDirectoryUsersDataSourceProvider();
                //case "":
                //    provider = new ActiveDirectoryConnectionProvider(Configuration);
                //    break;
                default:
                    throw new NotImplementedException(nameof(DataSourceProviderFactory));
            }
        }
    }
}
    