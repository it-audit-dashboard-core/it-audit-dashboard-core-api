﻿using IT.Audit.Dashboard.Domain.Shared;
using System;
using System.Threading.Tasks;

namespace IT.Audit.Dashboard.EntityFrameworkCore.Domain
{
    public interface IDashboardRepository
    {
        Task<DateTime> GetModifiedDate(ConnectionType connectionType, string tableName);
    }
}
