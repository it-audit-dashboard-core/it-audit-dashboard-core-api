﻿using IT.Audit.Dashboard.Domain.Shared;
using IT.Audit.Dashboard.Domain.Shared.Dashboard;
using System;

namespace IT.Audit.Dashboard.EntityFrameworkCore.Domain
{
    public class DashboardUser : IEntity, IDashboardUser
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime ModifiedDate { get; set; }
    }
}
