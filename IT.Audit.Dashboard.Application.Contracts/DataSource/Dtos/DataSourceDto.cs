﻿using IT.Audit.Dashboard.Domain.Shared;

namespace IT.Audit.Dashboard.Application.Dtos
{
    public class DataSourceDto
    {
        public ConnectionType ConnectionType { get; set; }
        public string TableName { get; set; }
    }
}
