﻿using System.Collections.Generic;

namespace IT.Audit.Dashboard.Application.Dtos
{
    public class DataSourceInputDto
    {
        public IReadOnlyList<PageSettingsOutputDto> PageSettings { get; set; }
    }
}
