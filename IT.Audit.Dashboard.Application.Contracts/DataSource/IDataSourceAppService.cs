﻿using IT.Audit.Dashboard.Application.Dtos;
using System.Threading.Tasks;

namespace IT.Audit.Dashboard.Application
{
    public interface IDataSourceAppService
    {
        Task<DataSourceOutputDto> GetAsync(string controller);
    }
}
