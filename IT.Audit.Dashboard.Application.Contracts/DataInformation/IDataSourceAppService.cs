﻿using IT.Audit.Dashboard.Application.Dtos;
using IT.Audit.Dashboard.Domain.Shared;
using System.Threading.Tasks;

namespace IT.Audit.Dashboard.Application
{
    public interface IDataSourceAppService
    {
        Task<DataSourceOutputDto> GetAsync(ConnectionType connectionType, string tableName, string cacheKey);

        Task<DataSourceOutputDto> GetFromDatabaseAsync(ConnectionType connectionType, string tableName);

        Task<DataSourceOutputDto> GetAsync(DataInformationInputDto inputDto);

        Task<DataSourceOutputDto> GetFromDatabaseAsync(DataInformationInputDto inputDto);
    }
}