﻿using System.Collections.Generic;

namespace IT.Audit.Dashboard.Application.Dtos
{
    public class DataInformationOutputDto
    {
        public DataInformationOutputDto()
        {
            DataSources = new List<DataSourceOutputDto>();
        }

        public IReadOnlyList<DataSourceOutputDto> DataSources { get; set; }
    }
}
