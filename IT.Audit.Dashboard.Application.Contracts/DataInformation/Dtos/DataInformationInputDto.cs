﻿using IT.Audit.Dashboard.Domain.Shared;

namespace IT.Audit.Dashboard.Application.Dtos
{
    public class DataInformationInputDto
    {
        public ConnectionType ConnectionType { get; set; }
    }
}
