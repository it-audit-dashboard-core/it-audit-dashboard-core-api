﻿using IT.Audit.Dashboard.Application.Dtos;
using System.Threading.Tasks;

namespace IT.Audit.Dashboard.Application
{
    public interface IDataInformationAppService
    {
        Task<DataInformationOutputDto> GetAsync(DataInformationInputDto inputDto);
    }
}