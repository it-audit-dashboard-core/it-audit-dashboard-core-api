﻿using IT.Audit.Dashboard.Application.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IT.Audit.Dashboard.Application
{
    public interface IIbaUsersAppService
    {
        Task<List<IbaUserDto>> GetAsync();

        Task<FilterDataDto> GetFilterData();
    }
}
