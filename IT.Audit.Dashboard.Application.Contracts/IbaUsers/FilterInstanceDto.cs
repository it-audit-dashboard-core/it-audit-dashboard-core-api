﻿namespace IT.Audit.Dashboard.Application.Dtos
{
    public class FilterInstanceDto
    {
        public string Name { get; set; }
    }
}