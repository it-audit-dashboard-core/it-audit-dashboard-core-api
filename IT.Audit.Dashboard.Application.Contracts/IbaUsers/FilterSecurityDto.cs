﻿namespace IT.Audit.Dashboard.Application.Dtos
{
    public class FilterSecurityDto
    {
        public FilterInstanceDto Instance { get; set; }
        public string Name { get; set; }
    }
}