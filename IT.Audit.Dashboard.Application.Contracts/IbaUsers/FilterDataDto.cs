﻿using IT.Audit.Dashboard.Domain;
using System.Collections.Generic;

namespace IT.Audit.Dashboard.Application.Dtos
{
    public class FilterDataDto
    {
        public List<FilterInstance> Instances { get; set; }
        public List<FilterSecurity> Securities { get; set; }
        public List<FilterRole> Roles { get; set; }
    }
}
