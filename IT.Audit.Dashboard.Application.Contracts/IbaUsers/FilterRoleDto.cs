﻿namespace IT.Audit.Dashboard.Application.Dtos
{
    public class FilterRoleDto
    {
        public FilterInstanceDto Instance { get; set; }
        public string Name { get; set; }
    }
}