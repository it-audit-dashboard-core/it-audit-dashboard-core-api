﻿using IT.Audit.Dashboard.Domain.Shared;
using System;
using System.Collections.Generic;

namespace IT.Audit.Dashboard.Application.Dtos
{
    public class InstanceSecurityRoleDto
    {
        public ParentResultDto InstanceResult { get; set; }
        public ParentResultDto SecurityResult { get; set; }
        public ParentResultDto RoleResult { get; set; }
    }

    public class ParentResultDto
    {
        public ParentResultDto(string name)
        {
            Name = name;
            Children = new List<ChildDto>();
        }

        public string Name { get; }

        public List<ChildDto> Children { get; set; }

        public int Count { get; set; }
    }

    public class ChildDto
    {
        public ChildDto(string parent)
        {
            Parent = parent;
        }

        public ChildDto(string label, string value, bool selected, bool disabled, string parent, bool isLabel)
        {
            Label = label;
            Value = value;
            Selected = selected;
            Disabled = disabled;
            Parent = parent;
            IsLabel = isLabel;
        }

        public string Label { get; }
        public string Value { get; }
        public bool Selected { get; }
        public bool Disabled { get; }
        public string Parent { get; }
        public bool IsLabel { get; }
    }

    public class Group
    {
        public string label { get; set; }
        public List<Child> children { get; set; }

        public Group()
        {
            children = new List<Child>();
        }
    }

    public class Child
    {
        public string label { get; set; }
        public string value { get; set; }
        public bool selected { get; set; }
        public bool disabled { get; set; }
    }

    public class IbaSearchFilterDto
    {
        public string Name { get; set; }
        public List<string> Instances { get; set; }
        public List<string> Securities { get; set; }
        public List<string> Roles { get; set; }
        public string Login { get; set; }
        public string Active { get; set; }
        public string UpdatedBy { get; set; }
        public string UpdatedFrom { get; set; }
        public string UpdatedTo { get; set; }
        public string Disabled { get; set; }
        public string ServiceDeskRef { get; set; }
        public string CreatedDateFrom { get; set; }
        public string CreatedDateTo { get; set; }
        public string CreatedRef { get; set; }
        public string CreatedUser { get; set; }
        public string UserName { get; set; }
        public string ADOU { get; set; }
        public string AccountStatus { get; set; }
        public string Search { get; set; }

        public DateTime? UpdatedFromDate
        {
            get
            {
                DateTime d;

                if (DateTime.TryParse(UpdatedFrom, out d))
                {
                    return d;
                }

                return null;
            }
        }

        public DateTime? UpdatedToDate
        {
            get
            {
                DateTime d;

                if (DateTime.TryParse(UpdatedTo, out d))
                {
                    return d;
                }

                return null;
            }
        }

        public DateTime? CreatedFromDate
        {
            get
            {
                DateTime d;

                if (DateTime.TryParse(CreatedDateFrom, out d))
                {
                    return d;
                }

                return null;
            }
        }

        public DateTime? CreatedToDate
        {
            get
            {
                DateTime d;

                if (DateTime.TryParse(CreatedDateTo, out d))
                {
                    return d;
                }

                return null;
            }
        }
        public IbaUserActiveType ActiveFlag
        {
            get
            {

                return GetActiveType(Active);
            }
        }

        public IbaUserActiveType DisabledFlag
        {
            get
            {

                return GetActiveType(Disabled);
            }
        }

        public IbaUserActiveType AccountStatusFlag
        {
            get
            {

                return GetActiveType(AccountStatus);
            }
        }

        private IbaUserActiveType GetActiveType(string flag)
        {
            if (string.IsNullOrEmpty(flag))
                return IbaUserActiveType.NA;

            switch (flag.ToUpper())
            {
                case "ACTIVE":
                    return IbaUserActiveType.Active;
                case "INACTIVE":
                    return IbaUserActiveType.Inactive;
                case "BOTH":
                    return IbaUserActiveType.Both;
                default:
                    return IbaUserActiveType.NA;
            }
        }
    }
}
