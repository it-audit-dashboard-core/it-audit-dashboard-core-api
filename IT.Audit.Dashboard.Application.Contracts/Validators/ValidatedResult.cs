﻿using FluentValidation;

namespace IT.Audit.Dashboard.Application.Dtos
{
    public class ValidatedResult<TOutput> 
    {
        public ValidatedResult(IMapper mapper)
        {

        }
        public ValidatedDto<TOutput> Build(AbstractValidator<TOutput> validator, TOutput output)
        {
            var result = validator.Validate(output);
            var errors;
            if (result.Errors.Count > 0)
                errors = _

            return new ValidatedDto<TOutput>(output, result.Errors);
        }
    }
}
