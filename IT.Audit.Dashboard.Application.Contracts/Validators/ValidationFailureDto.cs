﻿using FluentValidation;

namespace IT.Audit.Dashboard.Application.Dtos
{
    public class ValidationFailureDto
    {
        //
        // Summary:
        //     The name of the property.
        public string PropertyName { get; set; }
        //
        // Summary:
        //     The error message
        public string ErrorMessage { get; set; }
        //
        // Summary:
        //     The property value that caused the failure.
        public object AttemptedValue { get; set; }
        //
        // Summary:
        //     Custom state associated with the failure.
        public object CustomState { get; set; }
        //
        // Summary:
        //     Custom severity level associated with the failure.
        public Severity Severity { get; set; }
        //
        // Summary:
        //     Gets or sets the error code.
        public string ErrorCode { get; set; }
    }
}
