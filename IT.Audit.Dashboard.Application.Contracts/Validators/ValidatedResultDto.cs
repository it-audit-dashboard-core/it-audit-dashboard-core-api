﻿using System.Collections.Generic;

namespace IT.Audit.Dashboard.Application.Dtos
{
    public class ValidatedResultDto<TOuputDto>
    {
        public ValidatedResultDto(TOuputDto resultDto, List<ValidationFailureDto> errors = null)
        {
            ResultDto = resultDto;
            Errors = errors;
        }

        public TOuputDto ResultDto { get; set; }

        public List<ValidationFailureDto> Errors { get; set; }
    }
}
