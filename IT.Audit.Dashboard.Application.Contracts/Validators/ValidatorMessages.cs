﻿namespace IT.Audit.Dashboard.Application.Contracts.Validators
{
    public static class ValidatorMessages
    {
        public static string DataSourcesNotImportedToday = "Not all datasources were imported today";

        public static string DataSourcesNotSynchronised = "The datasources are not in sync";
    }
}
