﻿using System;

namespace IT.Audit.Dashboard.Application.Contracts.Logging
{
    public class ErrorLogDto : EntityDto
    {
        public DateTime ErrorTime { get; set; }
        public string UserName { get; set; }
        public string MachineName { get; set; }
        public string ModuleName { get; set; }
        public string FunctionName { get; set; }
        public string MessageText { get; set; }
    }

    // TODO: Change
    public class ErrorOutputDto {
        public string Result { get; set; }
    }

    public class EntityDto
    {
        public int Id { get; set; }
    }
}