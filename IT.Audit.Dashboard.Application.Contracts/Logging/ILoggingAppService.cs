﻿using IT.Audit.Dashboard.Application.Contracts.Logging;
using System.Threading.Tasks;

namespace IT.Audit.Dashboard.Application
{
    public interface ILoggingAppService
    {
        Task<ErrorOutputDto> LogError(ErrorLogDto errorLog);
    }
}