﻿using IT.Audit.Dashboard.Application.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IT.Audit.Dashboard.Application
{
    public interface IUserApplicationAppService
    {
        Task<ValidatedResultDto<List<UserApplicationDto>>> GetListAsync();
        Task<ValidatedResultDto<UserApplicationDto>> GetByKeyAsync(string key);
        Task<IEnumerable<UserApplicationDto>> GetFromFile(string path);
    }
}
