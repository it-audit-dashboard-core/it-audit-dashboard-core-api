﻿using System.Collections.Generic;

namespace IT.Audit.Dashboard.Application.Dtos
{
    public class UserApplicationDto
    {
        public int Id { get; set; }
        public string Key { get; set; }
        public string Title { get; set; }
        public string DataSources { get; set; }
        //public List<DataSourceOutputDto> DataSources { get; set; }
        public string Route { get; set; }
    }
}
