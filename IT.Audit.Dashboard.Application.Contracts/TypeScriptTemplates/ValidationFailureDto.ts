﻿
module IT.Audit.Dashboard.Application.Dtos {

    // $Classes/Enums/Interfaces(filter)[template][separator]
    // filter (optional): Matches the name or full name of the current item. * = match any, wrap in [] to match attributes or prefix with : to match interfaces or base classes.
    // template: The template to repeat for each matched item
    // separator (optional): A separator template that is placed between all templates e.g. $Properties[public $name: $Type][, ]

    // More info: http://frhagn.github.io/Typewriter/

    
    export class ValidationFailureDto {
        
        // PROPERTYNAME
        public propertyName: string = null;
        // ERRORMESSAGE
        public errorMessage: string = null;
        // ATTEMPTEDVALUE
        public attemptedValue: any = null;
        // CUSTOMSTATE
        public customState: any = null;
        // SEVERITY
        public severity: Severity = null;
        // ERRORCODE
        public errorCode: string = null;
    }
}