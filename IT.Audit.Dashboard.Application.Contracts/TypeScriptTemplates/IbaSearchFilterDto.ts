﻿
module IT.Audit.Dashboard.Application.Dtos {

    // $Classes/Enums/Interfaces(filter)[template][separator]
    // filter (optional): Matches the name or full name of the current item. * = match any, wrap in [] to match attributes or prefix with : to match interfaces or base classes.
    // template: The template to repeat for each matched item
    // separator (optional): A separator template that is placed between all templates e.g. $Properties[public $name: $Type][, ]

    // More info: http://frhagn.github.io/Typewriter/

    
    export class InstanceSecurityRoleDto {
        
        // INSTANCERESULT
        public instanceResult: ParentResultDto = null;
        // SECURITYRESULT
        public securityResult: ParentResultDto = null;
        // ROLERESULT
        public roleResult: ParentResultDto = null;
    }
    export class ParentResultDto {
        
        // NAME
        public name: string = null;
        // CHILDREN
        public children: ChildDto[] = [];
        // COUNT
        public count: number = 0;
    }
    export class ChildDto {
        
        // LABEL
        public label: string = null;
        // VALUE
        public value: string = null;
        // SELECTED
        public selected: boolean = false;
        // DISABLED
        public disabled: boolean = false;
        // PARENT
        public parent: string = null;
    }
    export class IbaSearchFilterDto {
        
        // NAME
        public name: string = null;
        // INSTANCES
        public instances: string[] = [];
        // SECURITIES
        public securities: string[] = [];
        // ROLES
        public roles: string[] = [];
        // LOGIN
        public login: string = null;
        // ACTIVE
        public active: string = null;
        // UPDATEDBY
        public updatedBy: string = null;
        // UPDATEDFROM
        public updatedFrom: string = null;
        // UPDATEDTO
        public updatedTo: string = null;
        // DISABLED
        public disabled: string = null;
        // SERVICEDESKREF
        public serviceDeskRef: string = null;
        // CREATEDDATEFROM
        public createdDateFrom: string = null;
        // CREATEDDATETO
        public createdDateTo: string = null;
        // CREATEDREF
        public createdRef: string = null;
        // CREATEDUSER
        public createdUser: string = null;
        // USERNAME
        public userName: string = null;
        // ADOU
        public adou: string = null;
        // ACCOUNTSTATUS
        public accountStatus: string = null;
        // SEARCH
        public search: string = null;
        // UPDATEDFROMDATE
        public updatedFromDate: Date = null;
        // UPDATEDTODATE
        public updatedToDate: Date = null;
        // CREATEDFROMDATE
        public createdFromDate: Date = null;
        // CREATEDTODATE
        public createdToDate: Date = null;
        // ACTIVEFLAG
        public activeFlag: IbaUserActiveType = null;
        // DISABLEDFLAG
        public disabledFlag: IbaUserActiveType = null;
        // ACCOUNTSTATUSFLAG
        public accountStatusFlag: IbaUserActiveType = null;
    }
}