﻿
module IT.Audit.Dashboard.Application.Dtos {

    // $Classes/Enums/Interfaces(filter)[template][separator]
    // filter (optional): Matches the name or full name of the current item. * = match any, wrap in [] to match attributes or prefix with : to match interfaces or base classes.
    // template: The template to repeat for each matched item
    // separator (optional): A separator template that is placed between all templates e.g. $Properties[public $name: $Type][, ]

    // More info: http://frhagn.github.io/Typewriter/

    
    export class ErrorLogDto {
        
        // ERRORTIME
        public errorTime: Date = new Date(0);
        // USERNAME
        public userName: string = null;
        // MACHINENAME
        public machineName: string = null;
        // MODULENAME
        public moduleName: string = null;
        // FUNCTIONNAME
        public functionName: string = null;
        // MESSAGETEXT
        public messageText: string = null;
    }
    export class EntityDto {
        
        // ID
        public id: number = 0;
    }
}