﻿
module IT.Audit.Dashboard.Application.Dtos {

    // $Classes/Enums/Interfaces(filter)[template][separator]
    // filter (optional): Matches the name or full name of the current item. * = match any, wrap in [] to match attributes or prefix with : to match interfaces or base classes.
    // template: The template to repeat for each matched item
    // separator (optional): A separator template that is placed between all templates e.g. $Properties[public $name: $Type][, ]

    // More info: http://frhagn.github.io/Typewriter/

    
    export class IbaUserDto {
        
        // NAME
        public name: string = null;
        // INSTANCE
        public instance: string = null;
        // ROLE
        public role: string = null;
        // SECURITY
        public security: string = null;
        // LOGIN
        public login: string = null;
        // ISACTIVE
        public isActive: boolean = false;
        // UPDATEDBY
        public updatedBy: string = null;
        // UPDATED
        public updated: Date = new Date(0);
        // DISABLED
        public disabled: boolean = false;
        // SERVICEDESKREF
        public serviceDeskRef: string = null;
        // CREATEDDATE
        public createdDate: Date = new Date(0);
        // CREATEDREF
        public createdRef: string = null;
        // CREATEDUSER
        public createdUser: string = null;
        // USERNAME
        public userName: string = null;
        // ADOU
        public adou: string = null;
        // ACCOUNTSTATUS
        public accountStatus: string = null;
        // OFFICE
        public office: string = null;
        // AD_DEPARTMENT
        public aD_Department: string = null;
        // CITY
        public city: string = null;
        // MANAGER
        public manager: string = null;
        // IBA_LOAD_DATE
        public ibA_Load_Date: Date = new Date(0);
    }
}