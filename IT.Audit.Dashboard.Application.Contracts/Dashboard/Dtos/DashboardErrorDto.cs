﻿using IT.Audit.Dashboard.Domain.Shared;

namespace IT.Audit.Dashboard.Application.Dtos
{
    public class DashboardErrorDto
    {
        public string Message { get; set; }
        public AlertLevel AlertLevel { get; set; }
    }
}
