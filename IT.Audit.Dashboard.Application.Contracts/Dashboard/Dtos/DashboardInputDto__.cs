﻿using IT.Audit.Dashboard.Domain.Shared;

namespace IT.Audit.Dashboard.Application.Dtos
{
    public class DashboardInputDto_
    {
        public ConnectionType ConnectionType { get; set; }
    }
}
