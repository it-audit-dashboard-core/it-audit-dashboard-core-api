﻿using IT.Audit.Dashboard.Domain.Shared;
using System.Collections.Generic;

namespace IT.Audit.Dashboard.Application.Dtos
{
    public class DashboardInputDto
    {
        public string UserName { get; set; }
        public bool RefreshCache { get; set; }
        public ConnectionType ConnectionType { get; set; }
    }
}
