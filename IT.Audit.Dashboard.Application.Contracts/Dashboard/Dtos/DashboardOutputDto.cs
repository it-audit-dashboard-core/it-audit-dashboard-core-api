﻿using System;

namespace IT.Audit.Dashboard.Application.Dtos
{
    public class DashboardOutputDto : IEquatable<DashboardOutputDto>
    {
        public DateTime ModifiedDate { get; set; }
        public string Name { get; set; }

        public bool Equals(DashboardOutputDto other)
        {
            if (other == null)
                return false;

            return other.ModifiedDate.ToShortDateString() == this.ModifiedDate.ToShortDateString();
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;

            var t = obj as DashboardOutputDto;  // The extra cast
            if (t == null)
                return false;
            else
                return this.ModifiedDate == t.ModifiedDate;
        }

        public override int GetHashCode()
        {
            throw new NotImplementedException();
        }
    }
}
