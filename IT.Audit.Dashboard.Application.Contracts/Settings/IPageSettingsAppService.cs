﻿using IT.Audit.Dashboard.Application.Dtos;
using IT.Audit.Dashboard.Domain.Shared;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IT.Audit.Dashboard.Application
{
    public interface IPageSettingsAppService
    {
        Task<List<PageSettingsOutputDto>> GetListAsync(ConnectionType connectionType);
    }
}
