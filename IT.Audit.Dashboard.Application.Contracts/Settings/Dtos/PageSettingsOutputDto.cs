﻿using IT.Audit.Dashboard.Domain.Shared;

namespace IT.Audit.Dashboard.Application.Dtos
{
    public class PageSettingsOutputDto
    {
        public string Name { get; set; }
        public ConnectionType ConnectionType { get; set; }
        public string TableName { get; set; }
        public string CacheKey { get; set; }
    }
}
