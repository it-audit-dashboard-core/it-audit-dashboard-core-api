﻿using IT.Audit.Dashboard.Domain.Security;
using System.Threading.Tasks;

namespace IT.Audit.Dashboard.Application.Contracts.Account
{
    public interface IUserAppService
    {
        Task<IdentityUser> GetAuthenticatedUser();
    }
}
