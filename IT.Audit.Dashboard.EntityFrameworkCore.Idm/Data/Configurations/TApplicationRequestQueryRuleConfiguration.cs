﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using IT.Audit.Dashboard.EntityFrameworkCore.Idm.Data;
using IT.Audit.Dashboard.EntityFrameworkCore.Idm.Models;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore;
using System;

#nullable disable

namespace IT.Audit.Dashboard.EntityFrameworkCore.Idm.Data.Configurations
{
    public partial class TApplicationRequestQueryRuleConfiguration : IEntityTypeConfiguration<TApplicationRequestQueryRule>
    {
        public void Configure(EntityTypeBuilder<TApplicationRequestQueryRule> entity)
        {
            entity.Property(e => e.Application).IsUnicode(false);

            entity.Property(e => e.ApplicationRequestQueryParameter1).IsUnicode(false);

            entity.Property(e => e.ApplicationRequestQueryParameter2).IsUnicode(false);

            entity.Property(e => e.ApplicationRequestQueryParameter3).IsUnicode(false);

            entity.Property(e => e.ApplicationRequestQueryParameter4).IsUnicode(false);

            entity.Property(e => e.ApplicationRequestQueryParameter5).IsUnicode(false);

            entity.Property(e => e.ItauditDashboardReport).IsUnicode(false);

            entity.Property(e => e.ItauditDashboardView).IsUnicode(false);

            entity.Property(e => e.UpdatedBy).IsUnicode(false);

            OnConfigurePartial(entity);
        }

        partial void OnConfigurePartial(EntityTypeBuilder<TApplicationRequestQueryRule> entity);
    }
}
