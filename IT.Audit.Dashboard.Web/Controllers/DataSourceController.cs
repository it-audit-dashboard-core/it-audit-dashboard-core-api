﻿using IT.Audit.Dashboard.Application;
using IT.Audit.Dashboard.Application.Dtos;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace IT.Audit.Dashboard.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DataSourceController : ControllerBase
    {
        private readonly IDataSourceAppService _dataSourceAppService;

        public DataSourceController(IDataSourceAppService dataSourceAppService)
        {
            _dataSourceAppService = dataSourceAppService;
        }

        [HttpGet]
        public async Task<DataSourceOutputDto> GetAsync(string controllerName)
        {
            return await _dataSourceAppService.GetAsync(controllerName);
        }
    }
}
