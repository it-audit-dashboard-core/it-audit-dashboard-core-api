import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';

import { IdentityService } from 'src/app/security/identity.service';
import { IdentityUserBase } from 'src/app/shared/security/identity-user-base';
import { IdentityUser } from 'src/app/security/identity-user';

//import { ValidatedResultDto, ValidationFailureDto } from '../../models/validator-result';
//import { DataInformationOutputDto, DataSourceDto, UserApplicationDto } from '../../models/user-application';

@Component({
  selector: 'app-nav-bar-header',
  templateUrl: './nav-bar-header.component.html',
  styleUrls: ['./nav-bar-header.component.css']
})
export class NavBarHeaderComponent implements OnInit {
  @Input() userName: string = "";
  environmentSettingsForm!: FormGroup;
  userNameForm!: FormGroup;
  userNameControl!: FormControl;
  environmentMachine: FormControl | undefined;

  // identityUser!: IdentityUserBase;
  // userApplicationResultDto!: ValidatedResultDto<UserApplicationDto>;
  // dataInformationResultDto!: ValidatedResultDto<DataInformationOutputDto>;
  // dataSources!: DataSourceDto[];
  // validationFailures!: ValidationFailureDto[];
  // userName: string | undefined;

  constructor(
    private identityService: IdentityService,
    private formBuilder: FormBuilder) {

  }

  ngOnInit(): void {
    this.createForm();
    this.createUserNameForm();
  }

  createUserNameForm() {
    this.userNameForm = this.formBuilder.group({
      userNameControl: new FormControl('')
    });
  }

  createForm() {
    this.environmentSettingsForm = this.formBuilder.group({
      environmentMachine: new FormControl(''),
    });
  }

  onChange(e: any) {
    //console.log(e.target?.value);
  }
}
