import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AdUsersModule } from "../ad-users/ad-users.module";
import { DashboardModule } from "../dashboard/dashboard.module";
import { IbaUsersModule } from "../iba-users/iba-users.module";
import { LayoutComponent } from "./layout.component";

const routes: Routes = [
  {
    path: '', component: LayoutComponent,
    children: [
      { path: '', loadChildren: () => DashboardModule },
      { path: 'apps/iba-users', loadChildren: () => IbaUsersModule },
      { path: 'apps/ad-users', loadChildren: () => AdUsersModule }
    ]
  }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LayoutRoutingModule { }
