import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutRoutingModule } from './layout-routing.module';

import { LayoutComponent } from './layout.component';
import { NavBarHeaderComponent } from './nav-bar-header/nav-bar-header.component';
import { NavBarFooterComponent } from './nav-bar-footer/nav-bar-footer.component';
import { IdentityService } from '../security/identity.service';


@NgModule({
  declarations: [
    LayoutComponent,
    NavBarHeaderComponent,
    NavBarFooterComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    LayoutRoutingModule
  ],
  providers: [
    IdentityService
  ]
})
export class LayoutModule { }
