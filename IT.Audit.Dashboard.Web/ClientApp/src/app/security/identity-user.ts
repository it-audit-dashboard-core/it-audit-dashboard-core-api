import { IdentityUserBase } from "../shared/security/identity-user-base";

export class IdentityUser extends IdentityUserBase{
    // props
    canAccesssIbaUsers: boolean = false;
    canAccesssADUsers: boolean = false;

    init(): void{
        super.init();

        // initialise props
        this.canAccesssIbaUsers = false;
        this.canAccesssADUsers = false;
    }
}
