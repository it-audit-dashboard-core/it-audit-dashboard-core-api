import { ValidationFailureDto } from "./validation-failure-dto";

export class ValidatedResultDto<TOutput> {

  // RESULTDTO
  public resultDto!: TOutput;
  // ERRORS
  public errors: ValidationFailureDto[] = [];
}

