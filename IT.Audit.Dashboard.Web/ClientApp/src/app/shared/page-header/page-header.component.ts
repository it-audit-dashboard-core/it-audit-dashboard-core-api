import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-page-header',
  templateUrl: './page-header.component.html',
  styleUrls: ['./page-header.component.css']
})
export class PageHeaderComponent implements OnInit {
  pageKey: string = "";

  constructor(private router: Router) {
  }

  ngOnInit(): void {
    // Implement pageService to get route information into a pageTitle class and a dataSourceInformation class
    this.pageKey = this.router.url.replace("/apps/", "");
    console.log('PageKey: ' + this.pageKey);
  }

}
