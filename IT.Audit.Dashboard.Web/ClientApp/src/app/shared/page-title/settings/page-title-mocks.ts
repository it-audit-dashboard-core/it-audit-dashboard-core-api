import { PageTitleDto } from "../page-title";

export const PAGE_TITLE_MOCKS: PageTitleDto[] = [
  {
    title: "IBA Users",
    subTitle: ""
  },
  {
    title: "AD Users",
    subTitle: ""
  },
  {
    title: "BDRM Users",
    subTitle: ""
  },
  {
    title: "Acturis Users",
    subTitle: ""
  },
  {
    title: "CDL Users",
    subTitle: ""
  }
];
