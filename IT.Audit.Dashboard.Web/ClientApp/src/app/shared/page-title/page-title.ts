export interface PageTitle {
  id: number;
  title: string;
  subTitle: string;
}

export class PageTitleDto {
  title: string;
  subTitle: string;
}
