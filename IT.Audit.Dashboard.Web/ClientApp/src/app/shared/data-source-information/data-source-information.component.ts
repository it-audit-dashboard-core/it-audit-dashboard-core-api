import { Component, Input, OnInit } from '@angular/core';


@Component({
  selector: 'app-data-source-information',
  templateUrl: './data-source-information.component.html',
  styleUrls: ['./data-source-information.component.css']
})
export class DatasourceInformationComponent implements OnInit {
  @Input() pageKey: string = "";
  //@Input() dataSources!: DataSourceOutputDto[];
  //@Input() validationFailures!: ValidationFailureDto[];

  constructor() { }

  ngOnInit(): void {
  }

  getModifiedDate(index: number): string {
    return "modifiedDate" + index;
  }

  setIcon(severity: number): string {
    switch (severity) {
      case 1:
        return "fa fa-exclamation-triangle pl-2 pr-2";
      default:
        return "fa fa-exclamation-triangle pl-2 pr-2"
    }
  }

  setClass(severity: number): string {
    switch (severity) {
      case 1:
        return "alert alert-danger fade in alert-dismissible show p-1 mt-1";
      default:
        return "alert alert-warning fade in alert-dismissible show p-1 mt-1"
    }
  }

  showAlert() {
    return 'list-group-item list-group-item-danger';// : 'list-group-item list-group-item-warning'
  }
}
