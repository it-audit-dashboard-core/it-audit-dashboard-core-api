export class DataSource {
  id: number;
  modifiedDate: Date = new Date(0);
  name: string = "";
}

export class DataSourceOutputDto
{
  public modifiedDate: Date = new Date(0);
  public name: string = "";
}

export interface DataSourceErrorDto
{
  message: string;
  alertLevel: number;
}
