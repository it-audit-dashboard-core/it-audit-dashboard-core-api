import { DataSource, DataSourceOutputDto } from "./data-source";

export class DataInformation {
  id: number;
  pageKey: string;
  dataSources: DataSource[] = [];
}

export class DataInformationDto {
  pageKey: string;
  dataSources: DataSourceOutputDto[] = [];
}

export class DataInformationOutputDto
{
  dataSources: DataSourceOutputDto[] = [];
}
