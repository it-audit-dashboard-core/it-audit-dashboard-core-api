import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ApplicationsMenuComponent } from './components/applications-menu/applications-menu.component';
import { ConfigurationService } from './configuration/configuration.service';
import { PageHeaderComponent } from './page-header/page-header.component';
import { PageTitleComponent } from './page-title/page-title.component';
import { DatasourceInformationComponent } from './data-source-information/data-source-information.component';


@NgModule({
  declarations: [    
    ApplicationsMenuComponent,
    PageHeaderComponent,
    PageTitleComponent,
    DatasourceInformationComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    PageHeaderComponent
  ],
  providers:[
    ConfigurationService
  ]
})
export class SharedModule { }
