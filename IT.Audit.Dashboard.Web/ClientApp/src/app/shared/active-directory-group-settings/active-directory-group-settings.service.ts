import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { ActiveDirectoryGroupSettings } from './active-directory-group-settings';

const APPLICATION_AD_GROUPS_LOCATION = "assets/application-active-directory-groups.json";
const APPLICATION_AD_GROUPS_KEY = "ActiveDirectoryGroupSettings";

@Injectable({
    providedIn: 'root'
})
export class AppSettingsService {

    constructor(private http: HttpClient) {
    }

    /*
        TODO: Switch for different return T
    */
    get<T>(operation = "operation", result?: T){
        return of(result as T);
    }

    getActiveDirectoryGroupSettings(): Observable<ActiveDirectoryGroupSettings> {

        let settings = localStorage.getItem(APPLICATION_AD_GROUPS_KEY);
        
        if (settings) {
            console.log("FROM CACHE!")
            return of(JSON.parse(settings));
        }

        return this.http.get<ActiveDirectoryGroupSettings>(APPLICATION_AD_GROUPS_LOCATION)
            .pipe(tap(settings => {
                console.log("FROM APPSETTINGS.JSON!")
                if (settings) this.addSettingsToCache(settings);
            }),
            catchError(
                this.handleError<ActiveDirectoryGroupSettings>('getSettings',
                    new ActiveDirectoryGroupSettings()))
            );
    }

    addSettingsToCache(settings: ActiveDirectoryGroupSettings) {
        localStorage.setItem(APPLICATION_AD_GROUPS_KEY,
            JSON.stringify(settings));
    }

    deleteSettingsFromCache(){
        localStorage.removeItem(APPLICATION_AD_GROUPS_KEY);
    }

    private handleError<T>(operation = "operation", result?: T) {
        return (error: any): Observable<T> => {
            switch (error.status) {
                case 404:
                    console.error("Can't find file: " + APPLICATION_AD_GROUPS_LOCATION);
                    break;
                default:
                    console.error(error);
                    break;
            }

            return of(result as T);
        }
    }
}
