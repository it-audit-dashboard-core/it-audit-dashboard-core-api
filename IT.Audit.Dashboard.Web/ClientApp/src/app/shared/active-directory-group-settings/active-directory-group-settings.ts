export class ActiveDirectoryGroupSettings {
    "activeDirectoryUsersGroup": string = "";
    "acturisUsersGroup": string = "";
    "acturisEditingGroup": string = "";
    "bdrmUsersGroup": string = "";
    "ibaUsersGroup": string = "";
    "userAdminGroup": string = "";
    "cdlUsersGroup": string = "";
    "cdlEditingGroup": string = "";
    "cdlUseraGroup": string = "";
    "cdlReferenceUpdateGroup": string = "";
    "adGroupMembershipUsersGroup": string = "";
    "genericApplicationUsersGroup": string = "";
    "genericTableEditorGroup": string = "";
    "idmUsersGroup": string = "";
}
