import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { IbaUsersComponent } from './iba-users.component';

const routes: Routes = [
  { path: '', component: IbaUsersComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IbaUsersRoutingModule { }
