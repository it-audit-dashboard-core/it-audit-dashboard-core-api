import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IbaUsersComponent } from './iba-users.component';
import { IbaUsersRoutingModule } from './iba-users-routing.module';
import { SharedModule } from '../shared/shared.module';


@NgModule({
  declarations: [
    IbaUsersComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    IbaUsersRoutingModule
  ]
})
export class IbaUsersModule { }
