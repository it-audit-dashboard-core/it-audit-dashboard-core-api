import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-iba-users',
  templateUrl: './iba-users.component.html',
  styleUrls: ['./iba-users.component.css']
})
export class IbaUsersComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

}
