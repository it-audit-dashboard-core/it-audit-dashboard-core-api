import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdUsersRoutingModule } from './ad-users-routing.module';
import { AdUsersComponent } from './ad-users.component';
import { SharedModule } from '../shared/shared.module';


@NgModule({
  declarations: [
    AdUsersComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    AdUsersRoutingModule
  ]
})
export class AdUsersModule { }
