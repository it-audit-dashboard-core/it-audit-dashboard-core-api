import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdUsersComponent } from './ad-users.component';

const routes: Routes = [
  { path: '', component: AdUsersComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdUsersRoutingModule { }
