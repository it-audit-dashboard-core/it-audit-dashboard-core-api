﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace IT.Dashboard.EntityFramework.ActiveDirectoryUsersDb.Models
{
    [Keyless]
    public partial class VwActiveManagerList
    {
        [Column("EmployeeID")]
        public int? EmployeeId { get; set; }
        [Column("User_Name")]
        [StringLength(255)]
        public string UserName { get; set; }
        [Column("Display_Name")]
        [StringLength(255)]
        public string DisplayName { get; set; }
        [Column("First_Name")]
        [StringLength(255)]
        public string FirstName { get; set; }
        [Column("Last_Name")]
        [StringLength(255)]
        public string LastName { get; set; }
        [Column("staff_number")]
        [StringLength(50)]
        public string StaffNumber { get; set; }
        [StringLength(255)]
        public string Office { get; set; }
        [Column("AD_Department")]
        [StringLength(255)]
        public string AdDepartment { get; set; }
        [Column("objectGUID")]
        public Guid? ObjectGuid { get; set; }
        [StringLength(1000)]
        public string Manager { get; set; }
        [Column("Staff_Count")]
        public int? StaffCount { get; set; }
        [StringLength(50)]
        public string Domain { get; set; }
        [StringLength(1000)]
        public string ManagersManager { get; set; }
    }
}