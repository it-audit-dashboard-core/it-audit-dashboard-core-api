﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace IT.Dashboard.EntityFramework.ActiveDirectoryUsersDb.Models
{
    [Table("t_AD_Search_Applications")]
    public partial class TAdSearchApplication
    {
        [Key]
        [Column("ID")]
        public int Id { get; set; }
        [Required]
        [StringLength(50)]
        public string ApplicationName { get; set; }
        [Column("GUAR_YN")]
        public bool GuarYn { get; set; }
    }
}