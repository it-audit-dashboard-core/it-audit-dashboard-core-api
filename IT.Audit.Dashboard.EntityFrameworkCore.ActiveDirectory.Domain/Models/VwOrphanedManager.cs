﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace IT.Dashboard.EntityFramework.ActiveDirectoryUsersDb.Models
{
    [Keyless]
    public partial class VwOrphanedManager
    {
        [Column("Display_Name")]
        [StringLength(255)]
        public string DisplayName { get; set; }
        [Column("User_Name")]
        [StringLength(255)]
        public string UserName { get; set; }
        [StringLength(255)]
        public string Office { get; set; }
        [Column("Job_Title")]
        [StringLength(255)]
        public string JobTitle { get; set; }
        [StringLength(255)]
        public string Division { get; set; }
        [Column("AD_OU")]
        [StringLength(255)]
        public string AdOu { get; set; }
        [StringLength(255)]
        public string City { get; set; }
        [StringLength(255)]
        public string PostCode { get; set; }
        [StringLength(255)]
        public string Country { get; set; }
        [Column("Staff_Count")]
        public int? StaffCount { get; set; }
    }
}