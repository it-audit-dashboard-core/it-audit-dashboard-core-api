﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace IT.Dashboard.EntityFramework.ActiveDirectoryUsersDb.Models
{
    [Keyless]
    public partial class VwActiveNewUsersWithoutManager
    {
        [Column("User_Name")]
        [StringLength(255)]
        public string UserName { get; set; }
        [Column("Display_Name")]
        [StringLength(255)]
        public string DisplayName { get; set; }
        [Column("Email_Address")]
        [StringLength(255)]
        public string EmailAddress { get; set; }
        [Column("Job_Title")]
        [StringLength(255)]
        public string JobTitle { get; set; }
        [StringLength(255)]
        public string Company { get; set; }
        [StringLength(255)]
        public string Office { get; set; }
        [Column("AD_Department")]
        [StringLength(255)]
        public string AdDepartment { get; set; }
        [StringLength(255)]
        public string Division { get; set; }
        [StringLength(255)]
        public string City { get; set; }
        [StringLength(255)]
        public string PostCode { get; set; }
        [StringLength(255)]
        public string Country { get; set; }
        [Column("AD_OU")]
        [StringLength(255)]
        public string AdOu { get; set; }
        [Column("Load_Date", TypeName = "datetime")]
        public DateTime? LoadDate { get; set; }
        [Column("EmployeeID")]
        [StringLength(50)]
        public string EmployeeId { get; set; }
    }
}