﻿using IT.Audit.Dashboard.Sql.Providers;
using IT.Audit.Dashboard.Sql.Repositories;
using Microsoft.Extensions.Configuration;
using Moq;
using Xunit;

namespace IT.Audit.Dashboard.Sql.Tests
{
    public class LoggingTests
    {
        private const string ConfigSectionKey = "ITAuditDashboard";
        private const string ConfigSectionValue = @"Data Source=SOX_DB_DEV\SILO;Initial Catalog=IT_Audit_Dashboard;Persist Security Info=True;User ID=reportuser;Password=reportuser";
        private const string ConnectionStringsSectionName = "ConnectionStrings";

        private LoggingRepository _logging;

        public LoggingTests()
        {
            var mockConfSection = new Mock<IConfigurationSection>();
            mockConfSection.SetupGet(m => m[It.Is<string>(s => s == ConfigSectionKey)]).Returns(ConfigSectionValue);

            var mockConfiguration = new Mock<IConfiguration>();
            mockConfiguration.Setup(a => a.GetSection(It.Is<string>(s => s == ConnectionStringsSectionName))).Returns(mockConfSection.Object);

            var factory = new ConnectionProviderFactory(mockConfiguration.Object);

            _logging = new LoggingRepository(factory);
        }

        [Fact]
        public void ShouldLogError()
        {
            var sut = _logging.LogError("emea\\ribone", "TestController Name", "TestAction Name", "Test Message");

            Assert.Equal("Message logged", sut.Result);
        }
    }
}
