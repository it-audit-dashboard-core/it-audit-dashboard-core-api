﻿using IT.Audit.Dashboard.Sql.Providers;
using IT.Audit.Dashboard.Sql.Repositories;
using Microsoft.Extensions.Configuration;
using Moq;
using System.Threading.Tasks;
using Xunit;

namespace IT.Audit.Dashboard.Sql.Tests
{
    public class IbaUsersTests
    {
        private const string ConfigSectionKey = "ITAuditDashboard";
        private const string ConfigSectionValue = @"Data Source=SOX_DB_UAT\SILO;Initial Catalog=SOX_IBA_BDRM;Persist Security Info=True;User ID=reportuser;Password=reportuser";
        private const string ConnectionStringsSectionName = "ConnectionStrings";

        private readonly IbaUsersRepository _ibaUsersRepository;

        public IbaUsersTests()
        {
            var mockConfSection = new Mock<IConfigurationSection>();
            mockConfSection.SetupGet(m => m[It.Is<string>(s => s == ConfigSectionKey)]).Returns(ConfigSectionValue);

            var mockConfiguration = new Mock<IConfiguration>();
            mockConfiguration.Setup(a => a.GetSection(It.Is<string>(s => s == ConnectionStringsSectionName))).Returns(mockConfSection.Object);

            var factory = new ConnectionProviderFactory(mockConfiguration.Object);

            _ibaUsersRepository = new IbaUsersRepository(factory);
        }

        [Fact]
        public async Task ShouldGetIbaUsers()
        {
            var sut = await _ibaUsersRepository.Get();

            Assert.True(sut.Count > 0);
        }

        [Theory]
        [InlineData("akshay", "userName", 21)]
        [InlineData("aathane", "login", 3)]
        public async Task ShouldGetFilteredUsers(string name, string field, int expected)
        {
            var sut = await _ibaUsersRepository.Get();

            Assert.True(sut.Count > 0);
        }
    }
}
