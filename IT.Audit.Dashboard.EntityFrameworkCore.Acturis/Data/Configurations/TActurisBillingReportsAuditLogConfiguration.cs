﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using IT.Audit.Dashboard.EntityFrameworkCore.Acturis.Data;
using IT.Audit.Dashboard.EntityFrameworkCore.Acturis.Models;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore;
using System;


namespace IT.Audit.Dashboard.EntityFrameworkCore.Acturis.Data.Configurations
{
    public partial class TActurisBillingReportsAuditLogConfiguration : IEntityTypeConfiguration<TActurisBillingReportsAuditLog>
    {
        public void Configure(EntityTypeBuilder<TActurisBillingReportsAuditLog> entity)
        {
            entity.Property(e => e.EmailSubject).IsUnicode(false);

            entity.Property(e => e.ReportName).IsUnicode(false);

            OnConfigurePartial(entity);
        }

        partial void OnConfigurePartial(EntityTypeBuilder<TActurisBillingReportsAuditLog> entity);
    }
}
