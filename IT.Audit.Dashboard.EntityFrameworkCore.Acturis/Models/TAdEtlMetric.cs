﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace IT.Audit.Dashboard.EntityFrameworkCore.Acturis.Models
{
    [Table("t_AD_ETL_Metrics")]
    public partial class TAdEtlMetric
    {
        [Key]
        [Column("ID")]
        public int Id { get; set; }
        [Required]
        [StringLength(255)]
        public string MetricDescription { get; set; }
        [Column("MetricTypeID")]
        public int MetricTypeId { get; set; }
        public bool ZeroValueIsError { get; set; }
    }
}