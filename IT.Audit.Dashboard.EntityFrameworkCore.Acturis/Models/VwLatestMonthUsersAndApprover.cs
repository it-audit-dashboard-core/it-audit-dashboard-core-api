﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace IT.Audit.Dashboard.EntityFrameworkCore.Acturis.Models
{
    [Keyless]
    public partial class VwLatestMonthUsersAndApprover
    {
        [Column("Report_Name")]
        [StringLength(50)]
        public string ReportName { get; set; }
        [Column("Organisation Name")]
        [StringLength(50)]
        public string OrganisationName { get; set; }
        [Column("Office Name")]
        [StringLength(50)]
        public string OfficeName { get; set; }
        [Column("Team Name")]
        [StringLength(50)]
        public string TeamName { get; set; }
        [Column("Logon ID")]
        [StringLength(50)]
        public string LogonId { get; set; }
        [Column("First Name")]
        [StringLength(50)]
        public string FirstName { get; set; }
        [Column("Last_Name")]
        [StringLength(8000)]
        public string LastName { get; set; }
        [Column("Email Address")]
        [StringLength(50)]
        public string EmailAddress { get; set; }
        [Column("Employment Status")]
        [StringLength(50)]
        public string EmploymentStatus { get; set; }
        [Column("Start Date", TypeName = "datetime")]
        public DateTime? StartDate { get; set; }
        [Column("End Date", TypeName = "datetime")]
        public DateTime? EndDate { get; set; }
        [Column("System Role")]
        [StringLength(50)]
        public string SystemRole { get; set; }
        [Column("Last Logon Attempt", TypeName = "datetime")]
        public DateTime? LastLogonAttempt { get; set; }
        [Column("Report_Date", TypeName = "datetime")]
        public DateTime? ReportDate { get; set; }
        [Column("AD_Login")]
        [StringLength(50)]
        public string AdLogin { get; set; }
        [Column("AD_Job_Title")]
        [StringLength(256)]
        public string AdJobTitle { get; set; }
        [Column("AD_Office")]
        [StringLength(256)]
        public string AdOffice { get; set; }
        [Column("User_AD_Status")]
        [StringLength(20)]
        public string UserAdStatus { get; set; }
        [Column("User_AD_Updated", TypeName = "datetime")]
        public DateTime? UserAdUpdated { get; set; }
        [StringLength(511)]
        public string Approver { get; set; }
        [Required]
        [Column("Approver_AD_Status")]
        [StringLength(20)]
        public string ApproverAdStatus { get; set; }
        [Column("Approver_AD_Updated", TypeName = "datetime")]
        public DateTime? ApproverAdUpdated { get; set; }
    }
}